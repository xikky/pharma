<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenusTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {

    Schema::create( 'menus', function ( Blueprint $table ) {

      $table->increments( 'id' )->unsigned()->index();
      $table->string( 'name' );
      $table->string( 'link' );
      $table->integer( 'parent' )->unsigned()->index();
      $table->integer( 'order' );
      $table->text( 'parameters' )->nullable();

      $table->foreign( 'parent' )->references( 'id' )->on( 'menus' );
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {

    Schema::drop( 'menus' );
  }
}
