<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class ProductBarcodeExistsRequest extends Request {

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize () {

    if ( Auth::check() ) {

      $roles = Auth::user()->roles()->lists( 'signature' )->toArray();

      if ( in_array( 'pharmacist', $roles ) ||
           in_array( 'admin', $roles ) ||
           in_array( 'moderator', $roles )
      )
        return true;
    }

    return false;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules () {

    return [
      'barcode' => 'required|exists:products,barcode'
    ];
  }

  public function response ( array $errors ) {

    if ( $this->ajax() ) {

      $data = [
        'messages'  => $errors[ 'barcode' ],
        'type'      => 'danger',
        'important' => false
      ];

      return Response::json( [
                               'message' => (string)View::make( 'partials.flash_response', $data )
                             ], 400 );
    }

    return $this->redirector->to( $this->getRedirectUrl() )
                            ->withInput( $this->except( $this->dontFlash ) )
                            ->withErrors( $errors );
  }

}