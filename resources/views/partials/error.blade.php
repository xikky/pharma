@if (count($errors) > 0)
  <div class="alert alert-danger alert-dismissible alert-pharma">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close" aria-hidden="true">
      <span aria-hidden="true">&times;</span>
    </button>

    <strong>{{ trans( 'common.woops' ) }}</strong> {{ trans( 'common.input_probs' ) }}<br><br>

    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif