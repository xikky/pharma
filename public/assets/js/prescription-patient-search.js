$( '#prescription-patient-search .search' ).click( function () {

  data = {
    _token      : $( this ).next().val(),
    nin_number  : $( '#prescription-patient-search .nin_number' ).val(),
    nin_country : $( '#prescription-patient-search .nin_country' ).val()
  };

  $.ajax( {
    method   : "POST",
    url      : '/ajax/prescription-patient-search',
    data     : data,
    error    : function ( data ) {

    },
    dataType : 'json'
  } );
} );