<?php

return [

  'language' => [
    'code'     => 'en',
    'language' => 'English'
  ],
  'currency' => [
    'code'   => 'EUR',
    'symbol' => '€'
  ],
  'form' => [
    'tablet'  => 'Tablet',
    'capsule' => 'Capsule',
    'powder'  => 'Powder'
  ],
];