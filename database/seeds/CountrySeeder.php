<?php

use App\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $countries = [
      [ 'name' => 'Malta', 'code' => 'MT' ],
      [ 'name' => 'England', 'code' => 'GB' ],
    ];

    foreach ( $countries as $country ) {

      Country::create( [
                         'name' => $country[ 'name' ],
                         'code' => $country[ 'code' ]
                       ] );
    }
  }
}
