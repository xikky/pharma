<table>
  <tr>
    <th>
      Date
    </th>
    <th>
      Doctor
    </th>
    <th>
      product
    </th>
    <th>
      Dosage
    </th>
    <th>
      Status
    </th>
  </tr>
  <tr></tr>
  @foreach( $prescriptions as $prescription )
    <tr>
      <td>{{ \Carbon\Carbon::parse($prescription['prescription']['prescription_created_at'])->format('d-m-Y') }}</td>
      <td>{{ $prescription['prescription']['prescription_fullname'] }}</td>
      <td></td>
      <td></td>
      <td>{{ $prescription['prescription']['prescription_dispensed_name'] }}</td>
    </tr>
    @foreach( $prescription['products'] as $product )
      <tr>
        <td></td>
        <td></td>
        <td>{{ $product['name'] }}</td>
        <td>{{ $product['pivot']['dosage'] }}</td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>{{ trans_choice( 'patient.for_days', $product['pivot']['period'], array( 'days' => $product['pivot']['period'] ) ) }}</td>
        <td colspan="2">{{ trans( 'patient.total_of', array( 'quantity' => $product['pivot']['quantity'], 'measurement' => $product['pivot']['measurement'] )) }}</td>
      </tr>
    @endforeach
    <tr>
      <td></td>
      <td></td>
      <td colspan="3">{{ $prescription['prescription']['description'] }}</td>
    </tr>
  @endforeach
</table>