<?php

// Patient
Breadcrumbs::register( 'patient', function ( $breadcrumbs ) {

  $breadcrumbs->push( 'Patient', route( 'patient' ) );
} );

// Patient > Products
Breadcrumbs::register( 'patient_products', function ( $breadcrumbs, $nin ) {

  $breadcrumbs->parent( 'patient' );

  if ( !empty( $nin ) )
    $breadcrumbs->push( 'Products: ' . $nin, route( 'patient_products', $nin ) );
  else
    $breadcrumbs->push( 'Products', route( 'patient_products' ) );
} );

// Patient > Search
Breadcrumbs::register( 'patient_search', function ( $breadcrumbs ) {

  $breadcrumbs->parent( 'patient' );
  $breadcrumbs->push( 'Search', route( 'patient_search' ) );
} );

// Patient > History
Breadcrumbs::register( 'patient_history', function ( $breadcrumbs, $nin ) {

  $breadcrumbs->parent( 'patient' );
  $breadcrumbs->push( 'Medical History: ' . $nin, route( 'patient_history', $nin ) );
} );

// Prescription
Breadcrumbs::register( 'prescription', function ( $breadcrumbs ) {

  $breadcrumbs->push( 'Prescription', route( 'prescription' ) );
} );