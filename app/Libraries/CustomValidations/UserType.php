<?php

namespace App\Libraries\CustomValidations;

use App\User;
use Illuminate\Support\Facades\Input;

class UserType {

  public function validate ( $attribute, $value, $parameters ) {

    $value .= Input::get( $parameters[ 1 ] );

    $user = User::where( 'nin', '=', $value )->first();

    if ( !empty( $user ) ) {

      if ( $parameters[ 0 ] == 'patient' ) {

        $user = $user->patient()->first();

        if ( empty( $user ) )
          return false;
      }
      else if ( $parameters[ 0 ] == 'pharmacist' ) {

        $user = $user->pharmacist()->first();

        if ( empty( $user ) )
          return false;
      }
      else if ( $parameters[ 0 ] == 'doctor' ) {

        $user = $user->doctor()->first();

        if ( empty( $user ) )
          return false;
      }
    }

    return true;
  }
}
