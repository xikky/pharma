@extends('app')

@section('content')

  <div id="home" class="sub-body">
    <div id="push-content"></div>
    <div id="center-content">
      <div class="container">
        <div class="row">
          <div class="col-xs-4">
            <a href="#">
              <div class="btn btn-pharma-link btn-home">
                <span>Drugs A-Z</span>
              </div>
            </a>
          </div>
          <div class="col-xs-4">
            <a href="#">
              <div class="btn btn-pharma-link btn-home">
                <span>Drugs by Condition</span>
              </div>
            </a>
          </div>
          <div class="col-xs-4">
            <a href="#">
              <div class="btn btn-pharma-link btn-home">
                <span>My Prescriptions</span>
              </div>
            </a>
          </div>
          <div class="col-xs-4">
            <a href="#">
              <div class="btn btn-pharma-link btn-home">
                <span>My Drugs</span>
              </div>
            </a>
          </div>
          <div class="col-xs-4">
            <a href="#">
              <div class="btn btn-pharma-link btn-home">
                <span>Live Consultations</span>
              </div>
            </a>
          </div>
          <div class="col-xs-4">
            <a href="#">
              <div class="btn btn-pharma-link btn-home">
                <span>Support</span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
