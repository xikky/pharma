<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrescriptionProductTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {

    Schema::create( 'prescription_product', function ( Blueprint $table ) {

      $table->integer( 'product_id' )->unsigned()->index();
      $table->integer( 'prescription_id' )->unsigned()->index();
      $table->string( 'dosage' );
      $table->string( 'frequency' );
      $table->string( 'duration' );
      $table->string( 'quantity' );
      $table->string( 'measurement' );
      $table->enum( 'form', array( 'tablets', 'powder', 'capsule' ) );
      $table->timestamp( 'submit_at' );
      $table->boolean( 'buy' );

      $table->foreign( 'product_id' )->references( 'id' )->on( 'products' );
      $table->foreign( 'prescription_id' )->references( 'id' )->on( 'prescriptions' );
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {

    Schema::drop( 'prescription_product' );
  }
}
