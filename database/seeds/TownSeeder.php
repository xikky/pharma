<?php

use App\Town;
use Illuminate\Database\Seeder;

class TownSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $towns = [
      'Mosta', 'Attard', 'Naxxar'
    ];

    foreach ( $towns as $town ) {

      Town::create( [
                      'country_id' => 1,
                      'name'       => $town
                    ] );
    }
  }
}
