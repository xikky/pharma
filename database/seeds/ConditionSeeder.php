<?php

use App\Condition;
use Illuminate\Database\Seeder;

class ConditionSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $conditions = [
      'condition 1', 'condition 2', 'condition 3', 'condition 4', 'condition 5', 'condition 6',
    ];

    foreach ( $conditions as $condition ) {

      Condition::create( [
                           'name'        => $condition,
                           'description' => "Description for $condition"
                         ] );
    }
  }
}
