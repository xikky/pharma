<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class PageController extends Controller {

  public function index () {

    Session::flash( 'active_menu', '' );

    return view( 'index' );
  }

  public function home () {

    Session::flash( 'active_menu', '' );

    return view( 'home' );
  }
}
