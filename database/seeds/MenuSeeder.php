<?php

use App\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $menus = [
      [ 'name'       => 'patient',
        'link'       => 'MenuController@patient',
        'parent'     => '1',
        'order'  => '1',
        'parameters' => null,
        'roles'  => [ '1', '2', '3', '4' ]
      ],
      [
        'name'       => 'patient_products',
        'link'       => 'MenuController@patientProducts',
        'parent'     => '1',
        'order'      => '1',
        'parameters' => 'user',
        'roles'      => [ '1', '2', '3' ]
      ],
      [
        'name'       => 'patient_search',
        'link'       => 'MenuController@patientSearch',
        'parent'     => '1',
        'order'      => '2',
        'parameters' => null,
        'roles'      => [ '1', '2', '3', '4' ]
      ],
      [
        'name' => 'patient_history',
        'link' => 'MenuController@patientHistory',
        'parent'     => '1',
        'order'      => '3',
        'parameters' => 'user',
        'roles'      => [ '1', '2', '3', '4' ]
      ],
      [
        'name'       => 'patient_flush',
        'link'       => 'MenuController@patientFlush',
        'parent'     => '1',
        'order' => '2',
        'parameters' => null,
        'roles'      => [ '1', '2', '3', '4' ]
      ],
      [
        'name'       => 'prescription',
        'link'       => 'MenuController@postPrescription',
        'parent'     => '6',
        'order'      => '2',
        'parameters' => null,
        'roles'      => [ '1', '2', '4' ]
      ]
    ];

    foreach ( $menus as $menu ) {

      $new = Menu::create( [
                             'name'   => $menu[ 'name' ],
                             'link'   => $menu[ 'link' ],
                             'parent' => $menu[ 'parent' ],
                             'order'  => $menu[ 'order' ],
                             'parameters' => $menu[ 'parameters' ],
                           ] );

      $new->roles()->sync( $menu[ 'roles' ] );
    }
  }
}
