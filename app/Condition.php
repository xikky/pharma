<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model {

  public $timestamps = false;

  public function scopeConditionInfo ( $query ) {

    return $query->select( 'conditions.name as condition_name',
                           'conditions.description as condition_description',
                           'condition_patient.created_at as condition_patient_created_at',
                           'condition_patient.description as condition_patient_description' )
                 ->selectRaw( 'CONCAT(users.firstname, " ", users.lastname) as doctor_fullname' )
                 ->join( 'patients', 'patients.id', '=', 'condition_patient.patient_id' )
                 ->join( 'doctors', 'doctors.id', '=', 'condition_patient.doctor_id' )
                 ->join( 'users', 'users.id', '=', 'doctors.user_id' );
  }

  public function scopeWhereDateNameDoctor ( $query, $filter_list ) {

    if ( !empty( $filter_list ) ) {

      $attributes = [ ];

      foreach ( $filter_list as $key => $filter ) {

        if ( $key == 0 )
          $attributes[ 'created_at' ] = "DATE_FORMAT( condition_patient.created_at, '%d-%m-%Y' ) LIKE '$filter%'";
        else if ( $key == 1 )
          $attributes[ 'fullname' ] = "CONCAT(users.firstname, ' ', users.lastname) LIKE '%$filter%'";
        else if ( $key == 2 )
          $attributes[ 'condition' ] = "conditions.name LIKE '%$filter%'";
      }

      $query->where( function ( $query ) use ( $attributes ) {

        foreach ( $attributes as $key => $value ) {

          $query->whereRaw( $value );
        }
      } );
    }

    return $query;
  }

  public function scopeOrderByDateNameDoctor ( $query, $sort_list ) {

    if ( !empty( $sort_list ) ) {

      foreach ( $sort_list as $key => $sort ) {

        if ( $sort == '0' )
          $order_by[ intval( $key ) ] = 'desc';
        else
          $order_by[ intval( $key ) ] = 'asc';
      }

      foreach ( $sort_list as $key => $sort ) {

        if ( $key == 0 )
          $query->orderBy( 'condition_patient_created_at', $order_by[ 0 ] );
        else if ( $key == 1 )
          $query->orderBy( 'doctor_fullname', $order_by[ 1 ] );
        else if ( $key == 2 )
          $query->orderBy( 'condition_name', $order_by[ 2 ] );
      }
    }
    else
      $query->orderBy( 'condition_patient.created_at', 'asc' )
                   ->orderBy( 'conditions.name', 'asc' );

    return $query;
  }
}
