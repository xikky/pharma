<script id="print-prescription-template" type="text/x-handlebars-template">
  <div id="print-prescription">
    <div class="container">
      <div class="content">
        <div class="left-col">
          <div class="cell date">
            Date:
          </div>
          <div class="cell doctor">
            Doctor:
          </div>
          <div class="cell products">
            products:
          </div>
        </div>
        <div class="right-col">
          <div class="cell date">
            @{{ date }}
          </div>
          <div class="cell doctor">
            @{{ doctor }}
          </div>
          <div class="cell multi-row products">
            @{{#each products}}
            <div class="sub-cell name">
              @{{ name }}
            </div>
            <div class="sub-cell dosage">
              @{{ dosage }}
            </div>
            <div class="sub-cell period">
              @{{ period }}
            </div>
            <div class="sub-cell total">
              @{{ total }}
            </div>
            @{{/each}}
          </div>
          <div class="cell description">
            @{{ description }}
          </div>
        </div>
      </div>
    </div>
  </div>
</script>