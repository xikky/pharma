<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrescriptionsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {

    Schema::create( 'prescriptions', function ( Blueprint $table ) {

      $table->increments( 'id' )->unsigned();
      $table->integer( 'patient_id' )->unsigned();
      $table->integer( 'pharmacist_id' )->unsigned()->nullable();
      $table->integer( 'doctor_id' )->unsigned();
      $table->enum( 'prescription_type', array( 'private', 'poyc' ) );
      $table->enum( 'dispensed', array( 'no', 'yes', 'pending' ) );
      $table->text( 'description' )->nullable();
      $table->timestamps();

      $table->foreign( 'patient_id' )->references( 'id' )->on( 'patients' );
      $table->foreign( 'pharmacist_id' )->references( 'id' )->on( 'pharmacists' );
      $table->foreign( 'doctor_id' )->references( 'id' )->on( 'doctors' );
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {

    Schema::table( 'prescriptions', function ( Blueprint $table ) {

      $table->dropForeign( 'prescriptions_patient_id_foreign' );
      $table->dropForeign( 'prescriptions_pharmacist_id_foreign' );
      $table->dropForeign( 'prescriptions_doctor_id_foreign' );
    } );

    Schema::drop( 'prescriptions' );
  }
}
