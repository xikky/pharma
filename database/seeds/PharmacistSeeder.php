<?php

use App\Pharmacist;
use Illuminate\Database\Seeder;

class PharmacistSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $pharmacists = [
      [ 'user_id' => '3', 'name' => "St. Albert's Pharmacy" ],
      [ 'user_id' => '6', 'name' => "Valletta Pharmacy" ],
      [ 'user_id' => '7', 'name' => "Bomba Go Hajt Pharmacy" ]
    ];

    foreach ( $pharmacists as $pharmacist ) {

      Pharmacist::create( [
                            'user_id' => $pharmacist[ 'user_id' ],
                            'name'    => $pharmacist[ 'name' ]
                          ] );
    }
  }
}
