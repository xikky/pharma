<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class ValidatorServiceProvider extends ServiceProvider {

  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot () {

    $this->extendUserType();
    $this->replaceUserType();

    $this->extendUserExists();

    $this->extendProductBarcodeMatch();
  }

  public function extendUserType () {

    Validator::extend( 'user_type', '\App\Libraries\CustomValidations\UserType@validate' );
  }

  public function replaceUserType () {

    Validator::replacer( 'user_type', function ( $message, $attribute, $rule, $parameters ) {

      return str_replace( ":type", $parameters[ 0 ], $message );
    } );
  }

  public function extendUserExists () {

    Validator::extend( 'user_exists', '\App\Libraries\CustomValidations\UserExists@validate' );
  }

  private function extendProductBarcodeMatch () {

    Validator::extend( 'product_barcode_match', '\App\Libraries\CustomValidations\ProductBarcodeMatch@validate' );
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register () {
    //
  }
}
