@if (Session::has('flash_message'))
  <div class="alert-pharma {{ Session::has('flash_message')[2] ? 'alert-important' : '' }}">
    <div class="alert alert-{{ Session::get('flash_message')[1] }} alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close" aria-hidden="true">
        <span aria-hidden="true">&times;</span>
      </button>
      {{ Session::get('flash_message')[0] }}
    </div>
  </div>
@endif