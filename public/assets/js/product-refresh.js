$( '#patient-products .refresh' ).click( function ( e ) {

  var products = {};

  $( '#products #product-items tr.product-item' ).each( function () {

    products[ $( this ).attr( 'data-id' ) ] = $( this ).find( '.quantity' ).val();
  } );

  data = {
    products : products,
    _token : $( this ).next().val()
  };

  $.ajax( {
    method   : "POST",
    url : '/ajax/refresh-product',
    data     : data,
    success  : function ( data ) {

      $( '.refresh' ).addClass( 'hidden' );
      $( '.top-message' ).html( data[ 'message' ] );

      $.each( data[ 'removed' ], function ( index, value ) {

        $( '.barcode-' + value ).remove();
      } );

      if ( $( '.product-item' ).length <= 0 ) {

        if ( !$( '.product-buy-parent button' ).hasClass( 'disabled' ) )
          $( '.product-buy-parent button' ).addClass( 'disabled' );

        $( '#products #product-items' ).html( data[ 'empty_row' ] );
      }
      else {

        $( '.product-item .count' ).each( function ( index ) {

          $( this ).html( index + 1 );
        } );

        $( '#products #product-items tr.product-item' ).each( function () {

          $( this ).find( '.total' ).html( data[ 'total_prices' ][ $( this ).attr( 'data-id' ) ] );
          $( this ).find( '.quantity' ).attr( 'data-refresh', 0 );
          $( this ).find( '.quantity' ).attr( 'data-value', $( this ).find( '.quantity' ).val() );
        } );
      }

      $( '#product-totals .quantity' ).html( data[ 'totals' ][ 'quantity' ] );
      $( '#product-totals .total' ).html( data[ 'totals' ][ 'total' ] );
    },
    dataType : 'json'
  } );
} );