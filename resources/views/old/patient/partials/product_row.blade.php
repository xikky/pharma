<tr class="barcode-{{ $id }} product-item" data-id="{{ $id }}">
  <td class="count">{{ $count }}</td>
  <td>{{ $name }}</td>
  <td><input type="text" class="quantity touchspin" value="{{ $quantity }}" data-value="{{ $quantity }}" data-refresh="0"></td>
  <td>{{ Config::get( 'config.currency' )['symbol'] . $price }}</td>
  <td class="total">{{ Config::get( 'config.currency' )['symbol'] . $total }}</td>
  <td>
    <button type="button" class="btn remove" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
    {!! Form::hidden( '_token', csrf_token(), ['class' => 'product-remove-token'] ) !!}
  </td>
</tr>
<tr class="barcode-{{ $id }} product-description">
  <td></td>
  <td colspan="4">{{ $description }}</td>
  <td></td>
</tr>