$( '#patient-products' ).on( 'input', '.product-barcode', function () {

  data = {
    barcode : $( this ).val(),
    _token  : $( this ).next().val()
  };

  $.ajax( {
    method   : "POST",
    url : '/ajax/add-product',
    data     : data,
    success  : function ( data ) {

      var last_tr = $( 'table#products #product-items tr:last' );

      if ( $( '.barcode-' + data[ 'id' ] ).length ) {

        $( '.barcode-' + data[ 'id' ] + '.product-description' ).remove();
        $( '.barcode-' + data[ 'id' ] ).replaceWith( data[ 'row' ] );
      }
      else
        last_tr.after( data[ 'row' ] );

      $( '.barcode-null' ).remove();

      $( '.barcode-' + data[ 'id' ] + '.product-item .touchspin' ).TouchSpin( {
        verticalbuttons : true
      } );

      $( '#product-totals .quantity' ).html( data[ 'totals' ][ 'quantity' ] );
      $( '#product-totals .total' ).html( data[ 'totals' ][ 'total' ] );

      $( '.product-barcode' ).val( '' );

      $( '.product-buy-parent button' ).removeClass( 'disabled' );
      $( '.product-buy-parent button' ).prop( 'disabled', false );

      $( '.product-barcode' ).parent().removeClass( 'has-error' );
      $( '.product-barcode' ).parent().addClass( 'has-success' );
      $( '.top-message' ).html( '' );
    },
    error    : function ( data ) {

      data = data[ 'responseJSON' ];

      $( '.top-message' ).html( data[ 'message' ] );
      $( '.product-barcode' ).parent().removeClass( 'has-success' );
      $( '.product-barcode' ).parent().addClass( 'has-error' );
    },
    dataType : 'json'
  } );
} );