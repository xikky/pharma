{!! Form::open(['action' => '\App\Libraries\Helpers\_Export@export' , 'class' => 'form-export',  'target' => '_blank' ]) !!}

@if ( !empty( $user ) )
  <button type="button" class="btn export" title="Export to CSV"><i class="glyphicon glyphicon-export"></i></button>
  {!! Form::hidden( 'action', '', ['class' => 'action'] ) !!}
  {!! Form::hidden( 'user', $user->id, ['class' => 'patient'] ) !!}
  {!! Form::hidden( 'sort', '', ['class' => 'sort'] ) !!}
  {!! Form::hidden( 'filter', '', ['class' => 'filter'] ) !!}
@endif

<button type="button" class="btn reset" title="Reset filter"><i class="glyphicon glyphicon-repeat icon-flipped"></i></button>
<button type="button" class="btn first" title="First page"><i class="glyphicon glyphicon-step-backward"></i></button>
<button type="button" class="btn prev" title="Previous page"><i class="glyphicon glyphicon-backward"></i></button>
<span class="pagedisplay"></span>
<button type="button" class="btn next" title="Next page"><i class="glyphicon glyphicon-forward"></i></button>
<button type="button" class="btn last" title="Last page"><i class="glyphicon glyphicon-step-forward"></i></button>
<select class="pagesize input-mini btn" title="Select page size">
  <option selected="selected" value="3">3</option>
  <option value="5">5</option>
  <option value="10">10</option>
  <option value="20">20</option>
</select>
<select class="pagenum input-mini btn" title="Select page number"></select>

{!! Form::close() !!}