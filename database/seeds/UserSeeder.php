<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $faker = Faker\Factory::create();

    $users = [
      [ 'nin' => '0001M', 'name' => 'admin', 'role' => [ '1' ] ],
      [ 'nin' => '0002M', 'name' => 'moderator', 'role' => [ '2' ] ],
      [ 'nin' => '0003M', 'name' => 'pharma', 'role' => [ '3' ] ],
      [ 'nin' => '0004M', 'name' => 'doctor', 'role' => [ '4' ] ],
      [ 'nin' => '0005M', 'name' => 'patient', 'role' => [ '5' ] ],
      [ 'nin' => '0006M', 'name' => 'superadmin', 'role' => [ '1', '2', '3', '4', '5' ] ],
      [ 'nin' => '0007M', 'name' => 'pharma2', 'role' => [ '3' ] ],
      [ 'nin' => '0008M', 'name' => 'doctor2', 'role' => [ '4' ] ],
      [ 'nin' => '0009M', 'name' => 'patient2', 'role' => [ '5' ] ],
    ];

    foreach ( $users as $user ) {

      $new = User::create( [
                             'nin'       => $user[ 'nin' ],
                             'name'        => $user[ 'name' ],
                             'firstname' => $faker->firstName,
                             'lastname'  => $faker->lastName,
                             'email'     => $faker->email,
                             'password'    => Hash::make( $user[ 'name' ] ),
                             'address'   => $faker->streetAddress,
                             'post_code' => $faker->postcode,
                             'town_id'   => 1,
                             'language_id' => 1
                           ] );

      $new->roles()->sync( $user[ 'role' ] );
    }
  }
}
