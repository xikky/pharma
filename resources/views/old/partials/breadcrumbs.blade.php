@if ($breadcrumbs)
  <div class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <ol class="breadcrumb">
            @foreach ($breadcrumbs as $breadcrumb)
              @if (!$breadcrumb->last)
                <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
              @else
                <li class="active">{{ $breadcrumb->title }}</li>
              @endif
            @endforeach
          </ol>
        </div>
      </div>
    </div>
  </div>
@endif