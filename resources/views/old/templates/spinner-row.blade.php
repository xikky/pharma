<script id="spinner-row-template" type="text/x-handlebars-template">
  <tr>
    <td colspan="@{{colspan}}">
      <div class="has-spinner @{{active}}">
        <span class="spinner @{{color}}"><i class="glyphicon glyphicon-refresh glyphicon-spin"></i></span>
      </div>
    </td>
  </tr>
</script>