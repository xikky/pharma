<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePatientsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {

    Schema::create( 'patients', function ( Blueprint $table ) {

      $table->increments( 'id' )->unsigned();
      $table->integer( 'user_id' )->unsigned();
      $table->timestamps();

      $table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {

    Schema::table( 'patients', function ( Blueprint $table ) {

      $table->dropForeign( 'patients_user_id_foreign' );
    } );

    Schema::drop( 'patients' );
  }
}
