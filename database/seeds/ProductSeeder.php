<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $products = [
      [
        'name'        => 'product 1',
        'description' => 'A description about product 1',
        'barcode'     => '1234',
        'price'       => '200'
      ],
      [
        'name'        => 'product 2',
        'description' => 'A description about product 2',
        'barcode'     => '2345',
        'price'       => '500'
      ],
      [
        'name'        => 'product 3',
        'description' => 'A description about product 3',
        'barcode'     => '3456',
        'price'       => '400'
      ],
      [
        'name'        => 'product 4',
        'description' => 'A description about product 4',
        'barcode'     => '4567',
        'price'       => '300'
      ],
      [
        'name'        => 'product 5',
        'description' => 'A description about product 5',
        'barcode'     => '5678',
        'price'       => '800'
      ],
      [
        'name'        => 'product 6',
        'description' => 'A description about product 6',
        'barcode'     => '6789',
        'price'       => '650'
      ]
    ];

    foreach ( $products as $product ) {

      Product::create( [
                         'name'        => $product[ 'name' ],
                         'description' => $product[ 'description' ],
                         'barcode'     => $product[ 'barcode' ],
                         'price'       => $product[ 'price' ]
                       ] );
    }
  }
}
