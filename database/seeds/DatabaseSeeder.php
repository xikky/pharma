<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call( RoleSeeder::class );
        $this->call( LanguageSeeder::class );
        $this->call( CountrySeeder::class );
        $this->call( TownSeeder::class );
        $this->call( MenuSeeder::class );
        $this->call( UserSeeder::class );
        $this->call( ProductSeeder::class );
        $this->call( ConditionSeeder::class );
        $this->call( DoctorSeeder::class );
        $this->call( PharmacistSeeder::class );
        $this->call( PatientSeeder::class );
        $this->call( PrescriptionSeeder::class );

        Model::reguard();
    }
}
