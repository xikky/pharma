<tr>
  <td></td>
  <td class="align-right">{{ trans( 'patient.total' ) }}</td>
  <td class="align-center quantity">0</td>
  <td></td>
  <td class="total">{{ Config::get( 'config.currency' )['symbol'] . '0.00' }}</td>
  <td></td>
</tr>