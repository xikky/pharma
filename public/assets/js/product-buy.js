$( '#patient-products .cart' ).click( function () {

  data = {
    _token : $( this ).next().val()
  };

  $.ajax( {
    method   : "POST",
    url      : '/ajax/buy-product',
    data     : data,
    success  : function ( data ) {

      $( '.top-message' ).html( data[ 'message' ] );

      $( '#products #product-items' ).html( data[ 'empty_row' ] );
      $( '#product-totals' ).html( data[ 'totals' ] );

      var breadcrumb = $( 'ol.breadcrumb .active' ).text();

      if ( breadcrumb.indexOf( ':' ) != -1 ) {

        breadcrumb = breadcrumb.substr( 0, breadcrumb.indexOf( ':' ) );
        $( 'ol.breadcrumb .active' ).text( breadcrumb );
      }

      var details_url = $( '#menu-patient-details a' ).attr( 'href' );

      details_url = details_url.substr( 0, details_url.indexOf( 'medical-history' ) + 15 );

      $( '#menu-patient-details a' ).attr( 'href', details_url );
    },
    error    : function ( data ) {

      data = data[ 'responseJSON' ];

      $( '.top-message' ).html( data[ 'message' ] );

    },
    complete : function ( xhr, status ) {

      if ( status == 'success' )
        history.replaceState( {}, "", "/patient/products" );
    },
    dataType : 'json'
  } );
} );