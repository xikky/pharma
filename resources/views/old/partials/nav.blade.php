<nav class="navbar navbar-pharma">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ action( 'PageController@home' ) }}">
        <img src="{{ url('assets/images/logo.png') }}" alt="{{ trans( 'common.logo_name') }}">
      </a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        @foreach ( $menu as $menu_item )
          @if ( !array_key_exists( 'submenu', $menu_item ) )
            <li id="menu-{{str_replace( '_', '-', $menu_item['name'] )}}" {{ $active == $menu_item['name'] ? 'class=active' : '' }}><a href="{{ action( $menu_item['link'] ) }}">{{ trans( 'menu.' . $menu_item['name'] ) }}</a></li>
          @else
            <li id="menu-{{str_replace( '_', '-', $menu_item['name'] )}}" class="dropdown {{ $active == $menu_item['name'] ? 'active' : '' }}">
              <a href="{{ action( $menu_item['link'] ) }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                 aria-expanded="false">{{ trans( 'menu.'.$menu_item['name'] ) }}</a>
              <ul class="dropdown-menu">
                @foreach ( $menu_item['submenu'] as $sub_menu_item )
                  <li id="menu-{{str_replace( '_', '-', $sub_menu_item['name'] )}}" {{ $active_sub == $sub_menu_item['name'] ? 'class=active' : '' }}><a href="{{ action( $sub_menu_item['link'], $sub_menu_item['parameters'] ) }}">{{ trans( 'menu.' . $sub_menu_item['name'] ) }}</a></li>
                @endforeach
              </ul>
            </li>
          @endif
        @endforeach
      </ul>

      <div class="nav navbar-nav navbar-right btn-group language-ddl">
        <li class="dropdown">
          <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
             aria-expanded="false">
            <img src="{{ asset( "assets/images/flags/{$user_language['code']}.png" ) }}" alt="{{ $user_language['language'] }}" class="language-flag">
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            @foreach ( $languages as $language )
              <li>
                <a href="{{ action('SiteController@language', [ 'code' => $language['code'], 'langauge' => $language['language'] ] ) }}" class="language-item">
                  <img src="{{ asset( "assets/images/flags/{$language['code']}.png" ) }}" alt="{{ $language['language'] }}" class="language-flag">
                  <span>{{ trans( 'common.' . strtolower( $language['language'] ) ) }}</span>
                </a>
              </li>
            @endforeach
          </ul>
        </li>
      </div>

      @if( Auth::check() )

        <p class="navbar-text navbar-right">
          <a href="/auth/logout" class="navbar-link">
            {{ trans( 'common.logout' ) }}
          </a>
        </p>

        <p class="navbar-text navbar-right"> | </p>

        <p class="navbar-text navbar-right">
          {{ trans( 'common.signed_in_as') }}
          <a href="{{ action('UserController@show', $user_logged ) }}" class="navbar-link">
            {{ $user_logged }}
          </a>
        </p>
      @endif
    </div>
  </div>
</nav>