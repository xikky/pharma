<div class="form-horizontal">

  <div class="form-group">
    <label class="col-sm-3 control-label">Type</label>
    <div class="col-sm-7">
      <select id="type" class="form-control">
        @foreach( $types as $type )
          <option value="{{ $type }}">{{ $type }}</option>
        @endforeach
      </select>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label">{{ trans('prescription.description') }}</label>
    <div class="col-sm-7">
      <textarea id="description" class="form-control" rows="5"></textarea>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label">{{ trans('prescription.product') }}</label>
    <div class="col-sm-7">
      <input type="text" id="product" class="form-control">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label">{{ trans('prescription.period') }}</label>
    <div class="col-sm-2">
      <input type="number" id="period-num" class="touchspin form-number-spinner">
    </div>
    <div class="col-sm-5">
      <select id="period-type" class="form-control">
        <option value="day">{{ trans('prescription.day') }}</option>
        <option value="week">{{ trans('prescription.month') }}</option>
        <option value="month">{{ trans('prescription.year') }}</option>
      </select>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label">{{ trans('prescription.frequency') }}</label>
    <div class="col-sm-7">
      <input type="number" id="dosage-1" class="touchspin form-number-spinner">
      <input type="number" id="dosage-2" class="touchspin form-number-spinner">
      <input type="number" id="dosage-3" class="touchspin form-number-spinner">
      <input type="number" id="dosage-4" class="touchspin form-number-spinner">
      <input type="number" id="dosage-5" class="touchspin form-number-spinner">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label">{{ trans('prescription.quantity') }}</label>
    <div class="col-sm-7">
      <input type="number" id="quantity" class="touchspin form-number-spinner">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label">{{ trans('prescription.form') }}</label>
    <div class="col-sm-7">
      <select id="type" class="form-control">
        @foreach( Config::get( 'config.form' ) as $key => $form )
          <option value="{{ $key }}">{{ $form }}</option>
        @endforeach
      </select>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label">{{ trans('prescription.dosage') }}</label>
    <div class="col-sm-7">
      <input type="text" id="product" class="form-control">
    </div>
  </div>

</div>