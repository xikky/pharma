<?php

namespace App\Libraries\CustomValidations;

use App\Product;
use Illuminate\Support\Facades\Input;

class ProductBarcodeMatch {

  public function validate ( $attribute, $value, $parameters ) {

    $product_id = Input::get( $parameters[ 0 ] );

    $barcode = Product::find( $product_id )->barcode;

    if ( $barcode != $value )
      return false;

    return true;
  }
}
