<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

  public $timestamps = false;
  protected $table = 'menus';

  function roles () {

    return $this->belongsToMany( 'App\Role' );
  }
}
