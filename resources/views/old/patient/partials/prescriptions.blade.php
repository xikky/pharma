@foreach( $rows as $row )
  <tr class="tablesorter-toggle">
    <td class="date">{{ \Carbon\Carbon::parse($row['prescription']['prescription_created_at'])->format('d-m-Y') }}</td>
    <td class="doctor">{{ $row['prescription']['prescription_fullname'] }}</td>
    <td></td>
    <td></td>
    <td class="status">{{ $row['prescription']['prescription_dispensed_name'] }}</td>
    <td>
      <button type="button" class="btn print" title="Print"><i class="glyphicon glyphicon-print"></i></button>
    </td>
  </tr>
  @foreach( $row['products'] as $product )
    <tr class="tablesorter-childRow product">
      @if ( $row['prescription']['prescription_dispensed'] == 'yes' || $row['prescription']['prescription_dispensed'] == 'pending' || $mode == 'prescription' )
        <td colspan="2"></td>
      @else
        <td></td>
        <td>{!! Form::submit( trans( 'patient.skip' ), ['class' => 'btn btn-pharma-secondary-1 product-barcode-skip', 'antonym' => trans( 'patient.unskip' ) ] ) !!}</td>
      @endif
      <td class="name">{{ $product['name'] }}</td>
      <td class="dosage">{{ $product['pivot']['dosage'] }}</td>
      @if ( $row['prescription']['prescription_dispensed'] == 'yes' || $row['prescription']['prescription_dispensed'] == 'pending' || $mode == 'prescription' )
        <td></td>
      @else
        <td>
          <div class="product-barcode-parent">
            {!! Form::text('barcode', '', ['class' => 'form-control product-barcode flash-msg', 'product-id' => $product['id'] ]) !!}
            {!! Form::hidden( '_token', csrf_token(), ['class' => 'barcode-token'] ) !!}
          </div>
        </td>
      @endif
      <td class="product-barcode-status">
        <span class="ok"><i class="glyphicon glyphicon-ok"></i></span>
        <span class="remove"><i class="glyphicon glyphicon-remove"></i></span>
        <span class="flash"><i class="glyphicon glyphicon-flash"></i></span>
      </td>
    </tr>
    <tr class="tablesorter-childRow product-details">
      <td colspan="2"></td>
      <td class="period">
        {{ trans_choice( 'patient.for_days', $product['pivot']['period'], array( 'days' => $product['pivot']['period'] ) ) }}
      </td>
      <td colspan="3" class="total">
        {{ trans( 'patient.total_of', array( 'quantity' => $product['pivot']['quantity'], 'measurement' => $product['pivot']['measurement'] )) }}
      </td>
    </tr>
  @endforeach
  @if ( !empty( $row['prescription']['description'] ) )
    <tr class="tablesorter-childRow prescription-description">
      <td></td>
      <td colspan="5" class="description">
        {{ $row['prescription']['description'] }}
      </td>
    </tr>
  @endif
  @if ( $row['prescription']['prescription_dispensed'] == 'no' && $mode != 'prescription' )
    <tr class="tablesorter-childRow complete">
      <td colspan="4"></td>
      <td>
        {!! Form::submit( trans( 'patient.complete' ), ['class' => 'btn btn-pharma-1 product-prescription flash-msg'] ) !!}
        {!! Form::hidden( '_token', csrf_token(), ['class' => 'prescription-token'] ) !!}
        {!! Form::hidden( 'id', $row['prescription']['id'], ['class' => 'prescription-id'] ) !!}
      </td>
      <td colspan="4"></td>
    </tr>
  @endif
@endforeach