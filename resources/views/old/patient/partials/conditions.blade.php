@foreach( $rows as $row )
  <tr class="tablesorter-toggle">
    <td>{{ \Carbon\Carbon::parse($row['created_at'])->format('d-m-Y') }}</td>
    <td>{{ $row['fullname'] }}</td>
    <td>{{ $row['name'] }}</td>
  </tr>
  <tr class="tablesorter-childRow">
    <td></td>
    <td colspan="2">{{ $row['description'] }}</td>
  </tr>
@endforeach