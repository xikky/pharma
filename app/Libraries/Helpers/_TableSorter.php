<?php

namespace App\Libraries\Helpers;

use App\User;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class _TableSorter extends Controller {

  public function getPage ( $page, $size, $sort_list, $filter_list ) {

    $action = Input::get( 'action' );
    $mode = Input::get( 'mode' );

    $sort_arr = $this->getStringArray( $sort_list, 'sort' );
    $filter_arr = $this->getStringArray( $filter_list, 'filter' );

    switch ( $action ) {
      case 'tbl-pvt-prescriptions' :
        return $this->getPvtPrescriptions( $page, $size, $sort_arr, $filter_arr, $action, $mode );
      case 'tbl-poyc-prescriptions' :
        return $this->getPoycPrescriptions( $page, $size, $sort_arr, $filter_arr, $action, $mode );
      case 'tbl-patient-products' :
        return $this->getPatientproducts( $page, $size, $sort_arr, $filter_arr, $action );
      case 'tbl-patient-conditions' :
        return $this->getPatientConditions( $page, $size, $sort_arr, $filter_arr, $action );
    }
  }

  public function getStringArray ( $array_string, $array_key ) {

    $array_string = str_replace( '&', '', explode( $array_key, $array_string ) );
    array_shift( $array_string );
    $array = [ ];

    foreach ( $array_string as $string ) {

      $a = explode( '=', $string );

      if ( count( $a ) == 2 )
        $array[ str_replace( [ '[', ']' ], '', $a[ 0 ] ) ] = $a[ 1 ];
    }

    return $array;
  }

  function getPvtPrescriptions ( $page, $size, $sort_list, $filter_list, $action, $mode ) {

    $user = User::find( Input::get( 'user' ) );

    $prescriptions = $user->patient()->first()->prescriptions()->private()
      ->productInfo()
      ->whereDateDoctorProduct( $filter_list )
                          ->orderByDispenseDateDoctor( $sort_list )
                          ->groupByPrescription();

    return $this->getPrescriptions( $page, $size, $prescriptions, $action, $mode );
  }

  function getPrescriptions ( $page, $size, $prescriptions, $action, $mode ) {

    $prescription_count = count( $prescriptions->get() );

    $prescriptions = $prescriptions->forPage( ( $page + 1 ), $size )->get();

    $details = [ ];

    foreach ( $prescriptions as $prescription ) {

      $details[] = [
        'prescription' => $prescription->toArray(),
        'products' => $prescription->products()->get()->toArray()
      ];
    }

    $data = [
      'rows' => $details,
      'mode' => $mode
    ];

    return Response::json( [
                             'html'  => (string)View::make( 'patient.partials.prescriptions', $data ),
                             'total' => $prescription_count,
                             'id'    => $action
                           ], 200 );
  }

  function getPoycPrescriptions ( $page, $size, $sort_list, $filter_list, $action, $mode ) {

    $user = User::find( Input::get( 'user' ) );

    $prescriptions = $user->patient()->first()->prescriptions()->poyc()
      ->productInfo()
      ->whereDateDoctorProduct( $filter_list )
                          ->orderByDispenseDateDoctor( $sort_list )
                          ->groupByPrescription();

    return $this->getPrescriptions( $page, $size, $prescriptions, $action, $mode );
  }

  private function getPatientProducts ( $page, $size, $sort_list, $filter_list, $action ) {

    $user = User::find( Input::get( 'user' ) );
    $patient = $user->patient()->first();
    $products = $patient->productsInfo( $patient[ 'id' ] )->whereDateProductPharmacist( $filter_list );

    $product_count = $products->count();

    $products = $products->orderByDateproductPharmacist( $sort_list )->forPage( ( $page + 1 ), $size )->get()->toArray();

    $product_list = [ ];

    foreach ( $products as $product ) {

      $product_list[] = [
        'submit_at'    => $product[ 'patient_product_date' ],
        'product_name' => $product[ 'product_name' ],
        'pharma_name'  => $product[ 'pharmacist_name' ]
      ];
    }

    $data = [
      'rows' => $product_list
    ];

    return Response::json( [
                             'html'  => (string)View::make( 'patient.partials.products', $data ),
                             'total' => $product_count,
                             'id'    => $action
                           ], 200 );
  }

  private function getPatientConditions ( $page, $size, $sort_list, $filter_list, $action ) {

    $user = User::find( Input::get( 'user' ) );

    $conditions = $user->patient()->first()->conditions();

    $condition_count = $conditions->count();

    $conditions = $conditions->conditionInfo()
                             ->whereDateNameDoctor( $filter_list )
                             ->orderByDateNameDoctor( $sort_list )
                             ->forPage( ( $page + 1 ), $size )->get();

    $condition_list = [ ];

    foreach ( $conditions as $condition ) {

      $condition_list[] = [
        'created_at'  => $condition[ 'condition_patient_created_at' ],
        'name'        => $condition[ 'condition_name' ],
        'fullname'    => $condition[ 'doctor_fullname' ],
        'description' => $condition[ 'condition_description' ]
      ];
    }

    $data = [
      'rows' => $condition_list
    ];

    return Response::json( [
                             'html'  => (string)View::make( 'patient.partials.conditions', $data ),
                             'total' => $condition_count,
                             'id'    => $action
                           ], 200 );
  }
}