/*! jQuery v2.1.4 | (c) 2005, 2015 jQuery Foundation, Inc. | jquery.org/license */
!function ( a, b ) {
  "object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b( a, !0 ) : function ( a ) {
    if ( !a.document )throw new Error( "jQuery requires a window with a document" );
    return b( a )
  } : b( a )
}( "undefined" != typeof window ? window : this, function ( a, b ) {
  var c = [], d = c.slice, e = c.concat, f = c.push, g = c.indexOf, h = {}, i = h.toString, j = h.hasOwnProperty, k = {}, l = a.document, m = "2.1.4", n = function ( a, b ) {return new n.fn.init( a, b )}, o = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, p = /^-ms-/, q = /-([\da-z])/gi, r = function ( a, b ) {return b.toUpperCase()};
  n.fn = n.prototype = {
    jquery  : m, constructor : n, selector : "", length : 0, toArray : function () {return d.call( this )}, get : function ( a ) {return null != a ? 0 > a ? this[ a + this.length ] : this[ a ] : d.call( this )}, pushStack : function ( a ) {
      var b = n.merge( this.constructor(), a );
      return b.prevObject = this, b.context = this.context, b
    }, each : function ( a, b ) {return n.each( this, a, b )}, map : function ( a ) {return this.pushStack( n.map( this, function ( b, c ) {return a.call( b, c, b )} ) )}, slice : function () {return this.pushStack( d.apply( this, arguments ) )}, first : function () {return this.eq( 0 )}, last : function () {return this.eq( -1 )}, eq : function ( a ) {
      var b = this.length, c = +a + (0 > a ? b : 0);
      return this.pushStack( c >= 0 && b > c ? [ this[ c ] ] : [] )
    }, end  : function () {return this.prevObject || this.constructor( null )}, push : f, sort : c.sort, splice : c.splice
  }, n.extend = n.fn.extend = function () {
    var a, b, c, d, e, f, g = arguments[ 0 ] || {}, h = 1, i = arguments.length, j = !1;
    for ( "boolean" == typeof g && (j = g, g = arguments[ h ] || {}, h++), "object" == typeof g || n.isFunction( g ) || (g = {}), h === i && (g = this, h--); i > h; h++ )if ( null != (a = arguments[ h ]) )for ( b in a )c = g[ b ], d = a[ b ], g !== d && (j && d && (n.isPlainObject( d ) || (e = n.isArray( d ))) ? (e ? (e = !1, f = c && n.isArray( c ) ? c : []) : f = c && n.isPlainObject( c ) ? c : {}, g[ b ] = n.extend( j, f, d )) : void 0 !== d && (g[ b ] = d));
    return g
  }, n.extend( {
    expando      : "jQuery" + (m + Math.random()).replace( /\D/g, "" ), isReady : !0, error : function ( a ) {throw new Error( a )}, noop : function () {}, isFunction : function ( a ) {return "function" === n.type( a )}, isArray : Array.isArray, isWindow : function ( a ) {return null != a && a === a.window}, isNumeric : function ( a ) {return !n.isArray( a ) && a - parseFloat( a ) + 1 >= 0}, isPlainObject : function ( a ) {return "object" !== n.type( a ) || a.nodeType || n.isWindow( a ) ? !1 : a.constructor && !j.call( a.constructor.prototype, "isPrototypeOf" ) ? !1 : !0}, isEmptyObject : function ( a ) {
      var b;
      for ( b in a )return !1;
      return !0
    }, type      : function ( a ) {return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? h[ i.call( a ) ] || "object" : typeof a}, globalEval : function ( a ) {
      var b, c = eval;
      a = n.trim( a ), a && (1 === a.indexOf( "use strict" ) ? (b = l.createElement( "script" ), b.text = a, l.head.appendChild( b ).parentNode.removeChild( b )) : c( a ))
    }, camelCase : function ( a ) {return a.replace( p, "ms-" ).replace( q, r )}, nodeName : function ( a, b ) {return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()}, each : function ( a, b, c ) {
      var d, e = 0, f = a.length, g = s( a );
      if ( c ) {
        if ( g ) {
          for ( ; f > e; e++ )if ( d = b.apply( a[ e ], c ), d === !1 )break
        } else for ( e in a )if ( d = b.apply( a[ e ], c ), d === !1 )break
      } else if ( g ) {
        for ( ; f > e; e++ )if ( d = b.call( a[ e ], e, a[ e ] ), d === !1 )break
      } else for ( e in a )if ( d = b.call( a[ e ], e, a[ e ] ), d === !1 )break;
      return a
    }, trim      : function ( a ) {return null == a ? "" : (a + "").replace( o, "" )}, makeArray : function ( a, b ) {
      var c = b || [];
      return null != a && (s( Object( a ) ) ? n.merge( c, "string" == typeof a ? [ a ] : a ) : f.call( c, a )), c
    }, inArray   : function ( a, b, c ) {return null == b ? -1 : g.call( b, a, c )}, merge : function ( a, b ) {
      for ( var c = +b.length, d = 0, e = a.length; c > d; d++ )a[ e++ ] = b[ d ];
      return a.length = e, a
    }, grep      : function ( a, b, c ) {
      for ( var d, e = [], f = 0, g = a.length, h = !c; g > f; f++ )d = !b( a[ f ], f ), d !== h && e.push( a[ f ] );
      return e
    }, map       : function ( a, b, c ) {
      var d, f = 0, g = a.length, h = s( a ), i = [];
      if ( h )for ( ; g > f; f++ )d = b( a[ f ], f, c ), null != d && i.push( d ); else for ( f in a )d = b( a[ f ], f, c ), null != d && i.push( d );
      return e.apply( [], i )
    }, guid      : 1, proxy : function ( a, b ) {
      var c, e, f;
      return "string" == typeof b && (c = a[ b ], b = a, a = c), n.isFunction( a ) ? (e = d.call( arguments, 2 ), f = function () {return a.apply( b || this, e.concat( d.call( arguments ) ) )}, f.guid = a.guid = a.guid || n.guid++, f) : void 0
    }, now       : Date.now, support : k
  } ), n.each( "Boolean Number String Function Array Date RegExp Object Error".split( " " ), function ( a, b ) {h[ "[object " + b + "]" ] = b.toLowerCase()} );
  function s ( a ) {
    var b = "length"in a && a.length, c = n.type( a );
    return "function" === c || n.isWindow( a ) ? !1 : 1 === a.nodeType && b ? !0 : "array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a
  }

  var t = function ( a ) {
    var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u = "sizzle" + 1 * new Date, v = a.document, w = 0, x = 0, y = ha(), z = ha(), A = ha(), B = function ( a, b ) {return a === b && (l = !0), 0}, C = 1 << 31, D = {}.hasOwnProperty, E = [], F = E.pop, G = E.push, H = E.push, I = E.slice, J = function ( a, b ) {
      for ( var c = 0, d = a.length; d > c; c++ )if ( a[ c ] === b )return c;
      return -1
    }, K                                                           = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", L = "[\\x20\\t\\r\\n\\f]", M = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", N = M.replace( "w", "w#" ), O = "\\[" + L + "*(" + M + ")(?:" + L + "*([*^$|!~]?=)" + L + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + N + "))|)" + L + "*\\]", P = ":(" + M + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + O + ")*)|.*)\\)|)", Q = new RegExp( L + "+", "g" ), R = new RegExp( "^" + L + "+|((?:^|[^\\\\])(?:\\\\.)*)" + L + "+$", "g" ), S = new RegExp( "^" + L + "*," + L + "*" ), T = new RegExp( "^" + L + "*([>+~]|" + L + ")" + L + "*" ), U = new RegExp( "=" + L + "*([^\\]'\"]*?)" + L + "*\\]", "g" ), V = new RegExp( P ), W = new RegExp( "^" + N + "$" ), X = { ID : new RegExp( "^#(" + M + ")" ), CLASS : new RegExp( "^\\.(" + M + ")" ), TAG : new RegExp( "^(" + M.replace( "w", "w*" ) + ")" ), ATTR : new RegExp( "^" + O ), PSEUDO : new RegExp( "^" + P ), CHILD : new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + L + "*(even|odd|(([+-]|)(\\d*)n|)" + L + "*(?:([+-]|)" + L + "*(\\d+)|))" + L + "*\\)|)", "i" ), bool : new RegExp( "^(?:" + K + ")$", "i" ), needsContext : new RegExp( "^" + L + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + L + "*((?:-\\d)?\\d*)" + L + "*\\)|)(?=[^-]|$)", "i" ) }, Y = /^(?:input|select|textarea|button)$/i, Z = /^h\d$/i, $ = /^[^{]+\{\s*\[native \w/, _ = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, aa = /[+~]/, ba = /'|\\/g, ca = new RegExp( "\\\\([\\da-f]{1,6}" + L + "?|(" + L + ")|.)", "ig" ), da = function ( a, b, c ) {
      var d = "0x" + b - 65536;
      return d !== d || c ? b : 0 > d ? String.fromCharCode( d + 65536 ) : String.fromCharCode( d >> 10 | 55296, 1023 & d | 56320 )
    }, ea                                                          = function () {m()};
    try {
      H.apply( E = I.call( v.childNodes ), v.childNodes ), E[ v.childNodes.length ].nodeType
    } catch ( fa ) {
      H = {
        apply : E.length ? function ( a, b ) {G.apply( a, I.call( b ) )} : function ( a, b ) {
          var c = a.length, d = 0;
          while ( a[ c++ ] = b[ d++ ] );
          a.length = c - 1
        }
      }
    }
    function ga ( a, b, d, e ) {
      var f, h, j, k, l, o, r, s, w, x;
      if ( (b ? b.ownerDocument || b : v) !== n && m( b ), b = b || n, d = d || [], k = b.nodeType, "string" != typeof a || !a || 1 !== k && 9 !== k && 11 !== k )return d;
      if ( !e && p ) {
        if ( 11 !== k && (f = _.exec( a )) )if ( j = f[ 1 ] ) {
          if ( 9 === k ) {
            if ( h = b.getElementById( j ), !h || !h.parentNode )return d;
            if ( h.id === j )return d.push( h ), d
          } else if ( b.ownerDocument && (h = b.ownerDocument.getElementById( j )) && t( b, h ) && h.id === j )return d.push( h ), d
        } else {
          if ( f[ 2 ] )return H.apply( d, b.getElementsByTagName( a ) ), d;
          if ( (j = f[ 3 ]) && c.getElementsByClassName )return H.apply( d, b.getElementsByClassName( j ) ), d
        }
        if ( c.qsa && (!q || !q.test( a )) ) {
          if ( s = r = u, w = b, x = 1 !== k && a, 1 === k && "object" !== b.nodeName.toLowerCase() ) {
            o = g( a ), (r = b.getAttribute( "id" )) ? s = r.replace( ba, "\\$&" ) : b.setAttribute( "id", s ), s = "[id='" + s + "'] ", l = o.length;
            while ( l-- )o[ l ] = s + ra( o[ l ] );
            w = aa.test( a ) && pa( b.parentNode ) || b, x = o.join( "," )
          }
          if ( x )try {
            return H.apply( d, w.querySelectorAll( x ) ), d
          } catch ( y ) {
          } finally {
            r || b.removeAttribute( "id" )
          }
        }
      }
      return i( a.replace( R, "$1" ), b, d, e )
    }

    function ha () {
      var a = [];

      function b ( c, e ) {return a.push( c + " " ) > d.cacheLength && delete b[ a.shift() ], b[ c + " " ] = e}

      return b
    }

    function ia ( a ) {return a[ u ] = !0, a}

    function ja ( a ) {
      var b = n.createElement( "div" );
      try {
        return !!a( b )
      } catch ( c ) {
        return !1
      } finally {
        b.parentNode && b.parentNode.removeChild( b ), b = null
      }
    }

    function ka ( a, b ) {
      var c = a.split( "|" ), e = a.length;
      while ( e-- )d.attrHandle[ c[ e ] ] = b
    }

    function la ( a, b ) {
      var c = b && a, d = c && 1 === a.nodeType && 1 === b.nodeType && (~b.sourceIndex || C) - (~a.sourceIndex || C);
      if ( d )return d;
      if ( c )while ( c = c.nextSibling )if ( c === b )return -1;
      return a ? 1 : -1
    }

    function ma ( a ) {
      return function ( b ) {
        var c = b.nodeName.toLowerCase();
        return "input" === c && b.type === a
      }
    }

    function na ( a ) {
      return function ( b ) {
        var c = b.nodeName.toLowerCase();
        return ("input" === c || "button" === c) && b.type === a
      }
    }

    function oa ( a ) {
      return ia( function ( b ) {
        return b = +b, ia( function ( c, d ) {
          var e, f = a( [], c.length, b ), g = f.length;
          while ( g-- )c[ e = f[ g ] ] && (c[ e ] = !(d[ e ] = c[ e ]))
        } )
      } )
    }

    function pa ( a ) {return a && "undefined" != typeof a.getElementsByTagName && a}

    c = ga.support = {}, f = ga.isXML = function ( a ) {
      var b = a && (a.ownerDocument || a).documentElement;
      return b ? "HTML" !== b.nodeName : !1
    }, m = ga.setDocument = function ( a ) {
      var b, e, g = a ? a.ownerDocument || a : v;
      return g !== n && 9 === g.nodeType && g.documentElement ? (n = g, o = g.documentElement, e = g.defaultView, e && e !== e.top && (e.addEventListener ? e.addEventListener( "unload", ea, !1 ) : e.attachEvent && e.attachEvent( "onunload", ea )), p = !f( g ), c.attributes = ja( function ( a ) {return a.className = "i", !a.getAttribute( "className" )} ), c.getElementsByTagName = ja( function ( a ) {return a.appendChild( g.createComment( "" ) ), !a.getElementsByTagName( "*" ).length} ), c.getElementsByClassName = $.test( g.getElementsByClassName ), c.getById = ja( function ( a ) {return o.appendChild( a ).id = u, !g.getElementsByName || !g.getElementsByName( u ).length} ), c.getById ? (d.find.ID = function ( a, b ) {
        if ( "undefined" != typeof b.getElementById && p ) {
          var c = b.getElementById( a );
          return c && c.parentNode ? [ c ] : []
        }
      }, d.filter.ID = function ( a ) {
        var b = a.replace( ca, da );
        return function ( a ) {return a.getAttribute( "id" ) === b}
      }) : (delete d.find.ID, d.filter.ID = function ( a ) {
        var b = a.replace( ca, da );
        return function ( a ) {
          var c = "undefined" != typeof a.getAttributeNode && a.getAttributeNode( "id" );
          return c && c.value === b
        }
      }), d.find.TAG = c.getElementsByTagName ? function ( a, b ) {return "undefined" != typeof b.getElementsByTagName ? b.getElementsByTagName( a ) : c.qsa ? b.querySelectorAll( a ) : void 0} : function ( a, b ) {
        var c, d = [], e = 0, f = b.getElementsByTagName( a );
        if ( "*" === a ) {
          while ( c = f[ e++ ] )1 === c.nodeType && d.push( c );
          return d
        }
        return f
      }, d.find.CLASS = c.getElementsByClassName && function ( a, b ) {return p ? b.getElementsByClassName( a ) : void 0}, r = [], q = [], (c.qsa = $.test( g.querySelectorAll )) && (ja( function ( a ) {o.appendChild( a ).innerHTML = "<a id='" + u + "'></a><select id='" + u + "-\f]' msallowcapture=''><option selected=''></option></select>", a.querySelectorAll( "[msallowcapture^='']" ).length && q.push( "[*^$]=" + L + "*(?:''|\"\")" ), a.querySelectorAll( "[selected]" ).length || q.push( "\\[" + L + "*(?:value|" + K + ")" ), a.querySelectorAll( "[id~=" + u + "-]" ).length || q.push( "~=" ), a.querySelectorAll( ":checked" ).length || q.push( ":checked" ), a.querySelectorAll( "a#" + u + "+*" ).length || q.push( ".#.+[+~]" )} ), ja( function ( a ) {
        var b = g.createElement( "input" );
        b.setAttribute( "type", "hidden" ), a.appendChild( b ).setAttribute( "name", "D" ), a.querySelectorAll( "[name=d]" ).length && q.push( "name" + L + "*[*^$|!~]?=" ), a.querySelectorAll( ":enabled" ).length || q.push( ":enabled", ":disabled" ), a.querySelectorAll( "*,:x" ), q.push( ",.*:" )
      } )), (c.matchesSelector = $.test( s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector )) && ja( function ( a ) {c.disconnectedMatch = s.call( a, "div" ), s.call( a, "[s!='']:x" ), r.push( "!=", P )} ), q = q.length && new RegExp( q.join( "|" ) ), r = r.length && new RegExp( r.join( "|" ) ), b = $.test( o.compareDocumentPosition ), t = b || $.test( o.contains ) ? function ( a, b ) {
        var c = 9 === a.nodeType ? a.documentElement : a, d = b && b.parentNode;
        return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains( d ) : a.compareDocumentPosition && 16 & a.compareDocumentPosition( d )))
      } : function ( a, b ) {
        if ( b )while ( b = b.parentNode )if ( b === a )return !0;
        return !1
      }, B = b ? function ( a, b ) {
        if ( a === b )return l = !0, 0;
        var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
        return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition( b ) : 1, 1 & d || !c.sortDetached && b.compareDocumentPosition( a ) === d ? a === g || a.ownerDocument === v && t( v, a ) ? -1 : b === g || b.ownerDocument === v && t( v, b ) ? 1 : k ? J( k, a ) - J( k, b ) : 0 : 4 & d ? -1 : 1)
      } : function ( a, b ) {
        if ( a === b )return l = !0, 0;
        var c, d = 0, e = a.parentNode, f = b.parentNode, h = [ a ], i = [ b ];
        if ( !e || !f )return a === g ? -1 : b === g ? 1 : e ? -1 : f ? 1 : k ? J( k, a ) - J( k, b ) : 0;
        if ( e === f )return la( a, b );
        c = a;
        while ( c = c.parentNode )h.unshift( c );
        c = b;
        while ( c = c.parentNode )i.unshift( c );
        while ( h[ d ] === i[ d ] )d++;
        return d ? la( h[ d ], i[ d ] ) : h[ d ] === v ? -1 : i[ d ] === v ? 1 : 0
      }, g) : n
    }, ga.matches = function ( a, b ) {return ga( a, null, null, b )}, ga.matchesSelector = function ( a, b ) {
      if ( (a.ownerDocument || a) !== n && m( a ), b = b.replace( U, "='$1']" ), !(!c.matchesSelector || !p || r && r.test( b ) || q && q.test( b )) )try {
        var d = s.call( a, b );
        if ( d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType )return d
      } catch ( e ) {
      }
      return ga( b, n, null, [ a ] ).length > 0
    }, ga.contains = function ( a, b ) {return (a.ownerDocument || a) !== n && m( a ), t( a, b )}, ga.attr = function ( a, b ) {
      (a.ownerDocument || a) !== n && m( a );
      var e = d.attrHandle[ b.toLowerCase() ], f = e && D.call( d.attrHandle, b.toLowerCase() ) ? e( a, b, !p ) : void 0;
      return void 0 !== f ? f : c.attributes || !p ? a.getAttribute( b ) : (f = a.getAttributeNode( b )) && f.specified ? f.value : null
    }, ga.error = function ( a ) {throw new Error( "Syntax error, unrecognized expression: " + a )}, ga.uniqueSort = function ( a ) {
      var b, d = [], e = 0, f = 0;
      if ( l = !c.detectDuplicates, k = !c.sortStable && a.slice( 0 ), a.sort( B ), l ) {
        while ( b = a[ f++ ] )b === a[ f ] && (e = d.push( f ));
        while ( e-- )a.splice( d[ e ], 1 )
      }
      return k = null, a
    }, e = ga.getText = function ( a ) {
      var b, c = "", d = 0, f = a.nodeType;
      if ( f ) {
        if ( 1 === f || 9 === f || 11 === f ) {
          if ( "string" == typeof a.textContent )return a.textContent;
          for ( a = a.firstChild; a; a = a.nextSibling )c += e( a )
        } else if ( 3 === f || 4 === f )return a.nodeValue
      } else while ( b = a[ d++ ] )c += e( b );
      return c
    }, d = ga.selectors = {
      cacheLength : 50, createPseudo : ia, match : X, attrHandle : {}, find : {}, relative : { ">" : { dir : "parentNode", first : !0 }, " " : { dir : "parentNode" }, "+" : { dir : "previousSibling", first : !0 }, "~" : { dir : "previousSibling" } }, preFilter : {
        ATTR : function ( a ) {return a[ 1 ] = a[ 1 ].replace( ca, da ), a[ 3 ] = (a[ 3 ] || a[ 4 ] || a[ 5 ] || "").replace( ca, da ), "~=" === a[ 2 ] && (a[ 3 ] = " " + a[ 3 ] + " "), a.slice( 0, 4 )}, CHILD : function ( a ) {return a[ 1 ] = a[ 1 ].toLowerCase(), "nth" === a[ 1 ].slice( 0, 3 ) ? (a[ 3 ] || ga.error( a[ 0 ] ), a[ 4 ] = +(a[ 4 ] ? a[ 5 ] + (a[ 6 ] || 1) : 2 * ("even" === a[ 3 ] || "odd" === a[ 3 ])), a[ 5 ] = +(a[ 7 ] + a[ 8 ] || "odd" === a[ 3 ])) : a[ 3 ] && ga.error( a[ 0 ] ), a}, PSEUDO : function ( a ) {
          var b, c = !a[ 6 ] && a[ 2 ];
          return X.CHILD.test( a[ 0 ] ) ? null : (a[ 3 ] ? a[ 2 ] = a[ 4 ] || a[ 5 ] || "" : c && V.test( c ) && (b = g( c, !0 )) && (b = c.indexOf( ")", c.length - b ) - c.length) && (a[ 0 ] = a[ 0 ].slice( 0, b ), a[ 2 ] = c.slice( 0, b )), a.slice( 0, 3 ))
        }
      }, filter   : {
        TAG       : function ( a ) {
          var b = a.replace( ca, da ).toLowerCase();
          return "*" === a ? function () {return !0} : function ( a ) {return a.nodeName && a.nodeName.toLowerCase() === b}
        }, CLASS  : function ( a ) {
          var b = y[ a + " " ];
          return b || (b = new RegExp( "(^|" + L + ")" + a + "(" + L + "|$)" )) && y( a, function ( a ) {return b.test( "string" == typeof a.className && a.className || "undefined" != typeof a.getAttribute && a.getAttribute( "class" ) || "" )} )
        }, ATTR   : function ( a, b, c ) {
          return function ( d ) {
            var e = ga.attr( d, a );
            return null == e ? "!=" === b : b ? (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf( c ) : "*=" === b ? c && e.indexOf( c ) > -1 : "$=" === b ? c && e.slice( -c.length ) === c : "~=" === b ? (" " + e.replace( Q, " " ) + " ").indexOf( c ) > -1 : "|=" === b ? e === c || e.slice( 0, c.length + 1 ) === c + "-" : !1) : !0
          }
        }, CHILD  : function ( a, b, c, d, e ) {
          var f = "nth" !== a.slice( 0, 3 ), g = "last" !== a.slice( -4 ), h = "of-type" === b;
          return 1 === d && 0 === e ? function ( a ) {return !!a.parentNode} : function ( b, c, i ) {
            var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling", q = b.parentNode, r = h && b.nodeName.toLowerCase(), s = !i && !h;
            if ( q ) {
              if ( f ) {
                while ( p ) {
                  l = b;
                  while ( l = l[ p ] )if ( h ? l.nodeName.toLowerCase() === r : 1 === l.nodeType )return !1;
                  o = p = "only" === a && !o && "nextSibling"
                }
                return !0
              }
              if ( o = [ g ? q.firstChild : q.lastChild ], g && s ) {
                k = q[ u ] || (q[ u ] = {}), j = k[ a ] || [], n = j[ 0 ] === w && j[ 1 ], m = j[ 0 ] === w && j[ 2 ], l = n && q.childNodes[ n ];
                while ( l = ++n && l && l[ p ] || (m = n = 0) || o.pop() )if ( 1 === l.nodeType && ++m && l === b ) {
                  k[ a ] = [ w, n, m ];
                  break
                }
              } else if ( s && (j = (b[ u ] || (b[ u ] = {}))[ a ]) && j[ 0 ] === w )m = j[ 1 ]; else while ( l = ++n && l && l[ p ] || (m = n = 0) || o.pop() )if ( (h ? l.nodeName.toLowerCase() === r : 1 === l.nodeType) && ++m && (s && ((l[ u ] || (l[ u ] = {}))[ a ] = [ w, m ]), l === b) )break;
              return m -= e, m === d || m % d === 0 && m / d >= 0
            }
          }
        }, PSEUDO : function ( a, b ) {
          var c, e = d.pseudos[ a ] || d.setFilters[ a.toLowerCase() ] || ga.error( "unsupported pseudo: " + a );
          return e[ u ] ? e( b ) : e.length > 1 ? (c = [ a, a, "", b ], d.setFilters.hasOwnProperty( a.toLowerCase() ) ? ia( function ( a, c ) {
            var d, f = e( a, b ), g = f.length;
            while ( g-- )d = J( a, f[ g ] ), a[ d ] = !(c[ d ] = f[ g ])
          } ) : function ( a ) {return e( a, 0, c )}) : e
        }
      }, pseudos  : {
        not         : ia( function ( a ) {
          var b = [], c = [], d = h( a.replace( R, "$1" ) );
          return d[ u ] ? ia( function ( a, b, c, e ) {
            var f, g = d( a, null, e, [] ), h = a.length;
            while ( h-- )(f = g[ h ]) && (a[ h ] = !(b[ h ] = f))
          } ) : function ( a, e, f ) {return b[ 0 ] = a, d( b, null, f, c ), b[ 0 ] = null, !c.pop()}
        } ), has    : ia( function ( a ) {return function ( b ) {return ga( a, b ).length > 0}} ), contains : ia( function ( a ) {return a = a.replace( ca, da ), function ( b ) {return (b.textContent || b.innerText || e( b )).indexOf( a ) > -1}} ), lang : ia( function ( a ) {
          return W.test( a || "" ) || ga.error( "unsupported lang: " + a ), a = a.replace( ca, da ).toLowerCase(), function ( b ) {
            var c;
            do if ( c = p ? b.lang : b.getAttribute( "xml:lang" ) || b.getAttribute( "lang" ) )return c = c.toLowerCase(), c === a || 0 === c.indexOf( a + "-" ); while ( (b = b.parentNode) && 1 === b.nodeType );
            return !1
          }
        } ), target : function ( b ) {
          var c = a.location && a.location.hash;
          return c && c.slice( 1 ) === b.id
        }, root     : function ( a ) {return a === o}, focus : function ( a ) {return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)}, enabled : function ( a ) {return a.disabled === !1}, disabled : function ( a ) {return a.disabled === !0}, checked : function ( a ) {
          var b = a.nodeName.toLowerCase();
          return "input" === b && !!a.checked || "option" === b && !!a.selected
        }, selected : function ( a ) {return a.parentNode && a.parentNode.selectedIndex, a.selected === !0}, empty : function ( a ) {
          for ( a = a.firstChild; a; a = a.nextSibling )if ( a.nodeType < 6 )return !1;
          return !0
        }, parent   : function ( a ) {return !d.pseudos.empty( a )}, header : function ( a ) {return Z.test( a.nodeName )}, input : function ( a ) {return Y.test( a.nodeName )}, button : function ( a ) {
          var b = a.nodeName.toLowerCase();
          return "input" === b && "button" === a.type || "button" === b
        }, text     : function ( a ) {
          var b;
          return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute( "type" )) || "text" === b.toLowerCase())
        }, first    : oa( function () {return [ 0 ]} ), last : oa( function ( a, b ) {return [ b - 1 ]} ), eq : oa( function ( a, b, c ) {return [ 0 > c ? c + b : c ]} ), even : oa( function ( a, b ) {
          for ( var c = 0; b > c; c += 2 )a.push( c );
          return a
        } ), odd    : oa( function ( a, b ) {
          for ( var c = 1; b > c; c += 2 )a.push( c );
          return a
        } ), lt     : oa( function ( a, b, c ) {
          for ( var d = 0 > c ? c + b : c; --d >= 0; )a.push( d );
          return a
        } ), gt     : oa( function ( a, b, c ) {
          for ( var d = 0 > c ? c + b : c; ++d < b; )a.push( d );
          return a
        } )
      }
    }, d.pseudos.nth = d.pseudos.eq;
    for ( b in{ radio : !0, checkbox : !0, file : !0, password : !0, image : !0 } )d.pseudos[ b ] = ma( b );
    for ( b in{ submit : !0, reset : !0 } )d.pseudos[ b ] = na( b );
    function qa () {}

    qa.prototype = d.filters = d.pseudos, d.setFilters = new qa, g = ga.tokenize = function ( a, b ) {
      var c, e, f, g, h, i, j, k = z[ a + " " ];
      if ( k )return b ? 0 : k.slice( 0 );
      h = a, i = [], j = d.preFilter;
      while ( h ) {
        (!c || (e = S.exec( h ))) && (e && (h = h.slice( e[ 0 ].length ) || h), i.push( f = [] )), c = !1, (e = T.exec( h )) && (c = e.shift(), f.push( { value : c, type : e[ 0 ].replace( R, " " ) } ), h = h.slice( c.length ));
        for ( g in d.filter )!(e = X[ g ].exec( h )) || j[ g ] && !(e = j[ g ]( e )) || (c = e.shift(), f.push( { value : c, type : g, matches : e } ), h = h.slice( c.length ));
        if ( !c )break
      }
      return b ? h.length : h ? ga.error( a ) : z( a, i ).slice( 0 )
    };
    function ra ( a ) {
      for ( var b = 0, c = a.length, d = ""; c > b; b++ )d += a[ b ].value;
      return d
    }

    function sa ( a, b, c ) {
      var d = b.dir, e = c && "parentNode" === d, f = x++;
      return b.first ? function ( b, c, f ) {while ( b = b[ d ] )if ( 1 === b.nodeType || e )return a( b, c, f )} : function ( b, c, g ) {
        var h, i, j = [ w, f ];
        if ( g ) {
          while ( b = b[ d ] )if ( (1 === b.nodeType || e) && a( b, c, g ) )return !0
        } else while ( b = b[ d ] )if ( 1 === b.nodeType || e ) {
          if ( i = b[ u ] || (b[ u ] = {}), (h = i[ d ]) && h[ 0 ] === w && h[ 1 ] === f )return j[ 2 ] = h[ 2 ];
          if ( i[ d ] = j, j[ 2 ] = a( b, c, g ) )return !0
        }
      }
    }

    function ta ( a ) {
      return a.length > 1 ? function ( b, c, d ) {
        var e = a.length;
        while ( e-- )if ( !a[ e ]( b, c, d ) )return !1;
        return !0
      } : a[ 0 ]
    }

    function ua ( a, b, c ) {
      for ( var d = 0, e = b.length; e > d; d++ )ga( a, b[ d ], c );
      return c
    }

    function va ( a, b, c, d, e ) {
      for ( var f, g = [], h = 0, i = a.length, j = null != b; i > h; h++ )(f = a[ h ]) && (!c || c( f, d, e )) && (g.push( f ), j && b.push( h ));
      return g
    }

    function wa ( a, b, c, d, e, f ) {
      return d && !d[ u ] && (d = wa( d )), e && !e[ u ] && (e = wa( e, f )), ia( function ( f, g, h, i ) {
        var j, k, l, m = [], n = [], o = g.length, p = f || ua( b || "*", h.nodeType ? [ h ] : h, [] ), q = !a || !f && b ? p : va( p, m, a, h, i ), r = c ? e || (f ? a : o || d) ? [] : g : q;
        if ( c && c( q, r, h, i ), d ) {
          j = va( r, n ), d( j, [], h, i ), k = j.length;
          while ( k-- )(l = j[ k ]) && (r[ n[ k ] ] = !(q[ n[ k ] ] = l))
        }
        if ( f ) {
          if ( e || a ) {
            if ( e ) {
              j = [], k = r.length;
              while ( k-- )(l = r[ k ]) && j.push( q[ k ] = l );
              e( null, r = [], j, i )
            }
            k = r.length;
            while ( k-- )(l = r[ k ]) && (j = e ? J( f, l ) : m[ k ]) > -1 && (f[ j ] = !(g[ j ] = l))
          }
        } else r = va( r === g ? r.splice( o, r.length ) : r ), e ? e( null, g, r, i ) : H.apply( g, r )
      } )
    }

    function xa ( a ) {
      for ( var b, c, e, f = a.length, g = d.relative[ a[ 0 ].type ], h = g || d.relative[ " " ], i = g ? 1 : 0, k = sa( function ( a ) {return a === b}, h, !0 ), l = sa( function ( a ) {return J( b, a ) > -1}, h, !0 ), m = [ function ( a, c, d ) {
        var e = !g && (d || c !== j) || ((b = c).nodeType ? k( a, c, d ) : l( a, c, d ));
        return b = null, e
      } ]; f > i; i++ )if ( c = d.relative[ a[ i ].type ] )m = [ sa( ta( m ), c ) ]; else {
        if ( c = d.filter[ a[ i ].type ].apply( null, a[ i ].matches ), c[ u ] ) {
          for ( e = ++i; f > e; e++ )if ( d.relative[ a[ e ].type ] )break;
          return wa( i > 1 && ta( m ), i > 1 && ra( a.slice( 0, i - 1 ).concat( { value : " " === a[ i - 2 ].type ? "*" : "" } ) ).replace( R, "$1" ), c, e > i && xa( a.slice( i, e ) ), f > e && xa( a = a.slice( e ) ), f > e && ra( a ) )
        }
        m.push( c )
      }
      return ta( m )
    }

    function ya ( a, b ) {
      var c = b.length > 0, e = a.length > 0, f = function ( f, g, h, i, k ) {
        var l, m, o, p = 0, q = "0", r = f && [], s = [], t = j, u = f || e && d.find.TAG( "*", k ), v = w += null == t ? 1 : Math.random() || .1, x = u.length;
        for ( k && (j = g !== n && g); q !== x && null != (l = u[ q ]); q++ ) {
          if ( e && l ) {
            m = 0;
            while ( o = a[ m++ ] )if ( o( l, g, h ) ) {
              i.push( l );
              break
            }
            k && (w = v)
          }
          c && ((l = !o && l) && p--, f && r.push( l ))
        }
        if ( p += q, c && q !== p ) {
          m = 0;
          while ( o = b[ m++ ] )o( r, s, g, h );
          if ( f ) {
            if ( p > 0 )while ( q-- )r[ q ] || s[ q ] || (s[ q ] = F.call( i ));
            s = va( s )
          }
          H.apply( i, s ), k && !f && s.length > 0 && p + b.length > 1 && ga.uniqueSort( i )
        }
        return k && (w = v, j = t), r
      };
      return c ? ia( f ) : f
    }

    return h = ga.compile = function ( a, b ) {
      var c, d = [], e = [], f = A[ a + " " ];
      if ( !f ) {
        b || (b = g( a )), c = b.length;
        while ( c-- )f = xa( b[ c ] ), f[ u ] ? d.push( f ) : e.push( f );
        f = A( a, ya( e, d ) ), f.selector = a
      }
      return f
    }, i = ga.select = function ( a, b, e, f ) {
      var i, j, k, l, m, n = "function" == typeof a && a, o = !f && g( a = n.selector || a );
      if ( e = e || [], 1 === o.length ) {
        if ( j = o[ 0 ] = o[ 0 ].slice( 0 ), j.length > 2 && "ID" === (k = j[ 0 ]).type && c.getById && 9 === b.nodeType && p && d.relative[ j[ 1 ].type ] ) {
          if ( b = (d.find.ID( k.matches[ 0 ].replace( ca, da ), b ) || [])[ 0 ], !b )return e;
          n && (b = b.parentNode), a = a.slice( j.shift().value.length )
        }
        i = X.needsContext.test( a ) ? 0 : j.length;
        while ( i-- ) {
          if ( k = j[ i ], d.relative[ l = k.type ] )break;
          if ( (m = d.find[ l ]) && (f = m( k.matches[ 0 ].replace( ca, da ), aa.test( j[ 0 ].type ) && pa( b.parentNode ) || b )) ) {
            if ( j.splice( i, 1 ), a = f.length && ra( j ), !a )return H.apply( e, f ), e;
            break
          }
        }
      }
      return (n || h( a, o ))( f, b, !p, e, aa.test( a ) && pa( b.parentNode ) || b ), e
    }, c.sortStable = u.split( "" ).sort( B ).join( "" ) === u, c.detectDuplicates = !!l, m(), c.sortDetached = ja( function ( a ) {return 1 & a.compareDocumentPosition( n.createElement( "div" ) )} ), ja( function ( a ) {return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute( "href" )} ) || ka( "type|href|height|width", function ( a, b, c ) {return c ? void 0 : a.getAttribute( b, "type" === b.toLowerCase() ? 1 : 2 )} ), c.attributes && ja( function ( a ) {return a.innerHTML = "<input/>", a.firstChild.setAttribute( "value", "" ), "" === a.firstChild.getAttribute( "value" )} ) || ka( "value", function ( a, b, c ) {return c || "input" !== a.nodeName.toLowerCase() ? void 0 : a.defaultValue} ), ja( function ( a ) {return null == a.getAttribute( "disabled" )} ) || ka( K, function ( a, b, c ) {
      var d;
      return c ? void 0 : a[ b ] === !0 ? b.toLowerCase() : (d = a.getAttributeNode( b )) && d.specified ? d.value : null
    } ), ga
  }( a );
  n.find = t, n.expr = t.selectors, n.expr[ ":" ] = n.expr.pseudos, n.unique = t.uniqueSort, n.text = t.getText, n.isXMLDoc = t.isXML, n.contains = t.contains;
  var u = n.expr.match.needsContext, v = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, w = /^.[^:#\[\.,]*$/;

  function x ( a, b, c ) {
    if ( n.isFunction( b ) )return n.grep( a, function ( a, d ) {return !!b.call( a, d, a ) !== c} );
    if ( b.nodeType )return n.grep( a, function ( a ) {return a === b !== c} );
    if ( "string" == typeof b ) {
      if ( w.test( b ) )return n.filter( b, a, c );
      b = n.filter( b, a )
    }
    return n.grep( a, function ( a ) {return g.call( b, a ) >= 0 !== c} )
  }

  n.filter = function ( a, b, c ) {
    var d = b[ 0 ];
    return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? n.find.matchesSelector( d, a ) ? [ d ] : [] : n.find.matches( a, n.grep( b, function ( a ) {return 1 === a.nodeType} ) )
  }, n.fn.extend( {
    find      : function ( a ) {
      var b, c = this.length, d = [], e = this;
      if ( "string" != typeof a )return this.pushStack( n( a ).filter( function () {for ( b = 0; c > b; b++ )if ( n.contains( e[ b ], this ) )return !0} ) );
      for ( b = 0; c > b; b++ )n.find( a, e[ b ], d );
      return d = this.pushStack( c > 1 ? n.unique( d ) : d ), d.selector = this.selector ? this.selector + " " + a : a, d
    }, filter : function ( a ) {return this.pushStack( x( this, a || [], !1 ) )}, not : function ( a ) {return this.pushStack( x( this, a || [], !0 ) )}, is : function ( a ) {return !!x( this, "string" == typeof a && u.test( a ) ? n( a ) : a || [], !1 ).length}
  } );
  var y, z = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, A = n.fn.init = function ( a, b ) {
    var c, d;
    if ( !a )return this;
    if ( "string" == typeof a ) {
      if ( c = "<" === a[ 0 ] && ">" === a[ a.length - 1 ] && a.length >= 3 ? [ null, a, null ] : z.exec( a ), !c || !c[ 1 ] && b )return !b || b.jquery ? (b || y).find( a ) : this.constructor( b ).find( a );
      if ( c[ 1 ] ) {
        if ( b = b instanceof n ? b[ 0 ] : b, n.merge( this, n.parseHTML( c[ 1 ], b && b.nodeType ? b.ownerDocument || b : l, !0 ) ), v.test( c[ 1 ] ) && n.isPlainObject( b ) )for ( c in b )n.isFunction( this[ c ] ) ? this[ c ]( b[ c ] ) : this.attr( c, b[ c ] );
        return this
      }
      return d = l.getElementById( c[ 2 ] ), d && d.parentNode && (this.length = 1, this[ 0 ] = d), this.context = l, this.selector = a, this
    }
    return a.nodeType ? (this.context = this[ 0 ] = a, this.length = 1, this) : n.isFunction( a ) ? "undefined" != typeof y.ready ? y.ready( a ) : a( n ) : (void 0 !== a.selector && (this.selector = a.selector, this.context = a.context), n.makeArray( a, this ))
  };
  A.prototype = n.fn, y = n( l );
  var B = /^(?:parents|prev(?:Until|All))/, C = { children : !0, contents : !0, next : !0, prev : !0 };
  n.extend( {
    dir        : function ( a, b, c ) {
      var d = [], e = void 0 !== c;
      while ( (a = a[ b ]) && 9 !== a.nodeType )if ( 1 === a.nodeType ) {
        if ( e && n( a ).is( c ) )break;
        d.push( a )
      }
      return d
    }, sibling : function ( a, b ) {
      for ( var c = []; a; a = a.nextSibling )1 === a.nodeType && a !== b && c.push( a );
      return c
    }
  } ), n.fn.extend( {
    has        : function ( a ) {
      var b = n( a, this ), c = b.length;
      return this.filter( function () {for ( var a = 0; c > a; a++ )if ( n.contains( this, b[ a ] ) )return !0} )
    }, closest : function ( a, b ) {
      for ( var c, d = 0, e = this.length, f = [], g = u.test( a ) || "string" != typeof a ? n( a, b || this.context ) : 0; e > d; d++ )for ( c = this[ d ]; c && c !== b; c = c.parentNode )if ( c.nodeType < 11 && (g ? g.index( c ) > -1 : 1 === c.nodeType && n.find.matchesSelector( c, a )) ) {
        f.push( c );
        break
      }
      return this.pushStack( f.length > 1 ? n.unique( f ) : f )
    }, index   : function ( a ) {return a ? "string" == typeof a ? g.call( n( a ), this[ 0 ] ) : g.call( this, a.jquery ? a[ 0 ] : a ) : this[ 0 ] && this[ 0 ].parentNode ? this.first().prevAll().length : -1}, add : function ( a, b ) {return this.pushStack( n.unique( n.merge( this.get(), n( a, b ) ) ) )}, addBack : function ( a ) {return this.add( null == a ? this.prevObject : this.prevObject.filter( a ) )}
  } );
  function D ( a, b ) {
    while ( (a = a[ b ]) && 1 !== a.nodeType );
    return a
  }

  n.each( {
    parent     : function ( a ) {
      var b = a.parentNode;
      return b && 11 !== b.nodeType ? b : null
    }, parents : function ( a ) {return n.dir( a, "parentNode" )}, parentsUntil : function ( a, b, c ) {return n.dir( a, "parentNode", c )}, next : function ( a ) {return D( a, "nextSibling" )}, prev : function ( a ) {return D( a, "previousSibling" )}, nextAll : function ( a ) {return n.dir( a, "nextSibling" )}, prevAll : function ( a ) {return n.dir( a, "previousSibling" )}, nextUntil : function ( a, b, c ) {return n.dir( a, "nextSibling", c )}, prevUntil : function ( a, b, c ) {return n.dir( a, "previousSibling", c )}, siblings : function ( a ) {return n.sibling( (a.parentNode || {}).firstChild, a )}, children : function ( a ) {return n.sibling( a.firstChild )}, contents : function ( a ) {return a.contentDocument || n.merge( [], a.childNodes )}
  }, function ( a, b ) {
    n.fn[ a ] = function ( c, d ) {
      var e = n.map( this, b, c );
      return "Until" !== a.slice( -5 ) && (d = c), d && "string" == typeof d && (e = n.filter( d, e )), this.length > 1 && (C[ a ] || n.unique( e ), B.test( a ) && e.reverse()), this.pushStack( e )
    }
  } );
  var E = /\S+/g, F = {};

  function G ( a ) {
    var b = F[ a ] = {};
    return n.each( a.match( E ) || [], function ( a, c ) {b[ c ] = !0} ), b
  }

  n.Callbacks = function ( a ) {
    a = "string" == typeof a ? F[ a ] || G( a ) : n.extend( {}, a );
    var b, c, d, e, f, g, h = [], i = !a.once && [], j = function ( l ) {
      for ( b = a.memory && l, c = !0, g = e || 0, e = 0, f = h.length, d = !0; h && f > g; g++ )if ( h[ g ].apply( l[ 0 ], l[ 1 ] ) === !1 && a.stopOnFalse ) {
        b = !1;
        break
      }
      d = !1, h && (i ? i.length && j( i.shift() ) : b ? h = [] : k.disable())
    }, k                    = {
      add       : function () {
        if ( h ) {
          var c = h.length;
          !function g ( b ) {
            n.each( b, function ( b, c ) {
              var d = n.type( c );
              "function" === d ? a.unique && k.has( c ) || h.push( c ) : c && c.length && "string" !== d && g( c )
            } )
          }( arguments ), d ? f = h.length : b && (e = c, j( b ))
        }
        return this
      }, remove : function () {
        return h && n.each( arguments, function ( a, b ) {
          var c;
          while ( (c = n.inArray( b, h, c )) > -1 )h.splice( c, 1 ), d && (f >= c && f--, g >= c && g--)
        } ), this
      }, has    : function ( a ) {return a ? n.inArray( a, h ) > -1 : !(!h || !h.length)}, empty : function () {return h = [], f = 0, this}, disable : function () {return h = i = b = void 0, this}, disabled : function () {return !h}, lock : function () {return i = void 0, b || k.disable(), this}, locked : function () {return !i}, fireWith : function ( a, b ) {return !h || c && !i || (b = b || [], b = [ a, b.slice ? b.slice() : b ], d ? i.push( b ) : j( b )), this}, fire : function () {return k.fireWith( this, arguments ), this}, fired : function () {return !!c}
    };
    return k
  }, n.extend( {
    Deferred : function ( a ) {
      var b = [ [ "resolve", "done", n.Callbacks( "once memory" ), "resolved" ], [ "reject", "fail", n.Callbacks( "once memory" ), "rejected" ], [ "notify", "progress", n.Callbacks( "memory" ) ] ], c = "pending", d = {
        state      : function () {return c}, always : function () {return e.done( arguments ).fail( arguments ), this}, then : function () {
          var a = arguments;
          return n.Deferred( function ( c ) {
            n.each( b, function ( b, f ) {
              var g = n.isFunction( a[ b ] ) && a[ b ];
              e[ f[ 1 ] ]( function () {
                var a = g && g.apply( this, arguments );
                a && n.isFunction( a.promise ) ? a.promise().done( c.resolve ).fail( c.reject ).progress( c.notify ) : c[ f[ 0 ] + "With" ]( this === d ? c.promise() : this, g ? [ a ] : arguments )
              } )
            } ), a = null
          } ).promise()
        }, promise : function ( a ) {return null != a ? n.extend( a, d ) : d}
      }, e  = {};
      return d.pipe = d.then, n.each( b, function ( a, f ) {
        var g = f[ 2 ], h = f[ 3 ];
        d[ f[ 1 ] ] = g.add, h && g.add( function () {c = h}, b[ 1 ^ a ][ 2 ].disable, b[ 2 ][ 2 ].lock ), e[ f[ 0 ] ] = function () {return e[ f[ 0 ] + "With" ]( this === e ? d : this, arguments ), this}, e[ f[ 0 ] + "With" ] = g.fireWith
      } ), d.promise( e ), a && a.call( e, e ), e
    }, when  : function ( a ) {
      var b = 0, c = d.call( arguments ), e = c.length, f = 1 !== e || a && n.isFunction( a.promise ) ? e : 0, g = 1 === f ? a : n.Deferred(), h = function ( a, b, c ) {return function ( e ) {b[ a ] = this, c[ a ] = arguments.length > 1 ? d.call( arguments ) : e, c === i ? g.notifyWith( b, c ) : --f || g.resolveWith( b, c )}}, i, j, k;
      if ( e > 1 )for ( i = new Array( e ), j = new Array( e ), k = new Array( e ); e > b; b++ )c[ b ] && n.isFunction( c[ b ].promise ) ? c[ b ].promise().done( h( b, k, c ) ).fail( g.reject ).progress( h( b, j, i ) ) : --f;
      return f || g.resolveWith( k, c ), g.promise()
    }
  } );
  var H;
  n.fn.ready = function ( a ) {return n.ready.promise().done( a ), this}, n.extend( { isReady : !1, readyWait : 1, holdReady : function ( a ) {a ? n.readyWait++ : n.ready( !0 )}, ready : function ( a ) {(a === !0 ? --n.readyWait : n.isReady) || (n.isReady = !0, a !== !0 && --n.readyWait > 0 || (H.resolveWith( l, [ n ] ), n.fn.triggerHandler && (n( l ).triggerHandler( "ready" ), n( l ).off( "ready" ))))} } );
  function I () {l.removeEventListener( "DOMContentLoaded", I, !1 ), a.removeEventListener( "load", I, !1 ), n.ready()}

  n.ready.promise = function ( b ) {return H || (H = n.Deferred(), "complete" === l.readyState ? setTimeout( n.ready ) : (l.addEventListener( "DOMContentLoaded", I, !1 ), a.addEventListener( "load", I, !1 ))), H.promise( b )}, n.ready.promise();
  var J = n.access = function ( a, b, c, d, e, f, g ) {
    var h = 0, i = a.length, j = null == c;
    if ( "object" === n.type( c ) ) {
      e = !0;
      for ( h in c )n.access( a, b, h, c[ h ], !0, f, g )
    } else if ( void 0 !== d && (e = !0, n.isFunction( d ) || (g = !0), j && (g ? (b.call( a, d ), b = null) : (j = b, b = function ( a, b, c ) {return j.call( n( a ), c )})), b) )for ( ; i > h; h++ )b( a[ h ], c, g ? d : d.call( a[ h ], h, b( a[ h ], c ) ) );
    return e ? a : j ? b.call( a ) : i ? b( a[ 0 ], c ) : f
  };
  n.acceptData = function ( a ) {return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType};
  function K () {Object.defineProperty( this.cache = {}, 0, { get : function () {return {}} } ), this.expando = n.expando + K.uid++}

  K.uid = 1, K.accepts = n.acceptData, K.prototype = {
    key        : function ( a ) {
      if ( !K.accepts( a ) )return 0;
      var b = {}, c = a[ this.expando ];
      if ( !c ) {
        c = K.uid++;
        try {
          b[ this.expando ] = { value : c }, Object.defineProperties( a, b )
        } catch ( d ) {
          b[ this.expando ] = c, n.extend( a, b )
        }
      }
      return this.cache[ c ] || (this.cache[ c ] = {}), c
    }, set     : function ( a, b, c ) {
      var d, e = this.key( a ), f = this.cache[ e ];
      if ( "string" == typeof b )f[ b ] = c; else if ( n.isEmptyObject( f ) )n.extend( this.cache[ e ], b ); else for ( d in b )f[ d ] = b[ d ];
      return f
    }, get     : function ( a, b ) {
      var c = this.cache[ this.key( a ) ];
      return void 0 === b ? c : c[ b ]
    }, access  : function ( a, b, c ) {
      var d;
      return void 0 === b || b && "string" == typeof b && void 0 === c ? (d = this.get( a, b ), void 0 !== d ? d : this.get( a, n.camelCase( b ) )) : (this.set( a, b, c ), void 0 !== c ? c : b)
    }, remove  : function ( a, b ) {
      var c, d, e, f = this.key( a ), g = this.cache[ f ];
      if ( void 0 === b )this.cache[ f ] = {}; else {
        n.isArray( b ) ? d = b.concat( b.map( n.camelCase ) ) : (e = n.camelCase( b ), b in g ? d = [ b, e ] : (d = e, d = d in g ? [ d ] : d.match( E ) || [])), c = d.length;
        while ( c-- )delete g[ d[ c ] ]
      }
    }, hasData : function ( a ) {return !n.isEmptyObject( this.cache[ a[ this.expando ] ] || {} )}, discard : function ( a ) {a[ this.expando ] && delete this.cache[ a[ this.expando ] ]}
  };
  var L = new K, M = new K, N = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, O = /([A-Z])/g;

  function P ( a, b, c ) {
    var d;
    if ( void 0 === c && 1 === a.nodeType )if ( d = "data-" + b.replace( O, "-$1" ).toLowerCase(), c = a.getAttribute( d ), "string" == typeof c ) {
      try {
        c = "true" === c ? !0 : "false" === c ? !1 : "null" === c ? null : +c + "" === c ? +c : N.test( c ) ? n.parseJSON( c ) : c
      } catch ( e ) {
      }
      M.set( a, b, c )
    } else c = void 0;
    return c
  }

  n.extend( {
    hasData       : function ( a ) {return M.hasData( a ) || L.hasData( a )}, data : function ( a, b, c ) {
      return M.access( a, b, c )
    }, removeData : function ( a, b ) {M.remove( a, b )}, _data : function ( a, b, c ) {return L.access( a, b, c )}, _removeData : function ( a, b ) {L.remove( a, b )}
  } ), n.fn.extend( {
    data          : function ( a, b ) {
      var c, d, e, f = this[ 0 ], g = f && f.attributes;
      if ( void 0 === a ) {
        if ( this.length && (e = M.get( f ), 1 === f.nodeType && !L.get( f, "hasDataAttrs" )) ) {
          c = g.length;
          while ( c-- )g[ c ] && (d = g[ c ].name, 0 === d.indexOf( "data-" ) && (d = n.camelCase( d.slice( 5 ) ), P( f, d, e[ d ] )));
          L.set( f, "hasDataAttrs", !0 )
        }
        return e
      }
      return "object" == typeof a ? this.each( function () {M.set( this, a )} ) : J( this, function ( b ) {
        var c, d = n.camelCase( a );
        if ( f && void 0 === b ) {
          if ( c = M.get( f, a ), void 0 !== c )return c;
          if ( c = M.get( f, d ), void 0 !== c )return c;
          if ( c = P( f, d, void 0 ), void 0 !== c )return c
        } else this.each( function () {
          var c = M.get( this, d );
          M.set( this, d, b ), -1 !== a.indexOf( "-" ) && void 0 !== c && M.set( this, a, b )
        } )
      }, null, b, arguments.length > 1, null, !0 )
    }, removeData : function ( a ) {return this.each( function () {M.remove( this, a )} )}
  } ), n.extend( {
    queue          : function ( a, b, c ) {
      var d;
      return a ? (b = (b || "fx") + "queue", d = L.get( a, b ), c && (!d || n.isArray( c ) ? d = L.access( a, b, n.makeArray( c ) ) : d.push( c )), d || []) : void 0
    }, dequeue     : function ( a, b ) {
      b = b || "fx";
      var c = n.queue( a, b ), d = c.length, e = c.shift(), f = n._queueHooks( a, b ), g = function () {n.dequeue( a, b )};
      "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift( "inprogress" ), delete f.stop, e.call( a, g, f )), !d && f && f.empty.fire()
    }, _queueHooks : function ( a, b ) {
      var c = b + "queueHooks";
      return L.get( a, c ) || L.access( a, c, { empty : n.Callbacks( "once memory" ).add( function () {L.remove( a, [ b + "queue", c ] )} ) } )
    }
  } ), n.fn.extend( {
    queue      : function ( a, b ) {
      var c = 2;
      return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? n.queue( this[ 0 ], a ) : void 0 === b ? this : this.each( function () {
        var c = n.queue( this, a, b );
        n._queueHooks( this, a ), "fx" === a && "inprogress" !== c[ 0 ] && n.dequeue( this, a )
      } )
    }, dequeue : function ( a ) {return this.each( function () {n.dequeue( this, a )} )}, clearQueue : function ( a ) {return this.queue( a || "fx", [] )}, promise : function ( a, b ) {
      var c, d = 1, e = n.Deferred(), f = this, g = this.length, h = function () {--d || e.resolveWith( f, [ f ] )};
      "string" != typeof a && (b = a, a = void 0), a = a || "fx";
      while ( g-- )c = L.get( f[ g ], a + "queueHooks" ), c && c.empty && (d++, c.empty.add( h ));
      return h(), e.promise( b )
    }
  } );
  var Q = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, R = [ "Top", "Right", "Bottom", "Left" ], S = function ( a, b ) {return a = b || a, "none" === n.css( a, "display" ) || !n.contains( a.ownerDocument, a )}, T = /^(?:checkbox|radio)$/i;
  !function () {
    var a = l.createDocumentFragment(), b = a.appendChild( l.createElement( "div" ) ), c = l.createElement( "input" );
    c.setAttribute( "type", "radio" ), c.setAttribute( "checked", "checked" ), c.setAttribute( "name", "t" ), b.appendChild( c ), k.checkClone = b.cloneNode( !0 ).cloneNode( !0 ).lastChild.checked, b.innerHTML = "<textarea>x</textarea>", k.noCloneChecked = !!b.cloneNode( !0 ).lastChild.defaultValue
  }();
  var U = "undefined";
  k.focusinBubbles = "onfocusin"in a;
  var V = /^key/, W = /^(?:mouse|pointer|contextmenu)|click/, X = /^(?:focusinfocus|focusoutblur)$/, Y = /^([^.]*)(?:\.(.+)|)$/;

  function Z () {return !0}

  function $ () {return !1}

  function _ () {
    try {
      return l.activeElement
    } catch ( a ) {
    }
  }

  n.event = {
    global      : {}, add : function ( a, b, c, d, e ) {
      var f, g, h, i, j, k, l, m, o, p, q, r = L.get( a );
      if ( r ) {
        c.handler && (f = c, c = f.handler, e = f.selector), c.guid || (c.guid = n.guid++), (i = r.events) || (i = r.events = {}), (g = r.handle) || (g = r.handle = function ( b ) {return typeof n !== U && n.event.triggered !== b.type ? n.event.dispatch.apply( a, arguments ) : void 0}), b = (b || "").match( E ) || [ "" ], j = b.length;
        while ( j-- )h = Y.exec( b[ j ] ) || [], o = q = h[ 1 ], p = (h[ 2 ] || "").split( "." ).sort(), o && (l = n.event.special[ o ] || {}, o = (e ? l.delegateType : l.bindType) || o, l = n.event.special[ o ] || {}, k = n.extend( { type : o, origType : q, data : d, handler : c, guid : c.guid, selector : e, needsContext : e && n.expr.match.needsContext.test( e ), namespace : p.join( "." ) }, f ), (m = i[ o ]) || (m = i[ o ] = [], m.delegateCount = 0, l.setup && l.setup.call( a, d, p, g ) !== !1 || a.addEventListener && a.addEventListener( o, g, !1 )), l.add && (l.add.call( a, k ), k.handler.guid || (k.handler.guid = c.guid)), e ? m.splice( m.delegateCount++, 0, k ) : m.push( k ), n.event.global[ o ] = !0)
      }
    }, remove   : function ( a, b, c, d, e ) {
      var f, g, h, i, j, k, l, m, o, p, q, r = L.hasData( a ) && L.get( a );
      if ( r && (i = r.events) ) {
        b = (b || "").match( E ) || [ "" ], j = b.length;
        while ( j-- )if ( h = Y.exec( b[ j ] ) || [], o = q = h[ 1 ], p = (h[ 2 ] || "").split( "." ).sort(), o ) {
          l = n.event.special[ o ] || {}, o = (d ? l.delegateType : l.bindType) || o, m = i[ o ] || [], h = h[ 2 ] && new RegExp( "(^|\\.)" + p.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ), g = f = m.length;
          while ( f-- )k = m[ f ], !e && q !== k.origType || c && c.guid !== k.guid || h && !h.test( k.namespace ) || d && d !== k.selector && ("**" !== d || !k.selector) || (m.splice( f, 1 ), k.selector && m.delegateCount--, l.remove && l.remove.call( a, k ));
          g && !m.length && (l.teardown && l.teardown.call( a, p, r.handle ) !== !1 || n.removeEvent( a, o, r.handle ), delete i[ o ])
        } else for ( o in i )n.event.remove( a, o + b[ j ], c, d, !0 );
        n.isEmptyObject( i ) && (delete r.handle, L.remove( a, "events" ))
      }
    }, trigger  : function ( b, c, d, e ) {
      var f, g, h, i, k, m, o, p = [ d || l ], q = j.call( b, "type" ) ? b.type : b, r = j.call( b, "namespace" ) ? b.namespace.split( "." ) : [];
      if ( g = h = d = d || l, 3 !== d.nodeType && 8 !== d.nodeType && !X.test( q + n.event.triggered ) && (q.indexOf( "." ) >= 0 && (r = q.split( "." ), q = r.shift(), r.sort()), k = q.indexOf( ":" ) < 0 && "on" + q, b = b[ n.expando ] ? b : new n.Event( q, "object" == typeof b && b ), b.isTrigger = e ? 2 : 3, b.namespace = r.join( "." ), b.namespace_re = b.namespace ? new RegExp( "(^|\\.)" + r.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) : null, b.result = void 0, b.target || (b.target = d), c = null == c ? [ b ] : n.makeArray( c, [ b ] ), o = n.event.special[ q ] || {}, e || !o.trigger || o.trigger.apply( d, c ) !== !1) ) {
        if ( !e && !o.noBubble && !n.isWindow( d ) ) {
          for ( i = o.delegateType || q, X.test( i + q ) || (g = g.parentNode); g; g = g.parentNode )p.push( g ), h = g;
          h === (d.ownerDocument || l) && p.push( h.defaultView || h.parentWindow || a )
        }
        f = 0;
        while ( (g = p[ f++ ]) && !b.isPropagationStopped() )b.type = f > 1 ? i : o.bindType || q, m = (L.get( g, "events" ) || {})[ b.type ] && L.get( g, "handle" ), m && m.apply( g, c ), m = k && g[ k ], m && m.apply && n.acceptData( g ) && (b.result = m.apply( g, c ), b.result === !1 && b.preventDefault());
        return b.type = q, e || b.isDefaultPrevented() || o._default && o._default.apply( p.pop(), c ) !== !1 || !n.acceptData( d ) || k && n.isFunction( d[ q ] ) && !n.isWindow( d ) && (h = d[ k ], h && (d[ k ] = null), n.event.triggered = q, d[ q ](), n.event.triggered = void 0, h && (d[ k ] = h)), b.result
      }
    }, dispatch : function ( a ) {
      a = n.event.fix( a );
      var b, c, e, f, g, h = [], i = d.call( arguments ), j = (L.get( this, "events" ) || {})[ a.type ] || [], k = n.event.special[ a.type ] || {};
      if ( i[ 0 ] = a, a.delegateTarget = this, !k.preDispatch || k.preDispatch.call( this, a ) !== !1 ) {
        h = n.event.handlers.call( this, a, j ), b = 0;
        while ( (f = h[ b++ ]) && !a.isPropagationStopped() ) {
          a.currentTarget = f.elem, c = 0;
          while ( (g = f.handlers[ c++ ]) && !a.isImmediatePropagationStopped() )(!a.namespace_re || a.namespace_re.test( g.namespace )) && (a.handleObj = g, a.data = g.data, e = ((n.event.special[ g.origType ] || {}).handle || g.handler).apply( f.elem, i ), void 0 !== e && (a.result = e) === !1 && (a.preventDefault(), a.stopPropagation()))
        }
        return k.postDispatch && k.postDispatch.call( this, a ), a.result
      }
    }, handlers : function ( a, b ) {
      var c, d, e, f, g = [], h = b.delegateCount, i = a.target;
      if ( h && i.nodeType && (!a.button || "click" !== a.type) )for ( ; i !== this; i = i.parentNode || this )if ( i.disabled !== !0 || "click" !== a.type ) {
        for ( d = [], c = 0; h > c; c++ )f = b[ c ], e = f.selector + " ", void 0 === d[ e ] && (d[ e ] = f.needsContext ? n( e, this ).index( i ) >= 0 : n.find( e, this, null, [ i ] ).length), d[ e ] && d.push( f );
        d.length && g.push( { elem : i, handlers : d } )
      }
      return h < b.length && g.push( { elem : this, handlers : b.slice( h ) } ), g
    }, props    : "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split( " " ), fixHooks : {}, keyHooks : { props : "char charCode key keyCode".split( " " ), filter : function ( a, b ) {return null == a.which && (a.which = null != b.charCode ? b.charCode : b.keyCode), a} }, mouseHooks : {
      props : "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split( " " ), filter : function ( a, b ) {
        var c, d, e, f = b.button;
        return null == a.pageX && null != b.clientX && (c = a.target.ownerDocument || l, d = c.documentElement, e = c.body, a.pageX = b.clientX + (d && d.scrollLeft || e && e.scrollLeft || 0) - (d && d.clientLeft || e && e.clientLeft || 0), a.pageY = b.clientY + (d && d.scrollTop || e && e.scrollTop || 0) - (d && d.clientTop || e && e.clientTop || 0)), a.which || void 0 === f || (a.which = 1 & f ? 1 : 2 & f ? 3 : 4 & f ? 2 : 0), a
      }
    }, fix      : function ( a ) {
      if ( a[ n.expando ] )return a;
      var b, c, d, e = a.type, f = a, g = this.fixHooks[ e ];
      g || (this.fixHooks[ e ] = g = W.test( e ) ? this.mouseHooks : V.test( e ) ? this.keyHooks : {}), d = g.props ? this.props.concat( g.props ) : this.props, a = new n.Event( f ), b = d.length;
      while ( b-- )c = d[ b ], a[ c ] = f[ c ];
      return a.target || (a.target = l), 3 === a.target.nodeType && (a.target = a.target.parentNode), g.filter ? g.filter( a, f ) : a
    }, special  : { load : { noBubble : !0 }, focus : { trigger : function () {return this !== _() && this.focus ? (this.focus(), !1) : void 0}, delegateType : "focusin" }, blur : { trigger : function () {return this === _() && this.blur ? (this.blur(), !1) : void 0}, delegateType : "focusout" }, click : { trigger : function () {return "checkbox" === this.type && this.click && n.nodeName( this, "input" ) ? (this.click(), !1) : void 0}, _default : function ( a ) {return n.nodeName( a.target, "a" )} }, beforeunload : { postDispatch : function ( a ) {void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)} } }, simulate : function ( a, b, c, d ) {
      var e = n.extend( new n.Event, c, { type : a, isSimulated : !0, originalEvent : {} } );
      d ? n.event.trigger( e, null, b ) : n.event.dispatch.call( b, e ), e.isDefaultPrevented() && c.preventDefault()
    }
  }, n.removeEvent = function ( a, b, c ) {a.removeEventListener && a.removeEventListener( b, c, !1 )}, n.Event = function ( a, b ) {return this instanceof n.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? Z : $) : this.type = a, b && n.extend( this, b ), this.timeStamp = a && a.timeStamp || n.now(), void(this[ n.expando ] = !0)) : new n.Event( a, b )}, n.Event.prototype = {
    isDefaultPrevented          : $, isPropagationStopped : $, isImmediatePropagationStopped : $, preventDefault : function () {
      var a = this.originalEvent;
      this.isDefaultPrevented = Z, a && a.preventDefault && a.preventDefault()
    }, stopPropagation          : function () {
      var a = this.originalEvent;
      this.isPropagationStopped = Z, a && a.stopPropagation && a.stopPropagation()
    }, stopImmediatePropagation : function () {
      var a = this.originalEvent;
      this.isImmediatePropagationStopped = Z, a && a.stopImmediatePropagation && a.stopImmediatePropagation(), this.stopPropagation()
    }
  }, n.each( { mouseenter : "mouseover", mouseleave : "mouseout", pointerenter : "pointerover", pointerleave : "pointerout" }, function ( a, b ) {
    n.event.special[ a ] = {
      delegateType : b, bindType : b, handle : function ( a ) {
        var c, d = this, e = a.relatedTarget, f = a.handleObj;
        return (!e || e !== d && !n.contains( d, e )) && (a.type = f.origType, c = f.handler.apply( this, arguments ), a.type = b), c
      }
    }
  } ), k.focusinBubbles || n.each( { focus : "focusin", blur : "focusout" }, function ( a, b ) {
    var c = function ( a ) {n.event.simulate( b, a.target, n.event.fix( a ), !0 )};
    n.event.special[ b ] = {
      setup       : function () {
        var d = this.ownerDocument || this, e = L.access( d, b );
        e || d.addEventListener( a, c, !0 ), L.access( d, b, (e || 0) + 1 )
      }, teardown : function () {
        var d = this.ownerDocument || this, e = L.access( d, b ) - 1;
        e ? L.access( d, b, e ) : (d.removeEventListener( a, c, !0 ), L.remove( d, b ))
      }
    }
  } ), n.fn.extend( {
    on         : function ( a, b, c, d, e ) {
      var f, g;
      if ( "object" == typeof a ) {
        "string" != typeof b && (c = c || b, b = void 0);
        for ( g in a )this.on( g, b, c, a[ g ], e );
        return this
      }
      if ( null == c && null == d ? (d = b, c = b = void 0) : null == d && ("string" == typeof b ? (d = c, c = void 0) : (d = c, c = b, b = void 0)), d === !1 )d = $; else if ( !d )return this;
      return 1 === e && (f = d, d = function ( a ) {return n().off( a ), f.apply( this, arguments )}, d.guid = f.guid || (f.guid = n.guid++)), this.each( function () {n.event.add( this, a, d, c, b )} )
    }, one     : function ( a, b, c, d ) {return this.on( a, b, c, d, 1 )}, off : function ( a, b, c ) {
      var d, e;
      if ( a && a.preventDefault && a.handleObj )return d = a.handleObj, n( a.delegateTarget ).off( d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler ), this;
      if ( "object" == typeof a ) {
        for ( e in a )this.off( e, b, a[ e ] );
        return this
      }
      return (b === !1 || "function" == typeof b) && (c = b, b = void 0), c === !1 && (c = $), this.each( function () {n.event.remove( this, a, c, b )} )
    }, trigger : function ( a, b ) {return this.each( function () {n.event.trigger( a, b, this )} )}, triggerHandler : function ( a, b ) {
      var c = this[ 0 ];
      return c ? n.event.trigger( a, b, c, !0 ) : void 0
    }
  } );
  var aa = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, ba = /<([\w:]+)/, ca = /<|&#?\w+;/, da = /<(?:script|style|link)/i, ea = /checked\s*(?:[^=]|=\s*.checked.)/i, fa = /^$|\/(?:java|ecma)script/i, ga = /^true\/(.*)/, ha = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, ia = { option : [ 1, "<select multiple='multiple'>", "</select>" ], thead : [ 1, "<table>", "</table>" ], col : [ 2, "<table><colgroup>", "</colgroup></table>" ], tr : [ 2, "<table><tbody>", "</tbody></table>" ], td : [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ], _default : [ 0, "", "" ] };
  ia.optgroup = ia.option, ia.tbody = ia.tfoot = ia.colgroup = ia.caption = ia.thead, ia.th = ia.td;
  function ja ( a, b ) {return n.nodeName( a, "table" ) && n.nodeName( 11 !== b.nodeType ? b : b.firstChild, "tr" ) ? a.getElementsByTagName( "tbody" )[ 0 ] || a.appendChild( a.ownerDocument.createElement( "tbody" ) ) : a}

  function ka ( a ) {return a.type = (null !== a.getAttribute( "type" )) + "/" + a.type, a}

  function la ( a ) {
    var b = ga.exec( a.type );
    return b ? a.type = b[ 1 ] : a.removeAttribute( "type" ), a
  }

  function ma ( a, b ) {for ( var c = 0, d = a.length; d > c; c++ )L.set( a[ c ], "globalEval", !b || L.get( b[ c ], "globalEval" ) )}

  function na ( a, b ) {
    var c, d, e, f, g, h, i, j;
    if ( 1 === b.nodeType ) {
      if ( L.hasData( a ) && (f = L.access( a ), g = L.set( b, f ), j = f.events) ) {
        delete g.handle, g.events = {};
        for ( e in j )for ( c = 0, d = j[ e ].length; d > c; c++ )n.event.add( b, e, j[ e ][ c ] )
      }
      M.hasData( a ) && (h = M.access( a ), i = n.extend( {}, h ), M.set( b, i ))
    }
  }

  function oa ( a, b ) {
    var c = a.getElementsByTagName ? a.getElementsByTagName( b || "*" ) : a.querySelectorAll ? a.querySelectorAll( b || "*" ) : [];
    return void 0 === b || b && n.nodeName( a, b ) ? n.merge( [ a ], c ) : c
  }

  function pa ( a, b ) {
    var c = b.nodeName.toLowerCase();
    "input" === c && T.test( a.type ) ? b.checked = a.checked : ("input" === c || "textarea" === c) && (b.defaultValue = a.defaultValue)
  }

  n.extend( {
    clone            : function ( a, b, c ) {
      var d, e, f, g, h = a.cloneNode( !0 ), i = n.contains( a.ownerDocument, a );
      if ( !(k.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || n.isXMLDoc( a )) )for ( g = oa( h ), f = oa( a ), d = 0, e = f.length; e > d; d++ )pa( f[ d ], g[ d ] );
      if ( b )if ( c )for ( f = f || oa( a ), g = g || oa( h ), d = 0, e = f.length; e > d; d++ )na( f[ d ], g[ d ] ); else na( a, h );
      return g = oa( h, "script" ), g.length > 0 && ma( g, !i && oa( a, "script" ) ), h
    }, buildFragment : function ( a, b, c, d ) {
      for ( var e, f, g, h, i, j, k = b.createDocumentFragment(), l = [], m = 0, o = a.length; o > m; m++ )if ( e = a[ m ], e || 0 === e )if ( "object" === n.type( e ) )n.merge( l, e.nodeType ? [ e ] : e ); else if ( ca.test( e ) ) {
        f = f || k.appendChild( b.createElement( "div" ) ), g = (ba.exec( e ) || [ "", "" ])[ 1 ].toLowerCase(), h = ia[ g ] || ia._default, f.innerHTML = h[ 1 ] + e.replace( aa, "<$1></$2>" ) + h[ 2 ], j = h[ 0 ];
        while ( j-- )f = f.lastChild;
        n.merge( l, f.childNodes ), f = k.firstChild, f.textContent = ""
      } else l.push( b.createTextNode( e ) );
      k.textContent = "", m = 0;
      while ( e = l[ m++ ] )if ( (!d || -1 === n.inArray( e, d )) && (i = n.contains( e.ownerDocument, e ), f = oa( k.appendChild( e ), "script" ), i && ma( f ), c) ) {
        j = 0;
        while ( e = f[ j++ ] )fa.test( e.type || "" ) && c.push( e )
      }
      return k
    }, cleanData     : function ( a ) {
      for ( var b, c, d, e, f = n.event.special, g = 0; void 0 !== (c = a[ g ]); g++ ) {
        if ( n.acceptData( c ) && (e = c[ L.expando ], e && (b = L.cache[ e ])) ) {
          if ( b.events )for ( d in b.events )f[ d ] ? n.event.remove( c, d ) : n.removeEvent( c, d, b.handle );
          L.cache[ e ] && delete L.cache[ e ]
        }
        delete M.cache[ c[ M.expando ] ]
      }
    }
  } ), n.fn.extend( {
    text           : function ( a ) {return J( this, function ( a ) {return void 0 === a ? n.text( this ) : this.empty().each( function () {(1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && (this.textContent = a)} )}, null, a, arguments.length )}, append : function () {
      return this.domManip( arguments, function ( a ) {
        if ( 1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType ) {
          var b = ja( this, a );
          b.appendChild( a )
        }
      } )
    }, prepend     : function () {
      return this.domManip( arguments, function ( a ) {
        if ( 1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType ) {
          var b = ja( this, a );
          b.insertBefore( a, b.firstChild )
        }
      } )
    }, before      : function () {return this.domManip( arguments, function ( a ) {this.parentNode && this.parentNode.insertBefore( a, this )} )}, after : function () {return this.domManip( arguments, function ( a ) {this.parentNode && this.parentNode.insertBefore( a, this.nextSibling )} )}, remove : function ( a, b ) {
      for ( var c, d = a ? n.filter( a, this ) : this, e = 0; null != (c = d[ e ]); e++ )b || 1 !== c.nodeType || n.cleanData( oa( c ) ), c.parentNode && (b && n.contains( c.ownerDocument, c ) && ma( oa( c, "script" ) ), c.parentNode.removeChild( c ));
      return this
    }, empty       : function () {
      for ( var a, b = 0; null != (a = this[ b ]); b++ )1 === a.nodeType && (n.cleanData( oa( a, !1 ) ), a.textContent = "");
      return this
    }, clone       : function ( a, b ) {return a = null == a ? !1 : a, b = null == b ? a : b, this.map( function () {return n.clone( this, a, b )} )}, html : function ( a ) {
      return J( this, function ( a ) {
        var b = this[ 0 ] || {}, c = 0, d = this.length;
        if ( void 0 === a && 1 === b.nodeType )return b.innerHTML;
        if ( "string" == typeof a && !da.test( a ) && !ia[ (ba.exec( a ) || [ "", "" ])[ 1 ].toLowerCase() ] ) {
          a = a.replace( aa, "<$1></$2>" );
          try {
            for ( ; d > c; c++ )b = this[ c ] || {}, 1 === b.nodeType && (n.cleanData( oa( b, !1 ) ), b.innerHTML = a);
            b = 0
          } catch ( e ) {
          }
        }
        b && this.empty().append( a )
      }, null, a, arguments.length )
    }, replaceWith : function () {
      var a = arguments[ 0 ];
      return this.domManip( arguments, function ( b ) {a = this.parentNode, n.cleanData( oa( this ) ), a && a.replaceChild( b, this )} ), a && (a.length || a.nodeType) ? this : this.remove()
    }, detach      : function ( a ) {return this.remove( a, !0 )}, domManip : function ( a, b ) {
      a = e.apply( [], a );
      var c, d, f, g, h, i, j = 0, l = this.length, m = this, o = l - 1, p = a[ 0 ], q = n.isFunction( p );
      if ( q || l > 1 && "string" == typeof p && !k.checkClone && ea.test( p ) )return this.each( function ( c ) {
        var d = m.eq( c );
        q && (a[ 0 ] = p.call( this, c, d.html() )), d.domManip( a, b )
      } );
      if ( l && (c = n.buildFragment( a, this[ 0 ].ownerDocument, !1, this ), d = c.firstChild, 1 === c.childNodes.length && (c = d), d) ) {
        for ( f = n.map( oa( c, "script" ), ka ), g = f.length; l > j; j++ )h = c, j !== o && (h = n.clone( h, !0, !0 ), g && n.merge( f, oa( h, "script" ) )), b.call( this[ j ], h, j );
        if ( g )for ( i = f[ f.length - 1 ].ownerDocument, n.map( f, la ), j = 0; g > j; j++ )h = f[ j ], fa.test( h.type || "" ) && !L.access( h, "globalEval" ) && n.contains( i, h ) && (h.src ? n._evalUrl && n._evalUrl( h.src ) : n.globalEval( h.textContent.replace( ha, "" ) ))
      }
      return this
    }
  } ), n.each( { appendTo : "append", prependTo : "prepend", insertBefore : "before", insertAfter : "after", replaceAll : "replaceWith" }, function ( a, b ) {
    n.fn[ a ] = function ( a ) {
      for ( var c, d = [], e = n( a ), g = e.length - 1, h = 0; g >= h; h++ )c = h === g ? this : this.clone( !0 ), n( e[ h ] )[ b ]( c ), f.apply( d, c.get() );
      return this.pushStack( d )
    }
  } );
  var qa, ra = {};

  function sa ( b, c ) {
    var d, e = n( c.createElement( b ) ).appendTo( c.body ), f = a.getDefaultComputedStyle && (d = a.getDefaultComputedStyle( e[ 0 ] )) ? d.display : n.css( e[ 0 ], "display" );
    return e.detach(), f
  }

  function ta ( a ) {
    var b = l, c = ra[ a ];
    return c || (c = sa( a, b ), "none" !== c && c || (qa = (qa || n( "<iframe frameborder='0' width='0' height='0'/>" )).appendTo( b.documentElement ), b = qa[ 0 ].contentDocument, b.write(), b.close(), c = sa( a, b ), qa.detach()), ra[ a ] = c), c
  }

  var ua = /^margin/, va = new RegExp( "^(" + Q + ")(?!px)[a-z%]+$", "i" ), wa = function ( b ) {return b.ownerDocument.defaultView.opener ? b.ownerDocument.defaultView.getComputedStyle( b, null ) : a.getComputedStyle( b, null )};

  function xa ( a, b, c ) {
    var d, e, f, g, h = a.style;
    return c = c || wa( a ), c && (g = c.getPropertyValue( b ) || c[ b ]), c && ("" !== g || n.contains( a.ownerDocument, a ) || (g = n.style( a, b )), va.test( g ) && ua.test( b ) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 !== g ? g + "" : g
  }

  function ya ( a, b ) {return { get : function () {return a() ? void delete this.get : (this.get = b).apply( this, arguments )} }}

  !function () {
    var b, c, d = l.documentElement, e = l.createElement( "div" ), f = l.createElement( "div" );
    if ( f.style ) {
      f.style.backgroundClip = "content-box", f.cloneNode( !0 ).style.backgroundClip = "", k.clearCloneStyle = "content-box" === f.style.backgroundClip, e.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute", e.appendChild( f );
      function g () {
        f.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", f.innerHTML = "", d.appendChild( e );
        var g = a.getComputedStyle( f, null );
        b = "1%" !== g.top, c = "4px" === g.width, d.removeChild( e )
      }

      a.getComputedStyle && n.extend( k, {
        pixelPosition : function () {return g(), b}, boxSizingReliable : function () {return null == c && g(), c}, reliableMarginRight : function () {
          var b, c = f.appendChild( l.createElement( "div" ) );
          return c.style.cssText = f.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", c.style.marginRight = c.style.width = "0", f.style.width = "1px", d.appendChild( e ), b = !parseFloat( a.getComputedStyle( c, null ).marginRight ), d.removeChild( e ), f.removeChild( c ), b
        }
      } )
    }
  }(), n.swap = function ( a, b, c, d ) {
    var e, f, g = {};
    for ( f in b )g[ f ] = a.style[ f ], a.style[ f ] = b[ f ];
    e = c.apply( a, d || [] );
    for ( f in b )a.style[ f ] = g[ f ];
    return e
  };
  var za = /^(none|table(?!-c[ea]).+)/, Aa = new RegExp( "^(" + Q + ")(.*)$", "i" ), Ba = new RegExp( "^([+-])=(" + Q + ")", "i" ), Ca = { position : "absolute", visibility : "hidden", display : "block" }, Da = { letterSpacing : "0", fontWeight : "400" }, Ea = [ "Webkit", "O", "Moz", "ms" ];

  function Fa ( a, b ) {
    if ( b in a )return b;
    var c = b[ 0 ].toUpperCase() + b.slice( 1 ), d = b, e = Ea.length;
    while ( e-- )if ( b = Ea[ e ] + c, b in a )return b;
    return d
  }

  function Ga ( a, b, c ) {
    var d = Aa.exec( b );
    return d ? Math.max( 0, d[ 1 ] - (c || 0) ) + (d[ 2 ] || "px") : b
  }

  function Ha ( a, b, c, d, e ) {
    for ( var f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0, g = 0; 4 > f; f += 2 )"margin" === c && (g += n.css( a, c + R[ f ], !0, e )), d ? ("content" === c && (g -= n.css( a, "padding" + R[ f ], !0, e )), "margin" !== c && (g -= n.css( a, "border" + R[ f ] + "Width", !0, e ))) : (g += n.css( a, "padding" + R[ f ], !0, e ), "padding" !== c && (g += n.css( a, "border" + R[ f ] + "Width", !0, e )));
    return g
  }

  function Ia ( a, b, c ) {
    var d = !0, e = "width" === b ? a.offsetWidth : a.offsetHeight, f = wa( a ), g = "border-box" === n.css( a, "boxSizing", !1, f );
    if ( 0 >= e || null == e ) {
      if ( e = xa( a, b, f ), (0 > e || null == e) && (e = a.style[ b ]), va.test( e ) )return e;
      d = g && (k.boxSizingReliable() || e === a.style[ b ]), e = parseFloat( e ) || 0
    }
    return e + Ha( a, b, c || (g ? "border" : "content"), d, f ) + "px"
  }

  function Ja ( a, b ) {
    for ( var c, d, e, f = [], g = 0, h = a.length; h > g; g++ )d = a[ g ], d.style && (f[ g ] = L.get( d, "olddisplay" ), c = d.style.display, b ? (f[ g ] || "none" !== c || (d.style.display = ""), "" === d.style.display && S( d ) && (f[ g ] = L.access( d, "olddisplay", ta( d.nodeName ) ))) : (e = S( d ), "none" === c && e || L.set( d, "olddisplay", e ? c : n.css( d, "display" ) )));
    for ( g = 0; h > g; g++ )d = a[ g ], d.style && (b && "none" !== d.style.display && "" !== d.style.display || (d.style.display = b ? f[ g ] || "" : "none"));
    return a
  }

  n.extend( {
    cssHooks     : {
      opacity : {
        get : function ( a, b ) {
          if ( b ) {
            var c = xa( a, "opacity" );
            return "" === c ? "1" : c
          }
        }
      }
    }, cssNumber : { columnCount : !0, fillOpacity : !0, flexGrow : !0, flexShrink : !0, fontWeight : !0, lineHeight : !0, opacity : !0, order : !0, orphans : !0, widows : !0, zIndex : !0, zoom : !0 }, cssProps : { "float" : "cssFloat" }, style : function ( a, b, c, d ) {
      if ( a && 3 !== a.nodeType && 8 !== a.nodeType && a.style ) {
        var e, f, g, h = n.camelCase( b ), i = a.style;
        return b = n.cssProps[ h ] || (n.cssProps[ h ] = Fa( i, h )), g = n.cssHooks[ b ] || n.cssHooks[ h ], void 0 === c ? g && "get"in g && void 0 !== (e = g.get( a, !1, d )) ? e : i[ b ] : (f = typeof c, "string" === f && (e = Ba.exec( c )) && (c = (e[ 1 ] + 1) * e[ 2 ] + parseFloat( n.css( a, b ) ), f = "number"), null != c && c === c && ("number" !== f || n.cssNumber[ h ] || (c += "px"), k.clearCloneStyle || "" !== c || 0 !== b.indexOf( "background" ) || (i[ b ] = "inherit"), g && "set"in g && void 0 === (c = g.set( a, c, d )) || (i[ b ] = c)), void 0)
      }
    }, css       : function ( a, b, c, d ) {
      var e, f, g, h = n.camelCase( b );
      return b = n.cssProps[ h ] || (n.cssProps[ h ] = Fa( a.style, h )), g = n.cssHooks[ b ] || n.cssHooks[ h ], g && "get"in g && (e = g.get( a, !0, c )), void 0 === e && (e = xa( a, b, d )), "normal" === e && b in Da && (e = Da[ b ]), "" === c || c ? (f = parseFloat( e ), c === !0 || n.isNumeric( f ) ? f || 0 : e) : e
    }
  } ), n.each( [ "height", "width" ], function ( a, b ) {
    n.cssHooks[ b ] = {
      get : function ( a, c, d ) {return c ? za.test( n.css( a, "display" ) ) && 0 === a.offsetWidth ? n.swap( a, Ca, function () {return Ia( a, b, d )} ) : Ia( a, b, d ) : void 0}, set : function ( a, c, d ) {
        var e = d && wa( a );
        return Ga( a, c, d ? Ha( a, b, d, "border-box" === n.css( a, "boxSizing", !1, e ), e ) : 0 )
      }
    }
  } ), n.cssHooks.marginRight = ya( k.reliableMarginRight, function ( a, b ) {return b ? n.swap( a, { display : "inline-block" }, xa, [ a, "marginRight" ] ) : void 0} ), n.each( { margin : "", padding : "", border : "Width" }, function ( a, b ) {
    n.cssHooks[ a + b ] = {
      expand : function ( c ) {
        for ( var d = 0, e = {}, f = "string" == typeof c ? c.split( " " ) : [ c ]; 4 > d; d++ )e[ a + R[ d ] + b ] = f[ d ] || f[ d - 2 ] || f[ 0 ];
        return e
      }
    }, ua.test( a ) || (n.cssHooks[ a + b ].set = Ga)
  } ), n.fn.extend( {
    css     : function ( a, b ) {
      return J( this, function ( a, b, c ) {
        var d, e, f = {}, g = 0;
        if ( n.isArray( b ) ) {
          for ( d = wa( a ), e = b.length; e > g; g++ )f[ b[ g ] ] = n.css( a, b[ g ], !1, d );
          return f
        }
        return void 0 !== c ? n.style( a, b, c ) : n.css( a, b )
      }, a, b, arguments.length > 1 )
    }, show : function () {return Ja( this, !0 )}, hide : function () {return Ja( this )}, toggle : function ( a ) {return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each( function () {S( this ) ? n( this ).show() : n( this ).hide()} )}
  } );
  function Ka ( a, b, c, d, e ) {return new Ka.prototype.init( a, b, c, d, e )}

  n.Tween = Ka, Ka.prototype = {
    constructor : Ka, init : function ( a, b, c, d, e, f ) {this.elem = a, this.prop = c, this.easing = e || "swing", this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (n.cssNumber[ c ] ? "" : "px")}, cur : function () {
      var a = Ka.propHooks[ this.prop ];
      return a && a.get ? a.get( this ) : Ka.propHooks._default.get( this )
    }, run      : function ( a ) {
      var b, c = Ka.propHooks[ this.prop ];
      return this.options.duration ? this.pos = b = n.easing[ this.easing ]( a, this.options.duration * a, 0, 1, this.options.duration ) : this.pos = b = a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call( this.elem, this.now, this ), c && c.set ? c.set( this ) : Ka.propHooks._default.set( this ), this
    }
  }, Ka.prototype.init.prototype = Ka.prototype, Ka.propHooks = {
    _default : {
      get    : function ( a ) {
        var b;
        return null == a.elem[ a.prop ] || a.elem.style && null != a.elem.style[ a.prop ] ? (b = n.css( a.elem, a.prop, "" ), b && "auto" !== b ? b : 0) : a.elem[ a.prop ]
      }, set : function ( a ) {n.fx.step[ a.prop ] ? n.fx.step[ a.prop ]( a ) : a.elem.style && (null != a.elem.style[ n.cssProps[ a.prop ] ] || n.cssHooks[ a.prop ]) ? n.style( a.elem, a.prop, a.now + a.unit ) : a.elem[ a.prop ] = a.now}
    }
  }, Ka.propHooks.scrollTop = Ka.propHooks.scrollLeft = { set : function ( a ) {a.elem.nodeType && a.elem.parentNode && (a.elem[ a.prop ] = a.now)} }, n.easing = { linear : function ( a ) {return a}, swing : function ( a ) {return .5 - Math.cos( a * Math.PI ) / 2} }, n.fx = Ka.prototype.init, n.fx.step = {};
  var La, Ma, Na = /^(?:toggle|show|hide)$/, Oa = new RegExp( "^(?:([+-])=|)(" + Q + ")([a-z%]*)$", "i" ), Pa = /queueHooks$/, Qa = [ Va ], Ra = {
    "*" : [ function ( a, b ) {
      var c = this.createTween( a, b ), d = c.cur(), e = Oa.exec( b ), f = e && e[ 3 ] || (n.cssNumber[ a ] ? "" : "px"), g = (n.cssNumber[ a ] || "px" !== f && +d) && Oa.exec( n.css( c.elem, a ) ), h = 1, i = 20;
      if ( g && g[ 3 ] !== f ) {
        f = f || g[ 3 ], e = e || [], g = +d || 1;
        do h = h || ".5", g /= h, n.style( c.elem, a, g + f ); while ( h !== (h = c.cur() / d) && 1 !== h && --i )
      }
      return e && (g = c.start = +g || +d || 0, c.unit = f, c.end = e[ 1 ] ? g + (e[ 1 ] + 1) * e[ 2 ] : +e[ 2 ]), c
    } ]
  };

  function Sa () {return setTimeout( function () {La = void 0} ), La = n.now()}

  function Ta ( a, b ) {
    var c, d = 0, e = { height : a };
    for ( b = b ? 1 : 0; 4 > d; d += 2 - b )c = R[ d ], e[ "margin" + c ] = e[ "padding" + c ] = a;
    return b && (e.opacity = e.width = a), e
  }

  function Ua ( a, b, c ) {for ( var d, e = (Ra[ b ] || []).concat( Ra[ "*" ] ), f = 0, g = e.length; g > f; f++ )if ( d = e[ f ].call( c, b, a ) )return d}

  function Va ( a, b, c ) {
    var d, e, f, g, h, i, j, k, l = this, m = {}, o = a.style, p = a.nodeType && S( a ), q = L.get( a, "fxshow" );
    c.queue || (h = n._queueHooks( a, "fx" ), null == h.unqueued && (h.unqueued = 0, i = h.empty.fire, h.empty.fire = function () {h.unqueued || i()}), h.unqueued++, l.always( function () {l.always( function () {h.unqueued--, n.queue( a, "fx" ).length || h.empty.fire()} )} )), 1 === a.nodeType && ("height"in b || "width"in b) && (c.overflow = [ o.overflow, o.overflowX, o.overflowY ], j = n.css( a, "display" ), k = "none" === j ? L.get( a, "olddisplay" ) || ta( a.nodeName ) : j, "inline" === k && "none" === n.css( a, "float" ) && (o.display = "inline-block")), c.overflow && (o.overflow = "hidden", l.always( function () {o.overflow = c.overflow[ 0 ], o.overflowX = c.overflow[ 1 ], o.overflowY = c.overflow[ 2 ]} ));
    for ( d in b )if ( e = b[ d ], Na.exec( e ) ) {
      if ( delete b[ d ], f = f || "toggle" === e, e === (p ? "hide" : "show") ) {
        if ( "show" !== e || !q || void 0 === q[ d ] )continue;
        p = !0
      }
      m[ d ] = q && q[ d ] || n.style( a, d )
    } else j = void 0;
    if ( n.isEmptyObject( m ) )"inline" === ("none" === j ? ta( a.nodeName ) : j) && (o.display = j); else {
      q ? "hidden"in q && (p = q.hidden) : q = L.access( a, "fxshow", {} ), f && (q.hidden = !p), p ? n( a ).show() : l.done( function () {n( a ).hide()} ), l.done( function () {
        var b;
        L.remove( a, "fxshow" );
        for ( b in m )n.style( a, b, m[ b ] )
      } );
      for ( d in m )g = Ua( p ? q[ d ] : 0, d, l ), d in q || (q[ d ] = g.start, p && (g.end = g.start, g.start = "width" === d || "height" === d ? 1 : 0))
    }
  }

  function Wa ( a, b ) {
    var c, d, e, f, g;
    for ( c in a )if ( d = n.camelCase( c ), e = b[ d ], f = a[ c ], n.isArray( f ) && (e = f[ 1 ], f = a[ c ] = f[ 0 ]), c !== d && (a[ d ] = f, delete a[ c ]), g = n.cssHooks[ d ], g && "expand"in g ) {
      f = g.expand( f ), delete a[ d ];
      for ( c in f )c in a || (a[ c ] = f[ c ], b[ c ] = e)
    } else b[ d ] = e
  }

  function Xa ( a, b, c ) {
    var d, e, f = 0, g = Qa.length, h = n.Deferred().always( function () {delete i.elem} ), i = function () {
      if ( e )return !1;
      for ( var b = La || Sa(), c = Math.max( 0, j.startTime + j.duration - b ), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; i > g; g++ )j.tweens[ g ].run( f );
      return h.notifyWith( a, [ j, f, c ] ), 1 > f && i ? c : (h.resolveWith( a, [ j ] ), !1)
    }, j        = h.promise( {
      elem    : a, props : n.extend( {}, b ), opts : n.extend( !0, { specialEasing : {} }, c ), originalProperties : b, originalOptions : c, startTime : La || Sa(), duration : c.duration, tweens : [], createTween : function ( b, c ) {
        var d = n.Tween( a, j.opts, b, c, j.opts.specialEasing[ b ] || j.opts.easing );
        return j.tweens.push( d ), d
      }, stop : function ( b ) {
        var c = 0, d = b ? j.tweens.length : 0;
        if ( e )return this;
        for ( e = !0; d > c; c++ )j.tweens[ c ].run( 1 );
        return b ? h.resolveWith( a, [ j, b ] ) : h.rejectWith( a, [ j, b ] ), this
      }
    } ), k      = j.props;
    for ( Wa( k, j.opts.specialEasing ); g > f; f++ )if ( d = Qa[ f ].call( j, a, k, j.opts ) )return d;
    return n.map( k, Ua, j ), n.isFunction( j.opts.start ) && j.opts.start.call( a, j ), n.fx.timer( n.extend( i, { elem : a, anim : j, queue : j.opts.queue } ) ), j.progress( j.opts.progress ).done( j.opts.done, j.opts.complete ).fail( j.opts.fail ).always( j.opts.always )
  }

  n.Animation = n.extend( Xa, {
    tweener      : function ( a, b ) {
      n.isFunction( a ) ? (b = a, a = [ "*" ]) : a = a.split( " " );
      for ( var c, d = 0, e = a.length; e > d; d++ )c = a[ d ], Ra[ c ] = Ra[ c ] || [], Ra[ c ].unshift( b )
    }, prefilter : function ( a, b ) {b ? Qa.unshift( a ) : Qa.push( a )}
  } ), n.speed = function ( a, b, c ) {
    var d = a && "object" == typeof a ? n.extend( {}, a ) : { complete : c || !c && b || n.isFunction( a ) && a, duration : a, easing : c && b || b && !n.isFunction( b ) && b };
    return d.duration = n.fx.off ? 0 : "number" == typeof d.duration ? d.duration : d.duration in n.fx.speeds ? n.fx.speeds[ d.duration ] : n.fx.speeds._default, (null == d.queue || d.queue === !0) && (d.queue = "fx"), d.old = d.complete, d.complete = function () {n.isFunction( d.old ) && d.old.call( this ), d.queue && n.dequeue( this, d.queue )}, d
  }, n.fn.extend( {
    fadeTo    : function ( a, b, c, d ) {return this.filter( S ).css( "opacity", 0 ).show().end().animate( { opacity : b }, a, c, d )}, animate : function ( a, b, c, d ) {
      var e = n.isEmptyObject( a ), f = n.speed( b, c, d ), g = function () {
        var b = Xa( this, n.extend( {}, a ), f );
        (e || L.get( this, "finish" )) && b.stop( !0 )
      };
      return g.finish = g, e || f.queue === !1 ? this.each( g ) : this.queue( f.queue, g )
    }, stop   : function ( a, b, c ) {
      var d = function ( a ) {
        var b = a.stop;
        delete a.stop, b( c )
      };
      return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue( a || "fx", [] ), this.each( function () {
        var b = !0, e = null != a && a + "queueHooks", f = n.timers, g = L.get( this );
        if ( e )g[ e ] && g[ e ].stop && d( g[ e ] ); else for ( e in g )g[ e ] && g[ e ].stop && Pa.test( e ) && d( g[ e ] );
        for ( e = f.length; e--; )f[ e ].elem !== this || null != a && f[ e ].queue !== a || (f[ e ].anim.stop( c ), b = !1, f.splice( e, 1 ));
        (b || !c) && n.dequeue( this, a )
      } )
    }, finish : function ( a ) {
      return a !== !1 && (a = a || "fx"), this.each( function () {
        var b, c = L.get( this ), d = c[ a + "queue" ], e = c[ a + "queueHooks" ], f = n.timers, g = d ? d.length : 0;
        for ( c.finish = !0, n.queue( this, a, [] ), e && e.stop && e.stop.call( this, !0 ), b = f.length; b--; )f[ b ].elem === this && f[ b ].queue === a && (f[ b ].anim.stop( !0 ), f.splice( b, 1 ));
        for ( b = 0; g > b; b++ )d[ b ] && d[ b ].finish && d[ b ].finish.call( this );
        delete c.finish
      } )
    }
  } ), n.each( [ "toggle", "show", "hide" ], function ( a, b ) {
    var c = n.fn[ b ];
    n.fn[ b ] = function ( a, d, e ) {return null == a || "boolean" == typeof a ? c.apply( this, arguments ) : this.animate( Ta( b, !0 ), a, d, e )}
  } ), n.each( { slideDown : Ta( "show" ), slideUp : Ta( "hide" ), slideToggle : Ta( "toggle" ), fadeIn : { opacity : "show" }, fadeOut : { opacity : "hide" }, fadeToggle : { opacity : "toggle" } }, function ( a, b ) {n.fn[ a ] = function ( a, c, d ) {return this.animate( b, a, c, d )}} ), n.timers = [], n.fx.tick = function () {
    var a, b = 0, c = n.timers;
    for ( La = n.now(); b < c.length; b++ )a = c[ b ], a() || c[ b ] !== a || c.splice( b--, 1 );
    c.length || n.fx.stop(), La = void 0
  }, n.fx.timer = function ( a ) {n.timers.push( a ), a() ? n.fx.start() : n.timers.pop()}, n.fx.interval = 13, n.fx.start = function () {Ma || (Ma = setInterval( n.fx.tick, n.fx.interval ))}, n.fx.stop = function () {clearInterval( Ma ), Ma = null}, n.fx.speeds = { slow : 600, fast : 200, _default : 400 }, n.fn.delay = function ( a, b ) {
    return a = n.fx ? n.fx.speeds[ a ] || a : a, b = b || "fx", this.queue( b, function ( b, c ) {
      var d = setTimeout( b, a );
      c.stop = function () {clearTimeout( d )}
    } )
  }, function () {
    var a = l.createElement( "input" ), b = l.createElement( "select" ), c = b.appendChild( l.createElement( "option" ) );
    a.type = "checkbox", k.checkOn = "" !== a.value, k.optSelected = c.selected, b.disabled = !0, k.optDisabled = !c.disabled, a = l.createElement( "input" ), a.value = "t", a.type = "radio", k.radioValue = "t" === a.value
  }();
  var Ya, Za, $a = n.expr.attrHandle;
  n.fn.extend( { attr : function ( a, b ) {return J( this, n.attr, a, b, arguments.length > 1 )}, removeAttr : function ( a ) {return this.each( function () {n.removeAttr( this, a )} )} } ), n.extend( {
    attr          : function ( a, b, c ) {
      var d, e, f = a.nodeType;
      if ( a && 3 !== f && 8 !== f && 2 !== f )return typeof a.getAttribute === U ? n.prop( a, b, c ) : (1 === f && n.isXMLDoc( a ) || (b = b.toLowerCase(), d = n.attrHooks[ b ] || (n.expr.match.bool.test( b ) ? Za : Ya)),
        void 0 === c ? d && "get"in d && null !== (e = d.get( a, b )) ? e : (e = n.find.attr( a, b ), null == e ? void 0 : e) : null !== c ? d && "set"in d && void 0 !== (e = d.set( a, c, b )) ? e : (a.setAttribute( b, c + "" ), c) : void n.removeAttr( a, b ))
    }, removeAttr : function ( a, b ) {
      var c, d, e = 0, f = b && b.match( E );
      if ( f && 1 === a.nodeType )while ( c = f[ e++ ] )d = n.propFix[ c ] || c, n.expr.match.bool.test( c ) && (a[ d ] = !1), a.removeAttribute( c )
    }, attrHooks  : {
      type : {
        set : function ( a, b ) {
          if ( !k.radioValue && "radio" === b && n.nodeName( a, "input" ) ) {
            var c = a.value;
            return a.setAttribute( "type", b ), c && (a.value = c), b
          }
        }
      }
    }
  } ), Za = { set : function ( a, b, c ) {return b === !1 ? n.removeAttr( a, c ) : a.setAttribute( c, c ), c} }, n.each( n.expr.match.bool.source.match( /\w+/g ), function ( a, b ) {
    var c = $a[ b ] || n.find.attr;
    $a[ b ] = function ( a, b, d ) {
      var e, f;
      return d || (f = $a[ b ], $a[ b ] = e, e = null != c( a, b, d ) ? b.toLowerCase() : null, $a[ b ] = f), e
    }
  } );
  var _a = /^(?:input|select|textarea|button)$/i;
  n.fn.extend( { prop : function ( a, b ) {return J( this, n.prop, a, b, arguments.length > 1 )}, removeProp : function ( a ) {return this.each( function () {delete this[ n.propFix[ a ] || a ]} )} } ), n.extend( {
    propFix      : { "for" : "htmlFor", "class" : "className" }, prop : function ( a, b, c ) {
      var d, e, f, g = a.nodeType;
      if ( a && 3 !== g && 8 !== g && 2 !== g )return f = 1 !== g || !n.isXMLDoc( a ), f && (b = n.propFix[ b ] || b, e = n.propHooks[ b ]), void 0 !== c ? e && "set"in e && void 0 !== (d = e.set( a, c, b )) ? d : a[ b ] = c : e && "get"in e && null !== (d = e.get( a, b )) ? d : a[ b ]
    }, propHooks : { tabIndex : { get : function ( a ) {return a.hasAttribute( "tabindex" ) || _a.test( a.nodeName ) || a.href ? a.tabIndex : -1} } }
  } ), k.optSelected || (n.propHooks.selected = {
    get : function ( a ) {
      var b = a.parentNode;
      return b && b.parentNode && b.parentNode.selectedIndex, null
    }
  }), n.each( [ "tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable" ], function () {n.propFix[ this.toLowerCase() ] = this} );
  var ab = /[\t\r\n\f]/g;
  n.fn.extend( {
    addClass       : function ( a ) {
      var b, c, d, e, f, g, h = "string" == typeof a && a, i = 0, j = this.length;
      if ( n.isFunction( a ) )return this.each( function ( b ) {n( this ).addClass( a.call( this, b, this.className ) )} );
      if ( h )for ( b = (a || "").match( E ) || []; j > i; i++ )if ( c = this[ i ], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace( ab, " " ) : " ") ) {
        f = 0;
        while ( e = b[ f++ ] )d.indexOf( " " + e + " " ) < 0 && (d += e + " ");
        g = n.trim( d ), c.className !== g && (c.className = g)
      }
      return this
    }, removeClass : function ( a ) {
      var b, c, d, e, f, g, h = 0 === arguments.length || "string" == typeof a && a, i = 0, j = this.length;
      if ( n.isFunction( a ) )return this.each( function ( b ) {n( this ).removeClass( a.call( this, b, this.className ) )} );
      if ( h )for ( b = (a || "").match( E ) || []; j > i; i++ )if ( c = this[ i ], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace( ab, " " ) : "") ) {
        f = 0;
        while ( e = b[ f++ ] )while ( d.indexOf( " " + e + " " ) >= 0 )d = d.replace( " " + e + " ", " " );
        g = a ? n.trim( d ) : "", c.className !== g && (c.className = g)
      }
      return this
    }, toggleClass : function ( a, b ) {
      var c = typeof a;
      return "boolean" == typeof b && "string" === c ? b ? this.addClass( a ) : this.removeClass( a ) : this.each( n.isFunction( a ) ? function ( c ) {n( this ).toggleClass( a.call( this, c, this.className, b ), b )} : function () {
        if ( "string" === c ) {
          var b, d = 0, e = n( this ), f = a.match( E ) || [];
          while ( b = f[ d++ ] )e.hasClass( b ) ? e.removeClass( b ) : e.addClass( b )
        } else(c === U || "boolean" === c) && (this.className && L.set( this, "__className__", this.className ), this.className = this.className || a === !1 ? "" : L.get( this, "__className__" ) || "")
      } )
    }, hasClass    : function ( a ) {
      for ( var b = " " + a + " ", c = 0, d = this.length; d > c; c++ )if ( 1 === this[ c ].nodeType && (" " + this[ c ].className + " ").replace( ab, " " ).indexOf( b ) >= 0 )return !0;
      return !1
    }
  } );
  var bb = /\r/g;
  n.fn.extend( {
    val : function ( a ) {
      var b, c, d, e = this[ 0 ];
      {
        if ( arguments.length )return d = n.isFunction( a ), this.each( function ( c ) {
          var e;
          1 === this.nodeType && (e = d ? a.call( this, c, n( this ).val() ) : a, null == e ? e = "" : "number" == typeof e ? e += "" : n.isArray( e ) && (e = n.map( e, function ( a ) {return null == a ? "" : a + ""} )), b = n.valHooks[ this.type ] || n.valHooks[ this.nodeName.toLowerCase() ], b && "set"in b && void 0 !== b.set( this, e, "value" ) || (this.value = e))
        } );
        if ( e )return b = n.valHooks[ e.type ] || n.valHooks[ e.nodeName.toLowerCase() ], b && "get"in b && void 0 !== (c = b.get( e, "value" )) ? c : (c = e.value, "string" == typeof c ? c.replace( bb, "" ) : null == c ? "" : c)
      }
    }
  } ), n.extend( {
    valHooks : {
      option    : {
        get : function ( a ) {
          var b = n.find.attr( a, "value" );
          return null != b ? b : n.trim( n.text( a ) )
        }
      }, select : {
        get    : function ( a ) {
          for ( var b, c, d = a.options, e = a.selectedIndex, f = "select-one" === a.type || 0 > e, g = f ? null : [], h = f ? e + 1 : d.length, i = 0 > e ? h : f ? e : 0; h > i; i++ )if ( c = d[ i ], !(!c.selected && i !== e || (k.optDisabled ? c.disabled : null !== c.getAttribute( "disabled" )) || c.parentNode.disabled && n.nodeName( c.parentNode, "optgroup" )) ) {
            if ( b = n( c ).val(), f )return b;
            g.push( b )
          }
          return g
        }, set : function ( a, b ) {
          var c, d, e = a.options, f = n.makeArray( b ), g = e.length;
          while ( g-- )d = e[ g ], (d.selected = n.inArray( d.value, f ) >= 0) && (c = !0);
          return c || (a.selectedIndex = -1), f
        }
      }
    }
  } ), n.each( [ "radio", "checkbox" ], function () {n.valHooks[ this ] = { set : function ( a, b ) {return n.isArray( b ) ? a.checked = n.inArray( n( a ).val(), b ) >= 0 : void 0} }, k.checkOn || (n.valHooks[ this ].get = function ( a ) {return null === a.getAttribute( "value" ) ? "on" : a.value})} ), n.each( "blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split( " " ), function ( a, b ) {n.fn[ b ] = function ( a, c ) {return arguments.length > 0 ? this.on( b, null, a, c ) : this.trigger( b )}} ), n.fn.extend( { hover : function ( a, b ) {return this.mouseenter( a ).mouseleave( b || a )}, bind : function ( a, b, c ) {return this.on( a, null, b, c )}, unbind : function ( a, b ) {return this.off( a, null, b )}, delegate : function ( a, b, c, d ) {return this.on( b, a, c, d )}, undelegate : function ( a, b, c ) {return 1 === arguments.length ? this.off( a, "**" ) : this.off( b, a || "**", c )} } );
  var cb = n.now(), db = /\?/;
  n.parseJSON = function ( a ) {return JSON.parse( a + "" )}, n.parseXML = function ( a ) {
    var b, c;
    if ( !a || "string" != typeof a )return null;
    try {
      c = new DOMParser, b = c.parseFromString( a, "text/xml" )
    } catch ( d ) {
      b = void 0
    }
    return (!b || b.getElementsByTagName( "parsererror" ).length) && n.error( "Invalid XML: " + a ), b
  };
  var eb = /#.*$/, fb = /([?&])_=[^&]*/, gb = /^(.*?):[ \t]*([^\r\n]*)$/gm, hb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, ib = /^(?:GET|HEAD)$/, jb = /^\/\//, kb = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, lb = {}, mb = {}, nb = "*/".concat( "*" ), ob = a.location.href, pb = kb.exec( ob.toLowerCase() ) || [];

  function qb ( a ) {
    return function ( b, c ) {
      "string" != typeof b && (c = b, b = "*");
      var d, e = 0, f = b.toLowerCase().match( E ) || [];
      if ( n.isFunction( c ) )while ( d = f[ e++ ] )"+" === d[ 0 ] ? (d = d.slice( 1 ) || "*", (a[ d ] = a[ d ] || []).unshift( c )) : (a[ d ] = a[ d ] || []).push( c )
    }
  }

  function rb ( a, b, c, d ) {
    var e = {}, f = a === mb;

    function g ( h ) {
      var i;
      return e[ h ] = !0, n.each( a[ h ] || [], function ( a, h ) {
        var j = h( b, c, d );
        return "string" != typeof j || f || e[ j ] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift( j ), g( j ), !1)
      } ), i
    }

    return g( b.dataTypes[ 0 ] ) || !e[ "*" ] && g( "*" )
  }

  function sb ( a, b ) {
    var c, d, e = n.ajaxSettings.flatOptions || {};
    for ( c in b )void 0 !== b[ c ] && ((e[ c ] ? a : d || (d = {}))[ c ] = b[ c ]);
    return d && n.extend( !0, a, d ), a
  }

  function tb ( a, b, c ) {
    var d, e, f, g, h = a.contents, i = a.dataTypes;
    while ( "*" === i[ 0 ] )i.shift(), void 0 === d && (d = a.mimeType || b.getResponseHeader( "Content-Type" ));
    if ( d )for ( e in h )if ( h[ e ] && h[ e ].test( d ) ) {
      i.unshift( e );
      break
    }
    if ( i[ 0 ]in c )f = i[ 0 ]; else {
      for ( e in c ) {
        if ( !i[ 0 ] || a.converters[ e + " " + i[ 0 ] ] ) {
          f = e;
          break
        }
        g || (g = e)
      }
      f = f || g
    }
    return f ? (f !== i[ 0 ] && i.unshift( f ), c[ f ]) : void 0
  }

  function ub ( a, b, c, d ) {
    var e, f, g, h, i, j = {}, k = a.dataTypes.slice();
    if ( k[ 1 ] )for ( g in a.converters )j[ g.toLowerCase() ] = a.converters[ g ];
    f = k.shift();
    while ( f )if ( a.responseFields[ f ] && (c[ a.responseFields[ f ] ] = b), !i && d && a.dataFilter && (b = a.dataFilter( b, a.dataType )), i = f, f = k.shift() )if ( "*" === f )f = i; else if ( "*" !== i && i !== f ) {
      if ( g = j[ i + " " + f ] || j[ "* " + f ], !g )for ( e in j )if ( h = e.split( " " ), h[ 1 ] === f && (g = j[ i + " " + h[ 0 ] ] || j[ "* " + h[ 0 ] ]) ) {
        g === !0 ? g = j[ e ] : j[ e ] !== !0 && (f = h[ 0 ], k.unshift( h[ 1 ] ));
        break
      }
      if ( g !== !0 )if ( g && a[ "throws" ] )b = g( b ); else try {
        b = g( b )
      } catch ( l ) {
        return { state : "parsererror", error : g ? l : "No conversion from " + i + " to " + f }
      }
    }
    return { state : "success", data : b }
  }

  n.extend( {
    active     : 0, lastModified : {}, etag : {}, ajaxSettings : { url : ob, type : "GET", isLocal : hb.test( pb[ 1 ] ), global : !0, processData : !0, async : !0, contentType : "application/x-www-form-urlencoded; charset=UTF-8", accepts : { "*" : nb, text : "text/plain", html : "text/html", xml : "application/xml, text/xml", json : "application/json, text/javascript" }, contents : { xml : /xml/, html : /html/, json : /json/ }, responseFields : { xml : "responseXML", text : "responseText", json : "responseJSON" }, converters : { "* text" : String, "text html" : !0, "text json" : n.parseJSON, "text xml" : n.parseXML }, flatOptions : { url : !0, context : !0 } }, ajaxSetup : function ( a, b ) {return b ? sb( sb( a, n.ajaxSettings ), b ) : sb( n.ajaxSettings, a )}, ajaxPrefilter : qb( lb ), ajaxTransport : qb( mb ), ajax : function ( a, b ) {
      "object" == typeof a && (b = a, a = void 0), b = b || {};
      var c, d, e, f, g, h, i, j, k = n.ajaxSetup( {}, b ), l = k.context || k, m = k.context && (l.nodeType || l.jquery) ? n( l ) : n.event, o = n.Deferred(), p = n.Callbacks( "once memory" ), q = k.statusCode || {}, r = {}, s = {}, t = 0, u = "canceled", v = {
        readyState               : 0, getResponseHeader : function ( a ) {
          var b;
          if ( 2 === t ) {
            if ( !f ) {
              f = {};
              while ( b = gb.exec( e ) )f[ b[ 1 ].toLowerCase() ] = b[ 2 ]
            }
            b = f[ a.toLowerCase() ]
          }
          return null == b ? null : b
        }, getAllResponseHeaders : function () {return 2 === t ? e : null}, setRequestHeader : function ( a, b ) {
          var c = a.toLowerCase();
          return t || (a = s[ c ] = s[ c ] || a, r[ a ] = b), this
        }, overrideMimeType      : function ( a ) {return t || (k.mimeType = a), this}, statusCode : function ( a ) {
          var b;
          if ( a )if ( 2 > t )for ( b in a )q[ b ] = [ q[ b ], a[ b ] ]; else v.always( a[ v.status ] );
          return this
        }, abort                 : function ( a ) {
          var b = a || u;
          return c && c.abort( b ), x( 0, b ), this
        }
      };
      if ( o.promise( v ).complete = p.add, v.success = v.done, v.error = v.fail, k.url = ((a || k.url || ob) + "").replace( eb, "" ).replace( jb, pb[ 1 ] + "//" ), k.type = b.method || b.type || k.method || k.type, k.dataTypes = n.trim( k.dataType || "*" ).toLowerCase().match( E ) || [ "" ], null == k.crossDomain && (h = kb.exec( k.url.toLowerCase() ), k.crossDomain = !(!h || h[ 1 ] === pb[ 1 ] && h[ 2 ] === pb[ 2 ] && (h[ 3 ] || ("http:" === h[ 1 ] ? "80" : "443")) === (pb[ 3 ] || ("http:" === pb[ 1 ] ? "80" : "443")))), k.data && k.processData && "string" != typeof k.data && (k.data = n.param( k.data, k.traditional )), rb( lb, k, b, v ), 2 === t )return v;
      i = n.event && k.global, i && 0 === n.active++ && n.event.trigger( "ajaxStart" ), k.type = k.type.toUpperCase(), k.hasContent = !ib.test( k.type ), d = k.url, k.hasContent || (k.data && (d = k.url += (db.test( d ) ? "&" : "?") + k.data, delete k.data), k.cache === !1 && (k.url = fb.test( d ) ? d.replace( fb, "$1_=" + cb++ ) : d + (db.test( d ) ? "&" : "?") + "_=" + cb++)), k.ifModified && (n.lastModified[ d ] && v.setRequestHeader( "If-Modified-Since", n.lastModified[ d ] ), n.etag[ d ] && v.setRequestHeader( "If-None-Match", n.etag[ d ] )), (k.data && k.hasContent && k.contentType !== !1 || b.contentType) && v.setRequestHeader( "Content-Type", k.contentType ), v.setRequestHeader( "Accept", k.dataTypes[ 0 ] && k.accepts[ k.dataTypes[ 0 ] ] ? k.accepts[ k.dataTypes[ 0 ] ] + ("*" !== k.dataTypes[ 0 ] ? ", " + nb + "; q=0.01" : "") : k.accepts[ "*" ] );
      for ( j in k.headers )v.setRequestHeader( j, k.headers[ j ] );
      if ( k.beforeSend && (k.beforeSend.call( l, v, k ) === !1 || 2 === t) )return v.abort();
      u = "abort";
      for ( j in{ success : 1, error : 1, complete : 1 } )v[ j ]( k[ j ] );
      if ( c = rb( mb, k, b, v ) ) {
        v.readyState = 1, i && m.trigger( "ajaxSend", [ v, k ] ), k.async && k.timeout > 0 && (g = setTimeout( function () {v.abort( "timeout" )}, k.timeout ));
        try {
          t = 1, c.send( r, x )
        } catch ( w ) {
          if ( !(2 > t) )throw w;
          x( -1, w )
        }
      } else x( -1, "No Transport" );
      function x ( a, b, f, h ) {
        var j, r, s, u, w, x = b;
        2 !== t && (t = 2, g && clearTimeout( g ), c = void 0, e = h || "", v.readyState = a > 0 ? 4 : 0, j = a >= 200 && 300 > a || 304 === a, f && (u = tb( k, v, f )), u = ub( k, u, v, j ), j ? (k.ifModified && (w = v.getResponseHeader( "Last-Modified" ), w && (n.lastModified[ d ] = w), w = v.getResponseHeader( "etag" ), w && (n.etag[ d ] = w)), 204 === a || "HEAD" === k.type ? x = "nocontent" : 304 === a ? x = "notmodified" : (x = u.state, r = u.data, s = u.error, j = !s)) : (s = x, (a || !x) && (x = "error", 0 > a && (a = 0))), v.status = a, v.statusText = (b || x) + "", j ? o.resolveWith( l, [ r, x, v ] ) : o.rejectWith( l, [ v, x, s ] ), v.statusCode( q ), q = void 0, i && m.trigger( j ? "ajaxSuccess" : "ajaxError", [ v, k, j ? r : s ] ), p.fireWith( l, [ v, x ] ), i && (m.trigger( "ajaxComplete", [ v, k ] ), --n.active || n.event.trigger( "ajaxStop" )))
      }

      return v
    }, getJSON : function ( a, b, c ) {return n.get( a, b, c, "json" )}, getScript : function ( a, b ) {return n.get( a, void 0, b, "script" )}
  } ), n.each( [ "get", "post" ], function ( a, b ) {n[ b ] = function ( a, c, d, e ) {return n.isFunction( c ) && (e = e || d, d = c, c = void 0), n.ajax( { url : a, type : b, dataType : e, data : c, success : d } )}} ), n._evalUrl = function ( a ) {return n.ajax( { url : a, type : "GET", dataType : "script", async : !1, global : !1, "throws" : !0 } )}, n.fn.extend( {
    wrapAll      : function ( a ) {
      var b;
      return n.isFunction( a ) ? this.each( function ( b ) {n( this ).wrapAll( a.call( this, b ) )} ) : (this[ 0 ] && (b = n( a, this[ 0 ].ownerDocument ).eq( 0 ).clone( !0 ), this[ 0 ].parentNode && b.insertBefore( this[ 0 ] ), b.map( function () {
        var a = this;
        while ( a.firstElementChild )a = a.firstElementChild;
        return a
      } ).append( this )), this)
    }, wrapInner : function ( a ) {
      return this.each( n.isFunction( a ) ? function ( b ) {n( this ).wrapInner( a.call( this, b ) )} : function () {
        var b = n( this ), c = b.contents();
        c.length ? c.wrapAll( a ) : b.append( a )
      } )
    }, wrap      : function ( a ) {
      var b = n.isFunction( a );
      return this.each( function ( c ) {n( this ).wrapAll( b ? a.call( this, c ) : a )} )
    }, unwrap    : function () {return this.parent().each( function () {n.nodeName( this, "body" ) || n( this ).replaceWith( this.childNodes )} ).end()}
  } ), n.expr.filters.hidden = function ( a ) {return a.offsetWidth <= 0 && a.offsetHeight <= 0}, n.expr.filters.visible = function ( a ) {return !n.expr.filters.hidden( a )};
  var vb = /%20/g, wb = /\[\]$/, xb = /\r?\n/g, yb = /^(?:submit|button|image|reset|file)$/i, zb = /^(?:input|select|textarea|keygen)/i;

  function Ab ( a, b, c, d ) {
    var e;
    if ( n.isArray( b ) )n.each( b, function ( b, e ) {c || wb.test( a ) ? d( a, e ) : Ab( a + "[" + ("object" == typeof e ? b : "") + "]", e, c, d )} ); else if ( c || "object" !== n.type( b ) )d( a, b ); else for ( e in b )Ab( a + "[" + e + "]", b[ e ], c, d )
  }

  n.param = function ( a, b ) {
    var c, d = [], e = function ( a, b ) {b = n.isFunction( b ) ? b() : null == b ? "" : b, d[ d.length ] = encodeURIComponent( a ) + "=" + encodeURIComponent( b )};
    if ( void 0 === b && (b = n.ajaxSettings && n.ajaxSettings.traditional), n.isArray( a ) || a.jquery && !n.isPlainObject( a ) )n.each( a, function () {e( this.name, this.value )} ); else for ( c in a )Ab( c, a[ c ], b, e );
    return d.join( "&" ).replace( vb, "+" )
  }, n.fn.extend( {
    serialize : function () {return n.param( this.serializeArray() )}, serializeArray : function () {
      return this.map( function () {
        var a = n.prop( this, "elements" );
        return a ? n.makeArray( a ) : this
      } ).filter( function () {
        var a = this.type;
        return this.name && !n( this ).is( ":disabled" ) && zb.test( this.nodeName ) && !yb.test( a ) && (this.checked || !T.test( a ))
      } ).map( function ( a, b ) {
        var c = n( this ).val();
        return null == c ? null : n.isArray( c ) ? n.map( c, function ( a ) {return { name : b.name, value : a.replace( xb, "\r\n" ) }} ) : { name : b.name, value : c.replace( xb, "\r\n" ) }
      } ).get()
    }
  } ), n.ajaxSettings.xhr = function () {
    try {
      return new XMLHttpRequest
    } catch ( a ) {
    }
  };
  var Bb = 0, Cb = {}, Db = { 0 : 200, 1223 : 204 }, Eb = n.ajaxSettings.xhr();
  a.attachEvent && a.attachEvent( "onunload", function () {for ( var a in Cb )Cb[ a ]()} ), k.cors = !!Eb && "withCredentials"in Eb, k.ajax = Eb = !!Eb, n.ajaxTransport( function ( a ) {
    var b;
    return k.cors || Eb && !a.crossDomain ? {
      send     : function ( c, d ) {
        var e, f = a.xhr(), g = ++Bb;
        if ( f.open( a.type, a.url, a.async, a.username, a.password ), a.xhrFields )for ( e in a.xhrFields )f[ e ] = a.xhrFields[ e ];
        a.mimeType && f.overrideMimeType && f.overrideMimeType( a.mimeType ), a.crossDomain || c[ "X-Requested-With" ] || (c[ "X-Requested-With" ] = "XMLHttpRequest");
        for ( e in c )f.setRequestHeader( e, c[ e ] );
        b = function ( a ) {return function () {b && (delete Cb[ g ], b = f.onload = f.onerror = null, "abort" === a ? f.abort() : "error" === a ? d( f.status, f.statusText ) : d( Db[ f.status ] || f.status, f.statusText, "string" == typeof f.responseText ? { text : f.responseText } : void 0, f.getAllResponseHeaders() ))}}, f.onload = b(), f.onerror = b( "error" ), b = Cb[ g ] = b( "abort" );
        try {
          f.send( a.hasContent && a.data || null )
        } catch ( h ) {
          if ( b )throw h
        }
      }, abort : function () {b && b()}
    } : void 0
  } ), n.ajaxSetup( { accepts : { script : "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" }, contents : { script : /(?:java|ecma)script/ }, converters : { "text script" : function ( a ) {return n.globalEval( a ), a} } } ), n.ajaxPrefilter( "script", function ( a ) {void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET")} ), n.ajaxTransport( "script", function ( a ) {
    if ( a.crossDomain ) {
      var b, c;
      return { send : function ( d, e ) {b = n( "<script>" ).prop( { async : !0, charset : a.scriptCharset, src : a.url } ).on( "load error", c = function ( a ) {b.remove(), c = null, a && e( "error" === a.type ? 404 : 200, a.type )} ), l.head.appendChild( b[ 0 ] )}, abort : function () {c && c()} }
    }
  } );
  var Fb = [], Gb = /(=)\?(?=&|$)|\?\?/;
  n.ajaxSetup( {
    jsonp : "callback", jsonpCallback : function () {
      var a = Fb.pop() || n.expando + "_" + cb++;
      return this[ a ] = !0, a
    }
  } ), n.ajaxPrefilter( "json jsonp", function ( b, c, d ) {
    var e, f, g, h = b.jsonp !== !1 && (Gb.test( b.url ) ? "url" : "string" == typeof b.data && !(b.contentType || "").indexOf( "application/x-www-form-urlencoded" ) && Gb.test( b.data ) && "data");
    return h || "jsonp" === b.dataTypes[ 0 ] ? (e = b.jsonpCallback = n.isFunction( b.jsonpCallback ) ? b.jsonpCallback() : b.jsonpCallback, h ? b[ h ] = b[ h ].replace( Gb, "$1" + e ) : b.jsonp !== !1 && (b.url += (db.test( b.url ) ? "&" : "?") + b.jsonp + "=" + e), b.converters[ "script json" ] = function () {return g || n.error( e + " was not called" ), g[ 0 ]}, b.dataTypes[ 0 ] = "json", f = a[ e ], a[ e ] = function () {g = arguments}, d.always( function () {a[ e ] = f, b[ e ] && (b.jsonpCallback = c.jsonpCallback, Fb.push( e )), g && n.isFunction( f ) && f( g[ 0 ] ), g = f = void 0} ), "script") : void 0
  } ), n.parseHTML = function ( a, b, c ) {
    if ( !a || "string" != typeof a )return null;
    "boolean" == typeof b && (c = b, b = !1), b = b || l;
    var d = v.exec( a ), e = !c && [];
    return d ? [ b.createElement( d[ 1 ] ) ] : (d = n.buildFragment( [ a ], b, e ), e && e.length && n( e ).remove(), n.merge( [], d.childNodes ))
  };
  var Hb = n.fn.load;
  n.fn.load = function ( a, b, c ) {
    if ( "string" != typeof a && Hb )return Hb.apply( this, arguments );
    var d, e, f, g = this, h = a.indexOf( " " );
    return h >= 0 && (d = n.trim( a.slice( h ) ), a = a.slice( 0, h )), n.isFunction( b ) ? (c = b, b = void 0) : b && "object" == typeof b && (e = "POST"), g.length > 0 && n.ajax( { url : a, type : e, dataType : "html", data : b } ).done( function ( a ) {f = arguments, g.html( d ? n( "<div>" ).append( n.parseHTML( a ) ).find( d ) : a )} ).complete( c && function ( a, b ) {g.each( c, f || [ a.responseText, b, a ] )} ), this
  }, n.each( [ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function ( a, b ) {n.fn[ b ] = function ( a ) {return this.on( b, a )}} ), n.expr.filters.animated = function ( a ) {return n.grep( n.timers, function ( b ) {return a === b.elem} ).length};
  var Ib = a.document.documentElement;

  function Jb ( a ) {return n.isWindow( a ) ? a : 9 === a.nodeType && a.defaultView}

  n.offset = {
    setOffset : function ( a, b, c ) {
      var d, e, f, g, h, i, j, k = n.css( a, "position" ), l = n( a ), m = {};
      "static" === k && (a.style.position = "relative"), h = l.offset(), f = n.css( a, "top" ), i = n.css( a, "left" ), j = ("absolute" === k || "fixed" === k) && (f + i).indexOf( "auto" ) > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat( f ) || 0, e = parseFloat( i ) || 0), n.isFunction( b ) && (b = b.call( a, c, h )), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using"in b ? b.using.call( a, m ) : l.css( m )
    }
  }, n.fn.extend( {
    offset          : function ( a ) {
      if ( arguments.length )return void 0 === a ? this : this.each( function ( b ) {n.offset.setOffset( this, a, b )} );
      var b, c, d = this[ 0 ], e = { top : 0, left : 0 }, f = d && d.ownerDocument;
      if ( f )return b = f.documentElement, n.contains( b, d ) ? (typeof d.getBoundingClientRect !== U && (e = d.getBoundingClientRect()), c = Jb( f ), { top : e.top + c.pageYOffset - b.clientTop, left : e.left + c.pageXOffset - b.clientLeft }) : e
    }, position     : function () {
      if ( this[ 0 ] ) {
        var a, b, c = this[ 0 ], d = { top : 0, left : 0 };
        return "fixed" === n.css( c, "position" ) ? b = c.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), n.nodeName( a[ 0 ], "html" ) || (d = a.offset()), d.top += n.css( a[ 0 ], "borderTopWidth", !0 ), d.left += n.css( a[ 0 ], "borderLeftWidth", !0 )), { top : b.top - d.top - n.css( c, "marginTop", !0 ), left : b.left - d.left - n.css( c, "marginLeft", !0 ) }
      }
    }, offsetParent : function () {
      return this.map( function () {
        var a = this.offsetParent || Ib;
        while ( a && !n.nodeName( a, "html" ) && "static" === n.css( a, "position" ) )a = a.offsetParent;
        return a || Ib
      } )
    }
  } ), n.each( { scrollLeft : "pageXOffset", scrollTop : "pageYOffset" }, function ( b, c ) {
    var d = "pageYOffset" === c;
    n.fn[ b ] = function ( e ) {
      return J( this, function ( b, e, f ) {
        var g = Jb( b );
        return void 0 === f ? g ? g[ c ] : b[ e ] : void(g ? g.scrollTo( d ? a.pageXOffset : f, d ? f : a.pageYOffset ) : b[ e ] = f)
      }, b, e, arguments.length, null )
    }
  } ), n.each( [ "top", "left" ], function ( a, b ) {n.cssHooks[ b ] = ya( k.pixelPosition, function ( a, c ) {return c ? (c = xa( a, b ), va.test( c ) ? n( a ).position()[ b ] + "px" : c) : void 0} )} ), n.each( { Height : "height", Width : "width" }, function ( a, b ) {
    n.each( { padding : "inner" + a, content : b, "" : "outer" + a }, function ( c, d ) {
      n.fn[ d ] = function ( d, e ) {
        var f = arguments.length && (c || "boolean" != typeof d), g = c || (d === !0 || e === !0 ? "margin" : "border");
        return J( this, function ( b, c, d ) {
          var e;
          return n.isWindow( b ) ? b.document.documentElement[ "client" + a ] : 9 === b.nodeType ? (e = b.documentElement, Math.max( b.body[ "scroll" + a ], e[ "scroll" + a ], b.body[ "offset" + a ], e[ "offset" + a ], e[ "client" + a ] )) : void 0 === d ? n.css( b, c, g ) : n.style( b, c, d, g )
        }, b, f ? d : void 0, f, null )
      }
    } )
  } ), n.fn.size = function () {return this.length}, n.fn.andSelf = n.fn.addBack, "function" == typeof define && define.amd && define( "jquery", [], function () {return n} );
  var Kb = a.jQuery, Lb = a.$;
  return n.noConflict = function ( b ) {return a.$ === n && (a.$ = Lb), b && a.jQuery === n && (a.jQuery = Kb), n}, typeof b === U && (a.jQuery = a.$ = n), n
} );

/*!
 * Bootstrap v3.3.5 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under the MIT license
 */
if ( "undefined" == typeof jQuery )throw new Error( "Bootstrap's JavaScript requires jQuery" );
+function ( a ) {
  "use strict";
  var b = a.fn.jquery.split( " " )[ 0 ].split( "." );
  if ( b[ 0 ] < 2 && b[ 1 ] < 9 || 1 == b[ 0 ] && 9 == b[ 1 ] && b[ 2 ] < 1 )throw new Error( "Bootstrap's JavaScript requires jQuery version 1.9.1 or higher" )
}( jQuery ), +function ( a ) {
  "use strict";
  function b () {
    var a = document.createElement( "bootstrap" ), b = { WebkitTransition : "webkitTransitionEnd", MozTransition : "transitionend", OTransition : "oTransitionEnd otransitionend", transition : "transitionend" };
    for ( var c in b )if ( void 0 !== a.style[ c ] )return { end : b[ c ] };
    return !1
  }

  a.fn.emulateTransitionEnd = function ( b ) {
    var c = !1, d = this;
    a( this ).one( "bsTransitionEnd", function () {c = !0} );
    var e = function () {c || a( d ).trigger( a.support.transition.end )};
    return setTimeout( e, b ), this
  }, a( function () {a.support.transition = b(), a.support.transition && (a.event.special.bsTransitionEnd = { bindType : a.support.transition.end, delegateType : a.support.transition.end, handle : function ( b ) {return a( b.target ).is( this ) ? b.handleObj.handler.apply( this, arguments ) : void 0} })} )
}( jQuery ), +function ( a ) {
  "use strict";
  function b ( b ) {
    return this.each( function () {
      var c = a( this ), e = c.data( "bs.alert" );
      e || c.data( "bs.alert", e = new d( this ) ), "string" == typeof b && e[ b ].call( c )
    } )
  }

  var c = '[data-dismiss="alert"]', d = function ( b ) {a( b ).on( "click", c, this.close )};
  d.VERSION = "3.3.5", d.TRANSITION_DURATION = 150, d.prototype.close = function ( b ) {
    function c () {g.detach().trigger( "closed.bs.alert" ).remove()}

    var e = a( this ), f = e.attr( "data-target" );
    f || (f = e.attr( "href" ), f = f && f.replace( /.*(?=#[^\s]*$)/, "" ));
    var g = a( f );
    b && b.preventDefault(), g.length || (g = e.closest( ".alert" )), g.trigger( b = a.Event( "close.bs.alert" ) ), b.isDefaultPrevented() || (g.removeClass( "in" ), a.support.transition && g.hasClass( "fade" ) ? g.one( "bsTransitionEnd", c ).emulateTransitionEnd( d.TRANSITION_DURATION ) : c())
  };
  var e = a.fn.alert;
  a.fn.alert = b, a.fn.alert.Constructor = d, a.fn.alert.noConflict = function () {return a.fn.alert = e, this}, a( document ).on( "click.bs.alert.data-api", c, d.prototype.close )
}( jQuery ), +function ( a ) {
  "use strict";
  function b ( b ) {
    return this.each( function () {
      var d = a( this ), e = d.data( "bs.button" ), f = "object" == typeof b && b;
      e || d.data( "bs.button", e = new c( this, f ) ), "toggle" == b ? e.toggle() : b && e.setState( b )
    } )
  }

  var c = function ( b, d ) {this.$element = a( b ), this.options = a.extend( {}, c.DEFAULTS, d ), this.isLoading = !1};
  c.VERSION = "3.3.5", c.DEFAULTS = { loadingText : "loading..." }, c.prototype.setState = function ( b ) {
    var c = "disabled", d = this.$element, e = d.is( "input" ) ? "val" : "html", f = d.data();
    b += "Text", null == f.resetText && d.data( "resetText", d[ e ]() ), setTimeout( a.proxy( function () {d[ e ]( null == f[ b ] ? this.options[ b ] : f[ b ] ), "loadingText" == b ? (this.isLoading = !0, d.addClass( c ).attr( c, c )) : this.isLoading && (this.isLoading = !1, d.removeClass( c ).removeAttr( c ))}, this ), 0 )
  }, c.prototype.toggle = function () {
    var a = !0, b = this.$element.closest( '[data-toggle="buttons"]' );
    if ( b.length ) {
      var c = this.$element.find( "input" );
      "radio" == c.prop( "type" ) ? (c.prop( "checked" ) && (a = !1), b.find( ".active" ).removeClass( "active" ), this.$element.addClass( "active" )) : "checkbox" == c.prop( "type" ) && (c.prop( "checked" ) !== this.$element.hasClass( "active" ) && (a = !1), this.$element.toggleClass( "active" )), c.prop( "checked", this.$element.hasClass( "active" ) ), a && c.trigger( "change" )
    } else this.$element.attr( "aria-pressed", !this.$element.hasClass( "active" ) ), this.$element.toggleClass( "active" )
  };
  var d = a.fn.button;
  a.fn.button = b, a.fn.button.Constructor = c, a.fn.button.noConflict = function () {return a.fn.button = d, this}, a( document ).on( "click.bs.button.data-api", '[data-toggle^="button"]', function ( c ) {
    var d = a( c.target );
    d.hasClass( "btn" ) || (d = d.closest( ".btn" )), b.call( d, "toggle" ), a( c.target ).is( 'input[type="radio"]' ) || a( c.target ).is( 'input[type="checkbox"]' ) || c.preventDefault()
  } ).on( "focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function ( b ) {a( b.target ).closest( ".btn" ).toggleClass( "focus", /^focus(in)?$/.test( b.type ) )} )
}( jQuery ), +function ( a ) {
  "use strict";
  function b ( b ) {
    return this.each( function () {
      var d = a( this ), e = d.data( "bs.carousel" ), f = a.extend( {}, c.DEFAULTS, d.data(), "object" == typeof b && b ), g = "string" == typeof b ? b : f.slide;
      e || d.data( "bs.carousel", e = new c( this, f ) ), "number" == typeof b ? e.to( b ) : g ? e[ g ]() : f.interval && e.pause().cycle()
    } )
  }

  var c = function ( b, c ) {this.$element = a( b ), this.$indicators = this.$element.find( ".carousel-indicators" ), this.options = c, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on( "keydown.bs.carousel", a.proxy( this.keydown, this ) ), "hover" == this.options.pause && !("ontouchstart"in document.documentElement) && this.$element.on( "mouseenter.bs.carousel", a.proxy( this.pause, this ) ).on( "mouseleave.bs.carousel", a.proxy( this.cycle, this ) )};
  c.VERSION = "3.3.5", c.TRANSITION_DURATION = 600, c.DEFAULTS = { interval : 5e3, pause : "hover", wrap : !0, keyboard : !0 }, c.prototype.keydown = function ( a ) {
    if ( !/input|textarea/i.test( a.target.tagName ) ) {
      switch ( a.which ) {
        case 37:
          this.prev();
          break;
        case 39:
          this.next();
          break;
        default:
          return
      }
      a.preventDefault()
    }
  }, c.prototype.cycle = function ( b ) {return b || (this.paused = !1), this.interval && clearInterval( this.interval ), this.options.interval && !this.paused && (this.interval = setInterval( a.proxy( this.next, this ), this.options.interval )), this}, c.prototype.getItemIndex = function ( a ) {return this.$items = a.parent().children( ".item" ), this.$items.index( a || this.$active )}, c.prototype.getItemForDirection = function ( a, b ) {
    var c = this.getItemIndex( b ), d = "prev" == a && 0 === c || "next" == a && c == this.$items.length - 1;
    if ( d && !this.options.wrap )return b;
    var e = "prev" == a ? -1 : 1, f = (c + e) % this.$items.length;
    return this.$items.eq( f )
  }, c.prototype.to = function ( a ) {
    var b = this, c = this.getItemIndex( this.$active = this.$element.find( ".item.active" ) );
    return a > this.$items.length - 1 || 0 > a ? void 0 : this.sliding ? this.$element.one( "slid.bs.carousel", function () {b.to( a )} ) : c == a ? this.pause().cycle() : this.slide( a > c ? "next" : "prev", this.$items.eq( a ) )
  }, c.prototype.pause = function ( b ) {return b || (this.paused = !0), this.$element.find( ".next, .prev" ).length && a.support.transition && (this.$element.trigger( a.support.transition.end ), this.cycle( !0 )), this.interval = clearInterval( this.interval ), this}, c.prototype.next = function () {return this.sliding ? void 0 : this.slide( "next" )}, c.prototype.prev = function () {return this.sliding ? void 0 : this.slide( "prev" )}, c.prototype.slide = function ( b, d ) {
    var e = this.$element.find( ".item.active" ), f = d || this.getItemForDirection( b, e ), g = this.interval, h = "next" == b ? "left" : "right", i = this;
    if ( f.hasClass( "active" ) )return this.sliding = !1;
    var j = f[ 0 ], k = a.Event( "slide.bs.carousel", { relatedTarget : j, direction : h } );
    if ( this.$element.trigger( k ), !k.isDefaultPrevented() ) {
      if ( this.sliding = !0, g && this.pause(), this.$indicators.length ) {
        this.$indicators.find( ".active" ).removeClass( "active" );
        var l = a( this.$indicators.children()[ this.getItemIndex( f ) ] );
        l && l.addClass( "active" )
      }
      var m = a.Event( "slid.bs.carousel", { relatedTarget : j, direction : h } );
      return a.support.transition && this.$element.hasClass( "slide" ) ? (f.addClass( b ), f[ 0 ].offsetWidth, e.addClass( h ), f.addClass( h ), e.one( "bsTransitionEnd", function () {f.removeClass( [ b, h ].join( " " ) ).addClass( "active" ), e.removeClass( [ "active", h ].join( " " ) ), i.sliding = !1, setTimeout( function () {i.$element.trigger( m )}, 0 )} ).emulateTransitionEnd( c.TRANSITION_DURATION )) : (e.removeClass( "active" ), f.addClass( "active" ), this.sliding = !1, this.$element.trigger( m )), g && this.cycle(), this
    }
  };
  var d = a.fn.carousel;
  a.fn.carousel = b, a.fn.carousel.Constructor = c, a.fn.carousel.noConflict = function () {return a.fn.carousel = d, this};
  var e = function ( c ) {
    var d, e = a( this ), f = a( e.attr( "data-target" ) || (d = e.attr( "href" )) && d.replace( /.*(?=#[^\s]+$)/, "" ) );
    if ( f.hasClass( "carousel" ) ) {
      var g = a.extend( {}, f.data(), e.data() ), h = e.attr( "data-slide-to" );
      h && (g.interval = !1), b.call( f, g ), h && f.data( "bs.carousel" ).to( h ), c.preventDefault()
    }
  };
  a( document ).on( "click.bs.carousel.data-api", "[data-slide]", e ).on( "click.bs.carousel.data-api", "[data-slide-to]", e ), a( window ).on( "load", function () {
    a( '[data-ride="carousel"]' ).each( function () {
      var c = a( this );
      b.call( c, c.data() )
    } )
  } )
}( jQuery ), +function ( a ) {
  "use strict";
  function b ( b ) {
    var c, d = b.attr( "data-target" ) || (c = b.attr( "href" )) && c.replace( /.*(?=#[^\s]+$)/, "" );
    return a( d )
  }

  function c ( b ) {
    return this.each( function () {
      var c = a( this ), e = c.data( "bs.collapse" ), f = a.extend( {}, d.DEFAULTS, c.data(), "object" == typeof b && b );
      !e && f.toggle && /show|hide/.test( b ) && (f.toggle = !1), e || c.data( "bs.collapse", e = new d( this, f ) ), "string" == typeof b && e[ b ]()
    } )
  }

  var d = function ( b, c ) {this.$element = a( b ), this.options = a.extend( {}, d.DEFAULTS, c ), this.$trigger = a( '[data-toggle="collapse"][href="#' + b.id + '"],[data-toggle="collapse"][data-target="#' + b.id + '"]' ), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass( this.$element, this.$trigger ), this.options.toggle && this.toggle()};
  d.VERSION = "3.3.5", d.TRANSITION_DURATION = 350, d.DEFAULTS = { toggle : !0 }, d.prototype.dimension = function () {
    var a = this.$element.hasClass( "width" );
    return a ? "width" : "height"
  }, d.prototype.show = function () {
    if ( !this.transitioning && !this.$element.hasClass( "in" ) ) {
      var b, e = this.$parent && this.$parent.children( ".panel" ).children( ".in, .collapsing" );
      if ( !(e && e.length && (b = e.data( "bs.collapse" ), b && b.transitioning)) ) {
        var f = a.Event( "show.bs.collapse" );
        if ( this.$element.trigger( f ), !f.isDefaultPrevented() ) {
          e && e.length && (c.call( e, "hide" ), b || e.data( "bs.collapse", null ));
          var g = this.dimension();
          this.$element.removeClass( "collapse" ).addClass( "collapsing" )[ g ]( 0 ).attr( "aria-expanded", !0 ), this.$trigger.removeClass( "collapsed" ).attr( "aria-expanded", !0 ), this.transitioning = 1;
          var h = function () {this.$element.removeClass( "collapsing" ).addClass( "collapse in" )[ g ]( "" ), this.transitioning = 0, this.$element.trigger( "shown.bs.collapse" )};
          if ( !a.support.transition )return h.call( this );
          var i = a.camelCase( [ "scroll", g ].join( "-" ) );
          this.$element.one( "bsTransitionEnd", a.proxy( h, this ) ).emulateTransitionEnd( d.TRANSITION_DURATION )[ g ]( this.$element[ 0 ][ i ] )
        }
      }
    }
  }, d.prototype.hide = function () {
    if ( !this.transitioning && this.$element.hasClass( "in" ) ) {
      var b = a.Event( "hide.bs.collapse" );
      if ( this.$element.trigger( b ), !b.isDefaultPrevented() ) {
        var c = this.dimension();
        this.$element[ c ]( this.$element[ c ]() )[ 0 ].offsetHeight, this.$element.addClass( "collapsing" ).removeClass( "collapse in" ).attr( "aria-expanded", !1 ), this.$trigger.addClass( "collapsed" ).attr( "aria-expanded", !1 ), this.transitioning = 1;
        var e = function () {this.transitioning = 0, this.$element.removeClass( "collapsing" ).addClass( "collapse" ).trigger( "hidden.bs.collapse" )};
        return a.support.transition ? void this.$element[ c ]( 0 ).one( "bsTransitionEnd", a.proxy( e, this ) ).emulateTransitionEnd( d.TRANSITION_DURATION ) : e.call( this )
      }
    }
  }, d.prototype.toggle = function () {this[ this.$element.hasClass( "in" ) ? "hide" : "show" ]()}, d.prototype.getParent = function () {
    return a( this.options.parent ).find( '[data-toggle="collapse"][data-parent="' + this.options.parent + '"]' ).each( a.proxy( function ( c, d ) {
      var e = a( d );
      this.addAriaAndCollapsedClass( b( e ), e )
    }, this ) ).end()
  }, d.prototype.addAriaAndCollapsedClass = function ( a, b ) {
    var c = a.hasClass( "in" );
    a.attr( "aria-expanded", c ), b.toggleClass( "collapsed", !c ).attr( "aria-expanded", c )
  };
  var e = a.fn.collapse;
  a.fn.collapse = c, a.fn.collapse.Constructor = d, a.fn.collapse.noConflict = function () {return a.fn.collapse = e, this}, a( document ).on( "click.bs.collapse.data-api", '[data-toggle="collapse"]', function ( d ) {
    var e = a( this );
    e.attr( "data-target" ) || d.preventDefault();
    var f = b( e ), g = f.data( "bs.collapse" ), h = g ? "toggle" : e.data();
    c.call( f, h )
  } )
}( jQuery ), +function ( a ) {
  "use strict";
  function b ( b ) {
    var c = b.attr( "data-target" );
    c || (c = b.attr( "href" ), c = c && /#[A-Za-z]/.test( c ) && c.replace( /.*(?=#[^\s]*$)/, "" ));
    var d = c && a( c );
    return d && d.length ? d : b.parent()
  }

  function c ( c ) {
    c && 3 === c.which || (a( e ).remove(), a( f ).each( function () {
      var d = a( this ), e = b( d ), f = { relatedTarget : this };
      e.hasClass( "open" ) && (c && "click" == c.type && /input|textarea/i.test( c.target.tagName ) && a.contains( e[ 0 ], c.target ) || (e.trigger( c = a.Event( "hide.bs.dropdown", f ) ), c.isDefaultPrevented() || (d.attr( "aria-expanded", "false" ), e.removeClass( "open" ).trigger( "hidden.bs.dropdown", f ))))
    } ))
  }

  function d ( b ) {
    return this.each( function () {
      var c = a( this ), d = c.data( "bs.dropdown" );
      d || c.data( "bs.dropdown", d = new g( this ) ), "string" == typeof b && d[ b ].call( c )
    } )
  }

  var e = ".dropdown-backdrop", f = '[data-toggle="dropdown"]', g = function ( b ) {a( b ).on( "click.bs.dropdown", this.toggle )};
  g.VERSION = "3.3.5", g.prototype.toggle = function ( d ) {
    var e = a( this );
    if ( !e.is( ".disabled, :disabled" ) ) {
      var f = b( e ), g = f.hasClass( "open" );
      if ( c(), !g ) {
        "ontouchstart"in document.documentElement && !f.closest( ".navbar-nav" ).length && a( document.createElement( "div" ) ).addClass( "dropdown-backdrop" ).insertAfter( a( this ) ).on( "click", c );
        var h = { relatedTarget : this };
        if ( f.trigger( d = a.Event( "show.bs.dropdown", h ) ), d.isDefaultPrevented() )return;
        e.trigger( "focus" ).attr( "aria-expanded", "true" ), f.toggleClass( "open" ).trigger( "shown.bs.dropdown", h )
      }
      return !1
    }
  }, g.prototype.keydown = function ( c ) {
    if ( /(38|40|27|32)/.test( c.which ) && !/input|textarea/i.test( c.target.tagName ) ) {
      var d = a( this );
      if ( c.preventDefault(), c.stopPropagation(), !d.is( ".disabled, :disabled" ) ) {
        var e = b( d ), g = e.hasClass( "open" );
        if ( !g && 27 != c.which || g && 27 == c.which )return 27 == c.which && e.find( f ).trigger( "focus" ), d.trigger( "click" );
        var h = " li:not(.disabled):visible a", i = e.find( ".dropdown-menu" + h );
        if ( i.length ) {
          var j = i.index( c.target );
          38 == c.which && j > 0 && j--, 40 == c.which && j < i.length - 1 && j++, ~j || (j = 0), i.eq( j ).trigger( "focus" )
        }
      }
    }
  };
  var h = a.fn.dropdown;
  a.fn.dropdown = d, a.fn.dropdown.Constructor = g, a.fn.dropdown.noConflict = function () {return a.fn.dropdown = h, this}, a( document ).on( "click.bs.dropdown.data-api", c ).on( "click.bs.dropdown.data-api", ".dropdown form", function ( a ) {a.stopPropagation()} ).on( "click.bs.dropdown.data-api", f, g.prototype.toggle ).on( "keydown.bs.dropdown.data-api", f, g.prototype.keydown ).on( "keydown.bs.dropdown.data-api", ".dropdown-menu", g.prototype.keydown )
}( jQuery ), +function ( a ) {
  "use strict";
  function b ( b, d ) {
    return this.each( function () {
      var e = a( this ), f = e.data( "bs.modal" ), g = a.extend( {}, c.DEFAULTS, e.data(), "object" == typeof b && b );
      f || e.data( "bs.modal", f = new c( this, g ) ), "string" == typeof b ? f[ b ]( d ) : g.show && f.show( d )
    } )
  }

  var c = function ( b, c ) {this.options = c, this.$body = a( document.body ), this.$element = a( b ), this.$dialog = this.$element.find( ".modal-dialog" ), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find( ".modal-content" ).load( this.options.remote, a.proxy( function () {this.$element.trigger( "loaded.bs.modal" )}, this ) )};
  c.VERSION = "3.3.5", c.TRANSITION_DURATION = 300, c.BACKDROP_TRANSITION_DURATION = 150, c.DEFAULTS = { backdrop : !0, keyboard : !0, show : !0 }, c.prototype.toggle = function ( a ) {return this.isShown ? this.hide() : this.show( a )}, c.prototype.show = function ( b ) {
    var d = this, e = a.Event( "show.bs.modal", { relatedTarget : b } );
    this.$element.trigger( e ), this.isShown || e.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass( "modal-open" ), this.escape(), this.resize(), this.$element.on( "click.dismiss.bs.modal", '[data-dismiss="modal"]', a.proxy( this.hide, this ) ), this.$dialog.on( "mousedown.dismiss.bs.modal", function () {d.$element.one( "mouseup.dismiss.bs.modal", function ( b ) {a( b.target ).is( d.$element ) && (d.ignoreBackdropClick = !0)} )} ), this.backdrop( function () {
      var e = a.support.transition && d.$element.hasClass( "fade" );
      d.$element.parent().length || d.$element.appendTo( d.$body ), d.$element.show().scrollTop( 0 ), d.adjustDialog(), e && d.$element[ 0 ].offsetWidth, d.$element.addClass( "in" ), d.enforceFocus();
      var f = a.Event( "shown.bs.modal", { relatedTarget : b } );
      e ? d.$dialog.one( "bsTransitionEnd", function () {d.$element.trigger( "focus" ).trigger( f )} ).emulateTransitionEnd( c.TRANSITION_DURATION ) : d.$element.trigger( "focus" ).trigger( f )
    } ))
  }, c.prototype.hide = function ( b ) {b && b.preventDefault(), b = a.Event( "hide.bs.modal" ), this.$element.trigger( b ), this.isShown && !b.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), a( document ).off( "focusin.bs.modal" ), this.$element.removeClass( "in" ).off( "click.dismiss.bs.modal" ).off( "mouseup.dismiss.bs.modal" ), this.$dialog.off( "mousedown.dismiss.bs.modal" ), a.support.transition && this.$element.hasClass( "fade" ) ? this.$element.one( "bsTransitionEnd", a.proxy( this.hideModal, this ) ).emulateTransitionEnd( c.TRANSITION_DURATION ) : this.hideModal())}, c.prototype.enforceFocus = function () {a( document ).off( "focusin.bs.modal" ).on( "focusin.bs.modal", a.proxy( function ( a ) {this.$element[ 0 ] === a.target || this.$element.has( a.target ).length || this.$element.trigger( "focus" )}, this ) )}, c.prototype.escape = function () {this.isShown && this.options.keyboard ? this.$element.on( "keydown.dismiss.bs.modal", a.proxy( function ( a ) {27 == a.which && this.hide()}, this ) ) : this.isShown || this.$element.off( "keydown.dismiss.bs.modal" )}, c.prototype.resize = function () {this.isShown ? a( window ).on( "resize.bs.modal", a.proxy( this.handleUpdate, this ) ) : a( window ).off( "resize.bs.modal" )}, c.prototype.hideModal = function () {
    var a = this;
    this.$element.hide(), this.backdrop( function () {a.$body.removeClass( "modal-open" ), a.resetAdjustments(), a.resetScrollbar(), a.$element.trigger( "hidden.bs.modal" )} )
  }, c.prototype.removeBackdrop = function () {this.$backdrop && this.$backdrop.remove(), this.$backdrop = null}, c.prototype.backdrop = function ( b ) {
    var d = this, e = this.$element.hasClass( "fade" ) ? "fade" : "";
    if ( this.isShown && this.options.backdrop ) {
      var f = a.support.transition && e;
      if ( this.$backdrop = a( document.createElement( "div" ) ).addClass( "modal-backdrop " + e ).appendTo( this.$body ), this.$element.on( "click.dismiss.bs.modal", a.proxy( function ( a ) {return this.ignoreBackdropClick ? void(this.ignoreBackdropClick = !1) : void(a.target === a.currentTarget && ("static" == this.options.backdrop ? this.$element[ 0 ].focus() : this.hide()))}, this ) ), f && this.$backdrop[ 0 ].offsetWidth, this.$backdrop.addClass( "in" ), !b )return;
      f ? this.$backdrop.one( "bsTransitionEnd", b ).emulateTransitionEnd( c.BACKDROP_TRANSITION_DURATION ) : b()
    } else if ( !this.isShown && this.$backdrop ) {
      this.$backdrop.removeClass( "in" );
      var g = function () {d.removeBackdrop(), b && b()};
      a.support.transition && this.$element.hasClass( "fade" ) ? this.$backdrop.one( "bsTransitionEnd", g ).emulateTransitionEnd( c.BACKDROP_TRANSITION_DURATION ) : g()
    } else b && b()
  }, c.prototype.handleUpdate = function () {this.adjustDialog()}, c.prototype.adjustDialog = function () {
    var a = this.$element[ 0 ].scrollHeight > document.documentElement.clientHeight;
    this.$element.css( { paddingLeft : !this.bodyIsOverflowing && a ? this.scrollbarWidth : "", paddingRight : this.bodyIsOverflowing && !a ? this.scrollbarWidth : "" } )
  }, c.prototype.resetAdjustments = function () {this.$element.css( { paddingLeft : "", paddingRight : "" } )}, c.prototype.checkScrollbar = function () {
    var a = window.innerWidth;
    if ( !a ) {
      var b = document.documentElement.getBoundingClientRect();
      a = b.right - Math.abs( b.left )
    }
    this.bodyIsOverflowing = document.body.clientWidth < a, this.scrollbarWidth = this.measureScrollbar()
  }, c.prototype.setScrollbar = function () {
    var a = parseInt( this.$body.css( "padding-right" ) || 0, 10 );
    this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css( "padding-right", a + this.scrollbarWidth )
  }, c.prototype.resetScrollbar = function () {this.$body.css( "padding-right", this.originalBodyPad )}, c.prototype.measureScrollbar = function () {
    var a = document.createElement( "div" );
    a.className = "modal-scrollbar-measure", this.$body.append( a );
    var b = a.offsetWidth - a.clientWidth;
    return this.$body[ 0 ].removeChild( a ), b
  };
  var d = a.fn.modal;
  a.fn.modal = b, a.fn.modal.Constructor = c, a.fn.modal.noConflict = function () {return a.fn.modal = d, this}, a( document ).on( "click.bs.modal.data-api", '[data-toggle="modal"]', function ( c ) {
    var d = a( this ), e = d.attr( "href" ), f = a( d.attr( "data-target" ) || e && e.replace( /.*(?=#[^\s]+$)/, "" ) ), g = f.data( "bs.modal" ) ? "toggle" : a.extend( { remote : !/#/.test( e ) && e }, f.data(), d.data() );
    d.is( "a" ) && c.preventDefault(), f.one( "show.bs.modal", function ( a ) {a.isDefaultPrevented() || f.one( "hidden.bs.modal", function () {d.is( ":visible" ) && d.trigger( "focus" )} )} ), b.call( f, g, this )
  } )
}( jQuery ), +function ( a ) {
  "use strict";
  function b ( b ) {
    return this.each( function () {
      var d = a( this ), e = d.data( "bs.tooltip" ), f = "object" == typeof b && b;
      (e || !/destroy|hide/.test( b )) && (e || d.data( "bs.tooltip", e = new c( this, f ) ), "string" == typeof b && e[ b ]())
    } )
  }

  var c = function ( a, b ) {this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.inState = null, this.init( "tooltip", a, b )};
  c.VERSION = "3.3.5", c.TRANSITION_DURATION = 150, c.DEFAULTS = { animation : !0, placement : "top", selector : !1, template : '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>', trigger : "hover focus", title : "", delay : 0, html : !1, container : !1, viewport : { selector : "body", padding : 0 } }, c.prototype.init = function ( b, c, d ) {
    if ( this.enabled = !0, this.type = b, this.$element = a( c ), this.options = this.getOptions( d ), this.$viewport = this.options.viewport && a( a.isFunction( this.options.viewport ) ? this.options.viewport.call( this, this.$element ) : this.options.viewport.selector || this.options.viewport ), this.inState = { click : !1, hover : !1, focus : !1 }, this.$element[ 0 ]instanceof document.constructor && !this.options.selector )throw new Error( "`selector` option must be specified when initializing " + this.type + " on the window.document object!" );
    for ( var e = this.options.trigger.split( " " ), f = e.length; f--; ) {
      var g = e[ f ];
      if ( "click" == g )this.$element.on( "click." + this.type, this.options.selector, a.proxy( this.toggle, this ) ); else if ( "manual" != g ) {
        var h = "hover" == g ? "mouseenter" : "focusin", i = "hover" == g ? "mouseleave" : "focusout";
        this.$element.on( h + "." + this.type, this.options.selector, a.proxy( this.enter, this ) ), this.$element.on( i + "." + this.type, this.options.selector, a.proxy( this.leave, this ) )
      }
    }
    this.options.selector ? this._options = a.extend( {}, this.options, { trigger : "manual", selector : "" } ) : this.fixTitle()
  }, c.prototype.getDefaults = function () {return c.DEFAULTS}, c.prototype.getOptions = function ( b ) {return b = a.extend( {}, this.getDefaults(), this.$element.data(), b ), b.delay && "number" == typeof b.delay && (b.delay = { show : b.delay, hide : b.delay }), b}, c.prototype.getDelegateOptions = function () {
    var b = {}, c = this.getDefaults();
    return this._options && a.each( this._options, function ( a, d ) {c[ a ] != d && (b[ a ] = d)} ), b
  }, c.prototype.enter = function ( b ) {
    var c = b instanceof this.constructor ? b : a( b.currentTarget ).data( "bs." + this.type );
    return c || (c = new this.constructor( b.currentTarget, this.getDelegateOptions() ), a( b.currentTarget ).data( "bs." + this.type, c )), b instanceof a.Event && (c.inState[ "focusin" == b.type ? "focus" : "hover" ] = !0), c.tip().hasClass( "in" ) || "in" == c.hoverState ? void(c.hoverState = "in") : (clearTimeout( c.timeout ), c.hoverState = "in", c.options.delay && c.options.delay.show ? void(c.timeout = setTimeout( function () {"in" == c.hoverState && c.show()}, c.options.delay.show )) : c.show())
  }, c.prototype.isInStateTrue = function () {
    for ( var a in this.inState )if ( this.inState[ a ] )return !0;
    return !1
  }, c.prototype.leave = function ( b ) {
    var c = b instanceof this.constructor ? b : a( b.currentTarget ).data( "bs." + this.type );
    return c || (c = new this.constructor( b.currentTarget, this.getDelegateOptions() ), a( b.currentTarget ).data( "bs." + this.type, c )), b instanceof a.Event && (c.inState[ "focusout" == b.type ? "focus" : "hover" ] = !1), c.isInStateTrue() ? void 0 : (clearTimeout( c.timeout ), c.hoverState = "out", c.options.delay && c.options.delay.hide ? void(c.timeout = setTimeout( function () {"out" == c.hoverState && c.hide()}, c.options.delay.hide )) : c.hide())
  }, c.prototype.show = function () {
    var b = a.Event( "show.bs." + this.type );
    if ( this.hasContent() && this.enabled ) {
      this.$element.trigger( b );
      var d = a.contains( this.$element[ 0 ].ownerDocument.documentElement, this.$element[ 0 ] );
      if ( b.isDefaultPrevented() || !d )return;
      var e = this, f = this.tip(), g = this.getUID( this.type );
      this.setContent(), f.attr( "id", g ), this.$element.attr( "aria-describedby", g ), this.options.animation && f.addClass( "fade" );
      var h = "function" == typeof this.options.placement ? this.options.placement.call( this, f[ 0 ], this.$element[ 0 ] ) : this.options.placement, i = /\s?auto?\s?/i, j = i.test( h );
      j && (h = h.replace( i, "" ) || "top"), f.detach().css( { top : 0, left : 0, display : "block" } ).addClass( h ).data( "bs." + this.type, this ), this.options.container ? f.appendTo( this.options.container ) : f.insertAfter( this.$element ), this.$element.trigger( "inserted.bs." + this.type );
      var k = this.getPosition(), l = f[ 0 ].offsetWidth, m = f[ 0 ].offsetHeight;
      if ( j ) {
        var n = h, o = this.getPosition( this.$viewport );
        h = "bottom" == h && k.bottom + m > o.bottom ? "top" : "top" == h && k.top - m < o.top ? "bottom" : "right" == h && k.right + l > o.width ? "left" : "left" == h && k.left - l < o.left ? "right" : h, f.removeClass( n ).addClass( h )
      }
      var p = this.getCalculatedOffset( h, k, l, m );
      this.applyPlacement( p, h );
      var q = function () {
        var a = e.hoverState;
        e.$element.trigger( "shown.bs." + e.type ), e.hoverState = null, "out" == a && e.leave( e )
      };
      a.support.transition && this.$tip.hasClass( "fade" ) ? f.one( "bsTransitionEnd", q ).emulateTransitionEnd( c.TRANSITION_DURATION ) : q()
    }
  }, c.prototype.applyPlacement = function ( b, c ) {
    var d = this.tip(), e = d[ 0 ].offsetWidth, f = d[ 0 ].offsetHeight, g = parseInt( d.css( "margin-top" ), 10 ), h = parseInt( d.css( "margin-left" ), 10 );
    isNaN( g ) && (g = 0), isNaN( h ) && (h = 0), b.top += g, b.left += h, a.offset.setOffset( d[ 0 ], a.extend( { using : function ( a ) {d.css( { top : Math.round( a.top ), left : Math.round( a.left ) } )} }, b ), 0 ), d.addClass( "in" );
    var i = d[ 0 ].offsetWidth, j = d[ 0 ].offsetHeight;
    "top" == c && j != f && (b.top = b.top + f - j);
    var k = this.getViewportAdjustedDelta( c, b, i, j );
    k.left ? b.left += k.left : b.top += k.top;
    var l = /top|bottom/.test( c ), m = l ? 2 * k.left - e + i : 2 * k.top - f + j, n = l ? "offsetWidth" : "offsetHeight";
    d.offset( b ), this.replaceArrow( m, d[ 0 ][ n ], l )
  }, c.prototype.replaceArrow = function ( a, b, c ) {this.arrow().css( c ? "left" : "top", 50 * (1 - a / b) + "%" ).css( c ? "top" : "left", "" )}, c.prototype.setContent = function () {
    var a = this.tip(), b = this.getTitle();
    a.find( ".tooltip-inner" )[ this.options.html ? "html" : "text" ]( b ), a.removeClass( "fade in top bottom left right" )
  }, c.prototype.hide = function ( b ) {
    function d () {"in" != e.hoverState && f.detach(), e.$element.removeAttr( "aria-describedby" ).trigger( "hidden.bs." + e.type ), b && b()}

    var e = this, f = a( this.$tip ), g = a.Event( "hide.bs." + this.type );
    return this.$element.trigger( g ), g.isDefaultPrevented() ? void 0 : (f.removeClass( "in" ), a.support.transition && f.hasClass( "fade" ) ? f.one( "bsTransitionEnd", d ).emulateTransitionEnd( c.TRANSITION_DURATION ) : d(), this.hoverState = null, this)
  }, c.prototype.fixTitle = function () {
    var a = this.$element;
    (a.attr( "title" ) || "string" != typeof a.attr( "data-original-title" )) && a.attr( "data-original-title", a.attr( "title" ) || "" ).attr( "title", "" )
  }, c.prototype.hasContent = function () {return this.getTitle()}, c.prototype.getPosition = function ( b ) {
    b = b || this.$element;
    var c = b[ 0 ], d = "BODY" == c.tagName, e = c.getBoundingClientRect();
    null == e.width && (e = a.extend( {}, e, { width : e.right - e.left, height : e.bottom - e.top } ));
    var f = d ? { top : 0, left : 0 } : b.offset(), g = { scroll : d ? document.documentElement.scrollTop || document.body.scrollTop : b.scrollTop() }, h = d ? { width : a( window ).width(), height : a( window ).height() } : null;
    return a.extend( {}, e, g, h, f )
  }, c.prototype.getCalculatedOffset = function ( a, b, c, d ) {return "bottom" == a ? { top : b.top + b.height, left : b.left + b.width / 2 - c / 2 } : "top" == a ? { top : b.top - d, left : b.left + b.width / 2 - c / 2 } : "left" == a ? { top : b.top + b.height / 2 - d / 2, left : b.left - c } : { top : b.top + b.height / 2 - d / 2, left : b.left + b.width }}, c.prototype.getViewportAdjustedDelta = function ( a, b, c, d ) {
    var e = { top : 0, left : 0 };
    if ( !this.$viewport )return e;
    var f = this.options.viewport && this.options.viewport.padding || 0, g = this.getPosition( this.$viewport );
    if ( /right|left/.test( a ) ) {
      var h = b.top - f - g.scroll, i = b.top + f - g.scroll + d;
      h < g.top ? e.top = g.top - h : i > g.top + g.height && (e.top = g.top + g.height - i)
    } else {
      var j = b.left - f, k = b.left + f + c;
      j < g.left ? e.left = g.left - j : k > g.right && (e.left = g.left + g.width - k)
    }
    return e
  }, c.prototype.getTitle = function () {
    var a, b = this.$element, c = this.options;
    return a = b.attr( "data-original-title" ) || ("function" == typeof c.title ? c.title.call( b[ 0 ] ) : c.title)
  }, c.prototype.getUID = function ( a ) {
    do a += ~~(1e6 * Math.random()); while ( document.getElementById( a ) );
    return a
  }, c.prototype.tip = function () {
    if ( !this.$tip && (this.$tip = a( this.options.template ), 1 != this.$tip.length) )throw new Error( this.type + " `template` option must consist of exactly 1 top-level element!" );
    return this.$tip
  }, c.prototype.arrow = function () {return this.$arrow = this.$arrow || this.tip().find( ".tooltip-arrow" )}, c.prototype.enable = function () {this.enabled = !0}, c.prototype.disable = function () {this.enabled = !1}, c.prototype.toggleEnabled = function () {this.enabled = !this.enabled}, c.prototype.toggle = function ( b ) {
    var c = this;
    b && (c = a( b.currentTarget ).data( "bs." + this.type ), c || (c = new this.constructor( b.currentTarget, this.getDelegateOptions() ), a( b.currentTarget ).data( "bs." + this.type, c ))), b ? (c.inState.click = !c.inState.click, c.isInStateTrue() ? c.enter( c ) : c.leave( c )) : c.tip().hasClass( "in" ) ? c.leave( c ) : c.enter( c )
  }, c.prototype.destroy = function () {
    var a = this;
    clearTimeout( this.timeout ), this.hide( function () {a.$element.off( "." + a.type ).removeData( "bs." + a.type ), a.$tip && a.$tip.detach(), a.$tip = null, a.$arrow = null, a.$viewport = null} )
  };
  var d = a.fn.tooltip;
  a.fn.tooltip = b, a.fn.tooltip.Constructor = c, a.fn.tooltip.noConflict = function () {return a.fn.tooltip = d, this}
}( jQuery ), +function ( a ) {
  "use strict";
  function b ( b ) {
    return this.each( function () {
      var d = a( this ), e = d.data( "bs.popover" ), f = "object" == typeof b && b;
      (e || !/destroy|hide/.test( b )) && (e || d.data( "bs.popover", e = new c( this, f ) ), "string" == typeof b && e[ b ]())
    } )
  }

  var c = function ( a, b ) {this.init( "popover", a, b )};
  if ( !a.fn.tooltip )throw new Error( "Popover requires tooltip.js" );
  c.VERSION = "3.3.5", c.DEFAULTS = a.extend( {}, a.fn.tooltip.Constructor.DEFAULTS, { placement : "right", trigger : "click", content : "", template : '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>' } ), c.prototype = a.extend( {}, a.fn.tooltip.Constructor.prototype ), c.prototype.constructor = c, c.prototype.getDefaults = function () {return c.DEFAULTS}, c.prototype.setContent = function () {
    var a = this.tip(), b = this.getTitle(), c = this.getContent();
    a.find( ".popover-title" )[ this.options.html ? "html" : "text" ]( b ), a.find( ".popover-content" ).children().detach().end()[ this.options.html ? "string" == typeof c ? "html" : "append" : "text" ]( c ), a.removeClass( "fade top bottom left right in" ), a.find( ".popover-title" ).html() || a.find( ".popover-title" ).hide()
  }, c.prototype.hasContent = function () {return this.getTitle() || this.getContent()}, c.prototype.getContent = function () {
    var a = this.$element, b = this.options;
    return a.attr( "data-content" ) || ("function" == typeof b.content ? b.content.call( a[ 0 ] ) : b.content)
  }, c.prototype.arrow = function () {return this.$arrow = this.$arrow || this.tip().find( ".arrow" )};
  var d = a.fn.popover;
  a.fn.popover = b, a.fn.popover.Constructor = c, a.fn.popover.noConflict = function () {return a.fn.popover = d, this}
}( jQuery ), +function ( a ) {
  "use strict";
  function b ( c, d ) {this.$body = a( document.body ), this.$scrollElement = a( a( c ).is( document.body ) ? window : c ), this.options = a.extend( {}, b.DEFAULTS, d ), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on( "scroll.bs.scrollspy", a.proxy( this.process, this ) ), this.refresh(), this.process()}

  function c ( c ) {
    return this.each( function () {
      var d = a( this ), e = d.data( "bs.scrollspy" ), f = "object" == typeof c && c;
      e || d.data( "bs.scrollspy", e = new b( this, f ) ), "string" == typeof c && e[ c ]()
    } )
  }

  b.VERSION = "3.3.5", b.DEFAULTS = { offset : 10 }, b.prototype.getScrollHeight = function () {return this.$scrollElement[ 0 ].scrollHeight || Math.max( this.$body[ 0 ].scrollHeight, document.documentElement.scrollHeight )}, b.prototype.refresh = function () {
    var b = this, c = "offset", d = 0;
    this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), a.isWindow( this.$scrollElement[ 0 ] ) || (c = "position", d = this.$scrollElement.scrollTop()), this.$body.find( this.selector ).map( function () {
      var b = a( this ), e = b.data( "target" ) || b.attr( "href" ), f = /^#./.test( e ) && a( e );
      return f && f.length && f.is( ":visible" ) && [ [ f[ c ]().top + d, e ] ] || null
    } ).sort( function ( a, b ) {return a[ 0 ] - b[ 0 ]} ).each( function () {b.offsets.push( this[ 0 ] ), b.targets.push( this[ 1 ] )} )
  }, b.prototype.process = function () {
    var a, b = this.$scrollElement.scrollTop() + this.options.offset, c = this.getScrollHeight(), d = this.options.offset + c - this.$scrollElement.height(), e = this.offsets, f = this.targets, g = this.activeTarget;
    if ( this.scrollHeight != c && this.refresh(), b >= d )return g != (a = f[ f.length - 1 ]) && this.activate( a );
    if ( g && b < e[ 0 ] )return this.activeTarget = null, this.clear();
    for ( a = e.length; a--; )g != f[ a ] && b >= e[ a ] && (void 0 === e[ a + 1 ] || b < e[ a + 1 ]) && this.activate( f[ a ] )
  }, b.prototype.activate = function ( b ) {
    this.activeTarget = b, this.clear();
    var c = this.selector + '[data-target="' + b + '"],' + this.selector + '[href="' + b + '"]', d = a( c ).parents( "li" ).addClass( "active" );
    d.parent( ".dropdown-menu" ).length && (d = d.closest( "li.dropdown" ).addClass( "active" )),
      d.trigger( "activate.bs.scrollspy" )
  }, b.prototype.clear = function () {a( this.selector ).parentsUntil( this.options.target, ".active" ).removeClass( "active" )};
  var d = a.fn.scrollspy;
  a.fn.scrollspy = c, a.fn.scrollspy.Constructor = b, a.fn.scrollspy.noConflict = function () {return a.fn.scrollspy = d, this}, a( window ).on( "load.bs.scrollspy.data-api", function () {
    a( '[data-spy="scroll"]' ).each( function () {
      var b = a( this );
      c.call( b, b.data() )
    } )
  } )
}( jQuery ), +function ( a ) {
  "use strict";
  function b ( b ) {
    return this.each( function () {
      var d = a( this ), e = d.data( "bs.tab" );
      e || d.data( "bs.tab", e = new c( this ) ), "string" == typeof b && e[ b ]()
    } )
  }

  var c = function ( b ) {this.element = a( b )};
  c.VERSION = "3.3.5", c.TRANSITION_DURATION = 150, c.prototype.show = function () {
    var b = this.element, c = b.closest( "ul:not(.dropdown-menu)" ), d = b.data( "target" );
    if ( d || (d = b.attr( "href" ), d = d && d.replace( /.*(?=#[^\s]*$)/, "" )), !b.parent( "li" ).hasClass( "active" ) ) {
      var e = c.find( ".active:last a" ), f = a.Event( "hide.bs.tab", { relatedTarget : b[ 0 ] } ), g = a.Event( "show.bs.tab", { relatedTarget : e[ 0 ] } );
      if ( e.trigger( f ), b.trigger( g ), !g.isDefaultPrevented() && !f.isDefaultPrevented() ) {
        var h = a( d );
        this.activate( b.closest( "li" ), c ), this.activate( h, h.parent(), function () {e.trigger( { type : "hidden.bs.tab", relatedTarget : b[ 0 ] } ), b.trigger( { type : "shown.bs.tab", relatedTarget : e[ 0 ] } )} )
      }
    }
  }, c.prototype.activate = function ( b, d, e ) {
    function f () {g.removeClass( "active" ).find( "> .dropdown-menu > .active" ).removeClass( "active" ).end().find( '[data-toggle="tab"]' ).attr( "aria-expanded", !1 ), b.addClass( "active" ).find( '[data-toggle="tab"]' ).attr( "aria-expanded", !0 ), h ? (b[ 0 ].offsetWidth, b.addClass( "in" )) : b.removeClass( "fade" ), b.parent( ".dropdown-menu" ).length && b.closest( "li.dropdown" ).addClass( "active" ).end().find( '[data-toggle="tab"]' ).attr( "aria-expanded", !0 ), e && e()}

    var g = d.find( "> .active" ), h = e && a.support.transition && (g.length && g.hasClass( "fade" ) || !!d.find( "> .fade" ).length);
    g.length && h ? g.one( "bsTransitionEnd", f ).emulateTransitionEnd( c.TRANSITION_DURATION ) : f(), g.removeClass( "in" )
  };
  var d = a.fn.tab;
  a.fn.tab = b, a.fn.tab.Constructor = c, a.fn.tab.noConflict = function () {return a.fn.tab = d, this};
  var e = function ( c ) {c.preventDefault(), b.call( a( this ), "show" )};
  a( document ).on( "click.bs.tab.data-api", '[data-toggle="tab"]', e ).on( "click.bs.tab.data-api", '[data-toggle="pill"]', e )
}( jQuery ), +function ( a ) {
  "use strict";
  function b ( b ) {
    return this.each( function () {
      var d = a( this ), e = d.data( "bs.affix" ), f = "object" == typeof b && b;
      e || d.data( "bs.affix", e = new c( this, f ) ), "string" == typeof b && e[ b ]()
    } )
  }

  var c = function ( b, d ) {this.options = a.extend( {}, c.DEFAULTS, d ), this.$target = a( this.options.target ).on( "scroll.bs.affix.data-api", a.proxy( this.checkPosition, this ) ).on( "click.bs.affix.data-api", a.proxy( this.checkPositionWithEventLoop, this ) ), this.$element = a( b ), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition()};
  c.VERSION = "3.3.5", c.RESET = "affix affix-top affix-bottom", c.DEFAULTS = { offset : 0, target : window }, c.prototype.getState = function ( a, b, c, d ) {
    var e = this.$target.scrollTop(), f = this.$element.offset(), g = this.$target.height();
    if ( null != c && "top" == this.affixed )return c > e ? "top" : !1;
    if ( "bottom" == this.affixed )return null != c ? e + this.unpin <= f.top ? !1 : "bottom" : a - d >= e + g ? !1 : "bottom";
    var h = null == this.affixed, i = h ? e : f.top, j = h ? g : b;
    return null != c && c >= e ? "top" : null != d && i + j >= a - d ? "bottom" : !1
  }, c.prototype.getPinnedOffset = function () {
    if ( this.pinnedOffset )return this.pinnedOffset;
    this.$element.removeClass( c.RESET ).addClass( "affix" );
    var a = this.$target.scrollTop(), b = this.$element.offset();
    return this.pinnedOffset = b.top - a
  }, c.prototype.checkPositionWithEventLoop = function () {setTimeout( a.proxy( this.checkPosition, this ), 1 )}, c.prototype.checkPosition = function () {
    if ( this.$element.is( ":visible" ) ) {
      var b = this.$element.height(), d = this.options.offset, e = d.top, f = d.bottom, g = Math.max( a( document ).height(), a( document.body ).height() );
      "object" != typeof d && (f = e = d), "function" == typeof e && (e = d.top( this.$element )), "function" == typeof f && (f = d.bottom( this.$element ));
      var h = this.getState( g, b, e, f );
      if ( this.affixed != h ) {
        null != this.unpin && this.$element.css( "top", "" );
        var i = "affix" + (h ? "-" + h : ""), j = a.Event( i + ".bs.affix" );
        if ( this.$element.trigger( j ), j.isDefaultPrevented() )return;
        this.affixed = h, this.unpin = "bottom" == h ? this.getPinnedOffset() : null, this.$element.removeClass( c.RESET ).addClass( i ).trigger( i.replace( "affix", "affixed" ) + ".bs.affix" )
      }
      "bottom" == h && this.$element.offset( { top : g - b - f } )
    }
  };
  var d = a.fn.affix;
  a.fn.affix = b, a.fn.affix.Constructor = c, a.fn.affix.noConflict = function () {return a.fn.affix = d, this}, a( window ).on( "load", function () {
    a( '[data-spy="affix"]' ).each( function () {
      var c = a( this ), d = c.data();
      d.offset = d.offset || {}, null != d.offsetBottom && (d.offset.bottom = d.offsetBottom), null != d.offsetTop && (d.offset.top = d.offsetTop), b.call( c, d )
    } )
  } )
}( jQuery );
/*
 *  Bootstrap TouchSpin - v3.0.1
 *  A mobile and touch friendly input spinner component for Bootstrap 3.
 *  http://www.virtuosoft.eu/code/bootstrap-touchspin/
 *
 *  Made by István Ujj-Mészáros
 *  Under Apache License v2.0 License
 */
!function ( a ) {
  "use strict";
  function b ( a, b ) {return a + ".touchspin_" + b}

  function c ( c, d ) {return a.map( c, function ( a ) {return b( a, d )} )}

  var d = 0;
  a.fn.TouchSpin = function ( b ) {
    if ( "destroy" === b )return void this.each( function () {
      var b = a( this ), d = b.data();
      a( document ).off( c( [ "mouseup", "touchend", "touchcancel", "mousemove", "touchmove", "scroll", "scrollstart" ], d.spinnerid ).join( " " ) )
    } );
    var e = { min : 0, max : 100, initval : "", step : 1, decimals : 0, stepinterval : 100, forcestepdivisibility : "round", stepintervaldelay : 500, verticalbuttons : !1, verticalupclass : "glyphicon glyphicon-chevron-up", verticaldownclass : "glyphicon glyphicon-chevron-down", prefix : "", postfix : "", prefix_extraclass : "", postfix_extraclass : "", booster : !0, boostat : 10, maxboostedstep : !1, mousewheel : !0, buttondown_class : "btn btn-default", buttonup_class : "btn btn-default", buttondown_txt : "-", buttonup_txt : "+" }, f = { min : "min", max : "max", initval : "init-val", step : "step", decimals : "decimals", stepinterval : "step-interval", verticalbuttons : "vertical-buttons", verticalupclass : "vertical-up-class", verticaldownclass : "vertical-down-class", forcestepdivisibility : "force-step-divisibility", stepintervaldelay : "step-interval-delay", prefix : "prefix", postfix : "postfix", prefix_extraclass : "prefix-extra-class", postfix_extraclass : "postfix-extra-class", booster : "booster", boostat : "boostat", maxboostedstep : "max-boosted-step", mousewheel : "mouse-wheel", buttondown_class : "button-down-class", buttonup_class : "button-up-class", buttondown_txt : "button-down-txt", buttonup_txt : "button-up-txt" };
    return this.each( function () {
      function g () {
        if ( !J.data( "alreadyinitialized" ) ) {
          if ( J.data( "alreadyinitialized", !0 ), d += 1, J.data( "spinnerid", d ), !J.is( "input" ) )return void console.log( "Must be an input." );
          j(), h(), u(), m(), p(), q(), r(), s(), D.input.css( "display", "block" )
        }
      }

      function h () {"" !== B.initval && "" === J.val() && J.val( B.initval )}

      function i ( a ) {
        l( a ), u();
        var b = D.input.val();
        "" !== b && (b = Number( D.input.val() ), D.input.val( b.toFixed( B.decimals ) ))
      }

      function j () {B = a.extend( {}, e, K, k(), b )}

      function k () {
        var b = {};
        return a.each( f, function ( a, c ) {
          var d = "bts-" + c;
          J.is( "[data-" + d + "]" ) && (b[ a ] = J.data( d ))
        } ), b
      }

      function l ( b ) {B = a.extend( {}, B, b )}

      function m () {
        var a = J.val(), b = J.parent();
        "" !== a && (a = Number( a ).toFixed( B.decimals )), J.data( "initvalue", a ).val( a ), J.addClass( "form-control" ), b.hasClass( "input-group" ) ? n( b ) : o()
      }

      function n ( b ) {
        b.addClass( "bootstrap-touchspin" );
        var c, d, e = J.prev(), f = J.next(), g = '<span class="input-group-addon bootstrap-touchspin-prefix">' + B.prefix + "</span>", h = '<span class="input-group-addon bootstrap-touchspin-postfix">' + B.postfix + "</span>";
        e.hasClass( "input-group-btn" ) ? (c = '<button class="' + B.buttondown_class + ' bootstrap-touchspin-down" type="button">' + B.buttondown_txt + "</button>", e.append( c )) : (c = '<span class="input-group-btn"><button class="' + B.buttondown_class + ' bootstrap-touchspin-down" type="button">' + B.buttondown_txt + "</button></span>", a( c ).insertBefore( J )), f.hasClass( "input-group-btn" ) ? (d = '<button class="' + B.buttonup_class + ' bootstrap-touchspin-up" type="button">' + B.buttonup_txt + "</button>", f.prepend( d )) : (d = '<span class="input-group-btn"><button class="' + B.buttonup_class + ' bootstrap-touchspin-up" type="button">' + B.buttonup_txt + "</button></span>", a( d ).insertAfter( J )), a( g ).insertBefore( J ), a( h ).insertAfter( J ), C = b
      }

      function o () {
        var b;
        b = B.verticalbuttons ? '<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix">' + B.prefix + '</span><span class="input-group-addon bootstrap-touchspin-postfix">' + B.postfix + '</span><span class="input-group-btn-vertical"><button class="' + B.buttondown_class + ' bootstrap-touchspin-up" type="button"><i class="' + B.verticalupclass + '"></i></button><button class="' + B.buttonup_class + ' bootstrap-touchspin-down" type="button"><i class="' + B.verticaldownclass + '"></i></button></span></div>' : '<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="' + B.buttondown_class + ' bootstrap-touchspin-down" type="button">' + B.buttondown_txt + '</button></span><span class="input-group-addon bootstrap-touchspin-prefix">' + B.prefix + '</span><span class="input-group-addon bootstrap-touchspin-postfix">' + B.postfix + '</span><span class="input-group-btn"><button class="' + B.buttonup_class + ' bootstrap-touchspin-up" type="button">' + B.buttonup_txt + "</button></span></div>", C = a( b ).insertBefore( J ), a( ".bootstrap-touchspin-prefix", C ).after( J ), J.hasClass( "input-sm" ) ? C.addClass( "input-group-sm" ) : J.hasClass( "input-lg" ) && C.addClass( "input-group-lg" )
      }

      function p () {D = { down : a( ".bootstrap-touchspin-down", C ), up : a( ".bootstrap-touchspin-up", C ), input : a( "input", C ), prefix : a( ".bootstrap-touchspin-prefix", C ).addClass( B.prefix_extraclass ), postfix : a( ".bootstrap-touchspin-postfix", C ).addClass( B.postfix_extraclass ) }}

      function q () {"" === B.prefix && D.prefix.hide(), "" === B.postfix && D.postfix.hide()}

      function r () {
        J.on( "keydown", function ( a ) {
          var b = a.keyCode || a.which;
          38 === b ? ("up" !== M && (w(), z()), a.preventDefault()) : 40 === b && ("down" !== M && (x(), y()), a.preventDefault())
        } ), J.on( "keyup", function ( a ) {
          var b = a.keyCode || a.which;
          38 === b ? A() : 40 === b && A()
        } ), J.on( "blur", function () {u()} ), D.down.on( "keydown", function ( a ) {
          var b = a.keyCode || a.which;
          (32 === b || 13 === b) && ("down" !== M && (x(), y()), a.preventDefault())
        } ), D.down.on( "keyup", function ( a ) {
          var b = a.keyCode || a.which;
          (32 === b || 13 === b) && A()
        } ), D.up.on( "keydown", function ( a ) {
          var b = a.keyCode || a.which;
          (32 === b || 13 === b) && ("up" !== M && (w(), z()), a.preventDefault())
        } ), D.up.on( "keyup", function ( a ) {
          var b = a.keyCode || a.which;
          (32 === b || 13 === b) && A()
        } ), D.down.on( "mousedown.touchspin", function ( a ) {D.down.off( "touchstart.touchspin" ), J.is( ":disabled" ) || (x(), y(), a.preventDefault(), a.stopPropagation())} ), D.down.on( "touchstart.touchspin", function ( a ) {D.down.off( "mousedown.touchspin" ), J.is( ":disabled" ) || (x(), y(), a.preventDefault(), a.stopPropagation())} ), D.up.on( "mousedown.touchspin", function ( a ) {D.up.off( "touchstart.touchspin" ), J.is( ":disabled" ) || (w(), z(), a.preventDefault(), a.stopPropagation())} ), D.up.on( "touchstart.touchspin", function ( a ) {D.up.off( "mousedown.touchspin" ), J.is( ":disabled" ) || (w(), z(), a.preventDefault(), a.stopPropagation())} ), D.up.on( "mouseout touchleave touchend touchcancel", function ( a ) {M && (a.stopPropagation(), A())} ), D.down.on( "mouseout touchleave touchend touchcancel", function ( a ) {M && (a.stopPropagation(), A())} ), D.down.on( "mousemove touchmove", function ( a ) {M && (a.stopPropagation(), a.preventDefault())} ), D.up.on( "mousemove touchmove", function ( a ) {M && (a.stopPropagation(), a.preventDefault())} ), a( document ).on( c( [ "mouseup", "touchend", "touchcancel" ], d ).join( " " ), function ( a ) {M && (a.preventDefault(), A())} ), a( document ).on( c( [ "mousemove", "touchmove", "scroll", "scrollstart" ], d ).join( " " ), function ( a ) {M && (a.preventDefault(), A())} ), J.on( "mousewheel DOMMouseScroll", function ( a ) {
          if ( B.mousewheel && J.is( ":focus" ) ) {
            var b = a.originalEvent.wheelDelta || -a.originalEvent.deltaY || -a.originalEvent.detail;
            a.stopPropagation(), a.preventDefault(), 0 > b ? x() : w()
          }
        } )
      }

      function s () {J.on( "touchspin.uponce", function () {A(), w()} ), J.on( "touchspin.downonce", function () {A(), x()} ), J.on( "touchspin.startupspin", function () {z()} ), J.on( "touchspin.startdownspin", function () {y()} ), J.on( "touchspin.stopspin", function () {A()} ), J.on( "touchspin.updatesettings", function ( a, b ) {i( b )} )}

      function t ( a ) {
        switch ( B.forcestepdivisibility ) {
          case"round":
            return (Math.round( a / B.step ) * B.step).toFixed( B.decimals );
          case"floor":
            return (Math.floor( a / B.step ) * B.step).toFixed( B.decimals );
          case"ceil":
            return (Math.ceil( a / B.step ) * B.step).toFixed( B.decimals );
          default:
            return a
        }
      }

      function u () {
        var a, b, c;
        a = J.val(), "" !== a && (B.decimals > 0 && "." === a || (b = parseFloat( a ), isNaN( b ) && (b = 0), c = b, b.toString() !== a && (c = b), b < B.min && (c = B.min), b > B.max && (c = B.max), c = t( c ), Number( a ).toString() !== c.toString() && (J.val( c ), J.trigger( "change" ))))
      }

      function v () {
        if ( B.booster ) {
          var a = Math.pow( 2, Math.floor( L / B.boostat ) ) * B.step;
          return B.maxboostedstep && a > B.maxboostedstep && (a = B.maxboostedstep, E = Math.round( E / a ) * a), Math.max( B.step, a )
        }
        return B.step
      }

      function w () {
        u(), E = parseFloat( D.input.val() ), isNaN( E ) && (E = 0);
        var a = E, b = v();
        E += b, E > B.max && (E = B.max, J.trigger( "touchspin.on.max" ), A()), D.input.val( Number( E ).toFixed( B.decimals ) ), a !== E && J.trigger( "change" )
      }

      function x () {
        u(), E = parseFloat( D.input.val() ), isNaN( E ) && (E = 0);
        var a = E, b = v();
        E -= b, E < B.min && (E = B.min, J.trigger( "touchspin.on.min" ), A()), D.input.val( E.toFixed( B.decimals ) ), a !== E && J.trigger( "change" )
      }

      function y () {A(), L = 0, M = "down", J.trigger( "touchspin.on.startspin" ), J.trigger( "touchspin.on.startdownspin" ), H = setTimeout( function () {F = setInterval( function () {L++, x()}, B.stepinterval )}, B.stepintervaldelay )}

      function z () {A(), L = 0, M = "up", J.trigger( "touchspin.on.startspin" ), J.trigger( "touchspin.on.startupspin" ), I = setTimeout( function () {G = setInterval( function () {L++, w()}, B.stepinterval )}, B.stepintervaldelay )}

      function A () {
        switch ( clearTimeout( H ), clearTimeout( I ), clearInterval( F ), clearInterval( G ), M ) {
          case"up":
            J.trigger( "touchspin.on.stopupspin" ), J.trigger( "touchspin.on.stopspin" );
            break;
          case"down":
            J.trigger( "touchspin.on.stopdownspin" ), J.trigger( "touchspin.on.stopspin" )
        }
        L = 0, M = !1
      }

      var B, C, D, E, F, G, H, I, J = a( this ), K = J.data(), L = 0, M = !1;
      g()
    } )
  }
}( jQuery );
!function ( a ) {"function" == typeof define && define.amd ? define( [ "jquery" ], a ) : "object" == typeof module && "object" == typeof module.exports ? module.exports = a( require( "jquery" ) ) : a( jQuery )}( function ( a ) {
  return function ( a ) {
    "use strict";
    a.extend( {
      tablesorter : new function () {
        function b ( b, c, d, e ) {
          for ( var f, g, h = q.parsers.length, i = !1, j = "", k = !0; "" === j && k; )d++, c[ d ] ? (i = c[ d ].cells[ e ], j = q.getElementText( b, i, e ), g = a( i ), b.debug && console.log( "Checking if value was empty on row " + d + ", column: " + e + ': "' + j + '"' )) : k = !1;
          for ( ; --h >= 0; )if ( f = q.parsers[ h ], f && "text" !== f.id && f.is && f.is( j, b.table, i, g ) )return f;
          return q.getParserById( "text" )
        }

        function c ( a, c ) {
          var d, e, f, g, h, i, j, k, l, m, n, o, p = a.table, r = 0, s = {};
          if ( a.$tbodies = a.$table.children( "tbody:not(." + a.cssInfoBlock + ")" ), n = "undefined" == typeof c ? a.$tbodies : c, o = n.length, 0 === o )return a.debug ? console.warn( "Warning: *Empty table!* Not building a parser cache" ) : "";
          for ( a.debug && (m = new Date, console[ console.group ? "group" : "log" ]( "Detecting parsers for each column" )), e = { extractors : [], parsers : [] }; o > r; ) {
            if ( d = n[ r ].rows, d.length )for ( f = a.columns, g = 0; f > g; g++ )h = a.$headerIndexed[ g ], i = q.getColumnData( p, a.headers, g ), l = q.getParserById( q.getData( h, i, "extractor" ) ), k = q.getParserById( q.getData( h, i, "sorter" ) ), j = "false" === q.getData( h, i, "parser" ), a.empties[ g ] = (q.getData( h, i, "empty" ) || a.emptyTo || (a.emptyToBottom ? "bottom" : "top")).toLowerCase(), a.strings[ g ] = (q.getData( h, i, "string" ) || a.stringTo || "max").toLowerCase(), j && (k = q.getParserById( "no-parser" )), l || (l = !1), k || (k = b( a, d, -1, g )), a.debug && (s[ "(" + g + ") " + h.text() ] = { parser : k.id, extractor : l ? l.id : "none", string : a.strings[ g ], empty : a.empties[ g ] }), e.parsers[ g ] = k, e.extractors[ g ] = l;
            r += e.parsers.length ? o : 1
          }
          a.debug && (q.isEmptyObject( s ) ? console.warn( "  No parsers detected!" ) : console[ console.table ? "table" : "log" ]( s ), console.log( "Completed detecting parsers" + q.benchmark( m ) ), console.groupEnd && console.groupEnd()), a.parsers = e.parsers, a.extractors = e.extractors
        }

        function d ( b, c, d ) {
          var e, f, g, h, i, j, k, l, m, n, o, p, r, s, t = b.config, u = t.parsers;
          if ( t.$tbodies = t.$table.children( "tbody:not(." + t.cssInfoBlock + ")" ), k = "undefined" == typeof d ? t.$tbodies : d, t.cache = {}, t.totalRows = 0, !u )return t.debug ? console.warn( "Warning: *Empty table!* Not building a cache" ) : "";
          for ( t.debug && (n = new Date), t.showProcessing && q.isProcessing( b, !0 ), j = 0; j < k.length; j++ ) {
            for ( s = [], e = t.cache[ j ] = { normalized : [] }, o = k[ j ] && k[ j ].rows.length || 0, h = 0; o > h; ++h )if ( p = { child : [], raw : [] }, l = a( k[ j ].rows[ h ] ), m = [], l.hasClass( t.cssChildRow ) && 0 !== h )for ( f = e.normalized.length - 1, r = e.normalized[ f ][ t.columns ], r.$row = r.$row.add( l ), l.prev().hasClass( t.cssChildRow ) || l.prev().addClass( q.css.cssHasChild ), g = l.children( "th, td" ), f = r.child.length, r.child[ f ] = [], i = 0; i < t.columns; i++ )r.child[ f ][ i ] = q.getParsedText( t, g[ i ], i ); else {
              for ( p.$row = l, p.order = h, i = 0; i < t.columns; ++i )"undefined" != typeof u[ i ] ? (f = q.getElementText( t, l[ 0 ].cells[ i ], i ), p.raw.push( f ), g = q.getParsedText( t, l[ 0 ].cells[ i ], i, f ), m.push( g ), "numeric" === (u[ i ].type || "").toLowerCase() && (s[ i ] = Math.max( Math.abs( g ) || 0, s[ i ] || 0 ))) : t.debug && console.warn( "No parser found for cell:", l[ 0 ].cells[ i ], "does it have a header?" );
              m[ t.columns ] = p, e.normalized.push( m )
            }
            e.colMax = s, t.totalRows += e.normalized.length
          }
          t.showProcessing && q.isProcessing( b ), t.debug && console.log( "Building cache for " + o + " rows" + q.benchmark( n ) ), a.isFunction( c ) && c( b )
        }

        function e ( a ) {return /^d/i.test( a ) || 1 === a}

        function f ( b ) {
          var c, d, f, g, i, j, k, l;
          for ( b.headerList = [], b.headerContent = [], b.debug && (k = new Date), b.columns = q.computeColumnIndex( b.$table.children( "thead, tfoot" ).children( "tr" ) ), g = b.cssIcon ? '<i class="' + (b.cssIcon === q.css.icon ? q.css.icon : b.cssIcon + " " + q.css.icon) + '"></i>' : "", b.$headers = a( a.map( b.$table.find( b.selectorHeaders ), function ( h, k ) {return d = a( h ), d.parent().hasClass( b.cssIgnoreRow ) ? void 0 : (c = q.getColumnData( b.table, b.headers, k, !0 ), b.headerContent[ k ] = d.html(), "" === b.headerTemplate || d.find( "." + q.css.headerIn ).length || (i = b.headerTemplate.replace( /\{content\}/g, d.html() ).replace( /\{icon\}/g, d.find( "." + q.css.icon ).length ? "" : g ), b.onRenderTemplate && (f = b.onRenderTemplate.apply( d, [ k, i ] ), f && "string" == typeof f && (i = f)), d.html( '<div class="' + q.css.headerIn + '">' + i + "</div>" )), b.onRenderHeader && b.onRenderHeader.apply( d, [ k, b, b.$table ] ), h.column = parseInt( d.attr( "data-column" ), 10 ), h.order = e( q.getData( d, c, "sortInitialOrder" ) || b.sortInitialOrder ) ? [ 1, 0, 2 ] : [ 0, 1, 2 ], h.count = -1, h.lockedOrder = !1, j = q.getData( d, c, "lockedOrder" ) || !1, "undefined" != typeof j && j !== !1 && (h.order = h.lockedOrder = e( j ) ? [ 1, 1, 1 ] : [ 0, 0, 0 ]), d.addClass( q.css.header + " " + b.cssHeader ), b.headerList[ k ] = h, d.parent().addClass( q.css.headerRow + " " + b.cssHeaderRow ).attr( "role", "row" ), b.tabIndex && d.attr( "tabindex", 0 ), h)} ) ), b.$headerIndexed = [], l = 0; l < b.columns; l++ )d = b.$headers.filter( '[data-column="' + l + '"]' ), b.$headerIndexed[ l ] = d.not( ".sorter-false" ).length ? d.not( ".sorter-false" ).filter( ":last" ) : d.filter( ":last" );
          b.$table.find( b.selectorHeaders ).attr( { scope : "col", role : "columnheader" } ), h( b.table ), b.debug && (console.log( "Built headers:" + q.benchmark( k ) ), console.log( b.$headers ))
        }

        function g ( a, b, e ) {
          var f = a.config;
          f.$table.find( f.selectorRemove ).remove(), c( f ), d( a ), o( f, b, e )
        }

        function h ( a ) {
          var b, c, d, e, f = a.config, g = f.$headers.length;
          for ( b = 0; g > b; b++ )d = f.$headers.eq( b ), e = q.getColumnData( a, f.headers, b, !0 ), c = "false" === q.getData( d, e, "sorter" ) || "false" === q.getData( d, e, "parser" ), d[ 0 ].sortDisabled = c, d[ c ? "addClass" : "removeClass" ]( "sorter-false" ).attr( "aria-disabled", "" + c ), a.id && (c ? d.removeAttr( "aria-controls" ) : d.attr( "aria-controls", a.id ))
        }

        function i ( b ) {
          var c, d, e, f, g, h, i, j, k = b.config, l = k.sortList, m = l.length, n = q.css.sortNone + " " + k.cssNone, o = [ q.css.sortAsc + " " + k.cssAsc, q.css.sortDesc + " " + k.cssDesc ], p = [ k.cssIconAsc, k.cssIconDesc, k.cssIconNone ], r = [ "ascending", "descending" ], s = a( b ).find( "tfoot tr" ).children().add( a( k.namespace + "_extra_headers" ) ).removeClass( o.join( " " ) );
          for ( k.$headers.removeClass( o.join( " " ) ).addClass( n ).attr( "aria-sort", "none" ).find( "." + q.css.icon ).removeClass( p.join( " " ) ).addClass( p[ 2 ] ), e = 0; m > e; e++ )if ( 2 !== l[ e ][ 1 ] && (c = k.$headers.not( ".sorter-false" ).filter( '[data-column="' + l[ e ][ 0 ] + '"]' + (1 === m ? ":last" : "") ), c.length) ) {
            for ( f = 0; f < c.length; f++ )c[ f ].sortDisabled || c.eq( f ).removeClass( n ).addClass( o[ l[ e ][ 1 ] ] ).attr( "aria-sort", r[ l[ e ][ 1 ] ] ).find( "." + q.css.icon ).removeClass( p[ 2 ] ).addClass( p[ l[ e ][ 1 ] ] );
            s.length && s.filter( '[data-column="' + l[ e ][ 0 ] + '"]' ).removeClass( n ).addClass( o[ l[ e ][ 1 ] ] )
          }
          for ( m = k.$headers.length, g = k.$headers.not( ".sorter-false" ), e = 0; m > e; e++ )h = g.eq( e ), h.length && (d = g[ e ], i = d.order[ (d.count + 1) % (k.sortReset ? 3 : 2) ], j = a.trim( h.text() ) + ": " + q.language[ h.hasClass( q.css.sortAsc ) ? "sortAsc" : h.hasClass( q.css.sortDesc ) ? "sortDesc" : "sortNone" ] + q.language[ 0 === i ? "nextAsc" : 1 === i ? "nextDesc" : "nextNone" ], h.attr( "aria-label", j ))
        }

        function j ( b, c ) {
          var d, e, f, g, h, i, j, k, l = b.config, m = c || l.sortList, n = m.length;
          for ( l.sortList = [], h = 0; n > h; h++ )if ( k = m[ h ], d = parseInt( k[ 0 ], 10 ), d < l.columns && l.$headerIndexed[ d ] ) {
            switch ( g = l.$headerIndexed[ d ][ 0 ], e = ("" + k[ 1 ]).match( /^(1|d|s|o|n)/ ), e = e ? e[ 0 ] : "" ) {
              case"1":
              case"d":
                e = 1;
                break;
              case"s":
                e = i || 0;
                break;
              case"o":
                j = g.order[ (i || 0) % (l.sortReset ? 3 : 2) ], e = 0 === j ? 1 : 1 === j ? 0 : 2;
                break;
              case"n":
                g.count = g.count + 1, e = g.order[ g.count % (l.sortReset ? 3 : 2) ];
                break;
              default:
                e = 0
            }
            i = 0 === h ? e : i, f = [ d, parseInt( e, 10 ) || 0 ], l.sortList.push( f ), e = a.inArray( f[ 1 ], g.order ), g.count = e >= 0 ? e : f[ 1 ] % (l.sortReset ? 3 : 2)
          }
        }

        function k ( a, b ) {return a && a[ b ] ? a[ b ].type || "" : ""}

        function l ( b, c, d ) {
          if ( b.isUpdating )return setTimeout( function () {l( b, c, d )}, 50 );
          var e, f, g, h, j, k, n, o = b.config, p = !d[ o.sortMultiSortKey ], r = o.$table, s = o.$headers.length;
          if ( r.trigger( "sortStart", b ), c.count = d[ o.sortResetKey ] ? 2 : (c.count + 1) % (o.sortReset ? 3 : 2), o.sortRestart )for ( f = c, g = 0; s > g; g++ )n = o.$headers.eq( g ), n[ 0 ] === f || !p && n.is( "." + q.css.sortDesc + ",." + q.css.sortAsc ) || (n[ 0 ].count = -1);
          if ( f = parseInt( a( c ).attr( "data-column" ), 10 ), p ) {
            if ( o.sortList = [], null !== o.sortForce )for ( e = o.sortForce, h = 0; h < e.length; h++ )e[ h ][ 0 ] !== f && o.sortList.push( e[ h ] );
            if ( j = c.order[ c.count ], 2 > j && (o.sortList.push( [ f, j ] ), c.colSpan > 1) )for ( h = 1; h < c.colSpan; h++ )o.sortList.push( [ f + h, j ] )
          } else {
            if ( o.sortAppend && o.sortList.length > 1 )for ( h = 0; h < o.sortAppend.length; h++ )k = q.isValueInArray( o.sortAppend[ h ][ 0 ], o.sortList ), k >= 0 && o.sortList.splice( k, 1 );
            if ( q.isValueInArray( f, o.sortList ) >= 0 )for ( h = 0; h < o.sortList.length; h++ )k = o.sortList[ h ], j = o.$headerIndexed[ k[ 0 ] ][ 0 ], k[ 0 ] === f && (k[ 1 ] = j.order[ c.count ], 2 === k[ 1 ] && (o.sortList.splice( h, 1 ), j.count = -1)); else if ( j = c.order[ c.count ], 2 > j && (o.sortList.push( [ f, j ] ), c.colSpan > 1) )for ( h = 1; h < c.colSpan; h++ )o.sortList.push( [ f + h, j ] )
          }
          if ( null !== o.sortAppend )for ( e = o.sortAppend, h = 0; h < e.length; h++ )e[ h ][ 0 ] !== f && o.sortList.push( e[ h ] );
          r.trigger( "sortBegin", b ), setTimeout( function () {i( b ), m( b ), q.appendCache( o ), r.trigger( "sortEnd", b )}, 1 )
        }

        function m ( a ) {
          var b, c, d, e, f, g, h, i, j, l, m, n = 0, o = a.config, p = o.textSorter || "", r = o.sortList, s = r.length, t = o.$tbodies.length;
          if ( !o.serverSideSorting && !q.isEmptyObject( o.cache ) ) {
            for ( o.debug && (f = new Date), c = 0; t > c; c++ )g = o.cache[ c ].colMax, h = o.cache[ c ].normalized, h.sort( function ( c, f ) {
              for ( b = 0; s > b; b++ ) {
                if ( e = r[ b ][ 0 ], i = r[ b ][ 1 ], n = 0 === i, o.sortStable && c[ e ] === f[ e ] && 1 === s )return c[ o.columns ].order - f[ o.columns ].order;
                if ( d = /n/i.test( k( o.parsers, e ) ), d && o.strings[ e ] ? (d = "boolean" == typeof o.string[ o.strings[ e ] ] ? (n ? 1 : -1) * (o.string[ o.strings[ e ] ] ? -1 : 1) : o.strings[ e ] ? o.string[ o.strings[ e ] ] || 0 : 0, j = o.numberSorter ? o.numberSorter( c[ e ], f[ e ], n, g[ e ], a ) : q[ "sortNumeric" + (n ? "Asc" : "Desc") ]( c[ e ], f[ e ], d, g[ e ], e, a )) : (l = n ? c : f, m = n ? f : c, j = "function" == typeof p ? p( l[ e ], m[ e ], n, e, a ) : "object" == typeof p && p.hasOwnProperty( e ) ? p[ e ]( l[ e ], m[ e ], n, e, a ) : q[ "sortNatural" + (n ? "Asc" : "Desc") ]( c[ e ], f[ e ], e, a, o )), j )return j
              }
              return c[ o.columns ].order - f[ o.columns ].order
            } );
            o.debug && console.log( "Sorting on " + r.toString() + " and dir " + i + " time" + q.benchmark( f ) )
          }
        }

        function n ( b, c ) {b.table.isUpdating && b.$table.trigger( "updateComplete", b.table ), a.isFunction( c ) && c( b.table )}

        function o ( b, c, d ) {
          var e = a.isArray( c ) ? c : b.sortList, f = "undefined" == typeof c ? b.resort : c;
          f === !1 || b.serverSideSorting || b.table.isProcessing ? (n( b, d ), q.applyWidget( b.table, !1 )) : e.length ? b.$table.trigger( "sorton", [ e, function () {n( b, d )}, !0 ] ) : b.$table.trigger( "sortReset", [ function () {n( b, d ), q.applyWidget( b.table, !1 )} ] )
        }

        function p ( b ) {
          var c = b.config, d = c.$table, e = "sortReset update updateRows updateAll updateHeaders addRows updateCell updateComplete sorton appendCache updateCache applyWidgetId applyWidgets refreshWidgets destroy mouseup mouseleave ".split( " " ).join( c.namespace + " " );
          d.unbind( e.replace( /\s+/g, " " ) ).bind( "sortReset" + c.namespace, function ( a, b ) {a.stopPropagation(), q.sortReset( this.config, b )} ).bind( "updateAll" + c.namespace, function ( a, b, c ) {a.stopPropagation(), q.updateAll( this.config, b, c )} ).bind( "update" + c.namespace + " updateRows" + c.namespace, function ( a, b, c ) {a.stopPropagation(), q.update( this.config, b, c )} ).bind( "updateHeaders" + c.namespace, function ( a, b ) {a.stopPropagation(), q.updateHeaders( this.config, b )} ).bind( "updateCell" + c.namespace, function ( a, b, c, d ) {a.stopPropagation(), q.updateCell( this.config, b, c, d )} ).bind( "addRows" + c.namespace, function ( a, b, c, d ) {a.stopPropagation(), q.addRows( this.config, b, c, d )} ).bind( "updateComplete" + c.namespace, function () {b.isUpdating = !1} ).bind( "sorton" + c.namespace, function ( a, b, c, d ) {a.stopPropagation(), q.sortOn( this.config, b, c, d )} ).bind( "appendCache" + c.namespace, function ( c, d, e ) {c.stopPropagation(), q.appendCache( this.config, e ), a.isFunction( d ) && d( b )} ).bind( "updateCache" + c.namespace, function ( a, b, c ) {a.stopPropagation(), q.updateCache( this.config, b, c )} ).bind( "applyWidgetId" + c.namespace, function ( a, c ) {a.stopPropagation(), q.getWidgetById( c ).format( b, this.config, this.config.widgetOptions )} ).bind( "applyWidgets" + c.namespace, function ( a, c ) {a.stopPropagation(), q.applyWidget( b, c )} ).bind( "refreshWidgets" + c.namespace, function ( a, c, d ) {a.stopPropagation(), q.refreshWidgets( b, c, d )} ).bind( "destroy" + c.namespace, function ( a, c, d ) {a.stopPropagation(), q.destroy( b, c, d )} ).bind( "resetToLoadState" + c.namespace, function ( d ) {d.stopPropagation(), q.removeWidget( b, !0, !1 ), c = a.extend( !0, q.defaults, c.originalSettings ), b.hasInitialized = !1, q.setup( b, c )} )
        }

        var q = this;
        q.version = "2.23.0", q.parsers = [], q.widgets = [], q.defaults = { theme : "default", widthFixed : !1, showProcessing : !1, headerTemplate : "{content}", onRenderTemplate : null, onRenderHeader : null, cancelSelection : !0, tabIndex : !0, dateFormat : "mmddyyyy", sortMultiSortKey : "shiftKey", sortResetKey : "ctrlKey", usNumberFormat : !0, delayInit : !1, serverSideSorting : !1, resort : !0, headers : {}, ignoreCase : !0, sortForce : null, sortList : [], sortAppend : null, sortStable : !1, sortInitialOrder : "asc", sortLocaleCompare : !1, sortReset : !1, sortRestart : !1, emptyTo : "bottom", stringTo : "max", textExtraction : "basic", textAttribute : "data-text", textSorter : null, numberSorter : null, widgets : [], widgetOptions : { zebra : [ "even", "odd" ] }, initWidgets : !0, widgetClass : "widget-{name}", initialized : null, tableClass : "", cssAsc : "", cssDesc : "", cssNone : "", cssHeader : "", cssHeaderRow : "", cssProcessing : "", cssChildRow : "tablesorter-childRow", cssIcon : "tablesorter-icon", cssIconNone : "", cssIconAsc : "", cssIconDesc : "", cssInfoBlock : "tablesorter-infoOnly", cssNoSort : "tablesorter-noSort", cssIgnoreRow : "tablesorter-ignoreRow", pointerClick : "click", pointerDown : "mousedown", pointerUp : "mouseup", selectorHeaders : "> thead th, > thead td", selectorSort : "th, td", selectorRemove : ".remove-me", debug : !1, headerList : [], empties : {}, strings : {}, parsers : [] }, q.css = { table : "tablesorter", cssHasChild : "tablesorter-hasChildRow", childRow : "tablesorter-childRow", colgroup : "tablesorter-colgroup", header : "tablesorter-header", headerRow : "tablesorter-headerRow", headerIn : "tablesorter-header-inner", icon : "tablesorter-icon", processing : "tablesorter-processing", sortAsc : "tablesorter-headerAsc", sortDesc : "tablesorter-headerDesc", sortNone : "tablesorter-headerUnSorted" }, q.language = { sortAsc : "Ascending sort applied, ", sortDesc : "Descending sort applied, ", sortNone : "No sort applied, ", nextAsc : "activate to apply an ascending sort", nextDesc : "activate to apply a descending sort", nextNone : "activate to remove the sort" }, q.instanceMethods = {}, q.isEmptyObject = function ( a ) {
          for ( var b in a )return !1;
          return !0
        }, q.getElementText = function ( b, c, d ) {
          if ( !c )return "";
          var e, f = b.textExtraction || "", g = c.jquery ? c : a( c );
          return "string" == typeof f ? "basic" === f && "undefined" != typeof(e = g.attr( b.textAttribute )) ? a.trim( e ) : a.trim( c.textContent || g.text() ) : "function" == typeof f ? a.trim( f( g[ 0 ], b.table, d ) ) : "function" == typeof(e = q.getColumnData( b.table, f, d )) ? a.trim( e( g[ 0 ], b.table, d ) ) : a.trim( g[ 0 ].textContent || g.text() )
        }, q.getParsedText = function ( a, b, c, d ) {
          "undefined" == typeof d && (d = q.getElementText( a, b, c ));
          var e = "" + d, f = a.parsers[ c ], g = a.extractors[ c ];
          return f && (g && "function" == typeof g.format && (d = g.format( d, a.table, b, c )), e = "no-parser" === f.id ? "" : f.format( "" + d, a.table, b, c ), a.ignoreCase && "string" == typeof e && (e = e.toLowerCase())), e
        }, q.construct = function ( b ) {
          return this.each( function () {
            var c = this, d = a.extend( !0, {}, q.defaults, b, q.instanceMethods );
            d.originalSettings = b, !c.hasInitialized && q.buildTable && "TABLE" !== this.nodeName ? q.buildTable( c, d ) : q.setup( c, d )
          } )
        }, q.setup = function ( b, e ) {
          if ( !b || !b.tHead || 0 === b.tBodies.length || b.hasInitialized === !0 )return void(e.debug && (b.hasInitialized ? console.warn( "Stopping initialization. Tablesorter has already been initialized" ) : console.error( "Stopping initialization! No table, thead or tbody" )));
          var g = "", h = a( b ), j = a.metadata;
          b.hasInitialized = !1, b.isProcessing = !0, b.config = e, a.data( b, "tablesorter", e ), e.debug && (console[ console.group ? "group" : "log" ]( "Initializing tablesorter" ), a.data( b, "startoveralltimer", new Date )), e.supportsDataObject = function ( a ) {return a[ 0 ] = parseInt( a[ 0 ], 10 ), a[ 0 ] > 1 || 1 === a[ 0 ] && parseInt( a[ 1 ], 10 ) >= 4}( a.fn.jquery.split( "." ) ), e.string = { max : 1, min : -1, emptymin : 1, emptymax : -1, zero : 0, none : 0, "null" : 0, top : !0, bottom : !1 }, e.emptyTo = e.emptyTo.toLowerCase(), e.stringTo = e.stringTo.toLowerCase(), /tablesorter\-/.test( h.attr( "class" ) ) || (g = "" !== e.theme ? " tablesorter-" + e.theme : ""), e.table = b, e.$table = h.addClass( q.css.table + " " + e.tableClass + g ).attr( "role", "grid" ), e.$headers = h.find( e.selectorHeaders ), e.namespace ? e.namespace = "." + e.namespace.replace( /\W/g, "" ) : e.namespace = ".tablesorter" + Math.random().toString( 16 ).slice( 2 ), e.$table.children().children( "tr" ).attr( "role", "row" ), e.$tbodies = h.children( "tbody:not(." + e.cssInfoBlock + ")" ).attr( { "aria-live" : "polite", "aria-relevant" : "all" } ), e.$table.children( "caption" ).length && (g = e.$table.children( "caption" )[ 0 ], g.id || (g.id = e.namespace.slice( 1 ) + "caption"), e.$table.attr( "aria-labelledby", g.id )), e.widgetInit = {}, e.textExtraction = e.$table.attr( "data-text-extraction" ) || e.textExtraction || "basic", f( e ), q.fixColumnWidth( b ), q.applyWidgetOptions( b, e ), c( e ), e.totalRows = 0, e.delayInit || d( b ), q.bindEvents( b, e.$headers, !0 ), p( b ), e.supportsDataObject && "undefined" != typeof h.data().sortlist ? e.sortList = h.data().sortlist : j && h.metadata() && h.metadata().sortlist && (e.sortList = h.metadata().sortlist), q.applyWidget( b, !0 ), e.sortList.length > 0 ? h.trigger( "sorton", [ e.sortList, {}, !e.initWidgets, !0 ] ) : (i( b ), e.initWidgets && q.applyWidget( b, !1 )), e.showProcessing && h.unbind( "sortBegin" + e.namespace + " sortEnd" + e.namespace ).bind( "sortBegin" + e.namespace + " sortEnd" + e.namespace, function ( a ) {clearTimeout( e.processTimer ), q.isProcessing( b ), "sortBegin" === a.type && (e.processTimer = setTimeout( function () {q.isProcessing( b, !0 )}, 500 ))} ), b.hasInitialized = !0, b.isProcessing = !1, e.debug && (console.log( "Overall initialization time: " + q.benchmark( a.data( b, "startoveralltimer" ) ) ), e.debug && console.groupEnd && console.groupEnd()), h.trigger( "tablesorter-initialized", b ), "function" == typeof e.initialized && e.initialized( b )
        }, q.fixColumnWidth = function ( b ) {
          b = a( b )[ 0 ];
          var c, d, e, f, g, h = b.config, i = h.$table.children( "colgroup" );
          if ( i.length && i.hasClass( q.css.colgroup ) && i.remove(), h.widthFixed && 0 === h.$table.children( "colgroup" ).length ) {
            for ( i = a( '<colgroup class="' + q.css.colgroup + '">' ), c = h.$table.width(), e = h.$tbodies.find( "tr:first" ).children( ":visible" ), f = e.length, g = 0; f > g; g++ )d = parseInt( e.eq( g ).width() / c * 1e3, 10 ) / 10 + "%", i.append( a( "<col>" ).css( "width", d ) );
            h.$table.prepend( i )
          }
        }, q.getColumnData = function ( b, c, d, e, f ) {
          if ( "undefined" != typeof c && null !== c ) {
            b = a( b )[ 0 ];
            var g, h, i = b.config, j = f || i.$headers, k = i.$headerIndexed && i.$headerIndexed[ d ] || j.filter( '[data-column="' + d + '"]:last' );
            if ( c[ d ] )return e ? c[ d ] : c[ j.index( k ) ];
            for ( h in c )if ( "string" == typeof h && (g = k.filter( h ).add( k.find( h ) ), g.length) )return c[ h ]
          }
        }, q.computeColumnIndex = function ( b ) {
          var c, d, e, f, g, h, i, j, k, l, m, n, o = [], p = [], q = {};
          for ( c = 0; c < b.length; c++ )for ( i = b[ c ].cells, d = 0; d < i.length; d++ ) {
            for ( h = i[ d ], g = a( h ), j = h.parentNode.rowIndex, k = j + "-" + g.index(), l = h.rowSpan || 1, m = h.colSpan || 1, "undefined" == typeof o[ j ] && (o[ j ] = []), e = 0; e < o[ j ].length + 1; e++ )if ( "undefined" == typeof o[ j ][ e ] ) {
              n = e;
              break
            }
            for ( q[ k ] = n, g.attr( { "data-column" : n } ), e = j; j + l > e; e++ )for ( "undefined" == typeof o[ e ] && (o[ e ] = []), p = o[ e ], f = n; n + m > f; f++ )p[ f ] = "x"
          }
          return p.length
        }, q.isProcessing = function ( b, c, d ) {
          b = a( b );
          var e = b[ 0 ].config, f = d || b.find( "." + q.css.header );
          c ? ("undefined" != typeof d && e.sortList.length > 0 && (f = f.filter( function () {return this.sortDisabled ? !1 : q.isValueInArray( parseFloat( a( this ).attr( "data-column" ) ), e.sortList ) >= 0} )), b.add( f ).addClass( q.css.processing + " " + e.cssProcessing )) : b.add( f ).removeClass( q.css.processing + " " + e.cssProcessing )
        }, q.processTbody = function ( b, c, d ) {
          b = a( b )[ 0 ];
          var e;
          return d ? (b.isProcessing = !0, c.before( '<colgroup class="tablesorter-savemyplace"/>' ), e = a.fn.detach ? c.detach() : c.remove()) : (e = a( b ).find( "colgroup.tablesorter-savemyplace" ), c.insertAfter( e ), e.remove(), void(b.isProcessing = !1))
        }, q.clearTableBody = function ( b ) {a( b )[ 0 ].config.$tbodies.children().detach()}, q.bindEvents = function ( b, c, e ) {
          b = a( b )[ 0 ];
          var f, g = null, h = b.config;
          e !== !0 && (c.addClass( h.namespace.slice( 1 ) + "_extra_headers" ), f = a.fn.closest ? c.closest( "table" )[ 0 ] : c.parents( "table" )[ 0 ], f && "TABLE" === f.nodeName && f !== b && a( f ).addClass( h.namespace.slice( 1 ) + "_extra_table" )), f = (h.pointerDown + " " + h.pointerUp + " " + h.pointerClick + " sort keyup ").replace( /\s+/g, " " ).split( " " ).join( h.namespace + " " ), c.find( h.selectorSort ).add( c.filter( h.selectorSort ) ).unbind( f ).bind( f, function ( c, e ) {
            var f, i, j, k = a( c.target ), m = " " + c.type + " ";
            if ( !(1 !== (c.which || c.button) && !m.match( " " + h.pointerClick + " | sort | keyup " ) || " keyup " === m && 13 !== c.which || m.match( " " + h.pointerClick + " " ) && "undefined" != typeof c.which || m.match( " " + h.pointerUp + " " ) && g !== c.target && e !== !0) ) {
              if ( m.match( " " + h.pointerDown + " " ) )return g = c.target, j = k.jquery.split( "." ), void("1" === j[ 0 ] && j[ 1 ] < 4 && c.preventDefault());
              if ( g = null, /(input|select|button|textarea)/i.test( c.target.nodeName ) || k.hasClass( h.cssNoSort ) || k.parents( "." + h.cssNoSort ).length > 0 || k.parents( "button" ).length > 0 )return !h.cancelSelection;
              h.delayInit && q.isEmptyObject( h.cache ) && d( b ), f = a.fn.closest ? a( this ).closest( "th, td" ) : /TH|TD/.test( this.nodeName ) ? a( this ) : a( this ).parents( "th, td" ), i = h.$headers[ f.prevAll().length ], i && !i.sortDisabled && l( b, i, c )
            }
          } ), h.cancelSelection && c.attr( "unselectable", "on" ).bind( "selectstart", !1 ).css( { "user-select" : "none", MozUserSelect : "none" } )
        }, q.sortReset = function ( b, c ) {
          var d = b.table;
          b.sortList = [], i( d ), m( d ), q.appendCache( b ), a.isFunction( c ) && c( d )
        }, q.updateAll = function ( a, b, c ) {
          var d = a.table;
          d.isUpdating = !0, q.refreshWidgets( d, !0, !0 ), f( a ), q.bindEvents( d, a.$headers, !0 ), p( d ), g( d, b, c )
        }, q.update = function ( a, b, c ) {
          var d = a.table;
          d.isUpdating = !0, h( d ), g( d, b, c )
        }, q.updateHeaders = function ( a, b ) {a.table.isUpdating = !0, f( a ), q.bindEvents( a.table, a.$headers, !0 ), n( a, b )}, q.updateCell = function ( b, c, d, e ) {
          b.table.isUpdating = !0, b.$table.find( b.selectorRemove ).remove();
          var f, g, h, i, j = (b.table, b.$tbodies), k = a( c ), l = j.index( a.fn.closest ? k.closest( "tbody" ) : k.parents( "tbody" ).filter( ":first" ) ), m = b.cache[ l ], p = a.fn.closest ? k.closest( "tr" ) : k.parents( "tr" ).filter( ":first" );
          c = k[ 0 ], j.length && l >= 0 && (g = j.eq( l ).find( "tr" ).index( p ), i = m.normalized[ g ], h = k.index(), f = q.getParsedText( b, c, h ), i[ h ] = f, i[ b.columns ].$row = p, "numeric" === (b.parsers[ h ].type || "").toLowerCase() && (m.colMax[ h ] = Math.max( Math.abs( f ) || 0, m.colMax[ h ] || 0 )), f = "undefined" !== d ? d : b.resort, f !== !1 ? o( b, f, e ) : n( b, e ))
        }, q.addRows = function ( b, d, e, f ) {
          var i, j, k, l, m, n, p, r = "string" == typeof d && 1 === b.$tbodies.length && /<tr/.test( d || "" ), s = b.table;
          if ( r )d = a( d ), b.$tbodies.append( d ); else if ( !(d && d instanceof jQuery && (a.fn.closest ? d.closest( "table" )[ 0 ] : d.parents( "table" )[ 0 ]) === b.table) )return b.debug && console.error( "addRows method requires (1) a jQuery selector reference to rows that have already been added to the table, or (2) row HTML string to be added to a table with only one tbody" ), !1;
          if ( s.isUpdating = !0, q.isEmptyObject( b.cache ) )h( s ), g( s, e, f ); else {
            for ( n = d.filter( "tr" ).attr( "role", "row" ).length, p = b.$tbodies.index( d.parents( "tbody" ).filter( ":first" ) ), b.parsers && b.parsers.length || c( b ), i = 0; n > i; i++ ) {
              for ( k = d[ i ].cells.length, m = [], l = { child : [], $row : d.eq( i ), order : b.cache[ p ].normalized.length }, j = 0; k > j; j++ )m[ j ] = q.getParsedText( b, d[ i ].cells[ j ], j ), "numeric" === (b.parsers[ j ].type || "").toLowerCase() && (b.cache[ p ].colMax[ j ] = Math.max( Math.abs( m[ j ] ) || 0, b.cache[ p ].colMax[ j ] || 0 ));
              m.push( l ), b.cache[ p ].normalized.push( m )
            }
            o( b, e, f )
          }
        }, q.updateCache = function ( a, b, e ) {a.parsers && a.parsers.length || c( a, e ), d( a.table, b, e )}, q.appendCache = function ( a, b ) {
          var c, d, e, f, g, h, i, j = a.table, k = a.widgetOptions, l = a.$tbodies, m = [], n = a.cache;
          if ( q.isEmptyObject( n ) )return a.appender ? a.appender( j, m ) : j.isUpdating ? a.$table.trigger( "updateComplete", j ) : "";
          for ( a.debug && (i = new Date), h = 0; h < l.length; h++ )if ( e = l.eq( h ), e.length ) {
            for ( f = q.processTbody( j, e, !0 ), c = n[ h ].normalized, d = c.length, g = 0; d > g; g++ )m.push( c[ g ][ a.columns ].$row ), a.appender && (!a.pager || a.pager.removeRows && k.pager_removeRows || a.pager.ajax) || f.append( c[ g ][ a.columns ].$row );
            q.processTbody( j, f, !1 )
          }
          a.appender && a.appender( j, m ), a.debug && console.log( "Rebuilt table" + q.benchmark( i ) ), b || a.appender || q.applyWidget( j ), j.isUpdating && a.$table.trigger( "updateComplete", j )
        }, q.sortOn = function ( b, c, e, f ) {
          var g = b.table;
          b.$table.trigger( "sortStart", g ), j( g, c ), i( g ), b.delayInit && q.isEmptyObject( b.cache ) && d( g ), b.$table.trigger( "sortBegin", g ), m( g ), q.appendCache( b, f ), b.$table.trigger( "sortEnd", g ), q.applyWidget( g ), a.isFunction( e ) && e( g )
        }, q.restoreHeaders = function ( b ) {
          var c, d, e = a( b )[ 0 ].config, f = e.$table.find( e.selectorHeaders ), g = f.length;
          for ( c = 0; g > c; c++ )d = f.eq( c ), d.find( "." + q.css.headerIn ).length && d.html( e.headerContent[ c ] )
        }, q.destroy = function ( b, c, d ) {
          if ( b = a( b )[ 0 ], b.hasInitialized ) {
            q.removeWidget( b, !0, !1 );
            var e, f = a( b ), g = b.config, h = g.debug, i = f.find( "thead:first" ), j = i.find( "tr." + q.css.headerRow ).removeClass( q.css.headerRow + " " + g.cssHeaderRow ), k = f.find( "tfoot:first > tr" ).children( "th, td" );
            c === !1 && a.inArray( "uitheme", g.widgets ) >= 0 && (f.trigger( "applyWidgetId", [ "uitheme" ] ), f.trigger( "applyWidgetId", [ "zebra" ] )), i.find( "tr" ).not( j ).remove(), e = "sortReset update updateRows updateAll updateHeaders updateCell addRows updateComplete sorton appendCache updateCache applyWidgetId applyWidgets refreshWidgets destroy mouseup mouseleave keypress " + "sortBegin sortEnd resetToLoadState ".split( " " ).join( g.namespace + " " ), f.removeData( "tablesorter" ).unbind( e.replace( /\s+/g, " " ) ), g.$headers.add( k ).removeClass( [ q.css.header, g.cssHeader, g.cssAsc, g.cssDesc, q.css.sortAsc, q.css.sortDesc, q.css.sortNone ].join( " " ) ).removeAttr( "data-column" ).removeAttr( "aria-label" ).attr( "aria-disabled", "true" ), j.find( g.selectorSort ).unbind( "mousedown mouseup keypress ".split( " " ).join( g.namespace + " " ).replace( /\s+/g, " " ) ), q.restoreHeaders( b ), f.toggleClass( q.css.table + " " + g.tableClass + " tablesorter-" + g.theme, c === !1 ), b.hasInitialized = !1, delete b.config.cache, "function" == typeof d && d( b ), h && console.log( "tablesorter has been removed" )
          }
        }, q.regex = { chunk : /(^([+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?)?$|^0x[0-9a-f]+$|\d+)/gi, chunks : /(^\\0|\\0$)/, hex : /^0x[0-9a-f]+$/i }, q.sortNatural = function ( a, b ) {
          if ( a === b )return 0;
          var c, d, e, f, g, h, i, j, k = q.regex;
          if ( k.hex.test( b ) ) {
            if ( d = parseInt( a.match( k.hex ), 16 ), f = parseInt( b.match( k.hex ), 16 ), f > d )return -1;
            if ( d > f )return 1
          }
          for ( c = a.replace( k.chunk, "\\0$1\\0" ).replace( k.chunks, "" ).split( "\\0" ), e = b.replace( k.chunk, "\\0$1\\0" ).replace( k.chunks, "" ).split( "\\0" ), j = Math.max( c.length, e.length ), i = 0; j > i; i++ ) {
            if ( g = isNaN( c[ i ] ) ? c[ i ] || 0 : parseFloat( c[ i ] ) || 0, h = isNaN( e[ i ] ) ? e[ i ] || 0 : parseFloat( e[ i ] ) || 0, isNaN( g ) !== isNaN( h ) )return isNaN( g ) ? 1 : -1;
            if ( typeof g != typeof h && (g += "", h += ""), h > g )return -1;
            if ( g > h )return 1
          }
          return 0
        }, q.sortNaturalAsc = function ( a, b, c, d, e ) {
          if ( a === b )return 0;
          var f = e.string[ e.empties[ c ] || e.emptyTo ];
          return "" === a && 0 !== f ? "boolean" == typeof f ? f ? -1 : 1 : -f || -1 : "" === b && 0 !== f ? "boolean" == typeof f ? f ? 1 : -1 : f || 1 : q.sortNatural( a, b )
        }, q.sortNaturalDesc = function ( a, b, c, d, e ) {
          if ( a === b )return 0;
          var f = e.string[ e.empties[ c ] || e.emptyTo ];
          return "" === a && 0 !== f ? "boolean" == typeof f ? f ? -1 : 1 : f || 1 : "" === b && 0 !== f ? "boolean" == typeof f ? f ? 1 : -1 : -f || -1 : q.sortNatural( b, a )
        }, q.sortText = function ( a, b ) {return a > b ? 1 : b > a ? -1 : 0}, q.getTextValue = function ( a, b, c ) {
          if ( c ) {
            var d, e = a ? a.length : 0, f = c + b;
            for ( d = 0; e > d; d++ )f += a.charCodeAt( d );
            return b * f
          }
          return 0
        }, q.sortNumericAsc = function ( a, b, c, d, e, f ) {
          if ( a === b )return 0;
          var g = f.config, h = g.string[ g.empties[ e ] || g.emptyTo ];
          return "" === a && 0 !== h ? "boolean" == typeof h ? h ? -1 : 1 : -h || -1 : "" === b && 0 !== h ? "boolean" == typeof h ? h ? 1 : -1 : h || 1 : (isNaN( a ) && (a = q.getTextValue( a, c, d )), isNaN( b ) && (b = q.getTextValue( b, c, d )), a - b)
        }, q.sortNumericDesc = function ( a, b, c, d, e, f ) {
          if ( a === b )return 0;
          var g = f.config, h = g.string[ g.empties[ e ] || g.emptyTo ];
          return "" === a && 0 !== h ? "boolean" == typeof h ? h ? -1 : 1 : h || 1 : "" === b && 0 !== h ? "boolean" == typeof h ? h ? 1 : -1 : -h || -1 : (isNaN( a ) && (a = q.getTextValue( a, c, d )), isNaN( b ) && (b = q.getTextValue( b, c, d )), b - a)
        }, q.sortNumeric = function ( a, b ) {return a - b}, q.characterEquivalents = { a : "áàâãäąå", A : "ÁÀÂÃÄĄÅ", c : "çćč", C : "ÇĆČ", e : "éèêëěę", E : "ÉÈÊËĚĘ", i : "íìİîïı", I : "ÍÌİÎÏ", o : "óòôõöō", O : "ÓÒÔÕÖŌ", ss : "ß", SS : "ẞ", u : "úùûüů", U : "ÚÙÛÜŮ" }, q.replaceAccents = function ( a ) {
          var b, c = "[", d = q.characterEquivalents;
          if ( !q.characterRegex ) {
            q.characterRegexArray = {};
            for ( b in d )"string" == typeof b && (c += d[ b ], q.characterRegexArray[ b ] = new RegExp( "[" + d[ b ] + "]", "g" ));
            q.characterRegex = new RegExp( c + "]" )
          }
          if ( q.characterRegex.test( a ) )for ( b in d )"string" == typeof b && (a = a.replace( q.characterRegexArray[ b ], b ));
          return a
        }, q.isValueInArray = function ( a, b ) {
          var c, d = b && b.length || 0;
          for ( c = 0; d > c; c++ )if ( b[ c ][ 0 ] === a )return c;
          return -1
        }, q.addParser = function ( a ) {
          var b, c = q.parsers.length, d = !0;
          for ( b = 0; c > b; b++ )q.parsers[ b ].id.toLowerCase() === a.id.toLowerCase() && (d = !1);
          d && q.parsers.push( a )
        }, q.addInstanceMethods = function ( b ) {a.extend( q.instanceMethods, b )}, q.getParserById = function ( a ) {
          if ( "false" == a )return !1;
          var b, c = q.parsers.length;
          for ( b = 0; c > b; b++ )if ( q.parsers[ b ].id.toLowerCase() === a.toString().toLowerCase() )return q.parsers[ b ];
          return !1
        }, q.addWidget = function ( a ) {q.widgets.push( a )}, q.hasWidget = function ( b, c ) {return b = a( b ), b.length && b[ 0 ].config && b[ 0 ].config.widgetInit[ c ] || !1}, q.getWidgetById = function ( a ) {
          var b, c, d = q.widgets.length;
          for ( b = 0; d > b; b++ )if ( c = q.widgets[ b ], c && c.hasOwnProperty( "id" ) && c.id.toLowerCase() === a.toLowerCase() )return c
        }, q.applyWidgetOptions = function ( b, c ) {
          var d, e, f = c.widgets.length, g = c.widgetOptions;
          if ( f )for ( d = 0; f > d; d++ )e = q.getWidgetById( c.widgets[ d ] ), e && "options"in e && (g = b.config.widgetOptions = a.extend( !0, {}, e.options, g ))
        }, q.applyWidget = function ( b, c, d ) {
          b = a( b )[ 0 ];
          var e, f, g, h, i, j, k, l, m, n, o = b.config, p = o.widgetOptions, r = " " + o.table.className + " ", s = [];
          if ( c === !1 || !b.hasInitialized || !b.isApplyingWidgets && !b.isUpdating ) {
            if ( o.debug && (k = new Date), n = new RegExp( "\\s" + o.widgetClass.replace( /\{name\}/i, "([\\w-]+)" ) + "\\s", "g" ), r.match( n ) && (m = r.match( n )) )for ( f = m.length, e = 0; f > e; e++ )o.widgets.push( m[ e ].replace( n, "$1" ) );
            if ( o.widgets.length ) {
              for ( b.isApplyingWidgets = !0, o.widgets = a.grep( o.widgets, function ( b, c ) {return a.inArray( b, o.widgets ) === c} ), g = o.widgets || [], f = g.length, e = 0; f > e; e++ )n = q.getWidgetById( g[ e ] ), n && n.id && (n.priority || (n.priority = 10), s[ e ] = n);
              for ( s.sort( function ( a, b ) {return a.priority < b.priority ? -1 : a.priority === b.priority ? 0 : 1} ), f = s.length, o.debug && console[ console.group ? "group" : "log" ]( "Start " + (c ? "initializing" : "applying") + " widgets" ), e = 0; f > e; e++ )h = s[ e ], h && (i = h.id, j = !1, o.debug && (l = new Date), (c || !o.widgetInit[ i ]) && (o.widgetInit[ i ] = !0, b.hasInitialized && q.applyWidgetOptions( b, o ), "init"in h && (j = !0, o.debug && console[ console.group ? "group" : "log" ]( "Initializing " + i + " widget" ), h.init( b, h, o, p ))), !c && "format"in h && (j = !0, o.debug && console[ console.group ? "group" : "log" ]( "Updating " + i + " widget" ), h.format( b, o, p, !1 )), o.debug && j && (console.log( "Completed " + (c ? "initializing " : "applying ") + i + " widget" + q.benchmark( l ) ), console.groupEnd && console.groupEnd()));
              o.debug && console.groupEnd && console.groupEnd(), c || "function" != typeof d || d( b )
            }
            setTimeout( function () {b.isApplyingWidgets = !1, a.data( b, "lastWidgetApplication", new Date )}, 0 ), o.debug && (m = o.widgets.length, console.log( "Completed " + (c === !0 ? "initializing " : "applying ") + m + " widget" + (1 !== m ? "s" : "") + q.benchmark( k ) ))
          }
        }, q.removeWidget = function ( b, c, d ) {
          b = a( b )[ 0 ];
          var e, f, g, h, i = b.config;
          if ( c === !0 )for ( c = [], h = q.widgets.length, g = 0; h > g; g++ )f = q.widgets[ g ], f && f.id && c.push( f.id ); else c = (a.isArray( c ) ? c.join( "," ) : c || "").toLowerCase().split( /[\s,]+/ );
          for ( h = c.length, e = 0; h > e; e++ )f = q.getWidgetById( c[ e ] ), g = a.inArray( c[ e ], i.widgets ), f && "remove"in f && (i.debug && g >= 0 && console.log( 'Removing "' + c[ e ] + '" widget' ), i.debug && console.log( (d ? "Refreshing" : "Removing") + ' "' + c[ e ] + '" widget' ), f.remove( b, i, i.widgetOptions, d ), i.widgetInit[ c[ e ] ] = !1), g >= 0 && d !== !0 && i.widgets.splice( g, 1 )
        }, q.refreshWidgets = function ( b, c, d ) {
          b = a( b )[ 0 ];
          var e, f = b.config, g = f.widgets, h = q.widgets, i = h.length, j = [], k = function ( b ) {a( b ).trigger( "refreshComplete" )};
          for ( e = 0; i > e; e++ )h[ e ] && h[ e ].id && (c || a.inArray( h[ e ].id, g ) < 0) && j.push( h[ e ].id );
          q.removeWidget( b, j.join( "," ), !0 ), d !== !0 ? (q.applyWidget( b, c || !1, k ), c && q.applyWidget( b, !1, k )) : k( b )
        }, q.getColumnText = function ( b, c, d ) {
          b = a( b )[ 0 ];
          var e, f, g, h, i, j, k, l, m, n, o = "function" == typeof d, p = "all" === c, r = { raw : [], parsed : [], $cell : [] }, s = b.config;
          if ( !q.isEmptyObject( s ) ) {
            for ( i = s.$tbodies.length, e = 0; i > e; e++ )for ( g = s.cache[ e ].normalized, j = g.length, f = 0; j > f; f++ )n = !0, h = g[ f ], l = p ? h.slice( 0, s.columns ) : h[ c ], h = h[ s.columns ], k = p ? h.raw : h.raw[ c ], m = p ? h.$row.children() : h.$row.children().eq( c ), o && (n = d( { tbodyIndex : e, rowIndex : f, parsed : l, raw : k, $row : h.$row, $cell : m } )), n !== !1 && (r.parsed.push( l ), r.raw.push( k ), r.$cell.push( m ));
            return r
          }
          s.debug && console.warn( "No cache found - aborting getColumnText function!" )
        }, q.getData = function ( b, c, d ) {
          var e, f, g = "", h = a( b );
          return h.length ? (e = a.metadata ? h.metadata() : !1, f = " " + (h.attr( "class" ) || ""), "undefined" != typeof h.data( d ) || "undefined" != typeof h.data( d.toLowerCase() ) ? g += h.data( d ) || h.data( d.toLowerCase() ) : e && "undefined" != typeof e[ d ] ? g += e[ d ] : c && "undefined" != typeof c[ d ] ? g += c[ d ] : " " !== f && f.match( " " + d + "-" ) && (g = f.match( new RegExp( "\\s" + d + "-([\\w-]+)" ) )[ 1 ] || ""), a.trim( g )) : ""
        }, q.formatFloat = function ( b, c ) {
          if ( "string" != typeof b || "" === b )return b;
          var d, e = c && c.config ? c.config.usNumberFormat !== !1 : "undefined" != typeof c ? c : !0;
          return b = e ? b.replace( /,/g, "" ) : b.replace( /[\s|\.]/g, "" ).replace( /,/g, "." ),
          /^\s*\([.\d]+\)/.test( b ) && (b = b.replace( /^\s*\(([.\d]+)\)/, "-$1" )), d = parseFloat( b ), isNaN( d ) ? a.trim( b ) : d
        }, q.isDigit = function ( a ) {return isNaN( a ) ? /^[\-+(]?\d+[)]?$/.test( a.toString().replace( /[,.'"\s]/g, "" ) ) : "" !== a}
      }
    } );
    var b = a.tablesorter;
    a.fn.extend( { tablesorter : b.construct } ), console && console.log || (b.logs = [], console = {}, console.log = console.warn = console.error = console.table = function () {b.logs.push( [ Date.now(), arguments ] )}), b.log = function () {console.log( arguments )}, b.benchmark = function ( a ) {return " (" + ((new Date).getTime() - a.getTime()) + "ms)"}, b.addParser( { id : "no-parser", is : function () {return !1}, format : function () {return ""}, type : "text" } ), b.addParser( {
      id      : "text", is : function () {return !0}, format : function ( c, d ) {
        var e = d.config;
        return c && (c = a.trim( e.ignoreCase ? c.toLocaleLowerCase() : c ), c = e.sortLocaleCompare ? b.replaceAccents( c ) : c), c
      }, type : "text"
    } ), b.addParser( {
      id      : "digit", is : function ( a ) {return b.isDigit( a )}, format : function ( c, d ) {
        var e = b.formatFloat( (c || "").replace( /[^\w,. \-()]/g, "" ), d );
        return c && "number" == typeof e ? e : c ? a.trim( c && d.config.ignoreCase ? c.toLocaleLowerCase() : c ) : c
      }, type : "numeric"
    } ), b.addParser( {
      id      : "currency", is : function ( a ) {return a = (a || "").replace( /[+\-,. ]/g, "" ), /^\(?\d+[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]|[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]\d+\)?$/.test( a )}, format : function ( c, d ) {
        var e = b.formatFloat( (c || "").replace( /[^\w,. \-()]/g, "" ), d );
        return c && "number" == typeof e ? e : c ? a.trim( c && d.config.ignoreCase ? c.toLocaleLowerCase() : c ) : c
      }, type : "numeric"
    } ), b.addParser( { id : "url", is : function ( a ) {return /^(https?|ftp|file):\/\//.test( a )}, format : function ( b ) {return b ? a.trim( b.replace( /(https?|ftp|file):\/\//, "" ) ) : b}, parsed : !0, type : "text" } ), b.addParser( {
      id      : "isoDate", is : function ( a ) {return /^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}/.test( a )}, format : function ( a, b ) {
        var c = a ? new Date( a.replace( /-/g, "/" ) ) : a;
        return c instanceof Date && isFinite( c ) ? c.getTime() : a
      }, type : "numeric"
    } ), b.addParser( { id : "percent", is : function ( a ) {return /(\d\s*?%|%\s*?\d)/.test( a ) && a.length < 15}, format : function ( a, c ) {return a ? b.formatFloat( a.replace( /%/g, "" ), c ) : a}, type : "numeric" } ), b.addParser( { id : "image", is : function ( a, b, c, d ) {return d.find( "img" ).length > 0}, format : function ( b, c, d ) {return a( d ).find( "img" ).attr( c.config.imgAttr || "alt" ) || b}, parsed : !0, type : "text" } ), b.addParser( {
      id      : "usLongDate", is : function ( a ) {return /^[A-Z]{3,10}\.?\s+\d{1,2},?\s+(\d{4})(\s+\d{1,2}:\d{2}(:\d{2})?(\s+[AP]M)?)?$/i.test( a ) || /^\d{1,2}\s+[A-Z]{3,10}\s+\d{4}/i.test( a )}, format : function ( a, b ) {
        var c = a ? new Date( a.replace( /(\S)([AP]M)$/i, "$1 $2" ) ) : a;
        return c instanceof Date && isFinite( c ) ? c.getTime() : a
      }, type : "numeric"
    } ), b.addParser( {
      id      : "shortDate", is : function ( a ) {return a = (a || "").replace( /\s+/g, " " ).replace( /[\-.,]/g, "/" ), /(^\d{1,2}[\/\s]\d{1,2}[\/\s]\d{4})|(^\d{4}[\/\s]\d{1,2}[\/\s]\d{1,2})/.test( a )}, format : function ( a, c, d, e ) {
        if ( a ) {
          var f, g, h = c.config, i = h.$headerIndexed[ e ], j = i.length && i[ 0 ].dateFormat || b.getData( i, b.getColumnData( c, h.headers, e ), "dateFormat" ) || h.dateFormat;
          return g = a.replace( /\s+/g, " " ).replace( /[\-.,]/g, "/" ), "mmddyyyy" === j ? g = g.replace( /(\d{1,2})[\/\s](\d{1,2})[\/\s](\d{4})/, "$3/$1/$2" ) : "ddmmyyyy" === j ? g = g.replace( /(\d{1,2})[\/\s](\d{1,2})[\/\s](\d{4})/, "$3/$2/$1" ) : "yyyymmdd" === j && (g = g.replace( /(\d{4})[\/\s](\d{1,2})[\/\s](\d{1,2})/, "$1/$2/$3" )), f = new Date( g ), f instanceof Date && isFinite( f ) ? f.getTime() : a
        }
        return a
      }, type : "numeric"
    } ), b.addParser( {
      id      : "time", is : function ( a ) {return /^(([0-2]?\d:[0-5]\d)|([0-1]?\d:[0-5]\d\s?([AP]M)))$/i.test( a )}, format : function ( a, b ) {
        var c = a ? new Date( "2000/01/01 " + a.replace( /(\S)([AP]M)$/i, "$1 $2" ) ) : a;
        return c instanceof Date && isFinite( c ) ? c.getTime() : a
      }, type : "numeric"
    } ), b.addParser( {
      id      : "metadata", is : function () {return !1}, format : function ( b, c, d ) {
        var e = c.config, f = e.parserMetadataName ? e.parserMetadataName : "sortValue";
        return a( d ).metadata()[ f ]
      }, type : "numeric"
    } ), b.addWidget( {
      id        : "zebra", priority : 90, format : function ( b, c, d ) {
        var e, f, g, h, i, j, k, l, m = new RegExp( c.cssChildRow, "i" ), n = c.$tbodies.add( a( c.namespace + "_extra_table" ).children( "tbody:not(." + c.cssInfoBlock + ")" ) );
        for ( c.debug && (i = new Date), j = 0; j < n.length; j++ )for ( g = 0, e = n.eq( j ).children( "tr:visible" ).not( c.selectorRemove ), l = e.length, k = 0; l > k; k++ )f = e.eq( k ), m.test( f[ 0 ].className ) || g++, h = g % 2 === 0, f.removeClass( d.zebra[ h ? 1 : 0 ] ).addClass( d.zebra[ h ? 0 : 1 ] )
      }, remove : function ( a, c, d, e ) {
        if ( !e ) {
          var f, g, h = c.$tbodies, i = (d.zebra || [ "even", "odd" ]).join( " " );
          for ( f = 0; f < h.length; f++ )g = b.processTbody( a, h.eq( f ), !0 ), g.children().removeClass( i ), b.processTbody( a, g, !1 )
        }
      }
    } )
  }( jQuery ), a.tablesorter
} );
/*! tablesorter (FORK) - updated 08-17-2015 (v2.23.0)*/
!function ( a ) {"function" == typeof define && define.amd ? define( [ "jquery" ], a ) : "object" == typeof module && "object" == typeof module.exports ? module.exports = a( require( "jquery" ) ) : a( jQuery )}( function ( a ) {
  return function ( a, b, c ) {
    "use strict";
    var d = a.tablesorter || {};
    d.storage = function ( d, e, f, g ) {
      d = a( d )[ 0 ];
      var h, i, j, k = !1, l = {}, m = d.config, n = m && m.widgetOptions, o = g && g.useSessionStorage || n && n.storage_useSessionStorage ? "sessionStorage" : "localStorage", p = a( d ), q = g && g.id || p.attr( g && g.group || n && n.storage_group || "data-table-group" ) || n && n.storage_tableId || d.id || a( ".tablesorter" ).index( p ), r = g && g.url || p.attr( g && g.page || n && n.storage_page || "data-table-page" ) || n && n.storage_fixedUrl || m && m.fixedUrl || b.location.pathname;
      if ( o in b )try {
        b[ o ].setItem( "_tmptest", "temp" ), k = !0, b[ o ].removeItem( "_tmptest" )
      } catch ( s ) {
        m && m.debug && console.warn( o + " is not supported in this browser" )
      }
      return a.parseJSON && (k ? l = a.parseJSON( b[ o ][ e ] || "null" ) || {} : (i = c.cookie.split( /[;\s|=]/ ), h = a.inArray( e, i ) + 1, l = 0 !== h ? a.parseJSON( i[ h ] || "null" ) || {} : {})), (f || "" === f) && b.JSON && JSON.hasOwnProperty( "stringify" ) ? (l[ r ] || (l[ r ] = {}), l[ r ][ q ] = f, k ? b[ o ][ e ] = JSON.stringify( l ) : (j = new Date, j.setTime( j.getTime() + 31536e6 ), c.cookie = e + "=" + JSON.stringify( l ).replace( /\"/g, '"' ) + "; expires=" + j.toGMTString() + "; path=/"), void 0) : l && l[ r ] ? l[ r ][ q ] : ""
    }
  }( jQuery, window, document ), function ( a ) {
    "use strict";
    var b = a.tablesorter || {};
    b.themes = { bootstrap : { table : "table table-bordered table-striped", caption : "caption", header : "bootstrap-header", sortNone : "", sortAsc : "", sortDesc : "", active : "", hover : "", icons : "", iconSortNone : "bootstrap-icon-unsorted", iconSortAsc : "icon-chevron-up glyphicon glyphicon-chevron-up", iconSortDesc : "icon-chevron-down glyphicon glyphicon-chevron-down", filterRow : "", footerRow : "", footerCells : "", even : "", odd : "" }, jui : { table : "ui-widget ui-widget-content ui-corner-all", caption : "ui-widget-content", header : "ui-widget-header ui-corner-all ui-state-default", sortNone : "", sortAsc : "", sortDesc : "", active : "ui-state-active", hover : "ui-state-hover", icons : "ui-icon", iconSortNone : "ui-icon-carat-2-n-s", iconSortAsc : "ui-icon-carat-1-n", iconSortDesc : "ui-icon-carat-1-s", filterRow : "", footerRow : "", footerCells : "", even : "ui-widget-content", odd : "ui-state-default" } }, a.extend( b.css, { wrapper : "tablesorter-wrapper" } ), b.addWidget( {
      id        : "uitheme", priority : 10, format : function ( c, d, e ) {
        var f, g, h, i, j, k, l, m, n, o, p, q, r = b.themes, s = d.$table.add( a( d.namespace + "_extra_table" ) ), t = d.$headers.add( a( d.namespace + "_extra_headers" ) ), u = d.theme || "jui", v = r[ u ] || {}, w = a.trim( [ v.sortNone, v.sortDesc, v.sortAsc, v.active ].join( " " ) ), x = a.trim( [ v.iconSortNone, v.iconSortDesc, v.iconSortAsc ].join( " " ) );
        for ( d.debug && (i = new Date), s.hasClass( "tablesorter-" + u ) && d.theme === d.appliedTheme && e.uitheme_applied || (e.uitheme_applied = !0, n = r[ d.appliedTheme ] || {}, q = !a.isEmptyObject( n ), o = q ? [ n.sortNone, n.sortDesc, n.sortAsc, n.active ].join( " " ) : "", p = q ? [ n.iconSortNone, n.iconSortDesc, n.iconSortAsc ].join( " " ) : "", q && (e.zebra[ 0 ] = a.trim( " " + e.zebra[ 0 ].replace( " " + n.even, "" ) ), e.zebra[ 1 ] = a.trim( " " + e.zebra[ 1 ].replace( " " + n.odd, "" ) ), d.$tbodies.children().removeClass( [ n.even, n.odd ].join( " " ) )), v.even && (e.zebra[ 0 ] += " " + v.even), v.odd && (e.zebra[ 1 ] += " " + v.odd), s.children( "caption" ).removeClass( n.caption || "" ).addClass( v.caption ), l = s.removeClass( (d.appliedTheme ? "tablesorter-" + (d.appliedTheme || "") : "") + " " + (n.table || "") ).addClass( "tablesorter-" + u + " " + (v.table || "") ).children( "tfoot" ), d.appliedTheme = d.theme, l.length && l.children( "tr" ).removeClass( n.footerRow || "" ).addClass( v.footerRow ).children( "th, td" ).removeClass( n.footerCells || "" ).addClass( v.footerCells ), t.removeClass( (q ? [ n.header, n.hover, o ].join( " " ) : "") || "" ).addClass( v.header ).not( ".sorter-false" ).unbind( "mouseenter.tsuitheme mouseleave.tsuitheme" ).bind( "mouseenter.tsuitheme mouseleave.tsuitheme", function ( b ) {a( this )[ "mouseenter" === b.type ? "addClass" : "removeClass" ]( v.hover || "" )} ), t.each( function () {
          var c = a( this );
          c.find( "." + b.css.wrapper ).length || c.wrapInner( '<div class="' + b.css.wrapper + '" style="position:relative;height:100%;width:100%"></div>' )
        } ), d.cssIcon && t.find( "." + b.css.icon ).removeClass( q ? [ n.icons, p ].join( " " ) : "" ).addClass( v.icons || "" ), s.hasClass( "hasFilters" ) && s.children( "thead" ).children( "." + b.css.filterRow ).removeClass( q ? n.filterRow || "" : "" ).addClass( v.filterRow || "" )), f = 0; f < d.columns; f++ )j = d.$headers.add( a( d.namespace + "_extra_headers" ) ).not( ".sorter-false" ).filter( '[data-column="' + f + '"]' ), k = b.css.icon ? j.find( "." + b.css.icon ) : a(), m = t.not( ".sorter-false" ).filter( '[data-column="' + f + '"]:last' ), m.length && (j.removeClass( w ), k.removeClass( x ), m[ 0 ].sortDisabled ? k.removeClass( v.icons || "" ) : (g = v.sortNone, h = v.iconSortNone, m.hasClass( b.css.sortAsc ) ? (g = [ v.sortAsc, v.active ].join( " " ), h = v.iconSortAsc) : m.hasClass( b.css.sortDesc ) && (g = [ v.sortDesc, v.active ].join( " " ), h = v.iconSortDesc), j.addClass( g ), k.addClass( h || "" )));
        d.debug && console.log( "Applying " + u + " theme" + b.benchmark( i ) )
      }, remove : function ( a, c, d, e ) {
        if ( d.uitheme_applied ) {
          var f = c.$table, g = c.appliedTheme || "jui", h = b.themes[ g ] || b.themes.jui, i = f.children( "thead" ).children(), j = h.sortNone + " " + h.sortDesc + " " + h.sortAsc, k = h.iconSortNone + " " + h.iconSortDesc + " " + h.iconSortAsc;
          f.removeClass( "tablesorter-" + g + " " + h.table ), d.uitheme_applied = !1, e || (f.find( b.css.header ).removeClass( h.header ), i.unbind( "mouseenter.tsuitheme mouseleave.tsuitheme" ).removeClass( h.hover + " " + j + " " + h.active ).filter( "." + b.css.filterRow ).removeClass( h.filterRow ), i.find( "." + b.css.icon ).removeClass( h.icons + " " + k ))
        }
      }
    } )
  }( jQuery ), function ( a ) {
    "use strict";
    var b = a.tablesorter || {};
    b.addWidget( {
      id        : "columns", priority : 30, options : { columns : [ "primary", "secondary", "tertiary" ] }, format : function ( c, d, e ) {
        var f, g, h, i, j, k, l, m, n = d.$table, o = d.$tbodies, p = d.sortList, q = p.length, r = e && e.columns || [ "primary", "secondary", "tertiary" ], s = r.length - 1;
        for ( l = r.join( " " ), g = 0; g < o.length; g++ )f = b.processTbody( c, o.eq( g ), !0 ), h = f.children( "tr" ), h.each( function () {if ( j = a( this ), "none" !== this.style.display && (k = j.children().removeClass( l ), p && p[ 0 ] && (k.eq( p[ 0 ][ 0 ] ).addClass( r[ 0 ] ), q > 1)) )for ( m = 1; q > m; m++ )k.eq( p[ m ][ 0 ] ).addClass( r[ m ] || r[ s ] )} ), b.processTbody( c, f, !1 );
        if ( i = e.columns_thead !== !1 ? [ "thead tr" ] : [], e.columns_tfoot !== !1 && i.push( "tfoot tr" ), i.length && (h = n.find( i.join( "," ) ).children().removeClass( l ), q) )for ( m = 0; q > m; m++ )h.filter( '[data-column="' + p[ m ][ 0 ] + '"]' ).addClass( r[ m ] || r[ s ] )
      }, remove : function ( c, d, e ) {
        var f, g, h = d.$tbodies, i = (e.columns || [ "primary", "secondary", "tertiary" ]).join( " " );
        for ( d.$headers.removeClass( i ), d.$table.children( "tfoot" ).children( "tr" ).children( "th, td" ).removeClass( i ), f = 0; f < h.length; f++ )g = b.processTbody( c, h.eq( f ), !0 ), g.children( "tr" ).each( function () {a( this ).children().removeClass( i )} ), b.processTbody( c, g, !1 )
      }
    } )
  }( jQuery ), function ( a ) {
    "use strict";
    var b = a.tablesorter || {}, c = b.css;
    a.extend( c, { filterRow : "tablesorter-filter-row", filter : "tablesorter-filter", filterDisabled : "disabled", filterRowHide : "hideme" } ), b.addWidget( {
      id : "filter", priority : 50, options : { filter_childRows : !1, filter_childByColumn : !1, filter_columnFilters : !0, filter_columnAnyMatch : !0, filter_cellFilter : "", filter_cssFilter : "", filter_defaultFilter : {}, filter_excludeFilter : {}, filter_external : "", filter_filteredRow : "filtered", filter_formatter : null, filter_functions : null, filter_hideEmpty : !0, filter_hideFilters : !1, filter_ignoreCase : !0, filter_liveSearch : !0, filter_onlyAvail : "filter-onlyAvail", filter_placeholder : { search : "", select : "" }, filter_reset : null, filter_saveFilters : !1, filter_searchDelay : 300, filter_searchFiltered : !0, filter_selectSource : null, filter_startsWith : !1, filter_useParsedData : !1, filter_serversideFiltering : !1, filter_defaultAttrib : "data-value", filter_selectSourceSeparator : "|" }, format : function ( a, c, d ) {c.$table.hasClass( "hasFilters" ) || b.filter.init( a, c, d )}, remove : function ( d, e, f, g ) {
        var h, i, j = e.$table, k = e.$tbodies, l = "addRows updateCell update updateRows updateComplete appendCache filterReset filterEnd search ".split( " " ).join( e.namespace + "filter " );
        if ( j.removeClass( "hasFilters" ).unbind( l.replace( /\s+/g, " " ) ).find( "." + c.filterRow ).remove(), !g ) {
          for ( h = 0; h < k.length; h++ )i = b.processTbody( d, k.eq( h ), !0 ), i.children().removeClass( f.filter_filteredRow ).show(), b.processTbody( d, i, !1 );
          f.filter_reset && a( document ).undelegate( f.filter_reset, "click.tsfilter" )
        }
      }
    } ), b.filter = {
      regex                 : { regex : /^\/((?:\\\/|[^\/])+)\/([mig]{0,3})?$/, child : /tablesorter-childRow/, filtered : /filtered/, type : /undefined|number/, exact : /(^[\"\'=]+)|([\"\'=]+$)/g, nondigit : /[^\w,. \-()]/g, operators : /[<>=]/g, query : "(q|query)" }, types : {
        or           : function ( c, d, e ) {
          if ( /\|/.test( d.iFilter ) || b.filter.regex.orSplit.test( d.filter ) ) {
            var f, g, h, i, j = a.extend( {}, d ), k = d.index, l = d.parsed[ k ], m = d.filter.split( b.filter.regex.orSplit ), n = d.iFilter.split( b.filter.regex.orSplit ), o = m.length;
            for ( f = 0; o > f; f++ ) {
              j.nestedFilters = !0, j.filter = "" + (b.filter.parseFilter( c, m[ f ], k, l ) || ""), j.iFilter = "" + (b.filter.parseFilter( c, n[ f ], k, l ) || ""), h = "(" + (b.filter.parseFilter( c, j.filter, k, l ) || "") + ")";
              try {
                if ( i = new RegExp( d.isMatch ? h : "^" + h + "$", c.widgetOptions.filter_ignoreCase ? "i" : "" ), g = i.test( j.exact ) || b.filter.processTypes( c, j, e ) )return g
              } catch ( p ) {
                return null
              }
            }
            return g || !1
          }
          return null
        }, and       : function ( c, d, e ) {
          if ( b.filter.regex.andTest.test( d.filter ) ) {
            var f, g, h, i, j, k = a.extend( {}, d ), l = d.index, m = d.parsed[ l ], n = d.filter.split( b.filter.regex.andSplit ), o = d.iFilter.split( b.filter.regex.andSplit ), p = n.length;
            for ( f = 0; p > f; f++ ) {
              k.nestedFilters = !0, k.filter = "" + (b.filter.parseFilter( c, n[ f ], l, m ) || ""), k.iFilter = "" + (b.filter.parseFilter( c, o[ f ], l, m ) || ""), i = ("(" + (b.filter.parseFilter( c, k.filter, l, m ) || "") + ")").replace( /\?/g, "\\S{1}" ).replace( /\*/g, "\\S*" );
              try {
                j = new RegExp( d.isMatch ? i : "^" + i + "$", c.widgetOptions.filter_ignoreCase ? "i" : "" ), h = j.test( k.exact ) || b.filter.processTypes( c, k, e ), g = 0 === f ? h : g && h
              } catch ( q ) {
                return null
              }
            }
            return g || !1
          }
          return null
        }, regex     : function ( a, c ) {
          if ( b.filter.regex.regex.test( c.filter ) ) {
            var d, e = c.filter_regexCache[ c.index ] || b.filter.regex.regex.exec( c.filter ), f = e instanceof RegExp;
            try {
              f || (c.filter_regexCache[ c.index ] = e = new RegExp( e[ 1 ], e[ 2 ] )), d = e.test( c.exact )
            } catch ( g ) {
              d = !1
            }
            return d
          }
          return null
        }, operators : function ( c, d ) {
          if ( /^[<>]=?/.test( d.iFilter ) && "" !== d.iExact ) {
            var e, f, g, h = c.table, i = d.index, j = d.parsed[ i ], k = b.formatFloat( d.iFilter.replace( b.filter.regex.operators, "" ), h ), l = c.parsers[ i ], m = k;
            return (j || "numeric" === l.type) && (g = a.trim( "" + d.iFilter.replace( b.filter.regex.operators, "" ) ), f = b.filter.parseFilter( c, g, i, !0 ), k = "number" != typeof f || "" === f || isNaN( f ) ? k : f), !j && "numeric" !== l.type || isNaN( k ) || "undefined" == typeof d.cache ? (g = isNaN( d.iExact ) ? d.iExact.replace( b.filter.regex.nondigit, "" ) : d.iExact, e = b.formatFloat( g, h )) : e = d.cache, />/.test( d.iFilter ) ? f = />=/.test( d.iFilter ) ? e >= k : e > k : /</.test( d.iFilter ) && (f = /<=/.test( d.iFilter ) ? k >= e : k > e), f || "" !== m || (f = !0), f
          }
          return null
        }, notMatch  : function ( c, d ) {
          if ( /^\!/.test( d.iFilter ) ) {
            var e, f = d.iFilter.replace( "!", "" ), g = b.filter.parseFilter( c, f, d.index, d.parsed[ d.index ] ) || "";
            return b.filter.regex.exact.test( g ) ? (g = g.replace( b.filter.regex.exact, "" ), "" === g ? !0 : a.trim( g ) !== d.iExact) : (e = d.iExact.search( a.trim( g ) ), "" === g ? !0 : !(c.widgetOptions.filter_startsWith ? 0 === e : e >= 0))
          }
          return null
        }, exact     : function ( c, d ) {
          if ( b.filter.regex.exact.test( d.iFilter ) ) {
            var e = d.iFilter.replace( b.filter.regex.exact, "" ), f = b.filter.parseFilter( c, e, d.index, d.parsed[ d.index ] ) || "";
            return d.anyMatch ? a.inArray( f, d.rowArray ) >= 0 : f == d.iExact
          }
          return null
        }, range     : function ( a, c ) {
          if ( b.filter.regex.toTest.test( c.iFilter ) ) {
            var d, e, f, g, h = a.table, i = c.index, j = c.parsed[ i ], k = c.iFilter.split( b.filter.regex.toSplit );
            return e = k[ 0 ].replace( b.filter.regex.nondigit, "" ) || "", f = b.formatFloat( b.filter.parseFilter( a, e, i, j ), h ), e = k[ 1 ].replace( b.filter.regex.nondigit, "" ) || "", g = b.formatFloat( b.filter.parseFilter( a, e, i, j ), h ), (j || "numeric" === a.parsers[ i ].type) && (d = a.parsers[ i ].format( "" + k[ 0 ], h, a.$headers.eq( i ), i ), f = "" === d || isNaN( d ) ? f : d, d = a.parsers[ i ].format( "" + k[ 1 ], h, a.$headers.eq( i ), i ), g = "" === d || isNaN( d ) ? g : d), !j && "numeric" !== a.parsers[ i ].type || isNaN( f ) || isNaN( g ) ? (e = isNaN( c.iExact ) ? c.iExact.replace( b.filter.regex.nondigit, "" ) : c.iExact, d = b.formatFloat( e, h )) : d = c.cache, f > g && (e = f, f = g, g = e), d >= f && g >= d || "" === f || "" === g
          }
          return null
        }, wild      : function ( a, c ) {
          if ( /[\?\*\|]/.test( c.iFilter ) ) {
            var d = c.index, e = c.parsed[ d ], f = "" + (b.filter.parseFilter( a, c.iFilter, d, e ) || "");
            !/\?\*/.test( f ) && c.nestedFilters && (f = c.isMatch ? f : "^(" + f + ")$");
            try {
              return new RegExp( f.replace( /\?/g, "\\S{1}" ).replace( /\*/g, "\\S*" ), a.widgetOptions.filter_ignoreCase ? "i" : "" ).test( c.exact )
            } catch ( g ) {
              return null
            }
          }
          return null
        }, fuzzy     : function ( a, c ) {
          if ( /^~/.test( c.iFilter ) ) {
            var d, e = 0, f = c.iExact.length, g = c.iFilter.slice( 1 ), h = b.filter.parseFilter( a, g, c.index, c.parsed[ c.index ] ) || "";
            for ( d = 0; f > d; d++ )c.iExact[ d ] === h[ e ] && (e += 1);
            return e === h.length ? !0 : !1
          }
          return null
        }
      }, init               : function ( d, e, f ) {
        b.language = a.extend( !0, {}, { to : "to", or : "or", and : "and" }, b.language );
        var g, h, i, j, k, l, m, n, o, p = b.filter.regex;
        if ( e.$table.addClass( "hasFilters" ), f.searchTimer = null, f.filter_initTimer = null, f.filter_formatterCount = 0, f.filter_formatterInit = [], f.filter_anyColumnSelector = '[data-column="all"],[data-column="any"]', f.filter_multipleColumnSelector = '[data-column*="-"],[data-column*=","]', m = "\\{" + b.filter.regex.query + "\\}", a.extend( p, { child : new RegExp( e.cssChildRow ), filtered : new RegExp( f.filter_filteredRow ), alreadyFiltered : new RegExp( "(\\s+(" + b.language.or + "|-|" + b.language.to + ")\\s+)", "i" ), toTest : new RegExp( "\\s+(-|" + b.language.to + ")\\s+", "i" ), toSplit : new RegExp( "(?:\\s+(?:-|" + b.language.to + ")\\s+)", "gi" ), andTest : new RegExp( "\\s+(" + b.language.and + "|&&)\\s+", "i" ), andSplit : new RegExp( "(?:\\s+(?:" + b.language.and + "|&&)\\s+)", "gi" ), orSplit : new RegExp( "(?:\\s+(?:" + b.language.or + ")\\s+|\\|)", "gi" ), iQuery : new RegExp( m, "i" ), igQuery : new RegExp( m, "ig" ) } ), m = e.$headers.filter( ".filter-false, .parser-false" ).length, f.filter_columnFilters !== !1 && m !== e.$headers.length && b.filter.buildRow( d, e, f ), i = "addRows updateCell update updateRows updateComplete appendCache filterReset filterEnd search ".split( " " ).join( e.namespace + "filter " ), e.$table.bind( i, function ( g, h ) {return m = f.filter_hideEmpty && a.isEmptyObject( e.cache ) && !(e.delayInit && "appendCache" === g.type), e.$table.find( "." + c.filterRow ).toggleClass( f.filter_filteredRow, m ), /(search|filter)/.test( g.type ) || (g.stopPropagation(), b.filter.buildDefault( d, !0 )), "filterReset" === g.type ? (e.$table.find( "." + c.filter ).add( f.filter_$externalFilters ).val( "" ), b.filter.searching( d, [] )) : "filterEnd" === g.type ? b.filter.buildDefault( d, !0 ) : (h = "search" === g.type ? h : "updateComplete" === g.type ? e.$table.data( "lastSearch" ) : "", /(update|add)/.test( g.type ) && "updateComplete" !== g.type && (e.lastCombinedFilter = null, e.lastSearch = []), b.filter.searching( d, h, !0 )), !1} ), f.filter_reset && (f.filter_reset instanceof a ? f.filter_reset.click( function () {e.$table.trigger( "filterReset" )} ) : a( f.filter_reset ).length && a( document ).undelegate( f.filter_reset, "click.tsfilter" ).delegate( f.filter_reset, "click.tsfilter", function () {e.$table.trigger( "filterReset" )} )), f.filter_functions )for ( k = 0; k < e.columns; k++ )if ( n = b.getColumnData( d, f.filter_functions, k ) )if ( j = e.$headerIndexed[ k ].removeClass( "filter-select" ), o = !(j.hasClass( "filter-false" ) || j.hasClass( "parser-false" )), g = "", n === !0 && o )b.filter.buildSelect( d, k ); else if ( "object" == typeof n && o ) {
          for ( h in n )"string" == typeof h && (g += "" === g ? '<option value="">' + (j.data( "placeholder" ) || j.attr( "data-placeholder" ) || f.filter_placeholder.select || "") + "</option>" : "", m = h, i = h, h.indexOf( f.filter_selectSourceSeparator ) >= 0 && (m = h.split( f.filter_selectSourceSeparator ), i = m[ 1 ], m = m[ 0 ]), g += "<option " + (i === m ? "" : 'data-function-name="' + h + '" ') + 'value="' + m + '">' + i + "</option>");
          e.$table.find( "thead" ).find( "select." + c.filter + '[data-column="' + k + '"]' ).append( g ), i = f.filter_selectSource, n = a.isFunction( i ) ? !0 : b.getColumnData( d, i, k ), n && b.filter.buildSelect( e.table, k, "", !0, j.hasClass( f.filter_onlyAvail ) )
        }
        b.filter.buildDefault( d, !0 ), b.filter.bindSearch( d, e.$table.find( "." + c.filter ), !0 ), f.filter_external && b.filter.bindSearch( d, f.filter_external ), f.filter_hideFilters && b.filter.hideFilters( d, e ), e.showProcessing && (i = "filterStart filterEnd ".split( " " ).join( e.namespace + "filter " ), e.$table.unbind( i.replace( /\s+/g, " " ) ).bind( i, function ( f, g ) {j = g ? e.$table.find( "." + c.header ).filter( "[data-column]" ).filter( function () {return "" !== g[ a( this ).data( "column" ) ]} ) : "", b.isProcessing( d, "filterStart" === f.type, g ? j : "" )} )), e.filteredRows = e.totalRows, i = "tablesorter-initialized pagerBeforeInitialized ".split( " " ).join( e.namespace + "filter " ), e.$table.unbind( i.replace( /\s+/g, " " ) ).bind( i, function () {
          var a = this.config.widgetOptions;
          l = b.filter.setDefaults( d, e, a ) || [], l.length && (e.delayInit && "" === l.join( "" ) || b.setFilters( d, l, !0 )), e.$table.trigger( "filterFomatterUpdate" ), setTimeout( function () {a.filter_initialized || b.filter.filterInitComplete( e )}, 100 )
        } ), e.pager && e.pager.initialized && !f.filter_initialized && (e.$table.trigger( "filterFomatterUpdate" ), setTimeout( function () {b.filter.filterInitComplete( e )}, 100 ))
      }, formatterUpdated   : function ( a, b ) {
        var c = a.closest( "table" )[ 0 ].config.widgetOptions;
        c.filter_initialized || (c.filter_formatterInit[ b ] = 1)
      }, filterInitComplete : function ( c ) {
        var d, e, f = c.widgetOptions, g = 0, h = function () {f.filter_initialized = !0, c.$table.trigger( "filterInit", c ), b.filter.findRows( c.table, c.$table.data( "lastSearch" ) || [] )};
        if ( a.isEmptyObject( f.filter_formatter ) )h(); else {
          for ( e = f.filter_formatterInit.length, d = 0; e > d; d++ )1 === f.filter_formatterInit[ d ] && g++;
          clearTimeout( f.filter_initTimer ), f.filter_initialized || g !== f.filter_formatterCount ? f.filter_initialized || (f.filter_initTimer = setTimeout( function () {h()}, 500 )) : h()
        }
      }, setDefaults        : function ( c, d, e ) {
        var f, g, h, i, j, k = b.getFilters( c ) || [];
        if ( e.filter_saveFilters && b.storage && (g = b.storage( c, "tablesorter-filters" ) || [], f = a.isArray( g ), f && "" === g.join( "" ) || !f || (k = g)), "" === k.join( "" ) )for ( j = d.$headers.add( e.filter_$externalFilters ).filter( "[" + e.filter_defaultAttrib + "]" ), h = 0; h <= d.columns; h++ )i = h === d.columns ? "all" : h, k[ h ] = j.filter( '[data-column="' + i + '"]' ).attr( e.filter_defaultAttrib ) || k[ h ] || "";
        return d.$table.data( "lastSearch", k ), k
      }, parseFilter        : function ( a, b, c, d ) {return d ? a.parsers[ c ].format( b, a.table, [], c ) : b}, buildRow : function ( d, e, f ) {
        var g, h, i, j, k, l, m, n, o = f.filter_cellFilter, p = e.columns, q = a.isArray( o ), r = '<tr role="row" class="' + c.filterRow + " " + e.cssIgnoreRow + '">';
        for ( h = 0; p > h; h++ )r += "<td", r += q ? o[ h ] ? ' class="' + o[ h ] + '"' : "" : "" !== o ? ' class="' + o + '"' : "", r += "></td>";
        for ( e.$filters = a( r += "</tr>" ).appendTo( e.$table.children( "thead" ).eq( 0 ) ).find( "td" ), h = 0; p > h; h++ )k = !1, i = e.$headerIndexed[ h ], m = b.getColumnData( d, f.filter_functions, h ), j = f.filter_functions && m && "function" != typeof m || i.hasClass( "filter-select" ), g = b.getColumnData( d, e.headers, h ), k = "false" === b.getData( i[ 0 ], g, "filter" ) || "false" === b.getData( i[ 0 ], g, "parser" ), j ? r = a( "<select>" ).appendTo( e.$filters.eq( h ) ) : (m = b.getColumnData( d, f.filter_formatter, h ), m ? (f.filter_formatterCount++, r = m( e.$filters.eq( h ), h ), r && 0 === r.length && (r = e.$filters.eq( h ).children( "input" )), r && (0 === r.parent().length || r.parent().length && r.parent()[ 0 ] !== e.$filters[ h ]) && e.$filters.eq( h ).append( r )) : r = a( '<input type="search">' ).appendTo( e.$filters.eq( h ) ), r && (n = i.data( "placeholder" ) || i.attr( "data-placeholder" ) || f.filter_placeholder.search || "", r.attr( "placeholder", n ))), r && (l = (a.isArray( f.filter_cssFilter ) ? "undefined" != typeof f.filter_cssFilter[ h ] ? f.filter_cssFilter[ h ] || "" : "" : f.filter_cssFilter) || "", r.addClass( c.filter + " " + l ).attr( "data-column", h ), k && (r.attr( "placeholder", "" ).addClass( c.filterDisabled )[ 0 ].disabled = !0))
      }, bindSearch         : function ( c, d, e ) {
        if ( c = a( c )[ 0 ], d = a( d ), d.length ) {
          var f, g = c.config, h = g.widgetOptions, i = g.namespace + "filter", j = h.filter_$externalFilters;
          e !== !0 && (f = h.filter_anyColumnSelector + "," + h.filter_multipleColumnSelector, h.filter_$anyMatch = d.filter( f ), j && j.length ? h.filter_$externalFilters = h.filter_$externalFilters.add( d ) : h.filter_$externalFilters = d, b.setFilters( c, g.$table.data( "lastSearch" ) || [], e === !1 )), f = "keypress keyup search change ".split( " " ).join( i + " " ), d.attr( "data-lastSearchTime", (new Date).getTime() ).unbind( f.replace( /\s+/g, " " ) ).bind( "keyup" + i, function ( d ) {
            if ( a( this ).attr( "data-lastSearchTime", (new Date).getTime() ), 27 === d.which )this.value = ""; else {
              if ( h.filter_liveSearch === !1 )return;
              if ( "" !== this.value && ("number" == typeof h.filter_liveSearch && this.value.length < h.filter_liveSearch || 13 !== d.which && 8 !== d.which && (d.which < 32 || d.which >= 37 && d.which <= 40)) )return
            }
            b.filter.searching( c, !0, !0 )
          } ).bind( "search change keypress ".split( " " ).join( i + " " ), function ( d ) {
            var e = a( this ).data( "column" );
            (13 === d.which || "search" === d.type || "change" === d.type && this.value !== g.lastSearch[ e ]) && (d.preventDefault(), a( this ).attr( "data-lastSearchTime", (new Date).getTime() ), b.filter.searching( c, !1, !0 ))
          } )
        }
      }, searching          : function ( a, c, d ) {
        var e = a.config.widgetOptions;
        clearTimeout( e.searchTimer ), "undefined" == typeof c || c === !0 ? e.searchTimer = setTimeout( function () {b.filter.checkFilters( a, c, d )}, e.filter_liveSearch ? e.filter_searchDelay : 10 ) : b.filter.checkFilters( a, c, d )
      }, checkFilters       : function ( d, e, f ) {
        var g = d.config, h = g.widgetOptions, i = a.isArray( e ), j = i ? e : b.getFilters( d, !0 ), k = (j || []).join( "" );
        return a.isEmptyObject( g.cache ) ? void(g.delayInit && g.pager && g.pager.initialized && g.$table.trigger( "updateCache", [ function () {b.filter.checkFilters( d, !1, f )} ] )) : (i && (b.setFilters( d, j, !1, f !== !0 ), h.filter_initialized || (g.lastCombinedFilter = "")), h.filter_hideFilters && g.$table.find( "." + c.filterRow ).trigger( "" === k ? "mouseleave" : "mouseenter" ), g.lastCombinedFilter !== k || e === !1 ? (e === !1 && (g.lastCombinedFilter = null, g.lastSearch = []), h.filter_initialized && g.$table.trigger( "filterStart", [ j ] ), g.showProcessing ? void setTimeout( function () {return b.filter.findRows( d, j, k ), !1}, 30 ) : (b.filter.findRows( d, j, k ), !1)) : void 0)
      }, hideFilters        : function ( d, e ) {
        var f;
        e.$table.find( "." + c.filterRow ).bind( "mouseenter mouseleave", function ( b ) {
          var d = b, g = a( this );
          clearTimeout( f ), f = setTimeout( function () {/enter|over/.test( d.type ) ? g.removeClass( c.filterRowHide ) : a( document.activeElement ).closest( "tr" )[ 0 ] !== g[ 0 ] && "" === e.lastCombinedFilter && g.addClass( c.filterRowHide )}, 200 )
        } ).find( "input, select" ).bind( "focus blur", function ( d ) {
          var g = d, h = a( this ).closest( "tr" );
          clearTimeout( f ), f = setTimeout( function () {clearTimeout( f ), "" === b.getFilters( e.$table ).join( "" ) && h.toggleClass( c.filterRowHide, "focus" !== g.type )}, 200 )
        } )
      }, defaultFilter      : function ( c, d ) {
        if ( "" === c )return c;
        var e = b.filter.regex.iQuery, f = d.match( b.filter.regex.igQuery ).length, g = f > 1 ? a.trim( c ).split( /\s/ ) : [ a.trim( c ) ], h = g.length - 1, i = 0, j = d;
        for ( 1 > h && f > 1 && (g[ 1 ] = g[ 0 ]); e.test( j ); )j = j.replace( e, g[ i++ ] || "" ), e.test( j ) && h > i && "" !== (g[ i ] || "") && (j = d.replace( e, j ));
        return j
      }, getLatestSearch    : function ( b ) {return b ? b.sort( function ( b, c ) {return a( c ).attr( "data-lastSearchTime" ) - a( b ).attr( "data-lastSearchTime" )} ) : b || a()}, multipleColumns : function ( c, d ) {
        var e, f, g, h, i, j, k, l, m, n = c.widgetOptions, o = n.filter_initialized || !d.filter( n.filter_anyColumnSelector ).length, p = [], q = a.trim( b.filter.getLatestSearch( d ).attr( "data-column" ) || "" );
        if ( o && /-/.test( q ) )for ( f = q.match( /(\d+)\s*-\s*(\d+)/g ), m = f.length, l = 0; m > l; l++ ) {
          for ( g = f[ l ].split( /\s*-\s*/ ), h = parseInt( g[ 0 ], 10 ) || 0, i = parseInt( g[ 1 ], 10 ) || c.columns - 1, h > i && (e = h, h = i, i = e), i >= c.columns && (i = c.columns - 1); i >= h; h++ )p.push( h );
          q = q.replace( f[ l ], "" )
        }
        if ( o && /,/.test( q ) )for ( j = q.split( /\s*,\s*/ ), m = j.length, k = 0; m > k; k++ )"" !== j[ k ] && (l = parseInt( j[ k ], 10 ), l < c.columns && p.push( l ));
        if ( !p.length )for ( l = 0; l < c.columns; l++ )p.push( l );
        return p
      }, processTypes       : function ( c, d, e ) {
        var f, g = null, h = null;
        for ( f in b.filter.types )a.inArray( f, e.excludeMatch ) < 0 && null === h && (h = b.filter.types[ f ]( c, d, e ), null !== h && (g = h));
        return g
      }, processRow         : function ( c, d, e ) {
        var f, g, h, i, j, k, l, m, n = b.filter.regex, o = c.widgetOptions, p = !0;
        if ( d.$cells = d.$row.children(), d.anyMatchFlag ) {
          if ( f = b.filter.multipleColumns( c, o.filter_$anyMatch ), d.anyMatch = !0, d.isMatch = !0, d.rowArray = d.$cells.map( function ( e ) {return a.inArray( e, f ) > -1 ? (d.parsed[ e ] ? m = d.cacheArray[ e ] : (m = d.rawArray[ e ], m = a.trim( o.filter_ignoreCase ? m.toLowerCase() : m ), c.sortLocaleCompare && (m = b.replaceAccents( m ))), m) : void 0} ).get(), d.filter = d.anyMatchFilter, d.iFilter = d.iAnyMatchFilter, d.exact = d.rowArray.join( " " ), d.iExact = o.filter_ignoreCase ? d.exact.toLowerCase() : d.exact, d.cache = d.cacheArray.slice( 0, -1 ).join( " " ), e.excludeMatch = e.noAnyMatch, j = b.filter.processTypes( c, d, e ), null !== j )p = j; else if ( o.filter_startsWith )for ( p = !1, f = Math.min( c.columns, d.rowArray.length ); !p && f > 0; )f--, p = p || 0 === d.rowArray[ f ].indexOf( d.iFilter ); else p = (d.iExact + d.childRowText).indexOf( d.iFilter ) >= 0;
          if ( d.anyMatch = !1, d.filters.join( "" ) === d.filter )return p
        }
        for ( f = 0; f < c.columns; f++ )d.filter = d.filters[ f ], d.index = f, e.excludeMatch = e.excludeFilter[ f ], d.filter && (d.cache = d.cacheArray[ f ], o.filter_useParsedData || d.parsed[ f ] ? d.exact = d.cache : (h = d.rawArray[ f ] || "", d.exact = c.sortLocaleCompare ? b.replaceAccents( h ) : h), d.iExact = !n.type.test( typeof d.exact ) && o.filter_ignoreCase ? d.exact.toLowerCase() : d.exact, d.isMatch = c.$headerIndexed[ d.index ].hasClass( "filter-match" ), h = p, l = o.filter_columnFilters ? c.$filters.add( c.$externalFilters ).filter( '[data-column="' + f + '"]' ).find( "select option:selected" ).attr( "data-function-name" ) || "" : "", c.sortLocaleCompare && (d.filter = b.replaceAccents( d.filter )), i = !0, o.filter_defaultFilter && n.iQuery.test( e.defaultColFilter[ f ] ) && (d.filter = b.filter.defaultFilter( d.filter, e.defaultColFilter[ f ] ), i = !1), d.iFilter = o.filter_ignoreCase ? (d.filter || "").toLowerCase() : d.filter, k = e.functions[ f ], g = c.$headerIndexed[ f ].hasClass( "filter-select" ), j = null, (k || g && i) && (k === !0 || g ? j = d.isMatch ? d.iExact.search( d.iFilter ) >= 0 : d.filter === d.exact : "function" == typeof k ? j = k( d.exact, d.cache, d.filter, f, d.$row, c, d ) : "function" == typeof k[ l || d.filter ] && (m = l || d.filter, j = k[ m ]( d.exact, d.cache, d.filter, f, d.$row, c, d ))), null === j ? (j = b.filter.processTypes( c, d, e ), null !== j ? h = j : (m = (d.iExact + d.childRowText).indexOf( b.filter.parseFilter( c, d.iFilter, f, d.parsed[ f ] ) ), h = !o.filter_startsWith && m >= 0 || o.filter_startsWith && 0 === m)) : h = j, p = h ? p : !1);
        return p
      }, findRows           : function ( c, d, e ) {
        if ( c.config.lastCombinedFilter !== e && c.config.widgetOptions.filter_initialized ) {
          var f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B = a.extend( [], d ), C = b.filter.regex, D = c.config, E = D.widgetOptions, F = { anyMatch : !1, filters : d, filter_regexCache : [] }, G = { noAnyMatch : [ "range", "notMatch", "operators" ], functions : [], excludeFilter : [], defaultColFilter : [], defaultAnyFilter : b.getColumnData( c, E.filter_defaultFilter, D.columns, !0 ) || "" };
          for ( F.parsed = D.$headers.map( function ( d ) {return D.parsers && D.parsers[ d ] && D.parsers[ d ].parsed || b.getData && "parsed" === b.getData( D.$headerIndexed[ d ], b.getColumnData( c, D.headers, d ), "filter" ) || a( this ).hasClass( "filter-parsed" )} ).get(), m = 0; m < D.columns; m++ )G.functions[ m ] = b.getColumnData( c, E.filter_functions, m ), G.defaultColFilter[ m ] = b.getColumnData( c, E.filter_defaultFilter, m ) || "", G.excludeFilter[ m ] = (b.getColumnData( c, E.filter_excludeFilter, m, !0 ) || "").split( /\s+/ );
          for ( D.debug && (console.log( "Filter: Starting filter widget search", d ), r = new Date), D.filteredRows = 0, D.totalRows = 0, e = (B || []).join( "" ), k = 0; k < D.$tbodies.length; k++ ) {
            if ( l = b.processTbody( c, D.$tbodies.eq( k ), !0 ), m = D.columns, g = D.cache[ k ].normalized, i = a( a.map( g, function ( a ) {return a[ m ].$row.get()} ) ), "" === e || E.filter_serversideFiltering )i.removeClass( E.filter_filteredRow ).not( "." + D.cssChildRow ).css( "display", "" ); else {
              if ( i = i.not( "." + D.cssChildRow ), f = i.length, (E.filter_$anyMatch && E.filter_$anyMatch.length || "undefined" != typeof d[ D.columns ]) && (F.anyMatchFlag = !0, F.anyMatchFilter = "" + (d[ D.columns ] || E.filter_$anyMatch && b.filter.getLatestSearch( E.filter_$anyMatch ).val() || ""), E.filter_columnAnyMatch) ) {
                for ( w = F.anyMatchFilter.split( C.andSplit ), x = !1, t = 0; t < w.length; t++ )y = w[ t ].split( ":" ), y.length > 1 && (z = parseInt( y[ 0 ], 10 ) - 1, z >= 0 && z < D.columns && (d[ z ] = y[ 1 ], w.splice( t, 1 ), t--, x = !0));
                x && (F.anyMatchFilter = w.join( " && " ))
              }
              if ( v = E.filter_searchFiltered, p = D.lastSearch || D.$table.data( "lastSearch" ) || [], v )for ( t = 0; m + 1 > t; t++ )s = d[ t ] || "", v || (t = m), v = !(!v || !p.length || 0 !== s.indexOf( p[ t ] || "" ) || C.alreadyFiltered.test( s ) || /[=\"\|!]/.test( s ) || /(>=?\s*-\d)/.test( s ) || /(<=?\s*\d)/.test( s ) || "" !== s && D.$filters && D.$filters.eq( t ).find( "select" ).length && !D.$headerIndexed[ t ].hasClass( "filter-match" ));
              for ( u = i.not( "." + E.filter_filteredRow ).length, v && 0 === u && (v = !1), D.debug && console.log( "Filter: Searching through " + (v && f > u ? u : "all") + " rows" ), F.anyMatchFlag && (D.sortLocaleCompare && (F.anyMatchFilter = b.replaceAccents( F.anyMatchFilter )), E.filter_defaultFilter && C.iQuery.test( G.defaultAnyFilter ) && (F.anyMatchFilter = b.filter.defaultFilter( F.anyMatchFilter, G.defaultAnyFilter ), v = !1), F.iAnyMatchFilter = E.filter_ignoreCase && D.ignoreCase ? F.anyMatchFilter.toLowerCase() : F.anyMatchFilter), j = 0; f > j; j++ )if ( A = i[ j ].className, n = j && C.child.test( A ), !(n || v && C.filtered.test( A )) ) {
                if ( F.$row = i.eq( j ), F.cacheArray = g[ j ], h = F.cacheArray[ D.columns ], F.rawArray = h.raw, F.childRowText = "", !E.filter_childByColumn ) {
                  for ( A = "", o = h.child, t = 0; t < o.length; t++ )A += " " + o[ t ].join( "" ) || "";
                  F.childRowText = E.filter_childRows ? E.filter_ignoreCase ? A.toLowerCase() : A : ""
                }
                if ( q = b.filter.processRow( D, F, G ), o = h.$row.filter( ":gt( 0 )" ), E.filter_childRows && o.length ) {
                  if ( E.filter_childByColumn )for ( t = 0; t < o.length; t++ )F.$row = o.eq( t ), F.cacheArray = h.child[ t ], F.rawArray = F.cacheArray, q = q || b.filter.processRow( D, F, G );
                  o.toggleClass( E.filter_filteredRow, !q )
                }
                h.$row.toggleClass( E.filter_filteredRow, !q )[ 0 ].display = q ? "" : "none"
              }
            }
            D.filteredRows += i.not( "." + E.filter_filteredRow ).length, D.totalRows += i.length, b.processTbody( c, l, !1 )
          }
          D.lastCombinedFilter = e, D.lastSearch = B, D.$table.data( "lastSearch", B ), E.filter_saveFilters && b.storage && b.storage( c, "tablesorter-filters", B ), D.debug && console.log( "Completed filter widget search" + b.benchmark( r ) ), E.filter_initialized && D.$table.trigger( "filterEnd", D ), setTimeout( function () {D.$table.trigger( "applyWidgets" )}, 0 )
        }
      }, getOptionSource    : function ( c, d, e ) {
        c = a( c )[ 0 ];
        var f, g, h, i, j = c.config, k = j.widgetOptions, l = [], m = !1, n = k.filter_selectSource, o = j.$table.data( "lastSearch" ) || [], p = a.isFunction( n ) ? !0 : b.getColumnData( c, n, d );
        if ( e && "" !== o[ d ] && (e = !1), p === !0 )m = n( c, d, e ); else {
          if ( p instanceof a || "string" === a.type( p ) && p.indexOf( "</option>" ) >= 0 )return p;
          a.isArray( p ) ? m = p : "object" === a.type( n ) && p && (m = p( c, d, e ))
        }
        if ( m === !1 && (m = b.filter.getOptions( c, d, e )), m = a.grep( m, function ( b, c ) {return a.inArray( b, m ) === c} ), j.$headerIndexed[ d ].hasClass( "filter-select-nosort" ) )return m;
        for ( i = m.length, h = 0; i > h; h++ )g = m[ h ], l.push( { t : g, p : j.parsers && j.parsers.length && j.parsers[ d ].format( g, c, [], d ) || g } );
        for ( f = j.textSorter || "", l.sort( function ( e, g ) {
          var h = e.p.toString(), i = g.p.toString();
          return a.isFunction( f ) ? f( h, i, !0, d, c ) : "object" == typeof f && f.hasOwnProperty( d ) ? f[ d ]( h, i, !0, d, c ) : b.sortNatural ? b.sortNatural( h, i ) : !0
        } ), m = [], i = l.length, h = 0; i > h; h++ )m.push( l[ h ].t );
        return m
      }, getOptions         : function ( b, c, d ) {
        b = a( b )[ 0 ];
        var e, f, g, h, i, j = b.config, k = j.widgetOptions, l = [];
        for ( f = 0; f < j.$tbodies.length; f++ )for ( i = j.cache[ f ], g = j.cache[ f ].normalized.length, e = 0; g > e; e++ )h = i.row ? i.row[ e ] : i.normalized[ e ][ j.columns ].$row[ 0 ], d && h.className.match( k.filter_filteredRow ) || (k.filter_useParsedData || j.parsers[ c ].parsed || j.$headerIndexed[ c ].hasClass( "filter-parsed" ) ? l.push( "" + i.normalized[ e ][ c ] ) : l.push( i.normalized[ e ][ j.columns ].raw[ c ] ));
        return l
      }, buildSelect        : function ( d, e, f, g, h ) {
        if ( d = a( d )[ 0 ], e = parseInt( e, 10 ), d.config.cache && !a.isEmptyObject( d.config.cache ) ) {
          var i, j, k, l, m, n, o = d.config, p = o.widgetOptions, q = o.$headerIndexed[ e ], r = '<option value="">' + (q.data( "placeholder" ) || q.attr( "data-placeholder" ) || p.filter_placeholder.select || "") + "</option>", s = o.$table.find( "thead" ).find( "select." + c.filter + '[data-column="' + e + '"]' ).val();
          if ( ("undefined" == typeof f || "" === f) && (f = b.filter.getOptionSource( d, e, h )), a.isArray( f ) ) {
            for ( i = 0; i < f.length; i++ )k = f[ i ] = ("" + f[ i ]).replace( /\"/g, "&quot;" ), j = k, k.indexOf( p.filter_selectSourceSeparator ) >= 0 && (l = k.split( p.filter_selectSourceSeparator ), j = l[ 0 ], k = l[ 1 ]), r += "" !== f[ i ] ? "<option " + (j === k ? "" : 'data-function-name="' + f[ i ] + '" ') + 'value="' + j + '">' + k + "</option>" : "";
            f = []
          }
          m = (o.$filters ? o.$filters : o.$table.children( "thead" )).find( "." + c.filter ), p.filter_$externalFilters && (m = m && m.length ? m.add( p.filter_$externalFilters ) : p.filter_$externalFilters), n = m.filter( 'select[data-column="' + e + '"]' ), n.length && (n[ g ? "html" : "append" ]( r ), a.isArray( f ) || n.append( f ).val( s ), n.val( s ))
        }
      }, buildDefault       : function ( a, c ) {
        var d, e, f, g = a.config, h = g.widgetOptions, i = g.columns;
        for ( d = 0; i > d; d++ )e = g.$headerIndexed[ d ], f = !(e.hasClass( "filter-false" ) || e.hasClass( "parser-false" )), (e.hasClass( "filter-select" ) || b.getColumnData( a, h.filter_functions, d ) === !0) && f && b.filter.buildSelect( a, d, "", c, e.hasClass( h.filter_onlyAvail ) )
      }
    }, b.getFilters = function ( d, e, f, g ) {
      var h, i, j, k, l = !1, m = d ? a( d )[ 0 ].config : "", n = m ? m.widgetOptions : "";
      if ( e !== !0 && n && !n.filter_columnFilters || a.isArray( f ) && f.join( "" ) === m.lastCombinedFilter )return a( d ).data( "lastSearch" );
      if ( m && (m.$filters && (i = m.$filters.find( "." + c.filter )), n.filter_$externalFilters && (i = i && i.length ? i.add( n.filter_$externalFilters ) : n.filter_$externalFilters),
        i && i.length) )for ( l = f || [], h = 0; h < m.columns + 1; h++ )k = h === m.columns ? n.filter_anyColumnSelector + "," + n.filter_multipleColumnSelector : '[data-column="' + h + '"]', j = i.filter( k ), j.length && (j = b.filter.getLatestSearch( j ), a.isArray( f ) ? (g && j.length > 1 && (j = j.slice( 1 )), h === m.columns && (k = j.filter( n.filter_anyColumnSelector ), j = k.length ? k : j), j.val( f[ h ] ).trigger( "change.tsfilter" )) : (l[ h ] = j.val() || "", h === m.columns ? j.slice( 1 ).filter( '[data-column*="' + j.attr( "data-column" ) + '"]' ).val( l[ h ] ) : j.slice( 1 ).val( l[ h ] )), h === m.columns && j.length && (n.filter_$anyMatch = j));
      return 0 === l.length && (l = !1), l
    }, b.setFilters = function ( c, d, e, f ) {
      var g = c ? a( c )[ 0 ].config : "", h = b.getFilters( c, !0, d, f );
      return g && e && (g.lastCombinedFilter = null, g.lastSearch = [], b.filter.searching( g.table, d, f ), g.$table.trigger( "filterFomatterUpdate" )), !!h
    }
  }( jQuery ), function ( a, b ) {
    "use strict";
    var c = a.tablesorter || {};
    a.extend( c.css, { sticky : "tablesorter-stickyHeader", stickyVis : "tablesorter-sticky-visible", stickyHide : "tablesorter-sticky-hidden", stickyWrap : "tablesorter-sticky-wrapper" } ), c.addHeaderResizeEvent = function ( b, c, d ) {
      if ( b = a( b )[ 0 ], b.config ) {
        var e = { timer : 250 }, f = a.extend( {}, e, d ), g = b.config, h = g.widgetOptions, i = function ( a ) {
          var b, c, d, e, f, i, j = g.$headers.length;
          for ( h.resize_flag = !0, c = [], b = 0; j > b; b++ )d = g.$headers.eq( b ), e = d.data( "savedSizes" ) || [ 0, 0 ], f = d[ 0 ].offsetWidth, i = d[ 0 ].offsetHeight, (f !== e[ 0 ] || i !== e[ 1 ]) && (d.data( "savedSizes", [ f, i ] ), c.push( d[ 0 ] ));
          c.length && a !== !1 && g.$table.trigger( "resize", [ c ] ), h.resize_flag = !1
        };
        return i( !1 ), clearInterval( h.resize_timer ), c ? (h.resize_flag = !1, !1) : void(h.resize_timer = setInterval( function () {h.resize_flag || i()}, f.timer ))
      }
    }, c.addWidget( {
      id        : "stickyHeaders", priority : 60, options : { stickyHeaders : "", stickyHeaders_attachTo : null, stickyHeaders_xScroll : null, stickyHeaders_yScroll : null, stickyHeaders_offset : 0, stickyHeaders_filteredToTop : !0, stickyHeaders_cloneId : "-sticky", stickyHeaders_addResizeEvent : !0, stickyHeaders_includeCaption : !0, stickyHeaders_zIndex : 2 }, format : function ( d, e, f ) {
        if ( !(e.$table.hasClass( "hasStickyHeaders" ) || a.inArray( "filter", e.widgets ) >= 0 && !e.$table.hasClass( "hasFilters" )) ) {
          var g, h, i, j, k = e.$table, l = a( f.stickyHeaders_attachTo ), m = e.namespace + "stickyheaders ", n = a( f.stickyHeaders_yScroll || f.stickyHeaders_attachTo || b ), o = a( f.stickyHeaders_xScroll || f.stickyHeaders_attachTo || b ), p = k.children( "thead:first" ), q = p.children( "tr" ).not( ".sticky-false" ).children(), r = k.children( "tfoot" ), s = isNaN( f.stickyHeaders_offset ) ? a( f.stickyHeaders_offset ) : "", t = s.length ? s.height() || 0 : parseInt( f.stickyHeaders_offset, 10 ) || 0, u = k.parent().closest( "." + c.css.table ).hasClass( "hasStickyHeaders" ) ? k.parent().closest( "table.tablesorter" )[ 0 ].config.widgetOptions.$sticky.parent() : [], v = u.length ? u.height() : 0, w = f.$sticky = k.clone().addClass( "containsStickyHeaders " + c.css.sticky + " " + f.stickyHeaders + " " + e.namespace.slice( 1 ) + "_extra_table" ).wrap( '<div class="' + c.css.stickyWrap + '">' ), x = w.parent().addClass( c.css.stickyHide ).css( { position : l.length ? "absolute" : "fixed", padding : parseInt( w.parent().parent().css( "padding-left" ), 10 ), top : t + v, left : 0, visibility : "hidden", zIndex : f.stickyHeaders_zIndex || 2 } ), y = w.children( "thead:first" ), z = "", A = 0, B = function ( a, c ) {
            var d, e, f, g, h, i = a.filter( ":visible" ), j = i.length;
            for ( d = 0; j > d; d++ )g = c.filter( ":visible" ).eq( d ), h = i.eq( d ), "border-box" === h.css( "box-sizing" ) ? e = h.outerWidth() : "collapse" === g.css( "border-collapse" ) ? b.getComputedStyle ? e = parseFloat( b.getComputedStyle( h[ 0 ], null ).width ) : (f = parseFloat( h.css( "border-width" ) ), e = h.outerWidth() - parseFloat( h.css( "padding-left" ) ) - parseFloat( h.css( "padding-right" ) ) - f) : e = h.width(), g.css( { width : e, "min-width" : e, "max-width" : e } )
          }, C                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    = function () {t = s.length ? s.height() || 0 : parseInt( f.stickyHeaders_offset, 10 ) || 0, A = 0, x.css( { left : l.length ? parseInt( l.css( "padding-left" ), 10 ) || 0 : k.offset().left - parseInt( k.css( "margin-left" ), 10 ) - o.scrollLeft() - A, width : k.outerWidth() } ), B( k, w ), B( q, j )}, D = function ( b ) {
            if ( k.is( ":visible" ) ) {
              v = u.length ? u.offset().top - n.scrollTop() + u.height() : 0;
              var d = k.offset(), e = a.isWindow( n[ 0 ] ), f = a.isWindow( o[ 0 ] ), g = (l.length ? e ? n.scrollTop() : n.offset().top : n.scrollTop()) + t + v, h = k.height() - (x.height() + (r.height() || 0)), i = g > d.top && g < d.top + h ? "visible" : "hidden", j = { visibility : i };
              l.length && (j.top = e ? g - l.offset().top : l.scrollTop()), f && (j.left = k.offset().left - parseInt( k.css( "margin-left" ), 10 ) - o.scrollLeft() - A), u.length && (j.top = (j.top || 0) + t + v), x.removeClass( c.css.stickyVis + " " + c.css.stickyHide ).addClass( "visible" === i ? c.css.stickyVis : c.css.stickyHide ).css( j ), (i !== z || b) && (C(), z = i)
            }
          };
          if ( l.length && !l.css( "position" ) && l.css( "position", "relative" ), w.attr( "id" ) && (w[ 0 ].id += f.stickyHeaders_cloneId), w.find( "thead:gt(0), tr.sticky-false" ).hide(), w.find( "tbody, tfoot" ).remove(), w.find( "caption" ).toggle( f.stickyHeaders_includeCaption ), j = y.children().children(), w.css( { height : 0, width : 0, margin : 0 } ), j.find( "." + c.css.resizer ).remove(), k.addClass( "hasStickyHeaders" ).bind( "pagerComplete" + m, function () {C()} ), c.bindEvents( d, y.children().children( "." + c.css.header ) ), k.after( x ), e.onRenderHeader )for ( i = y.children( "tr" ).children(), h = i.length, g = 0; h > g; g++ )e.onRenderHeader.apply( i.eq( g ), [ g, e, w ] );
          o.add( n ).unbind( "scroll resize ".split( " " ).join( m ).replace( /\s+/g, " " ) ).bind( "scroll resize ".split( " " ).join( m ), function ( a ) {D( "resize" === a.type )} ), e.$table.unbind( "stickyHeadersUpdate" + m ).bind( "stickyHeadersUpdate" + m, function () {D( !0 )} ), f.stickyHeaders_addResizeEvent && c.addHeaderResizeEvent( d ), k.hasClass( "hasFilters" ) && f.filter_columnFilters && (k.bind( "filterEnd" + m, function () {
            var d = a( document.activeElement ).closest( "td" ), g = d.parent().children().index( d );
            x.hasClass( c.css.stickyVis ) && f.stickyHeaders_filteredToTop && (b.scrollTo( 0, k.position().top ), g >= 0 && e.$filters && e.$filters.eq( g ).find( "a, select, input" ).filter( ":visible" ).focus())
          } ), c.filter.bindSearch( k, j.find( "." + c.css.filter ) ), f.filter_hideFilters && c.filter.hideFilters( w, e )), k.trigger( "stickyHeadersInit" )
        }
      }, remove : function ( d, e, f ) {
        var g = e.namespace + "stickyheaders ";
        e.$table.removeClass( "hasStickyHeaders" ).unbind( "pagerComplete filterEnd stickyHeadersUpdate ".split( " " ).join( g ).replace( /\s+/g, " " ) ).next( "." + c.css.stickyWrap ).remove(), f.$sticky && f.$sticky.length && f.$sticky.remove(), a( b ).add( f.stickyHeaders_xScroll ).add( f.stickyHeaders_yScroll ).add( f.stickyHeaders_attachTo ).unbind( "scroll resize ".split( " " ).join( g ).replace( /\s+/g, " " ) ), c.addHeaderResizeEvent( d, !1 )
      }
    } )
  }( jQuery, window ), function ( a, b ) {
    "use strict";
    var c = a.tablesorter || {};
    a.extend( c.css, { resizableContainer : "tablesorter-resizable-container", resizableHandle : "tablesorter-resizable-handle", resizableNoSelect : "tablesorter-disableSelection", resizableStorage : "tablesorter-resizable" } ), a( function () {
      var b = "<style>body." + c.css.resizableNoSelect + " { -ms-user-select: none; -moz-user-select: -moz-none;-khtml-user-select: none; -webkit-user-select: none; user-select: none; }." + c.css.resizableContainer + " { position: relative; height: 1px; }." + c.css.resizableHandle + " { position: absolute; display: inline-block; width: 8px;top: 1px; cursor: ew-resize; z-index: 3; user-select: none; -moz-user-select: none; }</style>";
      a( b ).appendTo( "body" )
    } ), c.resizable = {
      init                   : function ( b, d ) {
        if ( !b.$table.hasClass( "hasResizable" ) ) {
          b.$table.addClass( "hasResizable" );
          var e, f, g, h, i, j = b.$table, k = j.parent(), l = parseInt( j.css( "margin-top" ), 10 ), m = d.resizable_vars = { useStorage : c.storage && d.resizable !== !1, $wrap : k, mouseXPosition : 0, $target : null, $next : null, overflow : "auto" === k.css( "overflow" ) || "scroll" === k.css( "overflow" ) || "auto" === k.css( "overflow-x" ) || "scroll" === k.css( "overflow-x" ), storedSizes : [] };
          for ( c.resizableReset( b.table, !0 ), m.tableWidth = j.width(), m.fullWidth = Math.abs( k.width() - m.tableWidth ) < 20, m.useStorage && m.overflow && (c.storage( b.table, "tablesorter-table-original-css-width", m.tableWidth ), i = c.storage( b.table, "tablesorter-table-resized-width" ) || "auto", c.resizable.setWidth( j, i, !0 )), d.resizable_vars.storedSizes = h = (m.useStorage ? c.storage( b.table, c.css.resizableStorage ) : []) || [], c.resizable.setWidths( b, d, h ), c.resizable.updateStoredSizes( b, d ), d.$resizable_container = a( '<div class="' + c.css.resizableContainer + '">' ).css( { top : l } ).insertBefore( j ), g = 0; g < b.columns; g++ )f = b.$headerIndexed[ g ], i = c.getColumnData( b.table, b.headers, g ), e = "false" === c.getData( f, i, "resizable" ), e || a( '<div class="' + c.css.resizableHandle + '">' ).appendTo( d.$resizable_container ).attr( { "data-column" : g, unselectable : "on" } ).data( "header", f ).bind( "selectstart", !1 );
          j.one( "tablesorter-initialized", function () {c.resizable.setHandlePosition( b, d ), c.resizable.bindings( this.config, this.config.widgetOptions )} )
        }
      }, updateStoredSizes   : function ( a, b ) {
        var c, d, e = a.columns, f = b.resizable_vars;
        for ( f.storedSizes = [], c = 0; e > c; c++ )d = a.$headerIndexed[ c ], f.storedSizes[ c ] = d.is( ":visible" ) ? d.width() : 0
      }, setWidth            : function ( a, b, c ) {a.css( { width : b, "min-width" : c ? b : "", "max-width" : c ? b : "" } )}, setWidths : function ( b, d, e ) {
        var f, g, h = d.resizable_vars, i = a( b.namespace + "_extra_headers" ), j = b.$table.children( "colgroup" ).children( "col" );
        if ( e = e || h.storedSizes || [], e.length ) {
          for ( f = 0; f < b.columns; f++ )c.resizable.setWidth( b.$headerIndexed[ f ], e[ f ], h.overflow ), i.length && (g = i.eq( f ).add( j.eq( f ) ), c.resizable.setWidth( g, e[ f ], h.overflow ));
          g = a( b.namespace + "_extra_table" ), g.length && !c.hasWidget( b.table, "scroller" ) && c.resizable.setWidth( g, b.$table.outerWidth(), h.overflow )
        }
      }, setHandlePosition   : function ( b, d ) {
        var e, f = c.hasWidget( b.table, "scroller" ), g = b.$table.height(), h = d.$resizable_container.children(), i = Math.floor( h.width() / 2 );
        f && (g = 0, b.$table.closest( "." + c.css.scrollerWrap ).children().each( function () {
          var b = a( this );
          g += b.filter( '[style*="height"]' ).length ? b.height() : b.children( "table" ).height()
        } )), e = b.$table.position().left, h.each( function () {
          var c = a( this ), f = parseInt( c.attr( "data-column" ), 10 ), h = b.columns - 1, j = c.data( "header" );
          j && (j.is( ":visible" ) ? (h > f || f === h && d.resizable_addLastColumn) && c.css( { display : "inline-block", height : g, left : j.position().left - e + j.outerWidth() - i } ) : c.hide())
        } )
      }, toggleTextSelection : function ( b, d ) {
        var e = b.namespace + "tsresize";
        b.widgetOptions.resizable_vars.disabled = d, a( "body" ).toggleClass( c.css.resizableNoSelect, d ), d ? a( "body" ).attr( "unselectable", "on" ).bind( "selectstart" + e, !1 ) : a( "body" ).removeAttr( "unselectable" ).unbind( "selectstart" + e )
      }, bindings            : function ( d, e ) {
        var f = d.namespace + "tsresize";
        e.$resizable_container.children().bind( "mousedown", function ( b ) {
          var f, g = e.resizable_vars, h = a( d.namespace + "_extra_headers" ), i = a( b.target ).data( "header" );
          f = parseInt( i.attr( "data-column" ), 10 ), g.$target = i = i.add( h.filter( '[data-column="' + f + '"]' ) ), g.target = f, g.$next = b.shiftKey || e.resizable_targetLast ? i.parent().children().not( ".resizable-false" ).filter( ":last" ) : i.nextAll( ":not(.resizable-false)" ).eq( 0 ), f = parseInt( g.$next.attr( "data-column" ), 10 ), g.$next = g.$next.add( h.filter( '[data-column="' + f + '"]' ) ), g.next = f, g.mouseXPosition = b.pageX, c.resizable.updateStoredSizes( d, e ), c.resizable.toggleTextSelection( d, !0 )
        } ), a( document ).bind( "mousemove" + f, function ( a ) {
          var b = e.resizable_vars;
          b.disabled && 0 !== b.mouseXPosition && b.$target && (e.resizable_throttle ? (clearTimeout( b.timer ), b.timer = setTimeout( function () {c.resizable.mouseMove( d, e, a )}, isNaN( e.resizable_throttle ) ? 5 : e.resizable_throttle )) : c.resizable.mouseMove( d, e, a ))
        } ).bind( "mouseup" + f, function () {e.resizable_vars.disabled && (c.resizable.toggleTextSelection( d, !1 ), c.resizable.stopResize( d, e ), c.resizable.setHandlePosition( d, e ))} ), a( b ).bind( "resize" + f + " resizeEnd" + f, function () {c.resizable.setHandlePosition( d, e )} ), d.$table.bind( "columnUpdate" + f, function () {c.resizable.setHandlePosition( d, e )} ).find( "thead:first" ).add( a( d.namespace + "_extra_table" ).find( "thead:first" ) ).bind( "contextmenu" + f, function () {
          var a = 0 === e.resizable_vars.storedSizes.length;
          return c.resizableReset( d.table ), c.resizable.setHandlePosition( d, e ), e.resizable_vars.storedSizes = [], a
        } )
      }, mouseMove           : function ( b, d, e ) {
        if ( 0 !== d.resizable_vars.mouseXPosition && d.resizable_vars.$target ) {
          var f, g = 0, h = d.resizable_vars, i = h.$next, j = h.storedSizes[ h.target ], k = e.pageX - h.mouseXPosition;
          if ( h.overflow ) {
            if ( j + k > 0 ) {
              for ( h.storedSizes[ h.target ] += k, c.resizable.setWidth( h.$target, h.storedSizes[ h.target ], !0 ), f = 0; f < b.columns; f++ )g += h.storedSizes[ f ];
              c.resizable.setWidth( b.$table.add( a( b.namespace + "_extra_table" ) ), g )
            }
            i.length || (h.$wrap[ 0 ].scrollLeft = b.$table.width())
          } else h.fullWidth ? (h.storedSizes[ h.target ] += k, h.storedSizes[ h.next ] -= k, c.resizable.setWidths( b, d )) : (h.storedSizes[ h.target ] += k, c.resizable.setWidths( b, d ));
          h.mouseXPosition = e.pageX, b.$table.trigger( "stickyHeadersUpdate" )
        }
      }, stopResize          : function ( a, b ) {
        var d = b.resizable_vars;
        c.resizable.updateStoredSizes( a, b ), d.useStorage && (c.storage( a.table, c.css.resizableStorage, d.storedSizes ), c.storage( a.table, "tablesorter-table-resized-width", a.$table.width() )), d.mouseXPosition = 0, d.$target = d.$next = null, a.$table.trigger( "stickyHeadersUpdate" )
      }
    }, c.addWidget( {
      id : "resizable", priority : 40, options : { resizable : !0, resizable_addLastColumn : !1, resizable_widths : [], resizable_throttle : !1, resizable_targetLast : !1, resizable_fullWidth : null }, init : function ( a, b, d, e ) {c.resizable.init( d, e )}, remove : function ( b, d, e, f ) {
        if ( e.$resizable_container ) {
          var g = d.namespace + "tsresize";
          d.$table.add( a( d.namespace + "_extra_table" ) ).removeClass( "hasResizable" ).children( "thead" ).unbind( "contextmenu" + g ), e.$resizable_container.remove(), c.resizable.toggleTextSelection( d, !1 ), c.resizableReset( b, f ), a( document ).unbind( "mousemove" + g + " mouseup" + g )
        }
      }
    } ), c.resizableReset = function ( b, d ) {
      a( b ).each( function () {
        var a, e, f = this.config, g = f && f.widgetOptions, h = g.resizable_vars;
        if ( b && f && f.$headerIndexed.length ) {
          for ( h.overflow && h.tableWidth && (c.resizable.setWidth( f.$table, h.tableWidth, !0 ), h.useStorage && c.storage( b, "tablesorter-table-resized-width", "auto" )), a = 0; a < f.columns; a++ )e = f.$headerIndexed[ a ], g.resizable_widths && g.resizable_widths[ a ] ? c.resizable.setWidth( e, g.resizable_widths[ a ], h.overflow ) : e.hasClass( "resizable-false" ) || c.resizable.setWidth( e, "", h.overflow );
          f.$table.trigger( "stickyHeadersUpdate" ), c.storage && !d && c.storage( this, c.css.resizableStorage, {} )
        }
      } )
    }
  }( jQuery, window ), function ( a ) {
    "use strict";
    var b = a.tablesorter || {};
    b.addWidget( {
      id        : "saveSort", priority : 20, options : { saveSort : !0 }, init : function ( a, b, c, d ) {b.format( a, c, d, !0 )}, format : function ( c, d, e, f ) {
        var g, h, i = d.$table, j = e.saveSort !== !1, k = { sortList : d.sortList };
        d.debug && (h = new Date), i.hasClass( "hasSaveSort" ) ? j && c.hasInitialized && b.storage && (b.storage( c, "tablesorter-savesort", k ), d.debug && console.log( "saveSort widget: Saving last sort: " + d.sortList + b.benchmark( h ) )) : (i.addClass( "hasSaveSort" ), k = "", b.storage && (g = b.storage( c, "tablesorter-savesort" ), k = g && g.hasOwnProperty( "sortList" ) && a.isArray( g.sortList ) ? g.sortList : "", d.debug && console.log( 'saveSort: Last sort loaded: "' + k + '"' + b.benchmark( h ) ), i.bind( "saveSortReset", function ( a ) {a.stopPropagation(), b.storage( c, "tablesorter-savesort", "" )} )), f && k && k.length > 0 ? d.sortList = k : c.hasInitialized && k && k.length > 0 && i.trigger( "sorton", [ k ] ))
      }, remove : function ( a, c ) {c.$table.removeClass( "hasSaveSort" ), b.storage && b.storage( a, "tablesorter-savesort", "" )}
    } )
  }( jQuery ), a.tablesorter
} );
/*! Widget: Pager - updated 8/19/2015 (v2.23.1) */
!function ( a ) {
  "use strict";
  var b, c = a.tablesorter;
  c.addWidget( { id : "pager", priority : 55, options : { pager_output : "{startRow} to {endRow} of {totalRows} rows", pager_updateArrows : !0, pager_startPage : 0, pager_pageReset : 0, pager_size : 10, pager_maxOptionSize : 20, pager_savePages : !0, pager_storageKey : "tablesorter-pager", pager_fixedHeight : !1, pager_countChildRows : !1, pager_removeRows : !1, pager_ajaxUrl : null, pager_customAjaxUrl : function ( a, b ) {return b}, pager_ajaxError : null, pager_ajaxObject : { dataType : "json" }, pager_processAjaxOnInit : !0, pager_ajaxProcessing : function ( a ) {return [ 0, [], null ]}, pager_css : { container : "tablesorter-pager", errorRow : "tablesorter-errorRow", disabled : "disabled" }, pager_selectors : { container : ".pager", first : ".first", prev : ".prev", next : ".next", last : ".last", gotoPage : ".gotoPage", pageDisplay : ".pagedisplay", pageSize : ".pagesize" } }, init : function ( a ) {b.init( a )}, format : function ( a, c ) {return c.pager && c.pager.initialized ? void b.moveToPage( a, c.pager, !1 ) : b.initComplete( a, c )}, remove : function ( a, c, d, e ) {b.destroyPager( a, c, e )} } ), b = c.pager = {
    init                 : function ( d ) {
      if ( !d.hasInitialized || !d.config.pager.initialized ) {
        var e, f = d.config, g = f.widgetOptions, h = g.pager_selectors, i = f.pager = a.extend( { totalPages : 0, filteredRows : 0, filteredPages : 0, currentFilters : [], page : g.pager_startPage, startRow : 0, endRow : 0, ajaxCounter : 0, $size : null, last : {}, setSize : g.pager_size, setPage : g.pager_startPage }, f.pager );
        i.isInitializing || (i.isInitializing = !0, f.debug && console.log( "Pager: Initializing" ), i.size = a.data( d, "pagerLastSize" ) || g.pager_size, i.$container = a( h.container ).addClass( g.pager_css.container ).show(), i.$goto = i.$container.find( h.gotoPage ), i.$size = i.$container.find( h.pageSize ), i.totalRows = f.$tbodies.eq( 0 ).children( "tr" ).not( g.pager_countChildRows ? "" : "." + f.cssChildRow ).length, i.oldAjaxSuccess = i.oldAjaxSuccess || g.pager_ajaxObject.success, f.appender = b.appender, i.initializing = !0, g.pager_savePages && c.storage && (e = c.storage( d, g.pager_storageKey ) || {}, i.page = (isNaN( e.page ) ? i.page : e.page) || i.setPage || 0, i.size = (isNaN( e.size ) ? i.size : e.size) || i.setSize || 10, a.data( d, "pagerLastSize", i.size )), i.regexRows = new RegExp( "(" + (g.filter_filteredRow || "filtered") + "|" + f.selectorRemove.slice( 1 ) + "|" + f.cssChildRow + ")" ), i.initialized = !1, f.$table.trigger( "pagerBeforeInitialized", f ), b.enablePager( d, f, !1 ), i.ajaxObject = g.pager_ajaxObject, i.ajaxObject.url = g.pager_ajaxUrl, "string" == typeof g.pager_ajaxUrl ? (i.ajax = !0, g.filter_serversideFiltering = !0, f.serverSideSorting = !0, b.moveToPage( d, i )) : (i.ajax = !1, f.$table.trigger( "appendCache", [ {}, !0 ] )))
      }
    }, initComplete      : function ( a, d ) {
      var e = d.pager;
      b.bindEvents( a, d ), b.setPageSize( a, 0, d ), e.ajax || b.hideRowsSetup( a, d ), e.initialized = !0, e.initializing = !1, e.isInitializing = !1, d.debug && console.log( "Pager: Triggering pagerInitialized" ), d.$table.trigger( "pagerInitialized", d ), d.widgetOptions.filter_initialized && c.hasWidget( a, "filter" ) || b.updatePageDisplay( a, d, !e.ajax )
    }, bindEvents        : function ( c, d ) {
      var e, f, g = d.pager, h = d.widgetOptions, i = d.namespace + "pager", j = h.pager_selectors;
      d.$table.off( i ).on( "filterInit filterStart ".split( " " ).join( i + " " ), function ( b, c ) {g.currentFilters = a.isArray( c ) ? c : d.$table.data( "lastSearch" ), "filterStart" === b.type && h.pager_pageReset !== !1 && (d.lastCombinedFilter || "") !== (g.currentFilters || []).join( "" ) && (g.page = h.pager_pageReset)} ).on( "filterEnd sortEnd ".split( " " ).join( i + " " ), function () {g.currentFilters = d.$table.data( "lastSearch" ), (g.initialized || g.initializing) && (d.delayInit && d.rowsCopy && 0 === d.rowsCopy.length && b.updateCache( c ), b.updatePageDisplay( c, d, !1 ), d.$table.trigger( "applyWidgets" ))} ).on( "disablePager" + i, function ( a ) {a.stopPropagation(), b.showAllRows( c, d )} ).on( "enablePager" + i, function ( a ) {a.stopPropagation(), b.enablePager( c, d, !0 )} ).on( "destroyPager" + i, function ( a, e ) {a.stopPropagation(), b.destroyPager( c, d, e )} ).on( "updateComplete" + i, function ( a, c, e ) {
        if ( a.stopPropagation(), c && !e && !g.ajax ) {
          var f = d.$tbodies.eq( 0 ).children( "tr" ).not( d.selectorRemove );
          g.totalRows = f.length - (h.pager_countChildRows ? 0 : f.filter( "." + d.cssChildRow ).length), g.totalPages = Math.ceil( g.totalRows / g.size ), f.length && d.rowsCopy && 0 === d.rowsCopy.length && b.updateCache( c ), g.page >= g.totalPages && b.moveToLastPage( c, g ), b.hideRows( c, d ), b.changeHeight( c, d ), b.updatePageDisplay( c, d, !1 ), d.$table.trigger( "applyWidgets" ), b.updatePageDisplay( c, d )
        }
      } ).on( "pageSize refreshComplete ".split( " " ).join( i + " " ), function ( a, e ) {a.stopPropagation(), b.setPageSize( c, parseInt( e, 10 ) || g.setSize || 10, d ), b.hideRows( c, d ), b.updatePageDisplay( c, d, !1 )} ).on( "pageSet pagerUpdate ".split( " " ).join( i + " " ), function ( a, e ) {a.stopPropagation(), "pagerUpdate" === a.type && (e = "undefined" == typeof e ? g.page + 1 : e, g.last.page = !0), g.page = (parseInt( e, 10 ) || 1) - 1, b.moveToPage( c, g, !0 ), b.updatePageDisplay( c, d, !1 )} ).on( "pageAndSize" + i, function ( a, e, f ) {a.stopPropagation(), g.page = (parseInt( e, 10 ) || 1) - 1, b.setPageSize( c, parseInt( f, 10 ) || g.setSize || 10, d ), b.moveToPage( c, g, !0 ), b.hideRows( c, d ), b.updatePageDisplay( c, d, !1 )} ), e = [ j.first, j.prev, j.next, j.last ], f = [ "moveToFirstPage", "moveToPrevPage", "moveToNextPage", "moveToLastPage" ], d.debug && !g.$container.length && console.warn( "Pager: >> Container not found" ), g.$container.find( e.join( "," ) ).attr( "tabindex", 0 ).off( "click" + i ).on( "click" + i, function ( d ) {
        d.stopPropagation();
        var i, j = a( this ), k = e.length;
        if ( !j.hasClass( h.pager_css.disabled ) )for ( i = 0; k > i; i++ )if ( j.is( e[ i ] ) ) {
          b[ f[ i ] ]( c, g );
          break
        }
      } ), g.$goto.length ? g.$goto.off( "change" + i ).on( "change" + i, function () {g.page = a( this ).val() - 1, b.moveToPage( c, g, !0 ), b.updatePageDisplay( c, d, !1 )} ) : d.debug && console.warn( "Pager: >> Goto selector not found" ), g.$size.length ? (g.$size.find( "option" ).removeAttr( "selected" ), g.$size.off( "change" + i ).on( "change" + i, function () {return g.$size.val( a( this ).val() ), a( this ).hasClass( h.pager_css.disabled ) || (b.setPageSize( c, parseInt( a( this ).val(), 10 ), d ), b.changeHeight( c, d )), !1} )) : d.debug && console.warn( "Pager: >> Size selector not found" )
    }, pagerArrows       : function ( a, b ) {
      var c = a.pager, d = !!b, e = d || 0 === c.page, f = Math.min( c.totalPages, c.filteredPages ), g = d || c.page === f - 1 || 0 === f, h = a.widgetOptions, i = h.pager_selectors;
      h.pager_updateArrows && (c.$container.find( i.first + "," + i.prev ).toggleClass( h.pager_css.disabled, e ).attr( "aria-disabled", e ), c.$container.find( i.next + "," + i.last ).toggleClass( h.pager_css.disabled, g ).attr( "aria-disabled", g ))
    }, calcFilters       : function ( b, c ) {
      var d, e, f, g = c.widgetOptions, h = c.pager, i = c.$table.hasClass( "hasFilters" );
      if ( i && !g.pager_ajaxUrl )if ( a.isEmptyObject( c.cache ) )h.filteredRows = h.totalRows = c.$tbodies.eq( 0 ).children( "tr" ).not( g.pager_countChildRows ? "" : "." + c.cssChildRow ).length; else for ( h.filteredRows = 0, d = c.cache[ 0 ].normalized, f = d.length, e = 0; f > e; e++ )h.filteredRows += h.regexRows.test( d[ e ][ c.columns ].$row[ 0 ].className ) ? 0 : 1; else i || (h.filteredRows = h.totalRows)
    }, updatePageDisplay : function ( d, e, f ) {
      if ( !e.pager.initializing ) {
        var g, h, i, j, k, l, m = e.widgetOptions, n = e.pager, o = e.namespace + "pager", p = n.size || n.setSize || 10;
        if ( m.pager_countChildRows && h.push( e.cssChildRow ), n.$size.add( n.$goto ).removeClass( m.pager_css.disabled ).removeAttr( "disabled" ).attr( "aria-disabled", "false" ), n.totalPages = Math.ceil( n.totalRows / p ), e.totalRows = n.totalRows, b.calcFilters( d, e ), e.filteredRows = n.filteredRows, n.filteredPages = Math.ceil( n.filteredRows / p ) || 0, Math.min( n.totalPages, n.filteredPages ) >= 0 ) {
          if ( h = n.size * n.page > n.filteredRows && f, n.page = h ? m.pager_pageReset || 0 : n.page, n.startRow = h ? n.size * n.page + 1 : 0 === n.filteredRows ? 0 : n.size * n.page + 1, n.endRow = Math.min( n.filteredRows, n.totalRows, n.size * (n.page + 1) ), i = n.$container.find( m.pager_selectors.pageDisplay ), g = (n.ajaxData && n.ajaxData.output ? n.ajaxData.output || m.pager_output : m.pager_output).replace( /\{page([\-+]\d+)?\}/gi, function ( a, b ) {return n.totalPages ? n.page + (b ? parseInt( b, 10 ) : 1) : 0} ).replace( /\{\w+(\s*:\s*\w+)?\}/gi, function ( a ) {
              var b, c, d = a.replace( /[{}\s]/g, "" ), e = d.split( ":" ), f = n.ajaxData, g = /(rows?|pages?)$/i.test( d ) ? 0 : "";
              return /(startRow|page)/.test( e[ 0 ] ) && "input" === e[ 1 ] ? (b = ("" + ("page" === e[ 0 ] ? n.totalPages : n.totalRows)).length, c = "page" === e[ 0 ] ? n.page + 1 : n.startRow, '<input type="text" class="ts-' + e[ 0 ] + '" style="max-width:' + b + 'em" value="' + c + '"/>') : e.length > 1 && f && f[ e[ 0 ] ] ? f[ e[ 0 ] ][ e[ 1 ] ] : n[ d ] || (f ? f[ d ] : g) || g
            } ), n.$goto.length ) {
            for ( h = "", j = b.buildPageSelect( n, e ), l = j.length, k = 0; l > k; k++ )h += '<option value="' + j[ k ] + '">' + j[ k ] + "</option>";
            n.$goto.html( h ).val( n.page + 1 )
          }
          i.length && (i[ "INPUT" === i[ 0 ].nodeName ? "val" : "html" ]( g ), i.find( ".ts-startRow, .ts-page" ).off( "change" + o ).on( "change" + o, function () {
            var b = a( this ).val(), c = a( this ).hasClass( "ts-startRow" ) ? Math.floor( b / n.size ) + 1 : b;
            e.$table.trigger( "pageSet" + o, [ c ] )
          } ))
        }
        b.pagerArrows( e ), b.fixHeight( d, e ), n.initialized && f !== !1 && (e.debug && console.log( "Pager: Triggering pagerComplete" ), e.$table.trigger( "pagerComplete", e ), m.pager_savePages && c.storage && c.storage( d, m.pager_storageKey, { page : n.page, size : n.size } ))
      }
    }, buildPageSelect   : function ( b, c ) {
      var d, e, f, g, h, i, j = c.widgetOptions, k = Math.min( b.totalPages, b.filteredPages ) || 1, l = 5 * Math.ceil( k / j.pager_maxOptionSize / 5 ), m = k > j.pager_maxOptionSize, n = b.page + 1, o = l, p = k - l, q = [ 1 ], r = m ? l : 1;
      for ( d = r; k >= d; )q.push( d ), d += m ? l : 1;
      if ( q.push( k ), m ) {
        for ( f = [], e = Math.max( Math.floor( j.pager_maxOptionSize / l ) - 1, 5 ), o = n - e, 1 > o && (o = 1), p = n + e, p > k && (p = k), d = o; p >= d; d++ )f.push( d );
        q = a.grep( q, function ( b, c ) {return a.inArray( b, q ) === c} ), h = q.length, i = f.length, h - i > l / 2 && h + i > j.pager_maxOptionSize && (g = Math.floor( h / 2 ) - Math.floor( i / 2 ), Array.prototype.splice.apply( q, [ g, i ] )), q = q.concat( f )
      }
      return q = a.grep( q, function ( b, c ) {return a.inArray( b, q ) === c} ).sort( function ( a, b ) {return a - b} )
    }, fixHeight         : function ( b, c ) {
      var d, e, f = c.pager, g = c.widgetOptions, h = c.$tbodies.eq( 0 );
      h.find( "tr.pagerSavedHeightSpacer" ).remove(), g.pager_fixedHeight && !f.isDisabled && (e = a.data( b, "pagerSavedHeight" ), e && (d = e - h.height(), d > 5 && a.data( b, "pagerLastSize" ) === f.size && h.children( "tr:visible" ).length < f.size && h.append( '<tr class="pagerSavedHeightSpacer ' + c.selectorRemove.slice( 1 ) + '" style="height:' + d + 'px;"></tr>' )))
    }, changeHeight      : function ( c, d ) {
      var e, f = d.$tbodies.eq( 0 );
      f.find( "tr.pagerSavedHeightSpacer" ).remove(), f.children( "tr:visible" ).length || f.append( '<tr class="pagerSavedHeightSpacer ' + d.selectorRemove.slice( 1 ) + '"><td>&nbsp</td></tr>' ), e = f.children( "tr" ).eq( 0 ).height() * d.pager.size, a.data( c, "pagerSavedHeight", e ), b.fixHeight( c, d ), a.data( c, "pagerLastSize", d.pager.size )
    }, hideRows          : function ( a, b ) {
      if ( !b.widgetOptions.pager_ajaxUrl ) {
        var d, e, f, g, h, i = b.pager, j = b.widgetOptions, k = b.$tbodies.length, l = i.page * i.size, m = l + i.size, n = j && j.filter_filteredRow || "filtered", o = 0, p = 0;
        for ( i.cacheIndex = [], d = 0; k > d; d++ ) {
          for ( f = b.$tbodies.eq( d ).children( "tr" ), g = f.length, h = 0, o = 0, p = 0, e = 0; g > e; e++ )f[ e ].className.match( n ) || (p === l && f[ e ].className.match( b.cssChildRow ) ? f[ e ].style.display = "none" : (f[ e ].style.display = p >= l && m > p ? "" : "none", o !== p && p >= l && m > p && (i.cacheIndex.push( e ), o = p), p += f[ e ].className.match( b.cssChildRow + "|" + b.selectorRemove.slice( 1 ) ) && !j.pager_countChildRows ? 0 : 1, p === m && "none" !== f[ e ].style.display && f[ e ].className.match( c.css.cssHasChild ) && (h = e)));
          if ( h > 0 && f[ h ].className.match( c.css.cssHasChild ) )for ( ; ++h < g && f[ h ].className.match( b.cssChildRow ); )f[ h ].style.display = ""
        }
      }
    }, hideRowsSetup     : function ( c, d ) {
      var e = d.pager, f = d.namespace + "pager";
      e.size = parseInt( e.$size.val(), 10 ) || e.size || e.setSize || 10, a.data( c, "pagerLastSize", e.size ), b.pagerArrows( d ), d.widgetOptions.pager_removeRows || (b.hideRows( c, d ), d.$table.on( "sortEnd filterEnd ".split( " " ).join( f + " " ), function () {b.hideRows( c, d )} ))
    }, renderAjax        : function ( d, e, f, g, h, i ) {
      var j = f.pager, k = f.widgetOptions;
      if ( a.isFunction( k.pager_ajaxProcessing ) ) {
        var l, m, n, o, p, q, r, s, t, u, v, w, x, y, z = f.$table, A = "", B = k.pager_ajaxProcessing( d, e, g ) || [ 0, [] ], C = z.find( "thead th" ).length;
        if ( c.showError( e ), i )f.debug && console.error( "Pager: >> Ajax Error", g, h, i ), c.showError( e, g, h, i ), f.$tbodies.eq( 0 ).children( "tr" ).detach(), j.totalRows = 0; else {
          if ( a.isArray( B ) ? (n = isNaN( B[ 0 ] ) && !isNaN( B[ 1 ] ), x = B[ n ? 1 : 0 ], j.totalRows = isNaN( x ) ? j.totalRows || 0 : x, f.totalRows = f.filteredRows = j.filteredRows = j.totalRows, v = 0 === j.totalRows ? [] : B[ n ? 0 : 1 ] || [], u = B[ 2 ]) : (j.ajaxData = B, f.totalRows = j.totalRows = B.total, f.filteredRows = j.filteredRows = "undefined" != typeof B.filteredRows ? B.filteredRows : B.total, u = B.headers, v = B.rows || []), w = v && v.length, v instanceof jQuery )k.pager_processAjaxOnInit && (f.$tbodies.eq( 0 ).children( "tr" ).detach(), f.$tbodies.eq( 0 ).append( v )); else if ( w ) {
            for ( l = 0; w > l; l++ ) {
              for ( A += "<tr>", m = 0; m < v[ l ].length; m++ )A += /^\s*<td/.test( v[ l ][ m ] ) ? a.trim( v[ l ][ m ] ) : "<td>" + v[ l ][ m ] + "</td>";
              A += "</tr>"
            }
            k.pager_processAjaxOnInit && f.$tbodies.eq( 0 ).html( A )
          }
          if ( k.pager_processAjaxOnInit = !0, u && u.length === C )for ( o = z.hasClass( "hasStickyHeaders" ), q = o ? k.$sticky.children( "thead:first" ).children( "tr" ).children() : "", p = z.find( "tfoot tr:first" ).children(), r = f.$headers.filter( "th " ), y = r.length, m = 0; y > m; m++ )s = r.eq( m ), s.find( "." + c.css.icon ).length ? (t = s.find( "." + c.css.icon ).clone( !0 ), s.find( ".tablesorter-header-inner" ).html( u[ m ] ).append( t ), o && q.length && (t = q.eq( m ).find( "." + c.css.icon ).clone( !0 ), q.eq( m ).find( ".tablesorter-header-inner" ).html( u[ m ] ).append( t ))) : (s.find( ".tablesorter-header-inner" ).html( u[ m ] ), o && q.length && q.eq( m ).find( ".tablesorter-header-inner" ).html( u[ m ] )), p.eq( m ).html( u[ m ] )
        }
        f.showProcessing && c.isProcessing( e ), j.totalPages = Math.ceil( j.totalRows / (j.size || j.setSize || 10) ), j.last.totalRows = j.totalRows, j.last.currentFilters = j.currentFilters, j.last.sortList = (f.sortList || []).join( "," ), j.initializing = !1, b.updatePageDisplay( e, f, !1 ), z.trigger( "updateCache", [ function () {j.initialized && setTimeout( function () {f.debug && console.log( "Pager: Triggering pagerChange" ), z.trigger( "applyWidgets" ).trigger( "pagerChange", j ), b.updatePageDisplay( e, f )}, 0 )} ] )
      }
      j.initialized || f.$table.trigger( "applyWidgets" )
    }, getAjax           : function ( d, e ) {
      var f, g = b.getAjaxUrl( d, e ), h = a( document ), i = e.namespace + "pager", j = e.pager;
      "" !== g && (e.showProcessing && c.isProcessing( d, !0 ), h.on( "ajaxError" + i, function ( a, c, f, g ) {b.renderAjax( null, d, e, c, f, g ), h.off( "ajaxError" + i )} ), f = ++j.ajaxCounter, j.last.ajaxUrl = g, j.ajaxObject.url = g, j.ajaxObject.success = function ( a, c, g ) {f < j.ajaxCounter || (b.renderAjax( a, d, e, g ), h.off( "ajaxError" + i ), "function" == typeof j.oldAjaxSuccess && j.oldAjaxSuccess( a ))}, e.debug && console.log( "Pager: Ajax initialized", j.ajaxObject ), a.ajax( j.ajaxObject ))
    }, getAjaxUrl        : function ( b, c ) {
      var d, e, f = c.pager, g = c.widgetOptions, h = g.pager_ajaxUrl ? g.pager_ajaxUrl.replace( /\{page([\-+]\d+)?\}/, function ( a, b ) {return f.page + (b ? parseInt( b, 10 ) : 0)} ).replace( /\{size\}/g, f.size ) : "", i = c.sortList, j = f.currentFilters || a( b ).data( "lastSearch" ) || [], k = h.match( /\{\s*sort(?:List)?\s*:\s*(\w*)\s*\}/ ), l = h.match( /\{\s*filter(?:List)?\s*:\s*(\w*)\s*\}/ ), m = [];
      if ( k ) {
        for ( k = k[ 1 ], e = i.length, d = 0; e > d; d++ )m.push( k + "[" + i[ d ][ 0 ] + "]=" + i[ d ][ 1 ] );
        h = h.replace( /\{\s*sort(?:List)?\s*:\s*(\w*)\s*\}/g, m.length ? m.join( "&" ) : k ), m = []
      }
      if ( l ) {
        for ( l = l[ 1 ], e = j.length, d = 0; e > d; d++ )j[ d ] && m.push( l + "[" + d + "]=" + encodeURIComponent( j[ d ] ) );
        h = h.replace( /\{\s*filter(?:List)?\s*:\s*(\w*)\s*\}/g, m.length ? m.join( "&" ) : l ), f.currentFilters = j
      }
      return a.isFunction( g.pager_customAjaxUrl ) && (h = g.pager_customAjaxUrl( b, h )), c.debug && console.log( "Pager: Ajax url = " + h ), h
    }, renderTable       : function ( a, d ) {
      var e, f, g, h, i = a.config, j = i.pager, k = i.widgetOptions, l = i.$table.hasClass( "hasFilters" ), m = d && d.length || 0, n = j.page * j.size, o = j.size;
      if ( 1 > m )return void(i.debug && console.warn( "Pager: >> No rows for pager to render" ));
      if ( j.page >= j.totalPages )return b.moveToLastPage( a, j );
      if ( j.cacheIndex = [], j.isDisabled = !1, j.initialized && (i.debug && console.log( "Pager: Triggering pagerChange" ), i.$table.trigger( "pagerChange", i )), k.pager_removeRows ) {
        for ( c.clearTableBody( a ), e = c.processTbody( a, i.$tbodies.eq( 0 ), !0 ), f = l ? 0 : n, g = l ? 0 : n, h = 0; o > h && f < d.length; )l && /filtered/.test( d[ f ][ 0 ].className ) || (g++, g > n && o >= h && (h++, j.cacheIndex.push( f ), e.append( d[ f ] ))), f++;
        c.processTbody( a, e, !1 )
      } else b.hideRows( a, i );
      b.updatePageDisplay( a, i ), k.pager_startPage = j.page, k.pager_size = j.size, a.isUpdating && (i.debug && console.log( "Pager: Triggering updateComplete" ), i.$table.trigger( "updateComplete", [ a, !0 ] ))
    }, showAllRows       : function ( c, d ) {
      var e, f, g, h = d.pager, i = d.widgetOptions;
      for ( h.ajax ? b.pagerArrows( d, !0 ) : (h.isDisabled = !0, a.data( c, "pagerLastPage", h.page ), a.data( c, "pagerLastSize", h.size ), h.page = 0, h.size = h.totalRows, h.totalPages = 1, d.$table.addClass( "pagerDisabled" ).removeAttr( "aria-describedby" ).find( "tr.pagerSavedHeightSpacer" ).remove(), b.renderTable( c, d.rowsCopy ), d.$table.trigger( "applyWidgets" ), d.debug && console.log( "Pager: Disabled" )), f = h.$size.add( h.$goto ).add( h.$container.find( ".ts-startRow, .ts-page " ) ), g = f.length, e = 0; g > e; e++ )f.eq( e ).attr( "aria-disabled", "true" ).addClass( i.pager_css.disabled )[ 0 ].disabled = !0
    }, updateCache       : function ( c ) {
      var d = c.config, e = d.pager;
      d.$table.trigger( "updateCache", [ function () {
        if ( !a.isEmptyObject( c.config.cache ) ) {
          var f, g = [], h = c.config.cache[ 0 ].normalized;
          for ( e.totalRows = h.length, f = 0; f < e.totalRows; f++ )g.push( h[ f ][ d.columns ].$row );
          d.rowsCopy = g, b.moveToPage( c, e, !0 ), e.last.currentFilters = [ " " ]
        }
      } ] )
    }, moveToPage        : function ( d, e, f ) {
      if ( !e.isDisabled ) {
        if ( f !== !1 && e.initialized && a.isEmptyObject( d.config.cache ) )return b.updateCache( d );
        var g, h = d.config, i = h.widgetOptions, j = e.last;
        e.ajax && !i.filter_initialized && c.hasWidget( d, "filter" ) || (b.calcFilters( d, h ), g = Math.min( e.totalPages, e.filteredPages ), e.page < 0 && (e.page = 0), e.page > g - 1 && 0 !== g && (e.page = g - 1), j.currentFilters = "" === (j.currentFilters || []).join( "" ) ? [] : j.currentFilters, e.currentFilters = "" === (e.currentFilters || []).join( "" ) ? [] : e.currentFilters, (j.page !== e.page || j.size !== e.size || j.totalRows !== e.totalRows || (j.currentFilters || []).join( "," ) !== (e.currentFilters || []).join( "," ) || (j.ajaxUrl || "") !== (e.ajaxObject.url || "") || (j.optAjaxUrl || "") !== (i.pager_ajaxUrl || "") || j.sortList !== (h.sortList || []).join( "," )) && (h.debug && console.log( "Pager: Changing to page " + e.page ), e.last = { page : e.page, size : e.size, sortList : (h.sortList || []).join( "," ), totalRows : e.totalRows, currentFilters : e.currentFilters || [], ajaxUrl : e.ajaxObject.url || "", optAjaxUrl : i.pager_ajaxUrl }, e.ajax ? b.getAjax( d, h ) : e.ajax || b.renderTable( d, h.rowsCopy ), a.data( d, "pagerLastPage", e.page ), e.initialized && f !== !1 && (h.debug && console.log( "Pager: Triggering pageMoved" ), h.$table.trigger( "pageMoved", h ).trigger( "applyWidgets" ), !e.ajax && d.isUpdating && (h.debug && console.log( "Pager: Triggering updateComplete" ), h.$table.trigger( "updateComplete", [ d, !0 ] )))))
      }
    }, setPageSize       : function ( c, d, e ) {
      var f = e.pager;
      f.size = d || f.size || f.setSize || 10, f.$size.val( f.size ), a.data( c, "pagerLastPage", f.page ), a.data( c, "pagerLastSize", f.size ), f.totalPages = Math.ceil( f.totalRows / f.size ), f.filteredPages = Math.ceil( f.filteredRows / f.size ), b.moveToPage( c, f, !0 )
    }, moveToFirstPage   : function ( a, c ) {c.page = 0, b.moveToPage( a, c, !0 )}, moveToLastPage : function ( a, c ) {c.page = Math.min( c.totalPages, c.filteredPages ) - 1, b.moveToPage( a, c, !0 )}, moveToNextPage : function ( a, c ) {c.page++, c.page >= Math.min( c.totalPages, c.filteredPages ) - 1 && (c.page = Math.min( c.totalPages, c.filteredPages ) - 1), b.moveToPage( a, c, !0 )}, moveToPrevPage : function ( a, c ) {c.page--, c.page <= 0 && (c.page = 0), b.moveToPage( a, c, !0 )}, destroyPager : function ( a, d, e ) {
      var f = d.pager, g = d.widgetOptions.pager_selectors, h = [ g.first, g.prev, g.next, g.last, g.gotoPage, g.pageSize ].join( "," ), i = d.namespace + "pager";
      f.initialized = !1, d.$table.off( i ), f.$container.hide().find( h ).off( i ), e || (b.showAllRows( a, d ), d.appender = null, c.storage && c.storage( a, d.widgetOptions.pager_storageKey, "" ), delete a.config.pager, delete a.config.rowsCopy)
    }, enablePager       : function ( c, d, e ) {
      var f, g = d.pager;
      g.isDisabled = !1, g.page = a.data( c, "pagerLastPage" ) || g.page || 0, g.size = a.data( c, "pagerLastSize" ) || parseInt( g.$size.find( "option[selected]" ).val(), 10 ) || g.size || g.setSize || 10, g.$size.val( g.size ), g.totalPages = Math.ceil( Math.min( g.totalRows, g.filteredRows ) / g.size ), d.$table.removeClass( "pagerDisabled" ), c.id && (f = c.id + "_pager_info", g.$container.find( d.widgetOptions.pager_selectors.pageDisplay ).attr( "id", f ), d.$table.attr( "aria-describedby", f )), b.changeHeight( c, d ), e && (d.$table.trigger( "updateRows" ), b.setPageSize( c, g.size, d ), b.hideRowsSetup( c, d ), d.debug && console.log( "Pager: Enabled" ))
    }, appender          : function ( c, d ) {
      var e = c.config, f = e.widgetOptions, g = e.pager;
      g.ajax ? b.moveToPage( c, g, !0 ) : (e.rowsCopy = d, g.totalRows = f.pager_countChildRows ? e.$tbodies.eq( 0 ).children( "tr" ).length : d.length, g.size = a.data( c, "pagerLastSize" ) || g.size || f.pager_size || g.setSize || 10, g.totalPages = Math.ceil( g.totalRows / g.size ), b.moveToPage( c, g ), b.updatePageDisplay( c, e, !1 ))
    }
  }, c.showError = function ( b, c, d, e ) {
    var f, g = a( b ), h = g[ 0 ].config, i = h && h.widgetOptions, j = h.pager && h.pager.cssErrorRow || i && i.pager_css && i.pager_css.errorRow || "tablesorter-errorRow", k = typeof c, l = !0, m = "", n = function () {h.$table.find( "thead" ).find( "." + j ).remove()};
    if ( !g.length )return void console.error( "tablesorter showError: no table parameter passed" );
    if ( "function" == typeof h.pager.ajaxError ) {
      if ( l = h.pager.ajaxError( h, c, d, e ), l === !1 )return n();
      m = l
    } else if ( "function" == typeof i.pager_ajaxError ) {
      if ( l = i.pager_ajaxError( h, c, d, e ), l === !1 )return n();
      m = l
    }
    if ( "" === m )if ( "object" === k )m = 0 === c.status ? "Not connected, verify Network" : 404 === c.status ? "Requested page not found [404]" : 500 === c.status ? "Internal Server Error [500]" : "parsererror" === e ? "Requested JSON parse failed" : "timeout" === e ? "Time out error" : "abort" === e ? "Ajax Request aborted" : "Uncaught error: " + c.statusText + " [" + c.status + "]"; else {
      if ( "string" !== k )return n();
      m = c
    }
    f = a( /tr\>/.test( m ) ? m : '<tr><td colspan="' + h.columns + '">' + m + "</td></tr>" ).click( function () {a( this ).remove()} ).appendTo( h.$table.find( "thead:first" ) ).addClass( j + " " + h.selectorRemove.slice( 1 ) ).attr( { role : "alert", "aria-live" : "assertive" } )
  }
}( jQuery );
/*!

 handlebars v4.0.2

 Copyright (C) 2011-2015 by Yehuda Katz

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

 @license
 */
(function webpackUniversalModuleDefinition ( root, factory ) {
  if ( typeof exports === 'object' && typeof module === 'object' )
    module.exports = factory();
  else if ( typeof define === 'function' && define.amd )
    define( [], factory );
  else if ( typeof exports === 'object' )
    exports[ "Handlebars" ] = factory();
  else
    root[ "Handlebars" ] = factory();
})( this, function () {
  return /******/ (function ( modules ) { // webpackBootstrap
    /******/ 	// The module cache
    /******/
    var installedModules = {};

    /******/ 	// The require function
    /******/
    function __webpack_require__ ( moduleId ) {

      /******/ 		// Check if module is in cache
      /******/
      if ( installedModules[ moduleId ] )
      /******/      return installedModules[ moduleId ].exports;

      /******/ 		// Create a new module (and put it into the cache)
      /******/
      var module = installedModules[ moduleId ] = {
        /******/      exports : {},
        /******/      id      : moduleId,
        /******/      loaded  : false
        /******/
      };

      /******/ 		// Execute the module function
      /******/
      modules[ moduleId ].call( module.exports, module, module.exports, __webpack_require__ );

      /******/ 		// Flag the module as loaded
      /******/
      module.loaded = true;

      /******/ 		// Return the exports of the module
      /******/
      return module.exports;
      /******/
    }

    /******/ 	// expose the modules object (__webpack_modules__)
    /******/
    __webpack_require__.m = modules;

    /******/ 	// expose the module cache
    /******/
    __webpack_require__.c = installedModules;

    /******/ 	// __webpack_public_path__
    /******/
    __webpack_require__.p = "";

    /******/ 	// Load entry module and return exports
    /******/
    return __webpack_require__( 0 );
    /******/
  })
  /************************************************************************/
  /******/( [
              /* 0 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;

      var _handlebarsRuntime = __webpack_require__( 2 );

      var _handlebarsRuntime2 = _interopRequireDefault( _handlebarsRuntime );

      // Compiler imports

      var _handlebarsCompilerAst = __webpack_require__( 21 );

      var _handlebarsCompilerAst2 = _interopRequireDefault( _handlebarsCompilerAst );

      var _handlebarsCompilerBase = __webpack_require__( 22 );

      var _handlebarsCompilerCompiler = __webpack_require__( 27 );

      var _handlebarsCompilerJavascriptCompiler = __webpack_require__( 28 );

      var _handlebarsCompilerJavascriptCompiler2 = _interopRequireDefault( _handlebarsCompilerJavascriptCompiler );

      var _handlebarsCompilerVisitor = __webpack_require__( 25 );

      var _handlebarsCompilerVisitor2 = _interopRequireDefault( _handlebarsCompilerVisitor );

      var _handlebarsNoConflict = __webpack_require__( 20 );

      var _handlebarsNoConflict2 = _interopRequireDefault( _handlebarsNoConflict );

      var _create = _handlebarsRuntime2[ 'default' ].create;

      function create () {
        var hb = _create();

        hb.compile = function ( input, options ) {
          return _handlebarsCompilerCompiler.compile( input, options, hb );
        };
        hb.precompile = function ( input, options ) {
          return _handlebarsCompilerCompiler.precompile( input, options, hb );
        };

        hb.AST = _handlebarsCompilerAst2[ 'default' ];
        hb.Compiler = _handlebarsCompilerCompiler.Compiler;
        hb.JavaScriptCompiler = _handlebarsCompilerJavascriptCompiler2[ 'default' ];
        hb.Parser = _handlebarsCompilerBase.parser;
        hb.parse = _handlebarsCompilerBase.parse;

        return hb;
      }

      var inst = create();
      inst.create = create;

      _handlebarsNoConflict2[ 'default' ]( inst );

      inst.Visitor = _handlebarsCompilerVisitor2[ 'default' ];

      inst[ 'default' ] = inst;

      exports[ 'default' ] = inst;
      module.exports = exports[ 'default' ];

      /***/
    },
              /* 1 */
              /***/ function ( module, exports ) {

      "use strict";

      exports[ "default" ] = function ( obj ) {
        return obj && obj.__esModule ? obj : {
          "default" : obj
        };
      };

      exports.__esModule = true;

      /***/
    },
              /* 2 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireWildcard = __webpack_require__( 3 )[ 'default' ];

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;

      var _handlebarsBase = __webpack_require__( 4 );

      var base = _interopRequireWildcard( _handlebarsBase );

      // Each of these augment the Handlebars object. No need to setup here.
      // (This is done to easily share code between commonjs and browse envs)

      var _handlebarsSafeString = __webpack_require__( 18 );

      var _handlebarsSafeString2 = _interopRequireDefault( _handlebarsSafeString );

      var _handlebarsException = __webpack_require__( 6 );

      var _handlebarsException2 = _interopRequireDefault( _handlebarsException );

      var _handlebarsUtils = __webpack_require__( 5 );

      var Utils = _interopRequireWildcard( _handlebarsUtils );

      var _handlebarsRuntime = __webpack_require__( 19 );

      var runtime = _interopRequireWildcard( _handlebarsRuntime );

      var _handlebarsNoConflict = __webpack_require__( 20 );

      var _handlebarsNoConflict2 = _interopRequireDefault( _handlebarsNoConflict );

      // For compatibility and usage outside of module systems, make the Handlebars object a namespace
      function create () {
        var hb = new base.HandlebarsEnvironment();

        Utils.extend( hb, base );
        hb.SafeString = _handlebarsSafeString2[ 'default' ];
        hb.Exception = _handlebarsException2[ 'default' ];
        hb.Utils = Utils;
        hb.escapeExpression = Utils.escapeExpression;

        hb.VM = runtime;
        hb.template = function ( spec ) {
          return runtime.template( spec, hb );
        };

        return hb;
      }

      var inst = create();
      inst.create = create;

      _handlebarsNoConflict2[ 'default' ]( inst );

      inst[ 'default' ] = inst;

      exports[ 'default' ] = inst;
      module.exports = exports[ 'default' ];

      /***/
    },
              /* 3 */
              /***/ function ( module, exports ) {

      "use strict";

      exports[ "default" ] = function ( obj ) {
        if ( obj && obj.__esModule ) {
          return obj;
        } else {
          var newObj = {};

          if ( obj != null ) {
            for ( var key in obj ) {
              if ( Object.prototype.hasOwnProperty.call( obj, key ) ) newObj[ key ] = obj[ key ];
            }
          }

          newObj[ "default" ] = obj;
          return newObj;
        }
      };

      exports.__esModule = true;

      /***/
    },
              /* 4 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;
      exports.HandlebarsEnvironment = HandlebarsEnvironment;

      var _utils = __webpack_require__( 5 );

      var _exception = __webpack_require__( 6 );

      var _exception2 = _interopRequireDefault( _exception );

      var _helpers = __webpack_require__( 7 );

      var _decorators = __webpack_require__( 15 );

      var _logger = __webpack_require__( 17 );

      var _logger2 = _interopRequireDefault( _logger );

      var VERSION = '4.0.2';
      exports.VERSION = VERSION;
      var COMPILER_REVISION = 7;

      exports.COMPILER_REVISION = COMPILER_REVISION;
      var REVISION_CHANGES = {
        1 : '<= 1.0.rc.2', // 1.0.rc.2 is actually rev2 but doesn't report it
        2 : '== 1.0.0-rc.3',
        3 : '== 1.0.0-rc.4',
        4 : '== 1.x.x',
        5 : '== 2.0.0-alpha.x',
        6 : '>= 2.0.0-beta.1',
        7 : '>= 4.0.0'
      };

      exports.REVISION_CHANGES = REVISION_CHANGES;
      var objectType = '[object Object]';

      function HandlebarsEnvironment ( helpers, partials, decorators ) {
        this.helpers = helpers || {};
        this.partials = partials || {};
        this.decorators = decorators || {};

        _helpers.registerDefaultHelpers( this );
        _decorators.registerDefaultDecorators( this );
      }

      HandlebarsEnvironment.prototype = {
        constructor : HandlebarsEnvironment,

        logger : _logger2[ 'default' ],
        log    : _logger2[ 'default' ].log,

        registerHelper   : function registerHelper ( name, fn ) {
          if ( _utils.toString.call( name ) === objectType ) {
            if ( fn ) {
              throw new _exception2[ 'default' ]( 'Arg not supported with multiple helpers' );
            }
            _utils.extend( this.helpers, name );
          } else {
            this.helpers[ name ] = fn;
          }
        },
        unregisterHelper : function unregisterHelper ( name ) {
          delete this.helpers[ name ];
        },

        registerPartial   : function registerPartial ( name, partial ) {
          if ( _utils.toString.call( name ) === objectType ) {
            _utils.extend( this.partials, name );
          } else {
            if ( typeof partial === 'undefined' ) {
              throw new _exception2[ 'default' ]( 'Attempting to register a partial as undefined' );
            }
            this.partials[ name ] = partial;
          }
        },
        unregisterPartial : function unregisterPartial ( name ) {
          delete this.partials[ name ];
        },

        registerDecorator   : function registerDecorator ( name, fn ) {
          if ( _utils.toString.call( name ) === objectType ) {
            if ( fn ) {
              throw new _exception2[ 'default' ]( 'Arg not supported with multiple decorators' );
            }
            _utils.extend( this.decorators, name );
          } else {
            this.decorators[ name ] = fn;
          }
        },
        unregisterDecorator : function unregisterDecorator ( name ) {
          delete this.decorators[ name ];
        }
      };

      var log = _logger2[ 'default' ].log;

      exports.log = log;
      exports.createFrame = _utils.createFrame;
      exports.logger = _logger2[ 'default' ];

      /***/
    },
              /* 5 */
              /***/ function ( module, exports ) {

      'use strict';

      exports.__esModule = true;
      exports.extend = extend;
      exports.indexOf = indexOf;
      exports.escapeExpression = escapeExpression;
      exports.isEmpty = isEmpty;
      exports.createFrame = createFrame;
      exports.blockParams = blockParams;
      exports.appendContextPath = appendContextPath;
      var escape = {
        '&' : '&amp;',
        '<' : '&lt;',
        '>' : '&gt;',
        '"' : '&quot;',
        "'" : '&#x27;',
        '`' : '&#x60;',
        '=' : '&#x3D;'
      };

      var badChars = /[&<>"'`=]/g,
          possible = /[&<>"'`=]/;

      function escapeChar ( chr ) {
        return escape[ chr ];
      }

      function extend ( obj /* , ...source */ ) {
        for ( var i = 1; i < arguments.length; i++ ) {
          for ( var key in arguments[ i ] ) {
            if ( Object.prototype.hasOwnProperty.call( arguments[ i ], key ) ) {
              obj[ key ] = arguments[ i ][ key ];
            }
          }
        }

        return obj;
      }

      var toString = Object.prototype.toString;

      exports.toString = toString;
      // Sourced from lodash
      // https://github.com/bestiejs/lodash/blob/master/LICENSE.txt
      /* eslint-disable func-style */
      var isFunction = function isFunction ( value ) {
        return typeof value === 'function';
      };
      // fallback for older versions of Chrome and Safari
      /* istanbul ignore next */
      if ( isFunction( /x/ ) ) {
        exports.isFunction = isFunction = function ( value ) {
          return typeof value === 'function' && toString.call( value ) === '[object Function]';
        };
      }
      exports.isFunction = isFunction;

      /* eslint-enable func-style */

      /* istanbul ignore next */
      var isArray = Array.isArray || function ( value ) {
          return value && typeof value === 'object' ? toString.call( value ) === '[object Array]' : false;
        };

      exports.isArray = isArray;
      // Older IE versions do not directly support indexOf so we must implement our own, sadly.

      function indexOf ( array, value ) {
        for ( var i = 0, len = array.length; i < len; i++ ) {
          if ( array[ i ] === value ) {
            return i;
          }
        }
        return -1;
      }

      function escapeExpression ( string ) {
        if ( typeof string !== 'string' ) {
          // don't escape SafeStrings, since they're already safe
          if ( string && string.toHTML ) {
            return string.toHTML();
          } else if ( string == null ) {
            return '';
          } else if ( !string ) {
            return string + '';
          }

          // Force a string conversion as this will be done by the append regardless and
          // the regex test will do this transparently behind the scenes, causing issues if
          // an object's to string has escaped characters in it.
          string = '' + string;
        }

        if ( !possible.test( string ) ) {
          return string;
        }
        return string.replace( badChars, escapeChar );
      }

      function isEmpty ( value ) {
        if ( !value && value !== 0 ) {
          return true;
        } else if ( isArray( value ) && value.length === 0 ) {
          return true;
        } else {
          return false;
        }
      }

      function createFrame ( object ) {
        var frame = extend( {}, object );
        frame._parent = object;
        return frame;
      }

      function blockParams ( params, ids ) {
        params.path = ids;
        return params;
      }

      function appendContextPath ( contextPath, id ) {
        return (contextPath ? contextPath + '.' : '') + id;
      }

      /***/
    },
              /* 6 */
              /***/ function ( module, exports ) {

      'use strict';

      exports.__esModule = true;

      var errorProps = [ 'description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack' ];

      function Exception ( message, node ) {
        var loc    = node && node.loc,
            line   = undefined,
            column = undefined;
        if ( loc ) {
          line = loc.start.line;
          column = loc.start.column;

          message += ' - ' + line + ':' + column;
        }

        var tmp = Error.prototype.constructor.call( this, message );

        // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.
        for ( var idx = 0; idx < errorProps.length; idx++ ) {
          this[ errorProps[ idx ] ] = tmp[ errorProps[ idx ] ];
        }

        /* istanbul ignore else */
        if ( Error.captureStackTrace ) {
          Error.captureStackTrace( this, Exception );
        }

        if ( loc ) {
          this.lineNumber = line;
          this.column = column;
        }
      }

      Exception.prototype = new Error();

      exports[ 'default' ] = Exception;
      module.exports = exports[ 'default' ];

      /***/
    },
              /* 7 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;
      exports.registerDefaultHelpers = registerDefaultHelpers;

      var _helpersBlockHelperMissing = __webpack_require__( 8 );

      var _helpersBlockHelperMissing2 = _interopRequireDefault( _helpersBlockHelperMissing );

      var _helpersEach = __webpack_require__( 9 );

      var _helpersEach2 = _interopRequireDefault( _helpersEach );

      var _helpersHelperMissing = __webpack_require__( 10 );

      var _helpersHelperMissing2 = _interopRequireDefault( _helpersHelperMissing );

      var _helpersIf = __webpack_require__( 11 );

      var _helpersIf2 = _interopRequireDefault( _helpersIf );

      var _helpersLog = __webpack_require__( 12 );

      var _helpersLog2 = _interopRequireDefault( _helpersLog );

      var _helpersLookup = __webpack_require__( 13 );

      var _helpersLookup2 = _interopRequireDefault( _helpersLookup );

      var _helpersWith = __webpack_require__( 14 );

      var _helpersWith2 = _interopRequireDefault( _helpersWith );

      function registerDefaultHelpers ( instance ) {
        _helpersBlockHelperMissing2[ 'default' ]( instance );
        _helpersEach2[ 'default' ]( instance );
        _helpersHelperMissing2[ 'default' ]( instance );
        _helpersIf2[ 'default' ]( instance );
        _helpersLog2[ 'default' ]( instance );
        _helpersLookup2[ 'default' ]( instance );
        _helpersWith2[ 'default' ]( instance );
      }

      /***/
    },
              /* 8 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      exports.__esModule = true;

      var _utils = __webpack_require__( 5 );

      exports[ 'default' ] = function ( instance ) {
        instance.registerHelper( 'blockHelperMissing', function ( context, options ) {
          var inverse = options.inverse,
              fn      = options.fn;

          if ( context === true ) {
            return fn( this );
          } else if ( context === false || context == null ) {
            return inverse( this );
          } else if ( _utils.isArray( context ) ) {
            if ( context.length > 0 ) {
              if ( options.ids ) {
                options.ids = [ options.name ];
              }

              return instance.helpers.each( context, options );
            } else {
              return inverse( this );
            }
          } else {
            if ( options.data && options.ids ) {
              var data = _utils.createFrame( options.data );
              data.contextPath = _utils.appendContextPath( options.data.contextPath, options.name );
              options = { data : data };
            }

            return fn( context, options );
          }
        } );
      };

      module.exports = exports[ 'default' ];

      /***/
    },
              /* 9 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;

      var _utils = __webpack_require__( 5 );

      var _exception = __webpack_require__( 6 );

      var _exception2 = _interopRequireDefault( _exception );

      exports[ 'default' ] = function ( instance ) {
        instance.registerHelper( 'each', function ( context, options ) {
          if ( !options ) {
            throw new _exception2[ 'default' ]( 'Must pass iterator to #each' );
          }

          var fn          = options.fn,
              inverse     = options.inverse,
              i           = 0,
              ret         = '',
              data        = undefined,
              contextPath = undefined;

          if ( options.data && options.ids ) {
            contextPath = _utils.appendContextPath( options.data.contextPath, options.ids[ 0 ] ) + '.';
          }

          if ( _utils.isFunction( context ) ) {
            context = context.call( this );
          }

          if ( options.data ) {
            data = _utils.createFrame( options.data );
          }

          function execIteration ( field, index, last ) {
            // Don't iterate over undefined values since we can't execute blocks against them
            // in non-strict (js) mode.
            if ( context[ field ] == null ) {
              return;
            }

            if ( data ) {
              data.key = field;
              data.index = index;
              data.first = index === 0;
              data.last = !!last;

              if ( contextPath ) {
                data.contextPath = contextPath + field;
              }
            }

            ret = ret + fn( context[ field ], {
                data        : data,
                blockParams : _utils.blockParams( [ context[ field ], field ], [ contextPath + field, null ] )
              } );
          }

          if ( context && typeof context === 'object' ) {
            if ( _utils.isArray( context ) ) {
              for ( var j = context.length; i < j; i++ ) {
                execIteration( i, i, i === context.length - 1 );
              }
            } else {
              var priorKey = undefined;

              for ( var key in context ) {
                if ( context.hasOwnProperty( key ) ) {
                  // We're running the iterations one step out of sync so we can detect
                  // the last iteration without have to scan the object twice and create
                  // an itermediate keys array.
                  if ( priorKey !== undefined ) {
                    execIteration( priorKey, i - 1 );
                  }
                  priorKey = key;
                  i++;
                }
              }
              if ( priorKey !== undefined ) {
                execIteration( priorKey, i - 1, true );
              }
            }
          }

          if ( i === 0 ) {
            ret = inverse( this );
          }

          return ret;
        } );
      };

      module.exports = exports[ 'default' ];

      /***/
    },
              /* 10 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;

      var _exception = __webpack_require__( 6 );

      var _exception2 = _interopRequireDefault( _exception );

      exports[ 'default' ] = function ( instance ) {
        instance.registerHelper( 'helperMissing', function () /* [args, ]options */ {
          if ( arguments.length === 1 ) {
            // A missing field in a {{foo}} construct.
            return undefined;
          } else {
            // Someone is actually trying to call something, blow up.
            throw new _exception2[ 'default' ]( 'Missing helper: "' + arguments[ arguments.length - 1 ].name + '"' );
          }
        } );
      };

      module.exports = exports[ 'default' ];

      /***/
    },
              /* 11 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      exports.__esModule = true;

      var _utils = __webpack_require__( 5 );

      exports[ 'default' ] = function ( instance ) {
        instance.registerHelper( 'if', function ( conditional, options ) {
          if ( _utils.isFunction( conditional ) ) {
            conditional = conditional.call( this );
          }

          // Default behavior is to render the positive path if the value is truthy and not empty.
          // The `includeZero` option may be set to treat the condtional as purely not empty based on the
          // behavior of isEmpty. Effectively this determines if 0 is handled by the positive path or negative.
          if ( !options.hash.includeZero && !conditional || _utils.isEmpty( conditional ) ) {
            return options.inverse( this );
          } else {
            return options.fn( this );
          }
        } );

        instance.registerHelper( 'unless', function ( conditional, options ) {
          return instance.helpers[ 'if' ].call( this, conditional, { fn : options.inverse, inverse : options.fn, hash : options.hash } );
        } );
      };

      module.exports = exports[ 'default' ];

      /***/
    },
              /* 12 */
              /***/ function ( module, exports ) {

      'use strict';

      exports.__esModule = true;

      exports[ 'default' ] = function ( instance ) {
        instance.registerHelper( 'log', function () /* message, options */ {
          var args    = [ undefined ],
              options = arguments[ arguments.length - 1 ];
          for ( var i = 0; i < arguments.length - 1; i++ ) {
            args.push( arguments[ i ] );
          }

          var level = 1;
          if ( options.hash.level != null ) {
            level = options.hash.level;
          } else if ( options.data && options.data.level != null ) {
            level = options.data.level;
          }
          args[ 0 ] = level;

          instance.log.apply( instance, args );
        } );
      };

      module.exports = exports[ 'default' ];

      /***/
    },
              /* 13 */
              /***/ function ( module, exports ) {

      'use strict';

      exports.__esModule = true;

      exports[ 'default' ] = function ( instance ) {
        instance.registerHelper( 'lookup', function ( obj, field ) {
          return obj && obj[ field ];
        } );
      };

      module.exports = exports[ 'default' ];

      /***/
    },
              /* 14 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      exports.__esModule = true;

      var _utils = __webpack_require__( 5 );

      exports[ 'default' ] = function ( instance ) {
        instance.registerHelper( 'with', function ( context, options ) {
          if ( _utils.isFunction( context ) ) {
            context = context.call( this );
          }

          var fn = options.fn;

          if ( !_utils.isEmpty( context ) ) {
            var data = options.data;
            if ( options.data && options.ids ) {
              data = _utils.createFrame( options.data );
              data.contextPath = _utils.appendContextPath( options.data.contextPath, options.ids[ 0 ] );
            }

            return fn( context, {
              data        : data,
              blockParams : _utils.blockParams( [ context ], [ data && data.contextPath ] )
            } );
          } else {
            return options.inverse( this );
          }
        } );
      };

      module.exports = exports[ 'default' ];

      /***/
    },
              /* 15 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;
      exports.registerDefaultDecorators = registerDefaultDecorators;

      var _decoratorsInline = __webpack_require__( 16 );

      var _decoratorsInline2 = _interopRequireDefault( _decoratorsInline );

      function registerDefaultDecorators ( instance ) {
        _decoratorsInline2[ 'default' ]( instance );
      }

      /***/
    },
              /* 16 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      exports.__esModule = true;

      var _utils = __webpack_require__( 5 );

      exports[ 'default' ] = function ( instance ) {
        instance.registerDecorator( 'inline', function ( fn, props, container, options ) {
          var ret = fn;
          if ( !props.partials ) {
            props.partials = {};
            ret = function ( context, options ) {
              // Create a new partials stack frame prior to exec.
              var original = container.partials;
              container.partials = _utils.extend( {}, original, props.partials );
              var ret = fn( context, options );
              container.partials = original;
              return ret;
            };
          }

          props.partials[ options.args[ 0 ] ] = options.fn;

          return ret;
        } );
      };

      module.exports = exports[ 'default' ];

      /***/
    },
              /* 17 */
              /***/ function ( module, exports ) {

      'use strict';

      exports.__esModule = true;
      var logger = {
        methodMap : [ 'debug', 'info', 'warn', 'error' ],
        level     : 'info',

        // Maps a given level value to the `methodMap` indexes above.
        lookupLevel : function lookupLevel ( level ) {
          if ( typeof level === 'string' ) {
            var levelMap = logger.methodMap.indexOf( level.toLowerCase() );
            if ( levelMap >= 0 ) {
              level = levelMap;
            } else {
              level = parseInt( level, 10 );
            }
          }

          return level;
        },

        // Can be overridden in the host environment
        log : function log ( level ) {
          level = logger.lookupLevel( level );

          if ( typeof console !== 'undefined' && logger.lookupLevel( logger.level ) <= level ) {
            var method = logger.methodMap[ level ];
            if ( !console[ method ] ) {
              // eslint-disable-line no-console
              method = 'log';
            }

            for ( var _len = arguments.length, message = Array( _len > 1 ? _len - 1 : 0 ), _key = 1; _key < _len; _key++ ) {
              message[ _key - 1 ] = arguments[ _key ];
            }

            console[ method ].apply( console, message ); // eslint-disable-line no-console
          }
        }
      };

      exports[ 'default' ] = logger;
      module.exports = exports[ 'default' ];

      /***/
    },
              /* 18 */
              /***/ function ( module, exports ) {

      // Build out our basic SafeString type
      'use strict';

      exports.__esModule = true;
      function SafeString ( string ) {
        this.string = string;
      }

      SafeString.prototype.toString = SafeString.prototype.toHTML = function () {
        return '' + this.string;
      };

      exports[ 'default' ] = SafeString;
      module.exports = exports[ 'default' ];

      /***/
    },
              /* 19 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireWildcard = __webpack_require__( 3 )[ 'default' ];

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;
      exports.checkRevision = checkRevision;
      exports.template = template;
      exports.wrapProgram = wrapProgram;
      exports.resolvePartial = resolvePartial;
      exports.invokePartial = invokePartial;
      exports.noop = noop;

      var _utils = __webpack_require__( 5 );

      var Utils = _interopRequireWildcard( _utils );

      var _exception = __webpack_require__( 6 );

      var _exception2 = _interopRequireDefault( _exception );

      var _base = __webpack_require__( 4 );

      function checkRevision ( compilerInfo ) {
        var compilerRevision = compilerInfo && compilerInfo[ 0 ] || 1,
            currentRevision  = _base.COMPILER_REVISION;

        if ( compilerRevision !== currentRevision ) {
          if ( compilerRevision < currentRevision ) {
            var runtimeVersions  = _base.REVISION_CHANGES[ currentRevision ],
                compilerVersions = _base.REVISION_CHANGES[ compilerRevision ];
            throw new _exception2[ 'default' ]( 'Template was precompiled with an older version of Handlebars than the current runtime. ' + 'Please update your precompiler to a newer version (' + runtimeVersions + ') or downgrade your runtime to an older version (' + compilerVersions + ').' );
          } else {
            // Use the embedded version info since the runtime doesn't know about this revision yet
            throw new _exception2[ 'default' ]( 'Template was precompiled with a newer version of Handlebars than the current runtime. ' + 'Please update your runtime to a newer version (' + compilerInfo[ 1 ] + ').' );
          }
        }
      }

      function template ( templateSpec, env ) {
        /* istanbul ignore next */
        if ( !env ) {
          throw new _exception2[ 'default' ]( 'No environment passed to template' );
        }
        if ( !templateSpec || !templateSpec.main ) {
          throw new _exception2[ 'default' ]( 'Unknown template object: ' + typeof templateSpec );
        }

        templateSpec.main.decorator = templateSpec.main_d;

        // Note: Using env.VM references rather than local var references throughout this section to allow
        // for external users to override these as psuedo-supported APIs.
        env.VM.checkRevision( templateSpec.compiler );

        function invokePartialWrapper ( partial, context, options ) {
          if ( options.hash ) {
            context = Utils.extend( {}, context, options.hash );
            if ( options.ids ) {
              options.ids[ 0 ] = true;
            }
          }

          partial = env.VM.resolvePartial.call( this, partial, context, options );
          var result = env.VM.invokePartial.call( this, partial, context, options );

          if ( result == null && env.compile ) {
            options.partials[ options.name ] = env.compile( partial, templateSpec.compilerOptions, env );
            result = options.partials[ options.name ]( context, options );
          }
          if ( result != null ) {
            if ( options.indent ) {
              var lines = result.split( '\n' );
              for ( var i = 0, l = lines.length; i < l; i++ ) {
                if ( !lines[ i ] && i + 1 === l ) {
                  break;
                }

                lines[ i ] = options.indent + lines[ i ];
              }
              result = lines.join( '\n' );
            }
            return result;
          } else {
            throw new _exception2[ 'default' ]( 'The partial ' + options.name + ' could not be compiled when running in runtime-only mode' );
          }
        }

        // Just add water
        var container = {
          strict : function strict ( obj, name ) {
            if ( !(name in obj) ) {
              throw new _exception2[ 'default' ]( '"' + name + '" not defined in ' + obj );
            }
            return obj[ name ];
          },
          lookup : function lookup ( depths, name ) {
            var len = depths.length;
            for ( var i = 0; i < len; i++ ) {
              if ( depths[ i ] && depths[ i ][ name ] != null ) {
                return depths[ i ][ name ];
              }
            }
          },
          lambda : function lambda ( current, context ) {
            return typeof current === 'function' ? current.call( context ) : current;
          },

          escapeExpression : Utils.escapeExpression,
          invokePartial    : invokePartialWrapper,

          fn : function fn ( i ) {
            var ret = templateSpec[ i ];
            ret.decorator = templateSpec[ i + '_d' ];
            return ret;
          },

          programs : [],
          program  : function program ( i, data, declaredBlockParams, blockParams, depths ) {
            var programWrapper = this.programs[ i ],
                fn             = this.fn( i );
            if ( data || depths || blockParams || declaredBlockParams ) {
              programWrapper = wrapProgram( this, i, fn, data, declaredBlockParams, blockParams, depths );
            } else if ( !programWrapper ) {
              programWrapper = this.programs[ i ] = wrapProgram( this, i, fn );
            }
            return programWrapper;
          },

          data  : function data ( value, depth ) {
            while ( value && depth-- ) {
              value = value._parent;
            }
            return value;
          },
          merge : function merge ( param, common ) {
            var obj = param || common;

            if ( param && common && param !== common ) {
              obj = Utils.extend( {}, common, param );
            }

            return obj;
          },

          noop         : env.VM.noop,
          compilerInfo : templateSpec.compiler
        };

        function ret ( context ) {
          var options = arguments.length <= 1 || arguments[ 1 ] === undefined ? {} : arguments[ 1 ];

          var data = options.data;

          ret._setup( options );
          if ( !options.partial && templateSpec.useData ) {
            data = initData( context, data );
          }
          var depths      = undefined,
              blockParams = templateSpec.useBlockParams ? [] : undefined;
          if ( templateSpec.useDepths ) {
            if ( options.depths ) {
              depths = context !== options.depths[ 0 ] ? [ context ].concat( options.depths ) : options.depths;
            } else {
              depths = [ context ];
            }
          }

          function main ( context /*, options*/ ) {
            return '' + templateSpec.main( container, context, container.helpers, container.partials, data, blockParams, depths );
          }

          main = executeDecorators( templateSpec.main, main, container, options.depths || [], data, blockParams );
          return main( context, options );
        }

        ret.isTop = true;

        ret._setup = function ( options ) {
          if ( !options.partial ) {
            container.helpers = container.merge( options.helpers, env.helpers );

            if ( templateSpec.usePartial ) {
              container.partials = container.merge( options.partials, env.partials );
            }
            if ( templateSpec.usePartial || templateSpec.useDecorators ) {
              container.decorators = container.merge( options.decorators, env.decorators );
            }
          } else {
            container.helpers = options.helpers;
            container.partials = options.partials;
            container.decorators = options.decorators;
          }
        };

        ret._child = function ( i, data, blockParams, depths ) {
          if ( templateSpec.useBlockParams && !blockParams ) {
            throw new _exception2[ 'default' ]( 'must pass block params' );
          }
          if ( templateSpec.useDepths && !depths ) {
            throw new _exception2[ 'default' ]( 'must pass parent depths' );
          }

          return wrapProgram( container, i, templateSpec[ i ], data, 0, blockParams, depths );
        };
        return ret;
      }

      function wrapProgram ( container, i, fn, data, declaredBlockParams, blockParams, depths ) {
        function prog ( context ) {
          var options = arguments.length <= 1 || arguments[ 1 ] === undefined ? {} : arguments[ 1 ];

          var currentDepths = depths;
          if ( depths && context !== depths[ 0 ] ) {
            currentDepths = [ context ].concat( depths );
          }

          return fn( container, context, container.helpers, container.partials, options.data || data, blockParams && [ options.blockParams ].concat( blockParams ), currentDepths );
        }

        prog = executeDecorators( fn, prog, container, depths, data, blockParams );

        prog.program = i;
        prog.depth = depths ? depths.length : 0;
        prog.blockParams = declaredBlockParams || 0;
        return prog;
      }

      function resolvePartial ( partial, context, options ) {
        if ( !partial ) {
          if ( options.name === '@partial-block' ) {
            partial = options.data[ 'partial-block' ];
          } else {
            partial = options.partials[ options.name ];
          }
        } else if ( !partial.call && !options.name ) {
          // This is a dynamic partial that returned a string
          options.name = partial;
          partial = options.partials[ partial ];
        }
        return partial;
      }

      function invokePartial ( partial, context, options ) {
        options.partial = true;
        if ( options.ids ) {
          options.data.contextPath = options.ids[ 0 ] || options.data.contextPath;
        }

        var partialBlock = undefined;
        if ( options.fn && options.fn !== noop ) {
          partialBlock = options.data[ 'partial-block' ] = options.fn;

          if ( partialBlock.partials ) {
            options.partials = Utils.extend( {}, options.partials, partialBlock.partials );
          }
        }

        if ( partial === undefined && partialBlock ) {
          partial = partialBlock;
        }

        if ( partial === undefined ) {
          throw new _exception2[ 'default' ]( 'The partial ' + options.name + ' could not be found' );
        } else if ( partial instanceof Function ) {
          return partial( context, options );
        }
      }

      function noop () {
        return '';
      }

      function initData ( context, data ) {
        if ( !data || !('root' in data) ) {
          data = data ? _base.createFrame( data ) : {};
          data.root = context;
        }
        return data;
      }

      function executeDecorators ( fn, prog, container, depths, data, blockParams ) {
        if ( fn.decorator ) {
          var props = {};
          prog = fn.decorator( prog, props, container, depths && depths[ 0 ], data, blockParams, depths );
          Utils.extend( prog, props );
        }
        return prog;
      }

      /***/
    },
              /* 20 */
              /***/ function ( module, exports ) {

      /* WEBPACK VAR INJECTION */
      (function ( global ) {/* global window */
        'use strict';

        exports.__esModule = true;

        exports[ 'default' ] = function ( Handlebars ) {
          /* istanbul ignore next */
          var root        = typeof global !== 'undefined' ? global : window,
              $Handlebars = root.Handlebars;
          /* istanbul ignore next */
          Handlebars.noConflict = function () {
            if ( root.Handlebars === Handlebars ) {
              root.Handlebars = $Handlebars;
            }
          };
        };

        module.exports = exports[ 'default' ];
        /* WEBPACK VAR INJECTION */
      }.call( exports, (function () { return this; }()) ));

      /***/
    },
              /* 21 */
              /***/ function ( module, exports ) {

      'use strict';

      exports.__esModule = true;
      var AST = {
        // Public API used to evaluate derived attributes regarding AST nodes
        helpers : {
          // a mustache is definitely a helper if:
          // * it is an eligible helper, and
          // * it has at least one parameter or hash segment
          helperExpression : function helperExpression ( node ) {
            return node.type === 'SubExpression' || (node.type === 'MustacheStatement' || node.type === 'BlockStatement') && !!(node.params && node.params.length || node.hash);
          },

          scopedId : function scopedId ( path ) {
            return (/^\.|this\b/.test( path.original )
            );
          },

          // an ID is simple if it only has one part, and that part is not
          // `..` or `this`.
          simpleId : function simpleId ( path ) {
            return path.parts.length === 1 && !AST.helpers.scopedId( path ) && !path.depth;
          }
        }
      };

      // Must be exported as an object rather than the root of the module as the jison lexer
      // must modify the object to operate properly.
      exports[ 'default' ] = AST;
      module.exports = exports[ 'default' ];

      /***/
    },
              /* 22 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      var _interopRequireWildcard = __webpack_require__( 3 )[ 'default' ];

      exports.__esModule = true;
      exports.parse = parse;

      var _parser = __webpack_require__( 23 );

      var _parser2 = _interopRequireDefault( _parser );

      var _whitespaceControl = __webpack_require__( 24 );

      var _whitespaceControl2 = _interopRequireDefault( _whitespaceControl );

      var _helpers = __webpack_require__( 26 );

      var Helpers = _interopRequireWildcard( _helpers );

      var _utils = __webpack_require__( 5 );

      exports.parser = _parser2[ 'default' ];

      var yy = {};
      _utils.extend( yy, Helpers );

      function parse ( input, options ) {
        // Just return if an already-compiled AST was passed in.
        if ( input.type === 'Program' ) {
          return input;
        }

        _parser2[ 'default' ].yy = yy;

        // Altering the shared object here, but this is ok as parser is a sync operation
        yy.locInfo = function ( locInfo ) {
          return new yy.SourceLocation( options && options.srcName, locInfo );
        };

        var strip = new _whitespaceControl2[ 'default' ]( options );
        return strip.accept( _parser2[ 'default' ].parse( input ) );
      }

      /***/
    },
              /* 23 */
              /***/ function ( module, exports ) {

      /* istanbul ignore next */
      /* Jison generated parser */
      "use strict";

      var handlebars = (function () {
        var parser = {
          trace          : function trace () {},
          yy             : {},
          symbols_       : { "error" : 2, "root" : 3, "program" : 4, "EOF" : 5, "program_repetition0" : 6, "statement" : 7, "mustache" : 8, "block" : 9, "rawBlock" : 10, "partial" : 11, "partialBlock" : 12, "content" : 13, "COMMENT" : 14, "CONTENT" : 15, "openRawBlock" : 16, "rawBlock_repetition_plus0" : 17, "END_RAW_BLOCK" : 18, "OPEN_RAW_BLOCK" : 19, "helperName" : 20, "openRawBlock_repetition0" : 21, "openRawBlock_option0" : 22, "CLOSE_RAW_BLOCK" : 23, "openBlock" : 24, "block_option0" : 25, "closeBlock" : 26, "openInverse" : 27, "block_option1" : 28, "OPEN_BLOCK" : 29, "openBlock_repetition0" : 30, "openBlock_option0" : 31, "openBlock_option1" : 32, "CLOSE" : 33, "OPEN_INVERSE" : 34, "openInverse_repetition0" : 35, "openInverse_option0" : 36, "openInverse_option1" : 37, "openInverseChain" : 38, "OPEN_INVERSE_CHAIN" : 39, "openInverseChain_repetition0" : 40, "openInverseChain_option0" : 41, "openInverseChain_option1" : 42, "inverseAndProgram" : 43, "INVERSE" : 44, "inverseChain" : 45, "inverseChain_option0" : 46, "OPEN_ENDBLOCK" : 47, "OPEN" : 48, "mustache_repetition0" : 49, "mustache_option0" : 50, "OPEN_UNESCAPED" : 51, "mustache_repetition1" : 52, "mustache_option1" : 53, "CLOSE_UNESCAPED" : 54, "OPEN_PARTIAL" : 55, "partialName" : 56, "partial_repetition0" : 57, "partial_option0" : 58, "openPartialBlock" : 59, "OPEN_PARTIAL_BLOCK" : 60, "openPartialBlock_repetition0" : 61, "openPartialBlock_option0" : 62, "param" : 63, "sexpr" : 64, "OPEN_SEXPR" : 65, "sexpr_repetition0" : 66, "sexpr_option0" : 67, "CLOSE_SEXPR" : 68, "hash" : 69, "hash_repetition_plus0" : 70, "hashSegment" : 71, "ID" : 72, "EQUALS" : 73, "blockParams" : 74, "OPEN_BLOCK_PARAMS" : 75, "blockParams_repetition_plus0" : 76, "CLOSE_BLOCK_PARAMS" : 77, "path" : 78, "dataName" : 79, "STRING" : 80, "NUMBER" : 81, "BOOLEAN" : 82, "UNDEFINED" : 83, "NULL" : 84, "DATA" : 85, "pathSegments" : 86, "SEP" : 87, "$accept" : 0, "$end" : 1 },
          terminals_     : { 2 : "error", 5 : "EOF", 14 : "COMMENT", 15 : "CONTENT", 18 : "END_RAW_BLOCK", 19 : "OPEN_RAW_BLOCK", 23 : "CLOSE_RAW_BLOCK", 29 : "OPEN_BLOCK", 33 : "CLOSE", 34 : "OPEN_INVERSE", 39 : "OPEN_INVERSE_CHAIN", 44 : "INVERSE", 47 : "OPEN_ENDBLOCK", 48 : "OPEN", 51 : "OPEN_UNESCAPED", 54 : "CLOSE_UNESCAPED", 55 : "OPEN_PARTIAL", 60 : "OPEN_PARTIAL_BLOCK", 65 : "OPEN_SEXPR", 68 : "CLOSE_SEXPR", 72 : "ID", 73 : "EQUALS", 75 : "OPEN_BLOCK_PARAMS", 77 : "CLOSE_BLOCK_PARAMS", 80 : "STRING", 81 : "NUMBER", 82 : "BOOLEAN", 83 : "UNDEFINED", 84 : "NULL", 85 : "DATA", 87 : "SEP" },
          productions_   : [ 0, [ 3, 2 ], [ 4, 1 ], [ 7, 1 ], [ 7, 1 ], [ 7, 1 ], [ 7, 1 ], [ 7, 1 ], [ 7, 1 ], [ 7, 1 ], [ 13, 1 ], [ 10, 3 ], [ 16, 5 ], [ 9, 4 ], [ 9, 4 ], [ 24, 6 ], [ 27, 6 ], [ 38, 6 ], [ 43, 2 ], [ 45, 3 ], [ 45, 1 ], [ 26, 3 ], [ 8, 5 ], [ 8, 5 ], [ 11, 5 ], [ 12, 3 ], [ 59, 5 ], [ 63, 1 ], [ 63, 1 ], [ 64, 5 ], [ 69, 1 ], [ 71, 3 ], [ 74, 3 ], [ 20, 1 ], [ 20, 1 ], [ 20, 1 ], [ 20, 1 ], [ 20, 1 ], [ 20, 1 ], [ 20, 1 ], [ 56, 1 ], [ 56, 1 ], [ 79, 2 ], [ 78, 1 ], [ 86, 3 ], [ 86, 1 ], [ 6, 0 ], [ 6, 2 ], [ 17, 1 ], [ 17, 2 ], [ 21, 0 ], [ 21, 2 ], [ 22, 0 ], [ 22, 1 ], [ 25, 0 ], [ 25, 1 ], [ 28, 0 ], [ 28, 1 ], [ 30, 0 ], [ 30, 2 ], [ 31, 0 ], [ 31, 1 ], [ 32, 0 ], [ 32, 1 ], [ 35, 0 ], [ 35, 2 ], [ 36, 0 ], [ 36, 1 ], [ 37, 0 ], [ 37, 1 ], [ 40, 0 ], [ 40, 2 ], [ 41, 0 ], [ 41, 1 ], [ 42, 0 ], [ 42, 1 ], [ 46, 0 ], [ 46, 1 ], [ 49, 0 ], [ 49, 2 ], [ 50, 0 ], [ 50, 1 ], [ 52, 0 ], [ 52, 2 ], [ 53, 0 ], [ 53, 1 ], [ 57, 0 ], [ 57, 2 ], [ 58, 0 ], [ 58, 1 ], [ 61, 0 ], [ 61, 2 ], [ 62, 0 ], [ 62, 1 ], [ 66, 0 ], [ 66, 2 ], [ 67, 0 ], [ 67, 1 ], [ 70, 1 ], [ 70, 2 ], [ 76, 1 ], [ 76, 2 ] ],
          performAction  : function anonymous ( yytext, yyleng, yylineno, yy, yystate, $$, _$ ) {

            var $0 = $$.length - 1;
            switch ( yystate ) {
              case 1:
                return $$[ $0 - 1 ];
                break;
              case 2:
                this.$ = yy.prepareProgram( $$[ $0 ] );
                break;
              case 3:
                this.$ = $$[ $0 ];
                break;
              case 4:
                this.$ = $$[ $0 ];
                break;
              case 5:
                this.$ = $$[ $0 ];
                break;
              case 6:
                this.$ = $$[ $0 ];
                break;
              case 7:
                this.$ = $$[ $0 ];
                break;
              case 8:
                this.$ = $$[ $0 ];
                break;
              case 9:
                this.$ = {
                  type  : 'CommentStatement',
                  value : yy.stripComment( $$[ $0 ] ),
                  strip : yy.stripFlags( $$[ $0 ], $$[ $0 ] ),
                  loc   : yy.locInfo( this._$ )
                };

                break;
              case 10:
                this.$ = {
                  type     : 'ContentStatement',
                  original : $$[ $0 ],
                  value    : $$[ $0 ],
                  loc      : yy.locInfo( this._$ )
                };

                break;
              case 11:
                this.$ = yy.prepareRawBlock( $$[ $0 - 2 ], $$[ $0 - 1 ], $$[ $0 ], this._$ );
                break;
              case 12:
                this.$ = { path : $$[ $0 - 3 ], params : $$[ $0 - 2 ], hash : $$[ $0 - 1 ] };
                break;
              case 13:
                this.$ = yy.prepareBlock( $$[ $0 - 3 ], $$[ $0 - 2 ], $$[ $0 - 1 ], $$[ $0 ], false, this._$ );
                break;
              case 14:
                this.$ = yy.prepareBlock( $$[ $0 - 3 ], $$[ $0 - 2 ], $$[ $0 - 1 ], $$[ $0 ], true, this._$ );
                break;
              case 15:
                this.$ = { open : $$[ $0 - 5 ], path : $$[ $0 - 4 ], params : $$[ $0 - 3 ], hash : $$[ $0 - 2 ], blockParams : $$[ $0 - 1 ], strip : yy.stripFlags( $$[ $0 - 5 ], $$[ $0 ] ) };
                break;
              case 16:
                this.$ = { path : $$[ $0 - 4 ], params : $$[ $0 - 3 ], hash : $$[ $0 - 2 ], blockParams : $$[ $0 - 1 ], strip : yy.stripFlags( $$[ $0 - 5 ], $$[ $0 ] ) };
                break;
              case 17:
                this.$ = { path : $$[ $0 - 4 ], params : $$[ $0 - 3 ], hash : $$[ $0 - 2 ], blockParams : $$[ $0 - 1 ], strip : yy.stripFlags( $$[ $0 - 5 ], $$[ $0 ] ) };
                break;
              case 18:
                this.$ = { strip : yy.stripFlags( $$[ $0 - 1 ], $$[ $0 - 1 ] ), program : $$[ $0 ] };
                break;
              case 19:
                var inverse = yy.prepareBlock( $$[ $0 - 2 ], $$[ $0 - 1 ], $$[ $0 ], $$[ $0 ], false, this._$ ),
                    program = yy.prepareProgram( [ inverse ], $$[ $0 - 1 ].loc );
                program.chained = true;

                this.$ = { strip : $$[ $0 - 2 ].strip, program : program, chain : true };

                break;
              case 20:
                this.$ = $$[ $0 ];
                break;
              case 21:
                this.$ = { path : $$[ $0 - 1 ], strip : yy.stripFlags( $$[ $0 - 2 ], $$[ $0 ] ) };
                break;
              case 22:
                this.$ = yy.prepareMustache( $$[ $0 - 3 ], $$[ $0 - 2 ], $$[ $0 - 1 ], $$[ $0 - 4 ], yy.stripFlags( $$[ $0 - 4 ], $$[ $0 ] ), this._$ );
                break;
              case 23:
                this.$ = yy.prepareMustache( $$[ $0 - 3 ], $$[ $0 - 2 ], $$[ $0 - 1 ], $$[ $0 - 4 ], yy.stripFlags( $$[ $0 - 4 ], $$[ $0 ] ), this._$ );
                break;
              case 24:
                this.$ = {
                  type   : 'PartialStatement',
                  name   : $$[ $0 - 3 ],
                  params : $$[ $0 - 2 ],
                  hash   : $$[ $0 - 1 ],
                  indent : '',
                  strip  : yy.stripFlags( $$[ $0 - 4 ], $$[ $0 ] ),
                  loc    : yy.locInfo( this._$ )
                };

                break;
              case 25:
                this.$ = yy.preparePartialBlock( $$[ $0 - 2 ], $$[ $0 - 1 ], $$[ $0 ], this._$ );
                break;
              case 26:
                this.$ = { path : $$[ $0 - 3 ], params : $$[ $0 - 2 ], hash : $$[ $0 - 1 ], strip : yy.stripFlags( $$[ $0 - 4 ], $$[ $0 ] ) };
                break;
              case 27:
                this.$ = $$[ $0 ];
                break;
              case 28:
                this.$ = $$[ $0 ];
                break;
              case 29:
                this.$ = {
                  type   : 'SubExpression',
                  path   : $$[ $0 - 3 ],
                  params : $$[ $0 - 2 ],
                  hash   : $$[ $0 - 1 ],
                  loc    : yy.locInfo( this._$ )
                };

                break;
              case 30:
                this.$ = { type : 'Hash', pairs : $$[ $0 ], loc : yy.locInfo( this._$ ) };
                break;
              case 31:
                this.$ = { type : 'HashPair', key : yy.id( $$[ $0 - 2 ] ), value : $$[ $0 ], loc : yy.locInfo( this._$ ) };
                break;
              case 32:
                this.$ = yy.id( $$[ $0 - 1 ] );
                break;
              case 33:
                this.$ = $$[ $0 ];
                break;
              case 34:
                this.$ = $$[ $0 ];
                break;
              case 35:
                this.$ = { type : 'StringLiteral', value : $$[ $0 ], original : $$[ $0 ], loc : yy.locInfo( this._$ ) };
                break;
              case 36:
                this.$ = { type : 'NumberLiteral', value : Number( $$[ $0 ] ), original : Number( $$[ $0 ] ), loc : yy.locInfo( this._$ ) };
                break;
              case 37:
                this.$ = { type : 'BooleanLiteral', value : $$[ $0 ] === 'true', original : $$[ $0 ] === 'true', loc : yy.locInfo( this._$ ) };
                break;
              case 38:
                this.$ = { type : 'UndefinedLiteral', original : undefined, value : undefined, loc : yy.locInfo( this._$ ) };
                break;
              case 39:
                this.$ = { type : 'NullLiteral', original : null, value : null, loc : yy.locInfo( this._$ ) };
                break;
              case 40:
                this.$ = $$[ $0 ];
                break;
              case 41:
                this.$ = $$[ $0 ];
                break;
              case 42:
                this.$ = yy.preparePath( true, $$[ $0 ], this._$ );
                break;
              case 43:
                this.$ = yy.preparePath( false, $$[ $0 ], this._$ );
                break;
              case 44:
                $$[ $0 - 2 ].push( { part : yy.id( $$[ $0 ] ), original : $$[ $0 ], separator : $$[ $0 - 1 ] } );
                this.$ = $$[ $0 - 2 ];
                break;
              case 45:
                this.$ = [ { part : yy.id( $$[ $0 ] ), original : $$[ $0 ] } ];
                break;
              case 46:
                this.$ = [];
                break;
              case 47:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 48:
                this.$ = [ $$[ $0 ] ];
                break;
              case 49:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 50:
                this.$ = [];
                break;
              case 51:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 58:
                this.$ = [];
                break;
              case 59:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 64:
                this.$ = [];
                break;
              case 65:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 70:
                this.$ = [];
                break;
              case 71:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 78:
                this.$ = [];
                break;
              case 79:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 82:
                this.$ = [];
                break;
              case 83:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 86:
                this.$ = [];
                break;
              case 87:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 90:
                this.$ = [];
                break;
              case 91:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 94:
                this.$ = [];
                break;
              case 95:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 98:
                this.$ = [ $$[ $0 ] ];
                break;
              case 99:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
              case 100:
                this.$ = [ $$[ $0 ] ];
                break;
              case 101:
                $$[ $0 - 1 ].push( $$[ $0 ] );
                break;
            }
          },
          table          : [ { 3 : 1, 4 : 2, 5 : [ 2, 46 ], 6 : 3, 14 : [ 2, 46 ], 15 : [ 2, 46 ], 19 : [ 2, 46 ], 29 : [ 2, 46 ], 34 : [ 2, 46 ], 48 : [ 2, 46 ], 51 : [ 2, 46 ], 55 : [ 2, 46 ], 60 : [ 2, 46 ] }, { 1 : [ 3 ] }, { 5 : [ 1, 4 ] }, { 5 : [ 2, 2 ], 7 : 5, 8 : 6, 9 : 7, 10 : 8, 11 : 9, 12 : 10, 13 : 11, 14 : [ 1, 12 ], 15 : [ 1, 20 ], 16 : 17, 19 : [ 1, 23 ], 24 : 15, 27 : 16, 29 : [ 1, 21 ], 34 : [ 1, 22 ], 39 : [ 2, 2 ], 44 : [ 2, 2 ], 47 : [ 2, 2 ], 48 : [ 1, 13 ], 51 : [ 1, 14 ], 55 : [ 1, 18 ], 59 : 19, 60 : [ 1, 24 ] }, { 1 : [ 2, 1 ] }, { 5 : [ 2, 47 ], 14 : [ 2, 47 ], 15 : [ 2, 47 ], 19 : [ 2, 47 ], 29 : [ 2, 47 ], 34 : [ 2, 47 ], 39 : [ 2, 47 ], 44 : [ 2, 47 ], 47 : [ 2, 47 ], 48 : [ 2, 47 ], 51 : [ 2, 47 ], 55 : [ 2, 47 ], 60 : [ 2, 47 ] }, { 5 : [ 2, 3 ], 14 : [ 2, 3 ], 15 : [ 2, 3 ], 19 : [ 2, 3 ], 29 : [ 2, 3 ], 34 : [ 2, 3 ], 39 : [ 2, 3 ], 44 : [ 2, 3 ], 47 : [ 2, 3 ], 48 : [ 2, 3 ], 51 : [ 2, 3 ], 55 : [ 2, 3 ], 60 : [ 2, 3 ] }, { 5 : [ 2, 4 ], 14 : [ 2, 4 ], 15 : [ 2, 4 ], 19 : [ 2, 4 ], 29 : [ 2, 4 ], 34 : [ 2, 4 ], 39 : [ 2, 4 ], 44 : [ 2, 4 ], 47 : [ 2, 4 ], 48 : [ 2, 4 ], 51 : [ 2, 4 ], 55 : [ 2, 4 ], 60 : [ 2, 4 ] }, { 5 : [ 2, 5 ], 14 : [ 2, 5 ], 15 : [ 2, 5 ], 19 : [ 2, 5 ], 29 : [ 2, 5 ], 34 : [ 2, 5 ], 39 : [ 2, 5 ], 44 : [ 2, 5 ], 47 : [ 2, 5 ], 48 : [ 2, 5 ], 51 : [ 2, 5 ], 55 : [ 2, 5 ], 60 : [ 2, 5 ] }, { 5 : [ 2, 6 ], 14 : [ 2, 6 ], 15 : [ 2, 6 ], 19 : [ 2, 6 ], 29 : [ 2, 6 ], 34 : [ 2, 6 ], 39 : [ 2, 6 ], 44 : [ 2, 6 ], 47 : [ 2, 6 ], 48 : [ 2, 6 ], 51 : [ 2, 6 ], 55 : [ 2, 6 ], 60 : [ 2, 6 ] }, { 5 : [ 2, 7 ], 14 : [ 2, 7 ], 15 : [ 2, 7 ], 19 : [ 2, 7 ], 29 : [ 2, 7 ], 34 : [ 2, 7 ], 39 : [ 2, 7 ], 44 : [ 2, 7 ], 47 : [ 2, 7 ], 48 : [ 2, 7 ], 51 : [ 2, 7 ], 55 : [ 2, 7 ], 60 : [ 2, 7 ] }, { 5 : [ 2, 8 ], 14 : [ 2, 8 ], 15 : [ 2, 8 ], 19 : [ 2, 8 ], 29 : [ 2, 8 ], 34 : [ 2, 8 ], 39 : [ 2, 8 ], 44 : [ 2, 8 ], 47 : [ 2, 8 ], 48 : [ 2, 8 ], 51 : [ 2, 8 ], 55 : [ 2, 8 ], 60 : [ 2, 8 ] }, { 5 : [ 2, 9 ], 14 : [ 2, 9 ], 15 : [ 2, 9 ], 19 : [ 2, 9 ], 29 : [ 2, 9 ], 34 : [ 2, 9 ], 39 : [ 2, 9 ], 44 : [ 2, 9 ], 47 : [ 2, 9 ], 48 : [ 2, 9 ], 51 : [ 2, 9 ], 55 : [ 2, 9 ], 60 : [ 2, 9 ] }, { 20 : 25, 72 : [ 1, 35 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 20 : 36, 72 : [ 1, 35 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 4 : 37, 6 : 3, 14 : [ 2, 46 ], 15 : [ 2, 46 ], 19 : [ 2, 46 ], 29 : [ 2, 46 ], 34 : [ 2, 46 ], 39 : [ 2, 46 ], 44 : [ 2, 46 ], 47 : [ 2, 46 ], 48 : [ 2, 46 ], 51 : [ 2, 46 ], 55 : [ 2, 46 ], 60 : [ 2, 46 ] }, { 4 : 38, 6 : 3, 14 : [ 2, 46 ], 15 : [ 2, 46 ], 19 : [ 2, 46 ], 29 : [ 2, 46 ], 34 : [ 2, 46 ], 44 : [ 2, 46 ], 47 : [ 2, 46 ], 48 : [ 2, 46 ], 51 : [ 2, 46 ], 55 : [ 2, 46 ], 60 : [ 2, 46 ] }, { 13 : 40, 15 : [ 1, 20 ], 17 : 39 }, { 20 : 42, 56 : 41, 64 : 43, 65 : [ 1, 44 ], 72 : [ 1, 35 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 4 : 45, 6 : 3, 14 : [ 2, 46 ], 15 : [ 2, 46 ], 19 : [ 2, 46 ], 29 : [ 2, 46 ], 34 : [ 2, 46 ], 47 : [ 2, 46 ], 48 : [ 2, 46 ], 51 : [ 2, 46 ], 55 : [ 2, 46 ], 60 : [ 2, 46 ] }, { 5 : [ 2, 10 ], 14 : [ 2, 10 ], 15 : [ 2, 10 ], 18 : [ 2, 10 ], 19 : [ 2, 10 ], 29 : [ 2, 10 ], 34 : [ 2, 10 ], 39 : [ 2, 10 ], 44 : [ 2, 10 ], 47 : [ 2, 10 ], 48 : [ 2, 10 ], 51 : [ 2, 10 ], 55 : [ 2, 10 ], 60 : [ 2, 10 ] }, { 20 : 46, 72 : [ 1, 35 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 20 : 47, 72 : [ 1, 35 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 20 : 48, 72 : [ 1, 35 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 20 : 42, 56 : 49, 64 : 43, 65 : [ 1, 44 ], 72 : [ 1, 35 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 33 : [ 2, 78 ], 49 : 50, 65 : [ 2, 78 ], 72 : [ 2, 78 ], 80 : [ 2, 78 ], 81 : [ 2, 78 ], 82 : [ 2, 78 ], 83 : [ 2, 78 ], 84 : [ 2, 78 ], 85 : [ 2, 78 ] }, { 23 : [ 2, 33 ], 33 : [ 2, 33 ], 54 : [ 2, 33 ], 65 : [ 2, 33 ], 68 : [ 2, 33 ], 72 : [ 2, 33 ], 75 : [ 2, 33 ], 80 : [ 2, 33 ], 81 : [ 2, 33 ], 82 : [ 2, 33 ], 83 : [ 2, 33 ], 84 : [ 2, 33 ], 85 : [ 2, 33 ] }, { 23 : [ 2, 34 ], 33 : [ 2, 34 ], 54 : [ 2, 34 ], 65 : [ 2, 34 ], 68 : [ 2, 34 ], 72 : [ 2, 34 ], 75 : [ 2, 34 ], 80 : [ 2, 34 ], 81 : [ 2, 34 ], 82 : [ 2, 34 ], 83 : [ 2, 34 ], 84 : [ 2, 34 ], 85 : [ 2, 34 ] }, { 23 : [ 2, 35 ], 33 : [ 2, 35 ], 54 : [ 2, 35 ], 65 : [ 2, 35 ], 68 : [ 2, 35 ], 72 : [ 2, 35 ], 75 : [ 2, 35 ], 80 : [ 2, 35 ], 81 : [ 2, 35 ], 82 : [ 2, 35 ], 83 : [ 2, 35 ], 84 : [ 2, 35 ], 85 : [ 2, 35 ] }, { 23 : [ 2, 36 ], 33 : [ 2, 36 ], 54 : [ 2, 36 ], 65 : [ 2, 36 ], 68 : [ 2, 36 ], 72 : [ 2, 36 ], 75 : [ 2, 36 ], 80 : [ 2, 36 ], 81 : [ 2, 36 ], 82 : [ 2, 36 ], 83 : [ 2, 36 ], 84 : [ 2, 36 ], 85 : [ 2, 36 ] }, { 23 : [ 2, 37 ], 33 : [ 2, 37 ], 54 : [ 2, 37 ], 65 : [ 2, 37 ], 68 : [ 2, 37 ], 72 : [ 2, 37 ], 75 : [ 2, 37 ], 80 : [ 2, 37 ], 81 : [ 2, 37 ], 82 : [ 2, 37 ], 83 : [ 2, 37 ], 84 : [ 2, 37 ], 85 : [ 2, 37 ] }, { 23 : [ 2, 38 ], 33 : [ 2, 38 ], 54 : [ 2, 38 ], 65 : [ 2, 38 ], 68 : [ 2, 38 ], 72 : [ 2, 38 ], 75 : [ 2, 38 ], 80 : [ 2, 38 ], 81 : [ 2, 38 ], 82 : [ 2, 38 ], 83 : [ 2, 38 ], 84 : [ 2, 38 ], 85 : [ 2, 38 ] }, { 23 : [ 2, 39 ], 33 : [ 2, 39 ], 54 : [ 2, 39 ], 65 : [ 2, 39 ], 68 : [ 2, 39 ], 72 : [ 2, 39 ], 75 : [ 2, 39 ], 80 : [ 2, 39 ], 81 : [ 2, 39 ], 82 : [ 2, 39 ], 83 : [ 2, 39 ], 84 : [ 2, 39 ], 85 : [ 2, 39 ] }, { 23 : [ 2, 43 ], 33 : [ 2, 43 ], 54 : [ 2, 43 ], 65 : [ 2, 43 ], 68 : [ 2, 43 ], 72 : [ 2, 43 ], 75 : [ 2, 43 ], 80 : [ 2, 43 ], 81 : [ 2, 43 ], 82 : [ 2, 43 ], 83 : [ 2, 43 ], 84 : [ 2, 43 ], 85 : [ 2, 43 ], 87 : [ 1, 51 ] }, { 72 : [ 1, 35 ], 86 : 52 }, { 23 : [ 2, 45 ], 33 : [ 2, 45 ], 54 : [ 2, 45 ], 65 : [ 2, 45 ], 68 : [ 2, 45 ], 72 : [ 2, 45 ], 75 : [ 2, 45 ], 80 : [ 2, 45 ], 81 : [ 2, 45 ], 82 : [ 2, 45 ], 83 : [ 2, 45 ], 84 : [ 2, 45 ], 85 : [ 2, 45 ], 87 : [ 2, 45 ] }, { 52 : 53, 54 : [ 2, 82 ], 65 : [ 2, 82 ], 72 : [ 2, 82 ], 80 : [ 2, 82 ], 81 : [ 2, 82 ], 82 : [ 2, 82 ], 83 : [ 2, 82 ], 84 : [ 2, 82 ], 85 : [ 2, 82 ] }, { 25 : 54, 38 : 56, 39 : [ 1, 58 ], 43 : 57, 44 : [ 1, 59 ], 45 : 55, 47 : [ 2, 54 ] }, { 28 : 60, 43 : 61, 44 : [ 1, 59 ], 47 : [ 2, 56 ] }, { 13 : 63, 15 : [ 1, 20 ], 18 : [ 1, 62 ] }, { 15 : [ 2, 48 ], 18 : [ 2, 48 ] }, { 33 : [ 2, 86 ], 57 : 64, 65 : [ 2, 86 ], 72 : [ 2, 86 ], 80 : [ 2, 86 ], 81 : [ 2, 86 ], 82 : [ 2, 86 ], 83 : [ 2, 86 ], 84 : [ 2, 86 ], 85 : [ 2, 86 ] }, { 33 : [ 2, 40 ], 65 : [ 2, 40 ], 72 : [ 2, 40 ], 80 : [ 2, 40 ], 81 : [ 2, 40 ], 82 : [ 2, 40 ], 83 : [ 2, 40 ], 84 : [ 2, 40 ], 85 : [ 2, 40 ] }, { 33 : [ 2, 41 ], 65 : [ 2, 41 ], 72 : [ 2, 41 ], 80 : [ 2, 41 ], 81 : [ 2, 41 ], 82 : [ 2, 41 ], 83 : [ 2, 41 ], 84 : [ 2, 41 ], 85 : [ 2, 41 ] }, { 20 : 65, 72 : [ 1, 35 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 26 : 66, 47 : [ 1, 67 ] }, { 30 : 68, 33 : [ 2, 58 ], 65 : [ 2, 58 ], 72 : [ 2, 58 ], 75 : [ 2, 58 ], 80 : [ 2, 58 ], 81 : [ 2, 58 ], 82 : [ 2, 58 ], 83 : [ 2, 58 ], 84 : [ 2, 58 ], 85 : [ 2, 58 ] }, { 33 : [ 2, 64 ], 35 : 69, 65 : [ 2, 64 ], 72 : [ 2, 64 ], 75 : [ 2, 64 ], 80 : [ 2, 64 ], 81 : [ 2, 64 ], 82 : [ 2, 64 ], 83 : [ 2, 64 ], 84 : [ 2, 64 ], 85 : [ 2, 64 ] }, { 21 : 70, 23 : [ 2, 50 ], 65 : [ 2, 50 ], 72 : [ 2, 50 ], 80 : [ 2, 50 ], 81 : [ 2, 50 ], 82 : [ 2, 50 ], 83 : [ 2, 50 ], 84 : [ 2, 50 ], 85 : [ 2, 50 ] }, { 33 : [ 2, 90 ], 61 : 71, 65 : [ 2, 90 ], 72 : [ 2, 90 ], 80 : [ 2, 90 ], 81 : [ 2, 90 ], 82 : [ 2, 90 ], 83 : [ 2, 90 ], 84 : [ 2, 90 ], 85 : [ 2, 90 ] }, { 20 : 75, 33 : [ 2, 80 ], 50 : 72, 63 : 73, 64 : 76, 65 : [ 1, 44 ], 69 : 74, 70 : 77, 71 : 78, 72 : [ 1, 79 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 72 : [ 1, 80 ] }, { 23 : [ 2, 42 ], 33 : [ 2, 42 ], 54 : [ 2, 42 ], 65 : [ 2, 42 ], 68 : [ 2, 42 ], 72 : [ 2, 42 ], 75 : [ 2, 42 ], 80 : [ 2, 42 ], 81 : [ 2, 42 ], 82 : [ 2, 42 ], 83 : [ 2, 42 ], 84 : [ 2, 42 ], 85 : [ 2, 42 ], 87 : [ 1, 51 ] }, { 20 : 75, 53 : 81, 54 : [ 2, 84 ], 63 : 82, 64 : 76, 65 : [ 1, 44 ], 69 : 83, 70 : 77, 71 : 78, 72 : [ 1, 79 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 26 : 84, 47 : [ 1, 67 ] }, { 47 : [ 2, 55 ] }, { 4 : 85, 6 : 3, 14 : [ 2, 46 ], 15 : [ 2, 46 ], 19 : [ 2, 46 ], 29 : [ 2, 46 ], 34 : [ 2, 46 ], 39 : [ 2, 46 ], 44 : [ 2, 46 ], 47 : [ 2, 46 ], 48 : [ 2, 46 ], 51 : [ 2, 46 ], 55 : [ 2, 46 ], 60 : [ 2, 46 ] }, { 47 : [ 2, 20 ] }, { 20 : 86, 72 : [ 1, 35 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 4 : 87, 6 : 3, 14 : [ 2, 46 ], 15 : [ 2, 46 ], 19 : [ 2, 46 ], 29 : [ 2, 46 ], 34 : [ 2, 46 ], 47 : [ 2, 46 ], 48 : [ 2, 46 ], 51 : [ 2, 46 ], 55 : [ 2, 46 ], 60 : [ 2, 46 ] }, { 26 : 88, 47 : [ 1, 67 ] }, { 47 : [ 2, 57 ] }, { 5 : [ 2, 11 ], 14 : [ 2, 11 ], 15 : [ 2, 11 ], 19 : [ 2, 11 ], 29 : [ 2, 11 ], 34 : [ 2, 11 ], 39 : [ 2, 11 ], 44 : [ 2, 11 ], 47 : [ 2, 11 ], 48 : [ 2, 11 ], 51 : [ 2, 11 ], 55 : [ 2, 11 ], 60 : [ 2, 11 ] }, { 15 : [ 2, 49 ], 18 : [ 2, 49 ] }, { 20 : 75, 33 : [ 2, 88 ], 58 : 89, 63 : 90, 64 : 76, 65 : [ 1, 44 ], 69 : 91, 70 : 77, 71 : 78, 72 : [ 1, 79 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 65 : [ 2, 94 ], 66 : 92, 68 : [ 2, 94 ], 72 : [ 2, 94 ], 80 : [ 2, 94 ], 81 : [ 2, 94 ], 82 : [ 2, 94 ], 83 : [ 2, 94 ], 84 : [ 2, 94 ], 85 : [ 2, 94 ] }, { 5 : [ 2, 25 ], 14 : [ 2, 25 ], 15 : [ 2, 25 ], 19 : [ 2, 25 ], 29 : [ 2, 25 ], 34 : [ 2, 25 ], 39 : [ 2, 25 ], 44 : [ 2, 25 ], 47 : [ 2, 25 ], 48 : [ 2, 25 ], 51 : [ 2, 25 ], 55 : [ 2, 25 ], 60 : [ 2, 25 ] }, { 20 : 93, 72 : [ 1, 35 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 20 : 75, 31 : 94, 33 : [ 2, 60 ], 63 : 95, 64 : 76, 65 : [ 1, 44 ], 69 : 96, 70 : 77, 71 : 78, 72 : [ 1, 79 ], 75 : [ 2, 60 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 20 : 75, 33 : [ 2, 66 ], 36 : 97, 63 : 98, 64 : 76, 65 : [ 1, 44 ], 69 : 99, 70 : 77, 71 : 78, 72 : [ 1, 79 ], 75 : [ 2, 66 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 20 : 75, 22 : 100, 23 : [ 2, 52 ], 63 : 101, 64 : 76, 65 : [ 1, 44 ], 69 : 102, 70 : 77, 71 : 78, 72 : [ 1, 79 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 20 : 75, 33 : [ 2, 92 ], 62 : 103, 63 : 104, 64 : 76, 65 : [ 1, 44 ], 69 : 105, 70 : 77, 71 : 78, 72 : [ 1, 79 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 33 : [ 1, 106 ] }, { 33 : [ 2, 79 ], 65 : [ 2, 79 ], 72 : [ 2, 79 ], 80 : [ 2, 79 ], 81 : [ 2, 79 ], 82 : [ 2, 79 ], 83 : [ 2, 79 ], 84 : [ 2, 79 ], 85 : [ 2, 79 ] }, { 33 : [ 2, 81 ] }, { 23 : [ 2, 27 ], 33 : [ 2, 27 ], 54 : [ 2, 27 ], 65 : [ 2, 27 ], 68 : [ 2, 27 ], 72 : [ 2, 27 ], 75 : [ 2, 27 ], 80 : [ 2, 27 ], 81 : [ 2, 27 ], 82 : [ 2, 27 ], 83 : [ 2, 27 ], 84 : [ 2, 27 ], 85 : [ 2, 27 ] }, { 23 : [ 2, 28 ], 33 : [ 2, 28 ], 54 : [ 2, 28 ], 65 : [ 2, 28 ], 68 : [ 2, 28 ], 72 : [ 2, 28 ], 75 : [ 2, 28 ], 80 : [ 2, 28 ], 81 : [ 2, 28 ], 82 : [ 2, 28 ], 83 : [ 2, 28 ], 84 : [ 2, 28 ], 85 : [ 2, 28 ] }, { 23 : [ 2, 30 ], 33 : [ 2, 30 ], 54 : [ 2, 30 ], 68 : [ 2, 30 ], 71 : 107, 72 : [ 1, 108 ], 75 : [ 2, 30 ] }, { 23 : [ 2, 98 ], 33 : [ 2, 98 ], 54 : [ 2, 98 ], 68 : [ 2, 98 ], 72 : [ 2, 98 ], 75 : [ 2, 98 ] }, { 23 : [ 2, 45 ], 33 : [ 2, 45 ], 54 : [ 2, 45 ], 65 : [ 2, 45 ], 68 : [ 2, 45 ], 72 : [ 2, 45 ], 73 : [ 1, 109 ], 75 : [ 2, 45 ], 80 : [ 2, 45 ], 81 : [ 2, 45 ], 82 : [ 2, 45 ], 83 : [ 2, 45 ], 84 : [ 2, 45 ], 85 : [ 2, 45 ], 87 : [ 2, 45 ] }, { 23 : [ 2, 44 ], 33 : [ 2, 44 ], 54 : [ 2, 44 ], 65 : [ 2, 44 ], 68 : [ 2, 44 ], 72 : [ 2, 44 ], 75 : [ 2, 44 ], 80 : [ 2, 44 ], 81 : [ 2, 44 ], 82 : [ 2, 44 ], 83 : [ 2, 44 ], 84 : [ 2, 44 ], 85 : [ 2, 44 ], 87 : [ 2, 44 ] }, { 54 : [ 1, 110 ] }, { 54 : [ 2, 83 ], 65 : [ 2, 83 ], 72 : [ 2, 83 ], 80 : [ 2, 83 ], 81 : [ 2, 83 ], 82 : [ 2, 83 ], 83 : [ 2, 83 ], 84 : [ 2, 83 ], 85 : [ 2, 83 ] }, { 54 : [ 2, 85 ] }, { 5 : [ 2, 13 ], 14 : [ 2, 13 ], 15 : [ 2, 13 ], 19 : [ 2, 13 ], 29 : [ 2, 13 ], 34 : [ 2, 13 ], 39 : [ 2, 13 ], 44 : [ 2, 13 ], 47 : [ 2, 13 ], 48 : [ 2, 13 ], 51 : [ 2, 13 ], 55 : [ 2, 13 ], 60 : [ 2, 13 ] }, { 38 : 56, 39 : [ 1, 58 ], 43 : 57, 44 : [ 1, 59 ], 45 : 112, 46 : 111, 47 : [ 2, 76 ] }, { 33 : [ 2, 70 ], 40 : 113, 65 : [ 2, 70 ], 72 : [ 2, 70 ], 75 : [ 2, 70 ], 80 : [ 2, 70 ], 81 : [ 2, 70 ], 82 : [ 2, 70 ], 83 : [ 2, 70 ], 84 : [ 2, 70 ], 85 : [ 2, 70 ] }, { 47 : [ 2, 18 ] }, { 5 : [ 2, 14 ], 14 : [ 2, 14 ], 15 : [ 2, 14 ], 19 : [ 2, 14 ], 29 : [ 2, 14 ], 34 : [ 2, 14 ], 39 : [ 2, 14 ], 44 : [ 2, 14 ], 47 : [ 2, 14 ], 48 : [ 2, 14 ], 51 : [ 2, 14 ], 55 : [ 2, 14 ], 60 : [ 2, 14 ] }, { 33 : [ 1, 114 ] }, { 33 : [ 2, 87 ], 65 : [ 2, 87 ], 72 : [ 2, 87 ], 80 : [ 2, 87 ], 81 : [ 2, 87 ], 82 : [ 2, 87 ], 83 : [ 2, 87 ], 84 : [ 2, 87 ], 85 : [ 2, 87 ] }, { 33 : [ 2, 89 ] }, { 20 : 75, 63 : 116, 64 : 76, 65 : [ 1, 44 ], 67 : 115, 68 : [ 2, 96 ], 69 : 117, 70 : 77, 71 : 78, 72 : [ 1, 79 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 33 : [ 1, 118 ] }, { 32 : 119, 33 : [ 2, 62 ], 74 : 120, 75 : [ 1, 121 ] }, { 33 : [ 2, 59 ], 65 : [ 2, 59 ], 72 : [ 2, 59 ], 75 : [ 2, 59 ], 80 : [ 2, 59 ], 81 : [ 2, 59 ], 82 : [ 2, 59 ], 83 : [ 2, 59 ], 84 : [ 2, 59 ], 85 : [ 2, 59 ] }, { 33 : [ 2, 61 ], 75 : [ 2, 61 ] }, { 33 : [ 2, 68 ], 37 : 122, 74 : 123, 75 : [ 1, 121 ] }, { 33 : [ 2, 65 ], 65 : [ 2, 65 ], 72 : [ 2, 65 ], 75 : [ 2, 65 ], 80 : [ 2, 65 ], 81 : [ 2, 65 ], 82 : [ 2, 65 ], 83 : [ 2, 65 ], 84 : [ 2, 65 ], 85 : [ 2, 65 ] }, { 33 : [ 2, 67 ], 75 : [ 2, 67 ] }, { 23 : [ 1, 124 ] }, { 23 : [ 2, 51 ], 65 : [ 2, 51 ], 72 : [ 2, 51 ], 80 : [ 2, 51 ], 81 : [ 2, 51 ], 82 : [ 2, 51 ], 83 : [ 2, 51 ], 84 : [ 2, 51 ], 85 : [ 2, 51 ] }, { 23 : [ 2, 53 ] }, { 33 : [ 1, 125 ] }, { 33 : [ 2, 91 ], 65 : [ 2, 91 ], 72 : [ 2, 91 ], 80 : [ 2, 91 ], 81 : [ 2, 91 ], 82 : [ 2, 91 ], 83 : [ 2, 91 ], 84 : [ 2, 91 ], 85 : [ 2, 91 ] }, { 33 : [ 2, 93 ] }, { 5 : [ 2, 22 ], 14 : [ 2, 22 ], 15 : [ 2, 22 ], 19 : [ 2, 22 ], 29 : [ 2, 22 ], 34 : [ 2, 22 ], 39 : [ 2, 22 ], 44 : [ 2, 22 ], 47 : [ 2, 22 ], 48 : [ 2, 22 ], 51 : [ 2, 22 ], 55 : [ 2, 22 ], 60 : [ 2, 22 ] }, { 23 : [ 2, 99 ], 33 : [ 2, 99 ], 54 : [ 2, 99 ], 68 : [ 2, 99 ], 72 : [ 2, 99 ], 75 : [ 2, 99 ] }, { 73 : [ 1, 109 ] }, { 20 : 75, 63 : 126, 64 : 76, 65 : [ 1, 44 ], 72 : [ 1, 35 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 5 : [ 2, 23 ], 14 : [ 2, 23 ], 15 : [ 2, 23 ], 19 : [ 2, 23 ], 29 : [ 2, 23 ], 34 : [ 2, 23 ], 39 : [ 2, 23 ], 44 : [ 2, 23 ], 47 : [ 2, 23 ], 48 : [ 2, 23 ], 51 : [ 2, 23 ], 55 : [ 2, 23 ], 60 : [ 2, 23 ] }, { 47 : [ 2, 19 ] }, { 47 : [ 2, 77 ] }, { 20 : 75, 33 : [ 2, 72 ], 41 : 127, 63 : 128, 64 : 76, 65 : [ 1, 44 ], 69 : 129, 70 : 77, 71 : 78, 72 : [ 1, 79 ], 75 : [ 2, 72 ], 78 : 26, 79 : 27, 80 : [ 1, 28 ], 81 : [ 1, 29 ], 82 : [ 1, 30 ], 83 : [ 1, 31 ], 84 : [ 1, 32 ], 85 : [ 1, 34 ], 86 : 33 }, { 5 : [ 2, 24 ], 14 : [ 2, 24 ], 15 : [ 2, 24 ], 19 : [ 2, 24 ], 29 : [ 2, 24 ], 34 : [ 2, 24 ], 39 : [ 2, 24 ], 44 : [ 2, 24 ], 47 : [ 2, 24 ], 48 : [ 2, 24 ], 51 : [ 2, 24 ], 55 : [ 2, 24 ], 60 : [ 2, 24 ] }, { 68 : [ 1, 130 ] }, { 65 : [ 2, 95 ], 68 : [ 2, 95 ], 72 : [ 2, 95 ], 80 : [ 2, 95 ], 81 : [ 2, 95 ], 82 : [ 2, 95 ], 83 : [ 2, 95 ], 84 : [ 2, 95 ], 85 : [ 2, 95 ] }, { 68 : [ 2, 97 ] }, { 5 : [ 2, 21 ], 14 : [ 2, 21 ], 15 : [ 2, 21 ], 19 : [ 2, 21 ], 29 : [ 2, 21 ], 34 : [ 2, 21 ], 39 : [ 2, 21 ], 44 : [ 2, 21 ], 47 : [ 2, 21 ], 48 : [ 2, 21 ], 51 : [ 2, 21 ], 55 : [ 2, 21 ], 60 : [ 2, 21 ] }, { 33 : [ 1, 131 ] }, { 33 : [ 2, 63 ] }, { 72 : [ 1, 133 ], 76 : 132 }, { 33 : [ 1, 134 ] }, { 33 : [ 2, 69 ] }, { 15 : [ 2, 12 ] }, { 14 : [ 2, 26 ], 15 : [ 2, 26 ], 19 : [ 2, 26 ], 29 : [ 2, 26 ], 34 : [ 2, 26 ], 47 : [ 2, 26 ], 48 : [ 2, 26 ], 51 : [ 2, 26 ], 55 : [ 2, 26 ], 60 : [ 2, 26 ] }, { 23 : [ 2, 31 ], 33 : [ 2, 31 ], 54 : [ 2, 31 ], 68 : [ 2, 31 ], 72 : [ 2, 31 ], 75 : [ 2, 31 ] }, { 33 : [ 2, 74 ], 42 : 135, 74 : 136, 75 : [ 1, 121 ] }, { 33 : [ 2, 71 ], 65 : [ 2, 71 ], 72 : [ 2, 71 ], 75 : [ 2, 71 ], 80 : [ 2, 71 ], 81 : [ 2, 71 ], 82 : [ 2, 71 ], 83 : [ 2, 71 ], 84 : [ 2, 71 ], 85 : [ 2, 71 ] }, { 33 : [ 2, 73 ], 75 : [ 2, 73 ] }, { 23 : [ 2, 29 ], 33 : [ 2, 29 ], 54 : [ 2, 29 ], 65 : [ 2, 29 ], 68 : [ 2, 29 ], 72 : [ 2, 29 ], 75 : [ 2, 29 ], 80 : [ 2, 29 ], 81 : [ 2, 29 ], 82 : [ 2, 29 ], 83 : [ 2, 29 ], 84 : [ 2, 29 ], 85 : [ 2, 29 ] }, { 14 : [ 2, 15 ], 15 : [ 2, 15 ], 19 : [ 2, 15 ], 29 : [ 2, 15 ], 34 : [ 2, 15 ], 39 : [ 2, 15 ], 44 : [ 2, 15 ], 47 : [ 2, 15 ], 48 : [ 2, 15 ], 51 : [ 2, 15 ], 55 : [ 2, 15 ], 60 : [ 2, 15 ] }, { 72 : [ 1, 138 ], 77 : [ 1, 137 ] }, { 72 : [ 2, 100 ], 77 : [ 2, 100 ] }, { 14 : [ 2, 16 ], 15 : [ 2, 16 ], 19 : [ 2, 16 ], 29 : [ 2, 16 ], 34 : [ 2, 16 ], 44 : [ 2, 16 ], 47 : [ 2, 16 ], 48 : [ 2, 16 ], 51 : [ 2, 16 ], 55 : [ 2, 16 ], 60 : [ 2, 16 ] }, { 33 : [ 1, 139 ] }, { 33 : [ 2, 75 ] }, { 33 : [ 2, 32 ] }, { 72 : [ 2, 101 ], 77 : [ 2, 101 ] }, { 14 : [ 2, 17 ], 15 : [ 2, 17 ], 19 : [ 2, 17 ], 29 : [ 2, 17 ], 34 : [ 2, 17 ], 39 : [ 2, 17 ], 44 : [ 2, 17 ], 47 : [ 2, 17 ], 48 : [ 2, 17 ], 51 : [ 2, 17 ], 55 : [ 2, 17 ], 60 : [ 2, 17 ] } ],
          defaultActions : { 4 : [ 2, 1 ], 55 : [ 2, 55 ], 57 : [ 2, 20 ], 61 : [ 2, 57 ], 74 : [ 2, 81 ], 83 : [ 2, 85 ], 87 : [ 2, 18 ], 91 : [ 2, 89 ], 102 : [ 2, 53 ], 105 : [ 2, 93 ], 111 : [ 2, 19 ], 112 : [ 2, 77 ], 117 : [ 2, 97 ], 120 : [ 2, 63 ], 123 : [ 2, 69 ], 124 : [ 2, 12 ], 136 : [ 2, 75 ], 137 : [ 2, 32 ] },
          parseError     : function parseError ( str, hash ) {
            throw new Error( str );
          },
          parse          : function parse ( input ) {
            var self       = this,
                stack      = [ 0 ],
                vstack     = [ null ],
                lstack     = [],
                table      = this.table,
                yytext     = "",
                yylineno   = 0,
                yyleng     = 0,
                recovering = 0,
                TERROR     = 2,
                EOF        = 1;
            this.lexer.setInput( input );
            this.lexer.yy = this.yy;
            this.yy.lexer = this.lexer;
            this.yy.parser = this;
            if ( typeof this.lexer.yylloc == "undefined" ) this.lexer.yylloc = {};
            var yyloc = this.lexer.yylloc;
            lstack.push( yyloc );
            var ranges = this.lexer.options && this.lexer.options.ranges;
            if ( typeof this.yy.parseError === "function" ) this.parseError = this.yy.parseError;
            function popStack ( n ) {
              stack.length = stack.length - 2 * n;
              vstack.length = vstack.length - n;
              lstack.length = lstack.length - n;
            }

            function lex () {
              var token;
              token = self.lexer.lex() || 1;
              if ( typeof token !== "number" ) {
                token = self.symbols_[ token ] || token;
              }
              return token;
            }

            var symbol,
                preErrorSymbol,
                state,
                action,
                a,
                r,
                yyval = {},
                p,
                len,
                newState,
                expected;
            while ( true ) {
              state = stack[ stack.length - 1 ];
              if ( this.defaultActions[ state ] ) {
                action = this.defaultActions[ state ];
              } else {
                if ( symbol === null || typeof symbol == "undefined" ) {
                  symbol = lex();
                }
                action = table[ state ] && table[ state ][ symbol ];
              }
              if ( typeof action === "undefined" || !action.length || !action[ 0 ] ) {
                var errStr = "";
                if ( !recovering ) {
                  expected = [];
                  for ( p in table[ state ] ) if ( this.terminals_[ p ] && p > 2 ) {
                    expected.push( "'" + this.terminals_[ p ] + "'" );
                  }
                  if ( this.lexer.showPosition ) {
                    errStr = "Parse error on line " + (yylineno + 1) + ":\n" + this.lexer.showPosition() + "\nExpecting " + expected.join( ", " ) + ", got '" + (this.terminals_[ symbol ] || symbol) + "'";
                  } else {
                    errStr = "Parse error on line " + (yylineno + 1) + ": Unexpected " + (symbol == 1 ? "end of input" : "'" + (this.terminals_[ symbol ] || symbol) + "'");
                  }
                  this.parseError( errStr, { text : this.lexer.match, token : this.terminals_[ symbol ] || symbol, line : this.lexer.yylineno, loc : yyloc, expected : expected } );
                }
              }
              if ( action[ 0 ] instanceof Array && action.length > 1 ) {
                throw new Error( "Parse Error: multiple actions possible at state: " + state + ", token: " + symbol );
              }
              switch ( action[ 0 ] ) {
                case 1:
                  stack.push( symbol );
                  vstack.push( this.lexer.yytext );
                  lstack.push( this.lexer.yylloc );
                  stack.push( action[ 1 ] );
                  symbol = null;
                  if ( !preErrorSymbol ) {
                    yyleng = this.lexer.yyleng;
                    yytext = this.lexer.yytext;
                    yylineno = this.lexer.yylineno;
                    yyloc = this.lexer.yylloc;
                    if ( recovering > 0 ) recovering--;
                  } else {
                    symbol = preErrorSymbol;
                    preErrorSymbol = null;
                  }
                  break;
                case 2:
                  len = this.productions_[ action[ 1 ] ][ 1 ];
                  yyval.$ = vstack[ vstack.length - len ];
                  yyval._$ = { first_line : lstack[ lstack.length - (len || 1) ].first_line, last_line : lstack[ lstack.length - 1 ].last_line, first_column : lstack[ lstack.length - (len || 1) ].first_column, last_column : lstack[ lstack.length - 1 ].last_column };
                  if ( ranges ) {
                    yyval._$.range = [ lstack[ lstack.length - (len || 1) ].range[ 0 ], lstack[ lstack.length - 1 ].range[ 1 ] ];
                  }
                  r = this.performAction.call( yyval, yytext, yyleng, yylineno, this.yy, action[ 1 ], vstack, lstack );
                  if ( typeof r !== "undefined" ) {
                    return r;
                  }
                  if ( len ) {
                    stack = stack.slice( 0, -1 * len * 2 );
                    vstack = vstack.slice( 0, -1 * len );
                    lstack = lstack.slice( 0, -1 * len );
                  }
                  stack.push( this.productions_[ action[ 1 ] ][ 0 ] );
                  vstack.push( yyval.$ );
                  lstack.push( yyval._$ );
                  newState = table[ stack[ stack.length - 2 ] ][ stack[ stack.length - 1 ] ];
                  stack.push( newState );
                  break;
                case 3:
                  return true;
              }
            }
            return true;
          }
        };
        /* Jison generated lexer */
        var lexer = (function () {
          var lexer = {
            EOF           : 1,
            parseError    : function parseError ( str, hash ) {
              if ( this.yy.parser ) {
                this.yy.parser.parseError( str, hash );
              } else {
                throw new Error( str );
              }
            },
            setInput      : function setInput ( input ) {
              this._input = input;
              this._more = this._less = this.done = false;
              this.yylineno = this.yyleng = 0;
              this.yytext = this.matched = this.match = '';
              this.conditionStack = [ 'INITIAL' ];
              this.yylloc = { first_line : 1, first_column : 0, last_line : 1, last_column : 0 };
              if ( this.options.ranges ) this.yylloc.range = [ 0, 0 ];
              this.offset = 0;
              return this;
            },
            input         : function input () {
              var ch = this._input[ 0 ];
              this.yytext += ch;
              this.yyleng++;
              this.offset++;
              this.match += ch;
              this.matched += ch;
              var lines = ch.match( /(?:\r\n?|\n).*/g );
              if ( lines ) {
                this.yylineno++;
                this.yylloc.last_line++;
              } else {
                this.yylloc.last_column++;
              }
              if ( this.options.ranges ) this.yylloc.range[ 1 ]++;

              this._input = this._input.slice( 1 );
              return ch;
            },
            unput         : function unput ( ch ) {
              var len = ch.length;
              var lines = ch.split( /(?:\r\n?|\n)/g );

              this._input = ch + this._input;
              this.yytext = this.yytext.substr( 0, this.yytext.length - len - 1 );
              //this.yyleng -= len;
              this.offset -= len;
              var oldLines = this.match.split( /(?:\r\n?|\n)/g );
              this.match = this.match.substr( 0, this.match.length - 1 );
              this.matched = this.matched.substr( 0, this.matched.length - 1 );

              if ( lines.length - 1 ) this.yylineno -= lines.length - 1;
              var r = this.yylloc.range;

              this.yylloc = {
                first_line   : this.yylloc.first_line,
                last_line    : this.yylineno + 1,
                first_column : this.yylloc.first_column,
                last_column  : lines ? (lines.length === oldLines.length ? this.yylloc.first_column : 0) + oldLines[ oldLines.length - lines.length ].length - lines[ 0 ].length : this.yylloc.first_column - len
              };

              if ( this.options.ranges ) {
                this.yylloc.range = [ r[ 0 ], r[ 0 ] + this.yyleng - len ];
              }
              return this;
            },
            more          : function more () {
              this._more = true;
              return this;
            },
            less          : function less ( n ) {
              this.unput( this.match.slice( n ) );
            },
            pastInput     : function pastInput () {
              var past = this.matched.substr( 0, this.matched.length - this.match.length );
              return (past.length > 20 ? '...' : '') + past.substr( -20 ).replace( /\n/g, "" );
            },
            upcomingInput : function upcomingInput () {
              var next = this.match;
              if ( next.length < 20 ) {
                next += this._input.substr( 0, 20 - next.length );
              }
              return (next.substr( 0, 20 ) + (next.length > 20 ? '...' : '')).replace( /\n/g, "" );
            },
            showPosition  : function showPosition () {
              var pre = this.pastInput();
              var c = new Array( pre.length + 1 ).join( "-" );
              return pre + this.upcomingInput() + "\n" + c + "^";
            },
            next          : function next () {
              if ( this.done ) {
                return this.EOF;
              }
              if ( !this._input ) this.done = true;

              var token, match, tempMatch, index, col, lines;
              if ( !this._more ) {
                this.yytext = '';
                this.match = '';
              }
              var rules = this._currentRules();
              for ( var i = 0; i < rules.length; i++ ) {
                tempMatch = this._input.match( this.rules[ rules[ i ] ] );
                if ( tempMatch && (!match || tempMatch[ 0 ].length > match[ 0 ].length) ) {
                  match = tempMatch;
                  index = i;
                  if ( !this.options.flex ) break;
                }
              }
              if ( match ) {
                lines = match[ 0 ].match( /(?:\r\n?|\n).*/g );
                if ( lines ) this.yylineno += lines.length;
                this.yylloc = {
                  first_line   : this.yylloc.last_line,
                  last_line    : this.yylineno + 1,
                  first_column : this.yylloc.last_column,
                  last_column  : lines ? lines[ lines.length - 1 ].length - lines[ lines.length - 1 ].match( /\r?\n?/ )[ 0 ].length : this.yylloc.last_column + match[ 0 ].length
                };
                this.yytext += match[ 0 ];
                this.match += match[ 0 ];
                this.matches = match;
                this.yyleng = this.yytext.length;
                if ( this.options.ranges ) {
                  this.yylloc.range = [ this.offset, this.offset += this.yyleng ];
                }
                this._more = false;
                this._input = this._input.slice( match[ 0 ].length );
                this.matched += match[ 0 ];
                token = this.performAction.call( this, this.yy, this, rules[ index ], this.conditionStack[ this.conditionStack.length - 1 ] );
                if ( this.done && this._input ) this.done = false;
                if ( token ) return token; else return;
              }
              if ( this._input === "" ) {
                return this.EOF;
              } else {
                return this.parseError( 'Lexical error on line ' + (this.yylineno + 1) + '. Unrecognized text.\n' + this.showPosition(), { text : "", token : null, line : this.yylineno } );
              }
            },
            lex           : function lex () {
              var r = this.next();
              if ( typeof r !== 'undefined' ) {
                return r;
              } else {
                return this.lex();
              }
            },
            begin         : function begin ( condition ) {
              this.conditionStack.push( condition );
            },
            popState      : function popState () {
              return this.conditionStack.pop();
            },
            _currentRules : function _currentRules () {
              return this.conditions[ this.conditionStack[ this.conditionStack.length - 1 ] ].rules;
            },
            topState      : function topState () {
              return this.conditionStack[ this.conditionStack.length - 2 ];
            },
            pushState     : function begin ( condition ) {
              this.begin( condition );
            }
          };
          lexer.options = {};
          lexer.performAction = function anonymous ( yy, yy_, $avoiding_name_collisions, YY_START ) {

            function strip ( start, end ) {
              return yy_.yytext = yy_.yytext.substr( start, yy_.yyleng - end );
            }

            var YYSTATE = YY_START;
            switch ( $avoiding_name_collisions ) {
              case 0:
                if ( yy_.yytext.slice( -2 ) === "\\\\" ) {
                  strip( 0, 1 );
                  this.begin( "mu" );
                } else if ( yy_.yytext.slice( -1 ) === "\\" ) {
                  strip( 0, 1 );
                  this.begin( "emu" );
                } else {
                  this.begin( "mu" );
                }
                if ( yy_.yytext ) return 15;

                break;
              case 1:
                return 15;
                break;
              case 2:
                this.popState();
                return 15;

                break;
              case 3:
                this.begin( 'raw' );
                return 15;
                break;
              case 4:
                this.popState();
                // Should be using `this.topState()` below, but it currently
                // returns the second top instead of the first top. Opened an
                // issue about it at https://github.com/zaach/jison/issues/291
                if ( this.conditionStack[ this.conditionStack.length - 1 ] === 'raw' ) {
                  return 15;
                } else {
                  yy_.yytext = yy_.yytext.substr( 5, yy_.yyleng - 9 );
                  return 'END_RAW_BLOCK';
                }

                break;
              case 5:
                return 15;
                break;
              case 6:
                this.popState();
                return 14;

                break;
              case 7:
                return 65;
                break;
              case 8:
                return 68;
                break;
              case 9:
                return 19;
                break;
              case 10:
                this.popState();
                this.begin( 'raw' );
                return 23;

                break;
              case 11:
                return 55;
                break;
              case 12:
                return 60;
                break;
              case 13:
                return 29;
                break;
              case 14:
                return 47;
                break;
              case 15:
                this.popState();
                return 44;
                break;
              case 16:
                this.popState();
                return 44;
                break;
              case 17:
                return 34;
                break;
              case 18:
                return 39;
                break;
              case 19:
                return 51;
                break;
              case 20:
                return 48;
                break;
              case 21:
                this.unput( yy_.yytext );
                this.popState();
                this.begin( 'com' );

                break;
              case 22:
                this.popState();
                return 14;

                break;
              case 23:
                return 48;
                break;
              case 24:
                return 73;
                break;
              case 25:
                return 72;
                break;
              case 26:
                return 72;
                break;
              case 27:
                return 87;
                break;
              case 28:
                // ignore whitespace
                break;
              case 29:
                this.popState();
                return 54;
                break;
              case 30:
                this.popState();
                return 33;
                break;
              case 31:
                yy_.yytext = strip( 1, 2 ).replace( /\\"/g, '"' );
                return 80;
                break;
              case 32:
                yy_.yytext = strip( 1, 2 ).replace( /\\'/g, "'" );
                return 80;
                break;
              case 33:
                return 85;
                break;
              case 34:
                return 82;
                break;
              case 35:
                return 82;
                break;
              case 36:
                return 83;
                break;
              case 37:
                return 84;
                break;
              case 38:
                return 81;
                break;
              case 39:
                return 75;
                break;
              case 40:
                return 77;
                break;
              case 41:
                return 72;
                break;
              case 42:
                return 72;
                break;
              case 43:
                return 'INVALID';
                break;
              case 44:
                return 5;
                break;
            }
          };
          lexer.rules = [ /^(?:[^\x00]*?(?=(\{\{)))/, /^(?:[^\x00]+)/, /^(?:[^\x00]{2,}?(?=(\{\{|\\\{\{|\\\\\{\{|$)))/, /^(?:\{\{\{\{(?=[^/]))/, /^(?:\{\{\{\{\/[^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=[=}\s\/.])\}\}\}\})/, /^(?:[^\x00]*?(?=(\{\{\{\{)))/, /^(?:[\s\S]*?--(~)?\}\})/, /^(?:\()/, /^(?:\))/, /^(?:\{\{\{\{)/, /^(?:\}\}\}\})/, /^(?:\{\{(~)?>)/, /^(?:\{\{(~)?#>)/, /^(?:\{\{(~)?#\*?)/, /^(?:\{\{(~)?\/)/, /^(?:\{\{(~)?\^\s*(~)?\}\})/, /^(?:\{\{(~)?\s*else\s*(~)?\}\})/, /^(?:\{\{(~)?\^)/, /^(?:\{\{(~)?\s*else\b)/, /^(?:\{\{(~)?\{)/, /^(?:\{\{(~)?&)/, /^(?:\{\{(~)?!--)/, /^(?:\{\{(~)?![\s\S]*?\}\})/, /^(?:\{\{(~)?\*?)/, /^(?:=)/, /^(?:\.\.)/, /^(?:\.(?=([=~}\s\/.)|])))/, /^(?:[\/.])/, /^(?:\s+)/, /^(?:\}(~)?\}\})/, /^(?:(~)?\}\})/, /^(?:"(\\["]|[^"])*")/, /^(?:'(\\[']|[^'])*')/, /^(?:@)/, /^(?:true(?=([~}\s)])))/, /^(?:false(?=([~}\s)])))/, /^(?:undefined(?=([~}\s)])))/, /^(?:null(?=([~}\s)])))/, /^(?:-?[0-9]+(?:\.[0-9]+)?(?=([~}\s)])))/, /^(?:as\s+\|)/, /^(?:\|)/, /^(?:([^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=([=~}\s\/.)|]))))/, /^(?:\[[^\]]*\])/, /^(?:.)/, /^(?:$)/ ];
          lexer.conditions = { "mu" : { "rules" : [ 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44 ], "inclusive" : false }, "emu" : { "rules" : [ 2 ], "inclusive" : false }, "com" : { "rules" : [ 6 ], "inclusive" : false }, "raw" : { "rules" : [ 3, 4, 5 ], "inclusive" : false }, "INITIAL" : { "rules" : [ 0, 1, 44 ], "inclusive" : true } };
          return lexer;
        })();
        parser.lexer = lexer;
        function Parser () {
          this.yy = {};
        }

        Parser.prototype = parser;
        parser.Parser = Parser;
        return new Parser();
      })();
      exports.__esModule = true;
      exports[ 'default' ] = handlebars;

      /***/
    },
              /* 24 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;

      var _visitor = __webpack_require__( 25 );

      var _visitor2 = _interopRequireDefault( _visitor );

      function WhitespaceControl () {
        var options = arguments.length <= 0 || arguments[ 0 ] === undefined ? {} : arguments[ 0 ];

        this.options = options;
      }

      WhitespaceControl.prototype = new _visitor2[ 'default' ]();

      WhitespaceControl.prototype.Program = function ( program ) {
        var doStandalone = !this.options.ignoreStandalone;

        var isRoot = !this.isRootSeen;
        this.isRootSeen = true;

        var body = program.body;
        for ( var i = 0, l = body.length; i < l; i++ ) {
          var current = body[ i ],
              strip   = this.accept( current );

          if ( !strip ) {
            continue;
          }

          var _isPrevWhitespace = isPrevWhitespace( body, i, isRoot ),
              _isNextWhitespace = isNextWhitespace( body, i, isRoot ),
              openStandalone    = strip.openStandalone && _isPrevWhitespace,
              closeStandalone   = strip.closeStandalone && _isNextWhitespace,
              inlineStandalone  = strip.inlineStandalone && _isPrevWhitespace && _isNextWhitespace;

          if ( strip.close ) {
            omitRight( body, i, true );
          }
          if ( strip.open ) {
            omitLeft( body, i, true );
          }

          if ( doStandalone && inlineStandalone ) {
            omitRight( body, i );

            if ( omitLeft( body, i ) ) {
              // If we are on a standalone node, save the indent info for partials
              if ( current.type === 'PartialStatement' ) {
                // Pull out the whitespace from the final line
                current.indent = /([ \t]+$)/.exec( body[ i - 1 ].original )[ 1 ];
              }
            }
          }
          if ( doStandalone && openStandalone ) {
            omitRight( (current.program || current.inverse).body );

            // Strip out the previous content node if it's whitespace only
            omitLeft( body, i );
          }
          if ( doStandalone && closeStandalone ) {
            // Always strip the next node
            omitRight( body, i );

            omitLeft( (current.inverse || current.program).body );
          }
        }

        return program;
      };

      WhitespaceControl.prototype.BlockStatement = WhitespaceControl.prototype.DecoratorBlock = WhitespaceControl.prototype.PartialBlockStatement = function ( block ) {
        this.accept( block.program );
        this.accept( block.inverse );

        // Find the inverse program that is involed with whitespace stripping.
        var program      = block.program || block.inverse,
            inverse      = block.program && block.inverse,
            firstInverse = inverse,
            lastInverse  = inverse;

        if ( inverse && inverse.chained ) {
          firstInverse = inverse.body[ 0 ].program;

          // Walk the inverse chain to find the last inverse that is actually in the chain.
          while ( lastInverse.chained ) {
            lastInverse = lastInverse.body[ lastInverse.body.length - 1 ].program;
          }
        }

        var strip = {
          open  : block.openStrip.open,
          close : block.closeStrip.close,

          // Determine the standalone candiacy. Basically flag our content as being possibly standalone
          // so our parent can determine if we actually are standalone
          openStandalone  : isNextWhitespace( program.body ),
          closeStandalone : isPrevWhitespace( (firstInverse || program).body )
        };

        if ( block.openStrip.close ) {
          omitRight( program.body, null, true );
        }

        if ( inverse ) {
          var inverseStrip = block.inverseStrip;

          if ( inverseStrip.open ) {
            omitLeft( program.body, null, true );
          }

          if ( inverseStrip.close ) {
            omitRight( firstInverse.body, null, true );
          }
          if ( block.closeStrip.open ) {
            omitLeft( lastInverse.body, null, true );
          }

          // Find standalone else statments
          if ( !this.options.ignoreStandalone && isPrevWhitespace( program.body ) && isNextWhitespace( firstInverse.body ) ) {
            omitLeft( program.body );
            omitRight( firstInverse.body );
          }
        } else if ( block.closeStrip.open ) {
          omitLeft( program.body, null, true );
        }

        return strip;
      };

      WhitespaceControl.prototype.Decorator = WhitespaceControl.prototype.MustacheStatement = function ( mustache ) {
        return mustache.strip;
      };

      WhitespaceControl.prototype.PartialStatement = WhitespaceControl.prototype.CommentStatement = function ( node ) {
        /* istanbul ignore next */
        var strip = node.strip || {};
        return {
          inlineStandalone : true,
          open             : strip.open,
          close            : strip.close
        };
      };

      function isPrevWhitespace ( body, i, isRoot ) {
        if ( i === undefined ) {
          i = body.length;
        }

        // Nodes that end with newlines are considered whitespace (but are special
        // cased for strip operations)
        var prev    = body[ i - 1 ],
            sibling = body[ i - 2 ];
        if ( !prev ) {
          return isRoot;
        }

        if ( prev.type === 'ContentStatement' ) {
          return (sibling || !isRoot ? /\r?\n\s*?$/ : /(^|\r?\n)\s*?$/).test( prev.original );
        }
      }

      function isNextWhitespace ( body, i, isRoot ) {
        if ( i === undefined ) {
          i = -1;
        }

        var next    = body[ i + 1 ],
            sibling = body[ i + 2 ];
        if ( !next ) {
          return isRoot;
        }

        if ( next.type === 'ContentStatement' ) {
          return (sibling || !isRoot ? /^\s*?\r?\n/ : /^\s*?(\r?\n|$)/).test( next.original );
        }
      }

      // Marks the node to the right of the position as omitted.
      // I.e. {{foo}}' ' will mark the ' ' node as omitted.
      //
      // If i is undefined, then the first child will be marked as such.
      //
      // If mulitple is truthy then all whitespace will be stripped out until non-whitespace
      // content is met.
      function omitRight ( body, i, multiple ) {
        var current = body[ i == null ? 0 : i + 1 ];
        if ( !current || current.type !== 'ContentStatement' || !multiple && current.rightStripped ) {
          return;
        }

        var original = current.value;
        current.value = current.value.replace( multiple ? /^\s+/ : /^[ \t]*\r?\n?/, '' );
        current.rightStripped = current.value !== original;
      }

      // Marks the node to the left of the position as omitted.
      // I.e. ' '{{foo}} will mark the ' ' node as omitted.
      //
      // If i is undefined then the last child will be marked as such.
      //
      // If mulitple is truthy then all whitespace will be stripped out until non-whitespace
      // content is met.
      function omitLeft ( body, i, multiple ) {
        var current = body[ i == null ? body.length - 1 : i - 1 ];
        if ( !current || current.type !== 'ContentStatement' || !multiple && current.leftStripped ) {
          return;
        }

        // We omit the last node if it's whitespace only and not preceeded by a non-content node.
        var original = current.value;
        current.value = current.value.replace( multiple ? /\s+$/ : /[ \t]+$/, '' );
        current.leftStripped = current.value !== original;
        return current.leftStripped;
      }

      exports[ 'default' ] = WhitespaceControl;
      module.exports = exports[ 'default' ];

      /***/
    },
              /* 25 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;

      var _exception = __webpack_require__( 6 );

      var _exception2 = _interopRequireDefault( _exception );

      function Visitor () {
        this.parents = [];
      }

      Visitor.prototype = {
        constructor : Visitor,
        mutating    : false,

        // Visits a given value. If mutating, will replace the value if necessary.
        acceptKey : function acceptKey ( node, name ) {
          var value = this.accept( node[ name ] );
          if ( this.mutating ) {
            // Hacky sanity check: This may have a few false positives for type for the helper
            // methods but will generally do the right thing without a lot of overhead.
            if ( value && !Visitor.prototype[ value.type ] ) {
              throw new _exception2[ 'default' ]( 'Unexpected node type "' + value.type + '" found when accepting ' + name + ' on ' + node.type );
            }
            node[ name ] = value;
          }
        },

        // Performs an accept operation with added sanity check to ensure
        // required keys are not removed.
        acceptRequired : function acceptRequired ( node, name ) {
          this.acceptKey( node, name );

          if ( !node[ name ] ) {
            throw new _exception2[ 'default' ]( node.type + ' requires ' + name );
          }
        },

        // Traverses a given array. If mutating, empty respnses will be removed
        // for child elements.
        acceptArray : function acceptArray ( array ) {
          for ( var i = 0, l = array.length; i < l; i++ ) {
            this.acceptKey( array, i );

            if ( !array[ i ] ) {
              array.splice( i, 1 );
              i--;
              l--;
            }
          }
        },

        accept : function accept ( object ) {
          if ( !object ) {
            return;
          }

          /* istanbul ignore next: Sanity code */
          if ( !this[ object.type ] ) {
            throw new _exception2[ 'default' ]( 'Unknown type: ' + object.type, object );
          }

          if ( this.current ) {
            this.parents.unshift( this.current );
          }
          this.current = object;

          var ret = this[ object.type ]( object );

          this.current = this.parents.shift();

          if ( !this.mutating || ret ) {
            return ret;
          } else if ( ret !== false ) {
            return object;
          }
        },

        Program : function Program ( program ) {
          this.acceptArray( program.body );
        },

        MustacheStatement : visitSubExpression,
        Decorator         : visitSubExpression,

        BlockStatement : visitBlock,
        DecoratorBlock : visitBlock,

        PartialStatement      : visitPartial,
        PartialBlockStatement : function PartialBlockStatement ( partial ) {
          visitPartial.call( this, partial );

          this.acceptKey( partial, 'program' );
        },

        ContentStatement : function ContentStatement () /* content */ {},
        CommentStatement : function CommentStatement () /* comment */ {},

        SubExpression : visitSubExpression,

        PathExpression : function PathExpression () /* path */ {},

        StringLiteral    : function StringLiteral () /* string */ {},
        NumberLiteral    : function NumberLiteral () /* number */ {},
        BooleanLiteral   : function BooleanLiteral () /* bool */ {},
        UndefinedLiteral : function UndefinedLiteral () /* literal */ {},
        NullLiteral      : function NullLiteral () /* literal */ {},

        Hash     : function Hash ( hash ) {
          this.acceptArray( hash.pairs );
        },
        HashPair : function HashPair ( pair ) {
          this.acceptRequired( pair, 'value' );
        }
      };

      function visitSubExpression ( mustache ) {
        this.acceptRequired( mustache, 'path' );
        this.acceptArray( mustache.params );
        this.acceptKey( mustache, 'hash' );
      }

      function visitBlock ( block ) {
        visitSubExpression.call( this, block );

        this.acceptKey( block, 'program' );
        this.acceptKey( block, 'inverse' );
      }

      function visitPartial ( partial ) {
        this.acceptRequired( partial, 'name' );
        this.acceptArray( partial.params );
        this.acceptKey( partial, 'hash' );
      }

      exports[ 'default' ] = Visitor;
      module.exports = exports[ 'default' ];

      /***/
    },
              /* 26 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;
      exports.SourceLocation = SourceLocation;
      exports.id = id;
      exports.stripFlags = stripFlags;
      exports.stripComment = stripComment;
      exports.preparePath = preparePath;
      exports.prepareMustache = prepareMustache;
      exports.prepareRawBlock = prepareRawBlock;
      exports.prepareBlock = prepareBlock;
      exports.prepareProgram = prepareProgram;
      exports.preparePartialBlock = preparePartialBlock;

      var _exception = __webpack_require__( 6 );

      var _exception2 = _interopRequireDefault( _exception );

      function validateClose ( open, close ) {
        close = close.path ? close.path.original : close;

        if ( open.path.original !== close ) {
          var errorNode = { loc : open.path.loc };

          throw new _exception2[ 'default' ]( open.path.original + " doesn't match " + close, errorNode );
        }
      }

      function SourceLocation ( source, locInfo ) {
        this.source = source;
        this.start = {
          line   : locInfo.first_line,
          column : locInfo.first_column
        };
        this.end = {
          line   : locInfo.last_line,
          column : locInfo.last_column
        };
      }

      function id ( token ) {
        if ( /^\[.*\]$/.test( token ) ) {
          return token.substr( 1, token.length - 2 );
        } else {
          return token;
        }
      }

      function stripFlags ( open, close ) {
        return {
          open  : open.charAt( 2 ) === '~',
          close : close.charAt( close.length - 3 ) === '~'
        };
      }

      function stripComment ( comment ) {
        return comment.replace( /^\{\{~?\!-?-?/, '' ).replace( /-?-?~?\}\}$/, '' );
      }

      function preparePath ( data, parts, loc ) {
        loc = this.locInfo( loc );

        var original    = data ? '@' : '',
            dig         = [],
            depth       = 0,
            depthString = '';

        for ( var i = 0, l = parts.length; i < l; i++ ) {
          var part      = parts[ i ].part,

              // If we have [] syntax then we do not treat path references as operators,
              // i.e. foo.[this] resolves to approximately context.foo['this']
              isLiteral = parts[ i ].original !== part;
          original += (parts[ i ].separator || '') + part;

          if ( !isLiteral && (part === '..' || part === '.' || part === 'this') ) {
            if ( dig.length > 0 ) {
              throw new _exception2[ 'default' ]( 'Invalid path: ' + original, { loc : loc } );
            } else if ( part === '..' ) {
              depth++;
              depthString += '../';
            }
          } else {
            dig.push( part );
          }
        }

        return {
          type     : 'PathExpression',
          data     : data,
          depth    : depth,
          parts    : dig,
          original : original,
          loc      : loc
        };
      }

      function prepareMustache ( path, params, hash, open, strip, locInfo ) {
        // Must use charAt to support IE pre-10
        var escapeFlag = open.charAt( 3 ) || open.charAt( 2 ),
            escaped    = escapeFlag !== '{' && escapeFlag !== '&';

        var decorator = /\*/.test( open );
        return {
          type    : decorator ? 'Decorator' : 'MustacheStatement',
          path    : path,
          params  : params,
          hash    : hash,
          escaped : escaped,
          strip   : strip,
          loc     : this.locInfo( locInfo )
        };
      }

      function prepareRawBlock ( openRawBlock, contents, close, locInfo ) {
        validateClose( openRawBlock, close );

        locInfo = this.locInfo( locInfo );
        var program = {
          type  : 'Program',
          body  : contents,
          strip : {},
          loc   : locInfo
        };

        return {
          type         : 'BlockStatement',
          path         : openRawBlock.path,
          params       : openRawBlock.params,
          hash         : openRawBlock.hash,
          program      : program,
          openStrip    : {},
          inverseStrip : {},
          closeStrip   : {},
          loc          : locInfo
        };
      }

      function prepareBlock ( openBlock, program, inverseAndProgram, close, inverted, locInfo ) {
        if ( close && close.path ) {
          validateClose( openBlock, close );
        }

        var decorator = /\*/.test( openBlock.open );

        program.blockParams = openBlock.blockParams;

        var inverse      = undefined,
            inverseStrip = undefined;

        if ( inverseAndProgram ) {
          if ( decorator ) {
            throw new _exception2[ 'default' ]( 'Unexpected inverse block on decorator', inverseAndProgram );
          }

          if ( inverseAndProgram.chain ) {
            inverseAndProgram.program.body[ 0 ].closeStrip = close.strip;
          }

          inverseStrip = inverseAndProgram.strip;
          inverse = inverseAndProgram.program;
        }

        if ( inverted ) {
          inverted = inverse;
          inverse = program;
          program = inverted;
        }

        return {
          type         : decorator ? 'DecoratorBlock' : 'BlockStatement',
          path         : openBlock.path,
          params       : openBlock.params,
          hash         : openBlock.hash,
          program      : program,
          inverse      : inverse,
          openStrip    : openBlock.strip,
          inverseStrip : inverseStrip,
          closeStrip   : close && close.strip,
          loc          : this.locInfo( locInfo )
        };
      }

      function prepareProgram ( statements, loc ) {
        if ( !loc && statements.length ) {
          var firstLoc = statements[ 0 ].loc,
              lastLoc  = statements[ statements.length - 1 ].loc;

          /* istanbul ignore else */
          if ( firstLoc && lastLoc ) {
            loc = {
              source : firstLoc.source,
              start  : {
                line   : firstLoc.start.line,
                column : firstLoc.start.column
              },
              end    : {
                line   : lastLoc.end.line,
                column : lastLoc.end.column
              }
            };
          }
        }

        return {
          type  : 'Program',
          body  : statements,
          strip : {},
          loc   : loc
        };
      }

      function preparePartialBlock ( open, program, close, locInfo ) {
        validateClose( open, close );

        return {
          type       : 'PartialBlockStatement',
          name       : open.path,
          params     : open.params,
          hash       : open.hash,
          program    : program,
          openStrip  : open.strip,
          closeStrip : close && close.strip,
          loc        : this.locInfo( locInfo )
        };
      }

      /***/
    },
              /* 27 */
              /***/ function ( module, exports, __webpack_require__ ) {

      /* eslint-disable new-cap */

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;
      exports.Compiler = Compiler;
      exports.precompile = precompile;
      exports.compile = compile;

      var _exception = __webpack_require__( 6 );

      var _exception2 = _interopRequireDefault( _exception );

      var _utils = __webpack_require__( 5 );

      var _ast = __webpack_require__( 21 );

      var _ast2 = _interopRequireDefault( _ast );

      var slice = [].slice;

      function Compiler () {}

      // the foundHelper register will disambiguate helper lookup from finding a
      // function in a context. This is necessary for mustache compatibility, which
      // requires that context functions in blocks are evaluated by blockHelperMissing,
      // and then proceed as if the resulting value was provided to blockHelperMissing.

      Compiler.prototype = {
        compiler : Compiler,

        equals : function equals ( other ) {
          var len = this.opcodes.length;
          if ( other.opcodes.length !== len ) {
            return false;
          }

          for ( var i = 0; i < len; i++ ) {
            var opcode      = this.opcodes[ i ],
                otherOpcode = other.opcodes[ i ];
            if ( opcode.opcode !== otherOpcode.opcode || !argEquals( opcode.args, otherOpcode.args ) ) {
              return false;
            }
          }

          // We know that length is the same between the two arrays because they are directly tied
          // to the opcode behavior above.
          len = this.children.length;
          for ( var i = 0; i < len; i++ ) {
            if ( !this.children[ i ].equals( other.children[ i ] ) ) {
              return false;
            }
          }

          return true;
        },

        guid : 0,

        compile : function compile ( program, options ) {
          this.sourceNode = [];
          this.opcodes = [];
          this.children = [];
          this.options = options;
          this.stringParams = options.stringParams;
          this.trackIds = options.trackIds;

          options.blockParams = options.blockParams || [];

          // These changes will propagate to the other compiler components
          var knownHelpers = options.knownHelpers;
          options.knownHelpers = {
            'helperMissing'      : true,
            'blockHelperMissing' : true,
            'each'               : true,
            'if'                 : true,
            'unless'             : true,
            'with'               : true,
            'log'                : true,
            'lookup'             : true
          };
          if ( knownHelpers ) {
            for ( var _name in knownHelpers ) {
              /* istanbul ignore else */
              if ( _name in knownHelpers ) {
                options.knownHelpers[ _name ] = knownHelpers[ _name ];
              }
            }
          }

          return this.accept( program );
        },

        compileProgram : function compileProgram ( program ) {
          var childCompiler = new this.compiler(),
              // eslint-disable-line new-cap
              result        = childCompiler.compile( program, this.options ),
              guid          = this.guid++;

          this.usePartial = this.usePartial || result.usePartial;

          this.children[ guid ] = result;
          this.useDepths = this.useDepths || result.useDepths;

          return guid;
        },

        accept : function accept ( node ) {
          /* istanbul ignore next: Sanity code */
          if ( !this[ node.type ] ) {
            throw new _exception2[ 'default' ]( 'Unknown type: ' + node.type, node );
          }

          this.sourceNode.unshift( node );
          var ret = this[ node.type ]( node );
          this.sourceNode.shift();
          return ret;
        },

        Program : function Program ( program ) {
          this.options.blockParams.unshift( program.blockParams );

          var body       = program.body,
              bodyLength = body.length;
          for ( var i = 0; i < bodyLength; i++ ) {
            this.accept( body[ i ] );
          }

          this.options.blockParams.shift();

          this.isSimple = bodyLength === 1;
          this.blockParams = program.blockParams ? program.blockParams.length : 0;

          return this;
        },

        BlockStatement : function BlockStatement ( block ) {
          transformLiteralToPath( block );

          var program = block.program,
              inverse = block.inverse;

          program = program && this.compileProgram( program );
          inverse = inverse && this.compileProgram( inverse );

          var type = this.classifySexpr( block );

          if ( type === 'helper' ) {
            this.helperSexpr( block, program, inverse );
          } else if ( type === 'simple' ) {
            this.simpleSexpr( block );

            // now that the simple mustache is resolved, we need to
            // evaluate it by executing `blockHelperMissing`
            this.opcode( 'pushProgram', program );
            this.opcode( 'pushProgram', inverse );
            this.opcode( 'emptyHash' );
            this.opcode( 'blockValue', block.path.original );
          } else {
            this.ambiguousSexpr( block, program, inverse );

            // now that the simple mustache is resolved, we need to
            // evaluate it by executing `blockHelperMissing`
            this.opcode( 'pushProgram', program );
            this.opcode( 'pushProgram', inverse );
            this.opcode( 'emptyHash' );
            this.opcode( 'ambiguousBlockValue' );
          }

          this.opcode( 'append' );
        },

        DecoratorBlock : function DecoratorBlock ( decorator ) {
          var program = decorator.program && this.compileProgram( decorator.program );
          var params = this.setupFullMustacheParams( decorator, program, undefined ),
              path   = decorator.path;

          this.useDecorators = true;
          this.opcode( 'registerDecorator', params.length, path.original );
        },

        PartialStatement      : function PartialStatement ( partial ) {
          this.usePartial = true;

          var program = partial.program;
          if ( program ) {
            program = this.compileProgram( partial.program );
          }

          var params = partial.params;
          if ( params.length > 1 ) {
            throw new _exception2[ 'default' ]( 'Unsupported number of partial arguments: ' + params.length, partial );
          } else if ( !params.length ) {
            if ( this.options.explicitPartialContext ) {
              this.opcode( 'pushLiteral', 'undefined' );
            } else {
              params.push( { type : 'PathExpression', parts : [], depth : 0 } );
            }
          }

          var partialName = partial.name.original,
              isDynamic   = partial.name.type === 'SubExpression';
          if ( isDynamic ) {
            this.accept( partial.name );
          }

          this.setupFullMustacheParams( partial, program, undefined, true );

          var indent = partial.indent || '';
          if ( this.options.preventIndent && indent ) {
            this.opcode( 'appendContent', indent );
            indent = '';
          }

          this.opcode( 'invokePartial', isDynamic, partialName, indent );
          this.opcode( 'append' );
        },
        PartialBlockStatement : function PartialBlockStatement ( partialBlock ) {
          this.PartialStatement( partialBlock );
        },

        MustacheStatement : function MustacheStatement ( mustache ) {
          this.SubExpression( mustache );

          if ( mustache.escaped && !this.options.noEscape ) {
            this.opcode( 'appendEscaped' );
          } else {
            this.opcode( 'append' );
          }
        },
        Decorator         : function Decorator ( decorator ) {
          this.DecoratorBlock( decorator );
        },

        ContentStatement : function ContentStatement ( content ) {
          if ( content.value ) {
            this.opcode( 'appendContent', content.value );
          }
        },

        CommentStatement : function CommentStatement () {},

        SubExpression  : function SubExpression ( sexpr ) {
          transformLiteralToPath( sexpr );
          var type = this.classifySexpr( sexpr );

          if ( type === 'simple' ) {
            this.simpleSexpr( sexpr );
          } else if ( type === 'helper' ) {
            this.helperSexpr( sexpr );
          } else {
            this.ambiguousSexpr( sexpr );
          }
        },
        ambiguousSexpr : function ambiguousSexpr ( sexpr, program, inverse ) {
          var path    = sexpr.path,
              name    = path.parts[ 0 ],
              isBlock = program != null || inverse != null;

          this.opcode( 'getContext', path.depth );

          this.opcode( 'pushProgram', program );
          this.opcode( 'pushProgram', inverse );

          path.strict = true;
          this.accept( path );

          this.opcode( 'invokeAmbiguous', name, isBlock );
        },

        simpleSexpr : function simpleSexpr ( sexpr ) {
          var path = sexpr.path;
          path.strict = true;
          this.accept( path );
          this.opcode( 'resolvePossibleLambda' );
        },

        helperSexpr : function helperSexpr ( sexpr, program, inverse ) {
          var params = this.setupFullMustacheParams( sexpr, program, inverse ),
              path   = sexpr.path,
              name   = path.parts[ 0 ];

          if ( this.options.knownHelpers[ name ] ) {
            this.opcode( 'invokeKnownHelper', params.length, name );
          } else if ( this.options.knownHelpersOnly ) {
            throw new _exception2[ 'default' ]( 'You specified knownHelpersOnly, but used the unknown helper ' + name, sexpr );
          } else {
            path.strict = true;
            path.falsy = true;

            this.accept( path );
            this.opcode( 'invokeHelper', params.length, path.original, _ast2[ 'default' ].helpers.simpleId( path ) );
          }
        },

        PathExpression : function PathExpression ( path ) {
          this.addDepth( path.depth );
          this.opcode( 'getContext', path.depth );

          var name         = path.parts[ 0 ],
              scoped       = _ast2[ 'default' ].helpers.scopedId( path ),
              blockParamId = !path.depth && !scoped && this.blockParamIndex( name );

          if ( blockParamId ) {
            this.opcode( 'lookupBlockParam', blockParamId, path.parts );
          } else if ( !name ) {
            // Context reference, i.e. `{{foo .}}` or `{{foo ..}}`
            this.opcode( 'pushContext' );
          } else if ( path.data ) {
            this.options.data = true;
            this.opcode( 'lookupData', path.depth, path.parts, path.strict );
          } else {
            this.opcode( 'lookupOnContext', path.parts, path.falsy, path.strict, scoped );
          }
        },

        StringLiteral : function StringLiteral ( string ) {
          this.opcode( 'pushString', string.value );
        },

        NumberLiteral : function NumberLiteral ( number ) {
          this.opcode( 'pushLiteral', number.value );
        },

        BooleanLiteral : function BooleanLiteral ( bool ) {
          this.opcode( 'pushLiteral', bool.value );
        },

        UndefinedLiteral : function UndefinedLiteral () {
          this.opcode( 'pushLiteral', 'undefined' );
        },

        NullLiteral : function NullLiteral () {
          this.opcode( 'pushLiteral', 'null' );
        },

        Hash : function Hash ( hash ) {
          var pairs = hash.pairs,
              i     = 0,
              l     = pairs.length;

          this.opcode( 'pushHash' );

          for ( ; i < l; i++ ) {
            this.pushParam( pairs[ i ].value );
          }
          while ( i-- ) {
            this.opcode( 'assignToHash', pairs[ i ].key );
          }
          this.opcode( 'popHash' );
        },

        // HELPERS
        opcode : function opcode ( name ) {
          this.opcodes.push( { opcode : name, args : slice.call( arguments, 1 ), loc : this.sourceNode[ 0 ].loc } );
        },

        addDepth : function addDepth ( depth ) {
          if ( !depth ) {
            return;
          }

          this.useDepths = true;
        },

        classifySexpr : function classifySexpr ( sexpr ) {
          var isSimple = _ast2[ 'default' ].helpers.simpleId( sexpr.path );

          var isBlockParam = isSimple && !!this.blockParamIndex( sexpr.path.parts[ 0 ] );

          // a mustache is an eligible helper if:
          // * its id is simple (a single part, not `this` or `..`)
          var isHelper = !isBlockParam && _ast2[ 'default' ].helpers.helperExpression( sexpr );

          // if a mustache is an eligible helper but not a definite
          // helper, it is ambiguous, and will be resolved in a later
          // pass or at runtime.
          var isEligible = !isBlockParam && (isHelper || isSimple);

          // if ambiguous, we can possibly resolve the ambiguity now
          // An eligible helper is one that does not have a complex path, i.e. `this.foo`, `../foo` etc.
          if ( isEligible && !isHelper ) {
            var _name2  = sexpr.path.parts[ 0 ],
                options = this.options;

            if ( options.knownHelpers[ _name2 ] ) {
              isHelper = true;
            } else if ( options.knownHelpersOnly ) {
              isEligible = false;
            }
          }

          if ( isHelper ) {
            return 'helper';
          } else if ( isEligible ) {
            return 'ambiguous';
          } else {
            return 'simple';
          }
        },

        pushParams : function pushParams ( params ) {
          for ( var i = 0, l = params.length; i < l; i++ ) {
            this.pushParam( params[ i ] );
          }
        },

        pushParam : function pushParam ( val ) {
          var value = val.value != null ? val.value : val.original || '';

          if ( this.stringParams ) {
            if ( value.replace ) {
              value = value.replace( /^(\.?\.\/)*/g, '' ).replace( /\//g, '.' );
            }

            if ( val.depth ) {
              this.addDepth( val.depth );
            }
            this.opcode( 'getContext', val.depth || 0 );
            this.opcode( 'pushStringParam', value, val.type );

            if ( val.type === 'SubExpression' ) {
              // SubExpressions get evaluated and passed in
              // in string params mode.
              this.accept( val );
            }
          } else {
            if ( this.trackIds ) {
              var blockParamIndex = undefined;
              if ( val.parts && !_ast2[ 'default' ].helpers.scopedId( val ) && !val.depth ) {
                blockParamIndex = this.blockParamIndex( val.parts[ 0 ] );
              }
              if ( blockParamIndex ) {
                var blockParamChild = val.parts.slice( 1 ).join( '.' );
                this.opcode( 'pushId', 'BlockParam', blockParamIndex, blockParamChild );
              } else {
                value = val.original || value;
                if ( value.replace ) {
                  value = value.replace( /^this(?:\.|$)/, '' ).replace( /^\.\//, '' ).replace( /^\.$/, '' );
                }

                this.opcode( 'pushId', val.type, value );
              }
            }
            this.accept( val );
          }
        },

        setupFullMustacheParams : function setupFullMustacheParams ( sexpr, program, inverse, omitEmpty ) {
          var params = sexpr.params;
          this.pushParams( params );

          this.opcode( 'pushProgram', program );
          this.opcode( 'pushProgram', inverse );

          if ( sexpr.hash ) {
            this.accept( sexpr.hash );
          } else {
            this.opcode( 'emptyHash', omitEmpty );
          }

          return params;
        },

        blockParamIndex : function blockParamIndex ( name ) {
          for ( var depth = 0, len = this.options.blockParams.length; depth < len; depth++ ) {
            var blockParams = this.options.blockParams[ depth ],
                param       = blockParams && _utils.indexOf( blockParams, name );
            if ( blockParams && param >= 0 ) {
              return [ depth, param ];
            }
          }
        }
      };

      function precompile ( input, options, env ) {
        if ( input == null || typeof input !== 'string' && input.type !== 'Program' ) {
          throw new _exception2[ 'default' ]( 'You must pass a string or Handlebars AST to Handlebars.precompile. You passed ' + input );
        }

        options = options || {};
        if ( !('data' in options) ) {
          options.data = true;
        }
        if ( options.compat ) {
          options.useDepths = true;
        }

        var ast         = env.parse( input, options ),
            environment = new env.Compiler().compile( ast, options );
        return new env.JavaScriptCompiler().compile( environment, options );
      }

      function compile ( input, options, env ) {
        if ( options === undefined ) options = {};

        if ( input == null || typeof input !== 'string' && input.type !== 'Program' ) {
          throw new _exception2[ 'default' ]( 'You must pass a string or Handlebars AST to Handlebars.compile. You passed ' + input );
        }

        if ( !('data' in options) ) {
          options.data = true;
        }
        if ( options.compat ) {
          options.useDepths = true;
        }

        var compiled = undefined;

        function compileInput () {
          var ast          = env.parse( input, options ),
              environment  = new env.Compiler().compile( ast, options ),
              templateSpec = new env.JavaScriptCompiler().compile( environment, options, undefined, true );
          return env.template( templateSpec );
        }

        // Template is only compiled on first use and cached after that point.
        function ret ( context, execOptions ) {
          if ( !compiled ) {
            compiled = compileInput();
          }
          return compiled.call( this, context, execOptions );
        }

        ret._setup = function ( setupOptions ) {
          if ( !compiled ) {
            compiled = compileInput();
          }
          return compiled._setup( setupOptions );
        };
        ret._child = function ( i, data, blockParams, depths ) {
          if ( !compiled ) {
            compiled = compileInput();
          }
          return compiled._child( i, data, blockParams, depths );
        };
        return ret;
      }

      function argEquals ( a, b ) {
        if ( a === b ) {
          return true;
        }

        if ( _utils.isArray( a ) && _utils.isArray( b ) && a.length === b.length ) {
          for ( var i = 0; i < a.length; i++ ) {
            if ( !argEquals( a[ i ], b[ i ] ) ) {
              return false;
            }
          }
          return true;
        }
      }

      function transformLiteralToPath ( sexpr ) {
        if ( !sexpr.path.parts ) {
          var literal = sexpr.path;
          // Casting to string here to make false and 0 literal values play nicely with the rest
          // of the system.
          sexpr.path = {
            type     : 'PathExpression',
            data     : false,
            depth    : 0,
            parts    : [ literal.original + '' ],
            original : literal.original + '',
            loc      : literal.loc
          };
        }
      }

      /***/
    },
              /* 28 */
              /***/ function ( module, exports, __webpack_require__ ) {

      'use strict';

      var _interopRequireDefault = __webpack_require__( 1 )[ 'default' ];

      exports.__esModule = true;

      var _base = __webpack_require__( 4 );

      var _exception = __webpack_require__( 6 );

      var _exception2 = _interopRequireDefault( _exception );

      var _utils = __webpack_require__( 5 );

      var _codeGen = __webpack_require__( 29 );

      var _codeGen2 = _interopRequireDefault( _codeGen );

      function Literal ( value ) {
        this.value = value;
      }

      function JavaScriptCompiler () {}

      JavaScriptCompiler.prototype = {
        // PUBLIC API: You can override these methods in a subclass to provide
        // alternative compiled forms for name lookup and buffering semantics
        nameLookup    : function nameLookup ( parent, name /* , type*/ ) {
          if ( JavaScriptCompiler.isValidJavaScriptVariableName( name ) ) {
            return [ parent, '.', name ];
          } else {
            return [ parent, '[', JSON.stringify( name ), ']' ];
          }
        },
        depthedLookup : function depthedLookup ( name ) {
          return [ this.aliasable( 'container.lookup' ), '(depths, "', name, '")' ];
        },

        compilerInfo : function compilerInfo () {
          var revision = _base.COMPILER_REVISION,
              versions = _base.REVISION_CHANGES[ revision ];
          return [ revision, versions ];
        },

        appendToBuffer : function appendToBuffer ( source, location, explicit ) {
          // Force a source as this simplifies the merge logic.
          if ( !_utils.isArray( source ) ) {
            source = [ source ];
          }
          source = this.source.wrap( source, location );

          if ( this.environment.isSimple ) {
            return [ 'return ', source, ';' ];
          } else if ( explicit ) {
            // This is a case where the buffer operation occurs as a child of another
            // construct, generally braces. We have to explicitly output these buffer
            // operations to ensure that the emitted code goes in the correct location.
            return [ 'buffer += ', source, ';' ];
          } else {
            source.appendToBuffer = true;
            return source;
          }
        },

        initializeBuffer : function initializeBuffer () {
          return this.quotedString( '' );
        },
        // END PUBLIC API

        compile : function compile ( environment, options, context, asObject ) {
          this.environment = environment;
          this.options = options;
          this.stringParams = this.options.stringParams;
          this.trackIds = this.options.trackIds;
          this.precompile = !asObject;

          this.name = this.environment.name;
          this.isChild = !!context;
          this.context = context || {
              decorators   : [],
              programs     : [],
              environments : []
            };

          this.preamble();

          this.stackSlot = 0;
          this.stackVars = [];
          this.aliases = {};
          this.registers = { list : [] };
          this.hashes = [];
          this.compileStack = [];
          this.inlineStack = [];
          this.blockParams = [];

          this.compileChildren( environment, options );

          this.useDepths = this.useDepths || environment.useDepths || environment.useDecorators || this.options.compat;
          this.useBlockParams = this.useBlockParams || environment.useBlockParams;

          var opcodes  = environment.opcodes,
              opcode   = undefined,
              firstLoc = undefined,
              i        = undefined,
              l        = undefined;

          for ( i = 0, l = opcodes.length; i < l; i++ ) {
            opcode = opcodes[ i ];

            this.source.currentLocation = opcode.loc;
            firstLoc = firstLoc || opcode.loc;
            this[ opcode.opcode ].apply( this, opcode.args );
          }

          // Flush any trailing content that might be pending.
          this.source.currentLocation = firstLoc;
          this.pushSource( '' );

          /* istanbul ignore next */
          if ( this.stackSlot || this.inlineStack.length || this.compileStack.length ) {
            throw new _exception2[ 'default' ]( 'Compile completed with content left on stack' );
          }

          if ( !this.decorators.isEmpty() ) {
            this.useDecorators = true;

            this.decorators.prepend( 'var decorators = container.decorators;\n' );
            this.decorators.push( 'return fn;' );

            if ( asObject ) {
              this.decorators = Function.apply( this, [ 'fn', 'props', 'container', 'depth0', 'data', 'blockParams', 'depths', this.decorators.merge() ] );
            } else {
              this.decorators.prepend( 'function(fn, props, container, depth0, data, blockParams, depths) {\n' );
              this.decorators.push( '}\n' );
              this.decorators = this.decorators.merge();
            }
          } else {
            this.decorators = undefined;
          }

          var fn = this.createFunctionContext( asObject );
          if ( !this.isChild ) {
            var ret = {
              compiler : this.compilerInfo(),
              main     : fn
            };

            if ( this.decorators ) {
              ret.main_d = this.decorators; // eslint-disable-line camelcase
              ret.useDecorators = true;
            }

            var _context = this.context;
            var programs = _context.programs;
            var decorators = _context.decorators;

            for ( i = 0, l = programs.length; i < l; i++ ) {
              if ( programs[ i ] ) {
                ret[ i ] = programs[ i ];
                if ( decorators[ i ] ) {
                  ret[ i + '_d' ] = decorators[ i ];
                  ret.useDecorators = true;
                }
              }
            }

            if ( this.environment.usePartial ) {
              ret.usePartial = true;
            }
            if ( this.options.data ) {
              ret.useData = true;
            }
            if ( this.useDepths ) {
              ret.useDepths = true;
            }
            if ( this.useBlockParams ) {
              ret.useBlockParams = true;
            }
            if ( this.options.compat ) {
              ret.compat = true;
            }

            if ( !asObject ) {
              ret.compiler = JSON.stringify( ret.compiler );

              this.source.currentLocation = { start : { line : 1, column : 0 } };
              ret = this.objectLiteral( ret );

              if ( options.srcName ) {
                ret = ret.toStringWithSourceMap( { file : options.destName } );
                ret.map = ret.map && ret.map.toString();
              } else {
                ret = ret.toString();
              }
            } else {
              ret.compilerOptions = this.options;
            }

            return ret;
          } else {
            return fn;
          }
        },

        preamble : function preamble () {
          // track the last context pushed into place to allow skipping the
          // getContext opcode when it would be a noop
          this.lastContext = 0;
          this.source = new _codeGen2[ 'default' ]( this.options.srcName );
          this.decorators = new _codeGen2[ 'default' ]( this.options.srcName );
        },

        createFunctionContext : function createFunctionContext ( asObject ) {
          var varDeclarations = '';

          var locals = this.stackVars.concat( this.registers.list );
          if ( locals.length > 0 ) {
            varDeclarations += ', ' + locals.join( ', ' );
          }

          // Generate minimizer alias mappings
          //
          // When using true SourceNodes, this will update all references to the given alias
          // as the source nodes are reused in situ. For the non-source node compilation mode,
          // aliases will not be used, but this case is already being run on the client and
          // we aren't concern about minimizing the template size.
          var aliasCount = 0;
          for ( var alias in this.aliases ) {
            // eslint-disable-line guard-for-in
            var node = this.aliases[ alias ];

            if ( this.aliases.hasOwnProperty( alias ) && node.children && node.referenceCount > 1 ) {
              varDeclarations += ', alias' + ++aliasCount + '=' + alias;
              node.children[ 0 ] = 'alias' + aliasCount;
            }
          }

          var params = [ 'container', 'depth0', 'helpers', 'partials', 'data' ];

          if ( this.useBlockParams || this.useDepths ) {
            params.push( 'blockParams' );
          }
          if ( this.useDepths ) {
            params.push( 'depths' );
          }

          // Perform a second pass over the output to merge content when possible
          var source = this.mergeSource( varDeclarations );

          if ( asObject ) {
            params.push( source );

            return Function.apply( this, params );
          } else {
            return this.source.wrap( [ 'function(', params.join( ',' ), ') {\n  ', source, '}' ] );
          }
        },
        mergeSource           : function mergeSource ( varDeclarations ) {
          var isSimple    = this.environment.isSimple,
              appendOnly  = !this.forceBuffer,
              appendFirst = undefined,
              sourceSeen  = undefined,
              bufferStart = undefined,
              bufferEnd   = undefined;
          this.source.each( function ( line ) {
            if ( line.appendToBuffer ) {
              if ( bufferStart ) {
                line.prepend( '  + ' );
              } else {
                bufferStart = line;
              }
              bufferEnd = line;
            } else {
              if ( bufferStart ) {
                if ( !sourceSeen ) {
                  appendFirst = true;
                } else {
                  bufferStart.prepend( 'buffer += ' );
                }
                bufferEnd.add( ';' );
                bufferStart = bufferEnd = undefined;
              }

              sourceSeen = true;
              if ( !isSimple ) {
                appendOnly = false;
              }
            }
          } );

          if ( appendOnly ) {
            if ( bufferStart ) {
              bufferStart.prepend( 'return ' );
              bufferEnd.add( ';' );
            } else if ( !sourceSeen ) {
              this.source.push( 'return "";' );
            }
          } else {
            varDeclarations += ', buffer = ' + (appendFirst ? '' : this.initializeBuffer());

            if ( bufferStart ) {
              bufferStart.prepend( 'return buffer + ' );
              bufferEnd.add( ';' );
            } else {
              this.source.push( 'return buffer;' );
            }
          }

          if ( varDeclarations ) {
            this.source.prepend( 'var ' + varDeclarations.substring( 2 ) + (appendFirst ? '' : ';\n') );
          }

          return this.source.merge();
        },

        // [blockValue]
        //
        // On stack, before: hash, inverse, program, value
        // On stack, after: return value of blockHelperMissing
        //
        // The purpose of this opcode is to take a block of the form
        // `{{#this.foo}}...{{/this.foo}}`, resolve the value of `foo`, and
        // replace it on the stack with the result of properly
        // invoking blockHelperMissing.
        blockValue : function blockValue ( name ) {
          var blockHelperMissing = this.aliasable( 'helpers.blockHelperMissing' ),
              params             = [ this.contextName( 0 ) ];
          this.setupHelperArgs( name, 0, params );

          var blockName = this.popStack();
          params.splice( 1, 0, blockName );

          this.push( this.source.functionCall( blockHelperMissing, 'call', params ) );
        },

        // [ambiguousBlockValue]
        //
        // On stack, before: hash, inverse, program, value
        // Compiler value, before: lastHelper=value of last found helper, if any
        // On stack, after, if no lastHelper: same as [blockValue]
        // On stack, after, if lastHelper: value
        ambiguousBlockValue : function ambiguousBlockValue () {
          // We're being a bit cheeky and reusing the options value from the prior exec
          var blockHelperMissing = this.aliasable( 'helpers.blockHelperMissing' ),
              params             = [ this.contextName( 0 ) ];
          this.setupHelperArgs( '', 0, params, true );

          this.flushInline();

          var current = this.topStack();
          params.splice( 1, 0, current );

          this.pushSource( [ 'if (!', this.lastHelper, ') { ', current, ' = ', this.source.functionCall( blockHelperMissing, 'call', params ), '}' ] );
        },

        // [appendContent]
        //
        // On stack, before: ...
        // On stack, after: ...
        //
        // Appends the string value of `content` to the current buffer
        appendContent : function appendContent ( content ) {
          if ( this.pendingContent ) {
            content = this.pendingContent + content;
          } else {
            this.pendingLocation = this.source.currentLocation;
          }

          this.pendingContent = content;
        },

        // [append]
        //
        // On stack, before: value, ...
        // On stack, after: ...
        //
        // Coerces `value` to a String and appends it to the current buffer.
        //
        // If `value` is truthy, or 0, it is coerced into a string and appended
        // Otherwise, the empty string is appended
        append : function append () {
          if ( this.isInline() ) {
            this.replaceStack( function ( current ) {
              return [ ' != null ? ', current, ' : ""' ];
            } );

            this.pushSource( this.appendToBuffer( this.popStack() ) );
          } else {
            var local = this.popStack();
            this.pushSource( [ 'if (', local, ' != null) { ', this.appendToBuffer( local, undefined, true ), ' }' ] );
            if ( this.environment.isSimple ) {
              this.pushSource( [ 'else { ', this.appendToBuffer( "''", undefined, true ), ' }' ] );
            }
          }
        },

        // [appendEscaped]
        //
        // On stack, before: value, ...
        // On stack, after: ...
        //
        // Escape `value` and append it to the buffer
        appendEscaped : function appendEscaped () {
          this.pushSource( this.appendToBuffer( [ this.aliasable( 'container.escapeExpression' ), '(', this.popStack(), ')' ] ) );
        },

        // [getContext]
        //
        // On stack, before: ...
        // On stack, after: ...
        // Compiler value, after: lastContext=depth
        //
        // Set the value of the `lastContext` compiler value to the depth
        getContext : function getContext ( depth ) {
          this.lastContext = depth;
        },

        // [pushContext]
        //
        // On stack, before: ...
        // On stack, after: currentContext, ...
        //
        // Pushes the value of the current context onto the stack.
        pushContext : function pushContext () {
          this.pushStackLiteral( this.contextName( this.lastContext ) );
        },

        // [lookupOnContext]
        //
        // On stack, before: ...
        // On stack, after: currentContext[name], ...
        //
        // Looks up the value of `name` on the current context and pushes
        // it onto the stack.
        lookupOnContext : function lookupOnContext ( parts, falsy, strict, scoped ) {
          var i = 0;

          if ( !scoped && this.options.compat && !this.lastContext ) {
            // The depthed query is expected to handle the undefined logic for the root level that
            // is implemented below, so we evaluate that directly in compat mode
            this.push( this.depthedLookup( parts[ i++ ] ) );
          } else {
            this.pushContext();
          }

          this.resolvePath( 'context', parts, i, falsy, strict );
        },

        // [lookupBlockParam]
        //
        // On stack, before: ...
        // On stack, after: blockParam[name], ...
        //
        // Looks up the value of `parts` on the given block param and pushes
        // it onto the stack.
        lookupBlockParam : function lookupBlockParam ( blockParamId, parts ) {
          this.useBlockParams = true;

          this.push( [ 'blockParams[', blockParamId[ 0 ], '][', blockParamId[ 1 ], ']' ] );
          this.resolvePath( 'context', parts, 1 );
        },

        // [lookupData]
        //
        // On stack, before: ...
        // On stack, after: data, ...
        //
        // Push the data lookup operator
        lookupData : function lookupData ( depth, parts, strict ) {
          if ( !depth ) {
            this.pushStackLiteral( 'data' );
          } else {
            this.pushStackLiteral( 'container.data(data, ' + depth + ')' );
          }

          this.resolvePath( 'data', parts, 0, true, strict );
        },

        resolvePath : function resolvePath ( type, parts, i, falsy, strict ) {
          // istanbul ignore next

          var _this = this;

          if ( this.options.strict || this.options.assumeObjects ) {
            this.push( strictLookup( this.options.strict && strict, this, parts, type ) );
            return;
          }

          var len = parts.length;
          for ( ; i < len; i++ ) {
            /* eslint-disable no-loop-func */
            this.replaceStack( function ( current ) {
              var lookup = _this.nameLookup( current, parts[ i ], type );
              // We want to ensure that zero and false are handled properly if the context (falsy flag)
              // needs to have the special handling for these values.
              if ( !falsy ) {
                return [ ' != null ? ', lookup, ' : ', current ];
              } else {
                // Otherwise we can use generic falsy handling
                return [ ' && ', lookup ];
              }
            } );
            /* eslint-enable no-loop-func */
          }
        },

        // [resolvePossibleLambda]
        //
        // On stack, before: value, ...
        // On stack, after: resolved value, ...
        //
        // If the `value` is a lambda, replace it on the stack by
        // the return value of the lambda
        resolvePossibleLambda : function resolvePossibleLambda () {
          this.push( [ this.aliasable( 'container.lambda' ), '(', this.popStack(), ', ', this.contextName( 0 ), ')' ] );
        },

        // [pushStringParam]
        //
        // On stack, before: ...
        // On stack, after: string, currentContext, ...
        //
        // This opcode is designed for use in string mode, which
        // provides the string value of a parameter along with its
        // depth rather than resolving it immediately.
        pushStringParam : function pushStringParam ( string, type ) {
          this.pushContext();
          this.pushString( type );

          // If it's a subexpression, the string result
          // will be pushed after this opcode.
          if ( type !== 'SubExpression' ) {
            if ( typeof string === 'string' ) {
              this.pushString( string );
            } else {
              this.pushStackLiteral( string );
            }
          }
        },

        emptyHash : function emptyHash ( omitEmpty ) {
          if ( this.trackIds ) {
            this.push( '{}' ); // hashIds
          }
          if ( this.stringParams ) {
            this.push( '{}' ); // hashContexts
            this.push( '{}' ); // hashTypes
          }
          this.pushStackLiteral( omitEmpty ? 'undefined' : '{}' );
        },
        pushHash  : function pushHash () {
          if ( this.hash ) {
            this.hashes.push( this.hash );
          }
          this.hash = { values : [], types : [], contexts : [], ids : [] };
        },
        popHash   : function popHash () {
          var hash = this.hash;
          this.hash = this.hashes.pop();

          if ( this.trackIds ) {
            this.push( this.objectLiteral( hash.ids ) );
          }
          if ( this.stringParams ) {
            this.push( this.objectLiteral( hash.contexts ) );
            this.push( this.objectLiteral( hash.types ) );
          }

          this.push( this.objectLiteral( hash.values ) );
        },

        // [pushString]
        //
        // On stack, before: ...
        // On stack, after: quotedString(string), ...
        //
        // Push a quoted version of `string` onto the stack
        pushString : function pushString ( string ) {
          this.pushStackLiteral( this.quotedString( string ) );
        },

        // [pushLiteral]
        //
        // On stack, before: ...
        // On stack, after: value, ...
        //
        // Pushes a value onto the stack. This operation prevents
        // the compiler from creating a temporary variable to hold
        // it.
        pushLiteral : function pushLiteral ( value ) {
          this.pushStackLiteral( value );
        },

        // [pushProgram]
        //
        // On stack, before: ...
        // On stack, after: program(guid), ...
        //
        // Push a program expression onto the stack. This takes
        // a compile-time guid and converts it into a runtime-accessible
        // expression.
        pushProgram : function pushProgram ( guid ) {
          if ( guid != null ) {
            this.pushStackLiteral( this.programExpression( guid ) );
          } else {
            this.pushStackLiteral( null );
          }
        },

        // [registerDecorator]
        //
        // On stack, before: hash, program, params..., ...
        // On stack, after: ...
        //
        // Pops off the decorator's parameters, invokes the decorator,
        // and inserts the decorator into the decorators list.
        registerDecorator : function registerDecorator ( paramSize, name ) {
          var foundDecorator = this.nameLookup( 'decorators', name, 'decorator' ),
              options        = this.setupHelperArgs( name, paramSize );

          this.decorators.push( [ 'fn = ', this.decorators.functionCall( foundDecorator, '', [ 'fn', 'props', 'container', options ] ), ' || fn;' ] );
        },

        // [invokeHelper]
        //
        // On stack, before: hash, inverse, program, params..., ...
        // On stack, after: result of helper invocation
        //
        // Pops off the helper's parameters, invokes the helper,
        // and pushes the helper's return value onto the stack.
        //
        // If the helper is not found, `helperMissing` is called.
        invokeHelper : function invokeHelper ( paramSize, name, isSimple ) {
          var nonHelper = this.popStack(),
              helper    = this.setupHelper( paramSize, name ),
              simple    = isSimple ? [ helper.name, ' || ' ] : '';

          var lookup = [ '(' ].concat( simple, nonHelper );
          if ( !this.options.strict ) {
            lookup.push( ' || ', this.aliasable( 'helpers.helperMissing' ) );
          }
          lookup.push( ')' );

          this.push( this.source.functionCall( lookup, 'call', helper.callParams ) );
        },

        // [invokeKnownHelper]
        //
        // On stack, before: hash, inverse, program, params..., ...
        // On stack, after: result of helper invocation
        //
        // This operation is used when the helper is known to exist,
        // so a `helperMissing` fallback is not required.
        invokeKnownHelper : function invokeKnownHelper ( paramSize, name ) {
          var helper = this.setupHelper( paramSize, name );
          this.push( this.source.functionCall( helper.name, 'call', helper.callParams ) );
        },

        // [invokeAmbiguous]
        //
        // On stack, before: hash, inverse, program, params..., ...
        // On stack, after: result of disambiguation
        //
        // This operation is used when an expression like `{{foo}}`
        // is provided, but we don't know at compile-time whether it
        // is a helper or a path.
        //
        // This operation emits more code than the other options,
        // and can be avoided by passing the `knownHelpers` and
        // `knownHelpersOnly` flags at compile-time.
        invokeAmbiguous : function invokeAmbiguous ( name, helperCall ) {
          this.useRegister( 'helper' );

          var nonHelper = this.popStack();

          this.emptyHash();
          var helper = this.setupHelper( 0, name, helperCall );

          var helperName = this.lastHelper = this.nameLookup( 'helpers', name, 'helper' );

          var lookup = [ '(', '(helper = ', helperName, ' || ', nonHelper, ')' ];
          if ( !this.options.strict ) {
            lookup[ 0 ] = '(helper = ';
            lookup.push( ' != null ? helper : ', this.aliasable( 'helpers.helperMissing' ) );
          }

          this.push( [ '(', lookup, helper.paramsInit ? [ '),(', helper.paramsInit ] : [], '),', '(typeof helper === ', this.aliasable( '"function"' ), ' ? ', this.source.functionCall( 'helper', 'call', helper.callParams ), ' : helper))' ] );
        },

        // [invokePartial]
        //
        // On stack, before: context, ...
        // On stack after: result of partial invocation
        //
        // This operation pops off a context, invokes a partial with that context,
        // and pushes the result of the invocation back.
        invokePartial : function invokePartial ( isDynamic, name, indent ) {
          var params  = [],
              options = this.setupParams( name, 1, params );

          if ( isDynamic ) {
            name = this.popStack();
            delete options.name;
          }

          if ( indent ) {
            options.indent = JSON.stringify( indent );
          }
          options.helpers = 'helpers';
          options.partials = 'partials';
          options.decorators = 'container.decorators';

          if ( !isDynamic ) {
            params.unshift( this.nameLookup( 'partials', name, 'partial' ) );
          } else {
            params.unshift( name );
          }

          if ( this.options.compat ) {
            options.depths = 'depths';
          }
          options = this.objectLiteral( options );
          params.push( options );

          this.push( this.source.functionCall( 'container.invokePartial', '', params ) );
        },

        // [assignToHash]
        //
        // On stack, before: value, ..., hash, ...
        // On stack, after: ..., hash, ...
        //
        // Pops a value off the stack and assigns it to the current hash
        assignToHash : function assignToHash ( key ) {
          var value   = this.popStack(),
              context = undefined,
              type    = undefined,
              id      = undefined;

          if ( this.trackIds ) {
            id = this.popStack();
          }
          if ( this.stringParams ) {
            type = this.popStack();
            context = this.popStack();
          }

          var hash = this.hash;
          if ( context ) {
            hash.contexts[ key ] = context;
          }
          if ( type ) {
            hash.types[ key ] = type;
          }
          if ( id ) {
            hash.ids[ key ] = id;
          }
          hash.values[ key ] = value;
        },

        pushId : function pushId ( type, name, child ) {
          if ( type === 'BlockParam' ) {
            this.pushStackLiteral( 'blockParams[' + name[ 0 ] + '].path[' + name[ 1 ] + ']' + (child ? ' + ' + JSON.stringify( '.' + child ) : '') );
          } else if ( type === 'PathExpression' ) {
            this.pushString( name );
          } else if ( type === 'SubExpression' ) {
            this.pushStackLiteral( 'true' );
          } else {
            this.pushStackLiteral( 'null' );
          }
        },

        // HELPERS

        compiler : JavaScriptCompiler,

        compileChildren      : function compileChildren ( environment, options ) {
          var children = environment.children,
              child    = undefined,
              compiler = undefined;

          for ( var i = 0, l = children.length; i < l; i++ ) {
            child = children[ i ];
            compiler = new this.compiler(); // eslint-disable-line new-cap

            var index = this.matchExistingProgram( child );

            if ( index == null ) {
              this.context.programs.push( '' ); // Placeholder to prevent name conflicts for nested children
              index = this.context.programs.length;
              child.index = index;
              child.name = 'program' + index;
              this.context.programs[ index ] = compiler.compile( child, options, this.context, !this.precompile );
              this.context.decorators[ index ] = compiler.decorators;
              this.context.environments[ index ] = child;

              this.useDepths = this.useDepths || compiler.useDepths;
              this.useBlockParams = this.useBlockParams || compiler.useBlockParams;
            } else {
              child.index = index;
              child.name = 'program' + index;

              this.useDepths = this.useDepths || child.useDepths;
              this.useBlockParams = this.useBlockParams || child.useBlockParams;
            }
          }
        },
        matchExistingProgram : function matchExistingProgram ( child ) {
          for ( var i = 0, len = this.context.environments.length; i < len; i++ ) {
            var environment = this.context.environments[ i ];
            if ( environment && environment.equals( child ) ) {
              return i;
            }
          }
        },

        programExpression : function programExpression ( guid ) {
          var child         = this.environment.children[ guid ],
              programParams = [ child.index, 'data', child.blockParams ];

          if ( this.useBlockParams || this.useDepths ) {
            programParams.push( 'blockParams' );
          }
          if ( this.useDepths ) {
            programParams.push( 'depths' );
          }

          return 'container.program(' + programParams.join( ', ' ) + ')';
        },

        useRegister : function useRegister ( name ) {
          if ( !this.registers[ name ] ) {
            this.registers[ name ] = true;
            this.registers.list.push( name );
          }
        },

        push : function push ( expr ) {
          if ( !(expr instanceof Literal) ) {
            expr = this.source.wrap( expr );
          }

          this.inlineStack.push( expr );
          return expr;
        },

        pushStackLiteral : function pushStackLiteral ( item ) {
          this.push( new Literal( item ) );
        },

        pushSource : function pushSource ( source ) {
          if ( this.pendingContent ) {
            this.source.push( this.appendToBuffer( this.source.quotedString( this.pendingContent ), this.pendingLocation ) );
            this.pendingContent = undefined;
          }

          if ( source ) {
            this.source.push( source );
          }
        },

        replaceStack : function replaceStack ( callback ) {
          var prefix       = [ '(' ],
              stack        = undefined,
              createdStack = undefined,
              usedLiteral  = undefined;

          /* istanbul ignore next */
          if ( !this.isInline() ) {
            throw new _exception2[ 'default' ]( 'replaceStack on non-inline' );
          }

          // We want to merge the inline statement into the replacement statement via ','
          var top = this.popStack( true );

          if ( top instanceof Literal ) {
            // Literals do not need to be inlined
            stack = [ top.value ];
            prefix = [ '(', stack ];
            usedLiteral = true;
          } else {
            // Get or create the current stack name for use by the inline
            createdStack = true;
            var _name = this.incrStack();

            prefix = [ '((', this.push( _name ), ' = ', top, ')' ];
            stack = this.topStack();
          }

          var item = callback.call( this, stack );

          if ( !usedLiteral ) {
            this.popStack();
          }
          if ( createdStack ) {
            this.stackSlot--;
          }
          this.push( prefix.concat( item, ')' ) );
        },

        incrStack    : function incrStack () {
          this.stackSlot++;
          if ( this.stackSlot > this.stackVars.length ) {
            this.stackVars.push( 'stack' + this.stackSlot );
          }
          return this.topStackName();
        },
        topStackName : function topStackName () {
          return 'stack' + this.stackSlot;
        },
        flushInline  : function flushInline () {
          var inlineStack = this.inlineStack;
          this.inlineStack = [];
          for ( var i = 0, len = inlineStack.length; i < len; i++ ) {
            var entry = inlineStack[ i ];
            /* istanbul ignore if */
            if ( entry instanceof Literal ) {
              this.compileStack.push( entry );
            } else {
              var stack = this.incrStack();
              this.pushSource( [ stack, ' = ', entry, ';' ] );
              this.compileStack.push( stack );
            }
          }
        },
        isInline     : function isInline () {
          return this.inlineStack.length;
        },

        popStack : function popStack ( wrapped ) {
          var inline = this.isInline(),
              item   = (inline ? this.inlineStack : this.compileStack).pop();

          if ( !wrapped && item instanceof Literal ) {
            return item.value;
          } else {
            if ( !inline ) {
              /* istanbul ignore next */
              if ( !this.stackSlot ) {
                throw new _exception2[ 'default' ]( 'Invalid stack pop' );
              }
              this.stackSlot--;
            }
            return item;
          }
        },

        topStack : function topStack () {
          var stack = this.isInline() ? this.inlineStack : this.compileStack,
              item  = stack[ stack.length - 1 ];

          /* istanbul ignore if */
          if ( item instanceof Literal ) {
            return item.value;
          } else {
            return item;
          }
        },

        contextName : function contextName ( context ) {
          if ( this.useDepths && context ) {
            return 'depths[' + context + ']';
          } else {
            return 'depth' + context;
          }
        },

        quotedString : function quotedString ( str ) {
          return this.source.quotedString( str );
        },

        objectLiteral : function objectLiteral ( obj ) {
          return this.source.objectLiteral( obj );
        },

        aliasable : function aliasable ( name ) {
          var ret = this.aliases[ name ];
          if ( ret ) {
            ret.referenceCount++;
            return ret;
          }

          ret = this.aliases[ name ] = this.source.wrap( name );
          ret.aliasable = true;
          ret.referenceCount = 1;

          return ret;
        },

        setupHelper : function setupHelper ( paramSize, name, blockHelper ) {
          var params     = [],
              paramsInit = this.setupHelperArgs( name, paramSize, params, blockHelper );
          var foundHelper = this.nameLookup( 'helpers', name, 'helper' );

          return {
            params     : params,
            paramsInit : paramsInit,
            name       : foundHelper,
            callParams : [ this.contextName( 0 ) ].concat( params )
          };
        },

        setupParams : function setupParams ( helper, paramSize, params ) {
          var options    = {},
              contexts   = [],
              types      = [],
              ids        = [],
              objectArgs = !params,
              param      = undefined;

          if ( objectArgs ) {
            params = [];
          }

          options.name = this.quotedString( helper );
          options.hash = this.popStack();

          if ( this.trackIds ) {
            options.hashIds = this.popStack();
          }
          if ( this.stringParams ) {
            options.hashTypes = this.popStack();
            options.hashContexts = this.popStack();
          }

          var inverse = this.popStack(),
              program = this.popStack();

          // Avoid setting fn and inverse if neither are set. This allows
          // helpers to do a check for `if (options.fn)`
          if ( program || inverse ) {
            options.fn = program || 'container.noop';
            options.inverse = inverse || 'container.noop';
          }

          // The parameters go on to the stack in order (making sure that they are evaluated in order)
          // so we need to pop them off the stack in reverse order
          var i = paramSize;
          while ( i-- ) {
            param = this.popStack();
            params[ i ] = param;

            if ( this.trackIds ) {
              ids[ i ] = this.popStack();
            }
            if ( this.stringParams ) {
              types[ i ] = this.popStack();
              contexts[ i ] = this.popStack();
            }
          }

          if ( objectArgs ) {
            options.args = this.source.generateArray( params );
          }

          if ( this.trackIds ) {
            options.ids = this.source.generateArray( ids );
          }
          if ( this.stringParams ) {
            options.types = this.source.generateArray( types );
            options.contexts = this.source.generateArray( contexts );
          }

          if ( this.options.data ) {
            options.data = 'data';
          }
          if ( this.useBlockParams ) {
            options.blockParams = 'blockParams';
          }
          return options;
        },

        setupHelperArgs : function setupHelperArgs ( helper, paramSize, params, useRegister ) {
          var options = this.setupParams( helper, paramSize, params );
          options = this.objectLiteral( options );
          if ( useRegister ) {
            this.useRegister( 'options' );
            params.push( 'options' );
            return [ 'options=', options ];
          } else if ( params ) {
            params.push( options );
            return '';
          } else {
            return options;
          }
        }
      };

      (function () {
        var reservedWords = ('break else new var' + ' case finally return void' + ' catch for switch while' + ' continue function this with' + ' default if throw' + ' delete in try' + ' do instanceof typeof' + ' abstract enum int short' + ' boolean export interface static' + ' byte extends long super' + ' char final native synchronized' + ' class float package throws' + ' const goto private transient' + ' debugger implements protected volatile' + ' double import public let yield await' + ' null true false').split( ' ' );

        var compilerWords = JavaScriptCompiler.RESERVED_WORDS = {};

        for ( var i = 0, l = reservedWords.length; i < l; i++ ) {
          compilerWords[ reservedWords[ i ] ] = true;
        }
      })();

      JavaScriptCompiler.isValidJavaScriptVariableName = function ( name ) {
        return !JavaScriptCompiler.RESERVED_WORDS[ name ] && /^[a-zA-Z_$][0-9a-zA-Z_$]*$/.test( name );
      };

      function strictLookup ( requireTerminal, compiler, parts, type ) {
        var stack = compiler.popStack(),
            i     = 0,
            len   = parts.length;
        if ( requireTerminal ) {
          len--;
        }

        for ( ; i < len; i++ ) {
          stack = compiler.nameLookup( stack, parts[ i ], type );
        }

        if ( requireTerminal ) {
          return [ compiler.aliasable( 'container.strict' ), '(', stack, ', ', compiler.quotedString( parts[ i ] ), ')' ];
        } else {
          return stack;
        }
      }

      exports[ 'default' ] = JavaScriptCompiler;
      module.exports = exports[ 'default' ];

      /***/
    },
              /* 29 */
              /***/ function ( module, exports, __webpack_require__ ) {

      /* global define */
      'use strict';

      exports.__esModule = true;

      var _utils = __webpack_require__( 5 );

      var SourceNode = undefined;

      try {
        /* istanbul ignore next */
        if ( false ) {
          // We don't support this in AMD environments. For these environments, we asusme that
          // they are running on the browser and thus have no need for the source-map library.
          var SourceMap = require( 'source-map' );
          SourceNode = SourceMap.SourceNode;
        }
      } catch ( err ) {
      }
      /* NOP */

      /* istanbul ignore if: tested but not covered in istanbul due to dist build  */
      if ( !SourceNode ) {
        SourceNode = function ( line, column, srcFile, chunks ) {
          this.src = '';
          if ( chunks ) {
            this.add( chunks );
          }
        };
        /* istanbul ignore next */
        SourceNode.prototype = {
          add                   : function add ( chunks ) {
            if ( _utils.isArray( chunks ) ) {
              chunks = chunks.join( '' );
            }
            this.src += chunks;
          },
          prepend               : function prepend ( chunks ) {
            if ( _utils.isArray( chunks ) ) {
              chunks = chunks.join( '' );
            }
            this.src = chunks + this.src;
          },
          toStringWithSourceMap : function toStringWithSourceMap () {
            return { code : this.toString() };
          },
          toString              : function toString () {
            return this.src;
          }
        };
      }

      function castChunk ( chunk, codeGen, loc ) {
        if ( _utils.isArray( chunk ) ) {
          var ret = [];

          for ( var i = 0, len = chunk.length; i < len; i++ ) {
            ret.push( codeGen.wrap( chunk[ i ], loc ) );
          }
          return ret;
        } else if ( typeof chunk === 'boolean' || typeof chunk === 'number' ) {
          // Handle primitives that the SourceNode will throw up on
          return chunk + '';
        }
        return chunk;
      }

      function CodeGen ( srcFile ) {
        this.srcFile = srcFile;
        this.source = [];
      }

      CodeGen.prototype = {
        isEmpty : function isEmpty () {
          return !this.source.length;
        },
        prepend : function prepend ( source, loc ) {
          this.source.unshift( this.wrap( source, loc ) );
        },
        push    : function push ( source, loc ) {
          this.source.push( this.wrap( source, loc ) );
        },

        merge : function merge () {
          var source = this.empty();
          this.each( function ( line ) {
            source.add( [ '  ', line, '\n' ] );
          } );
          return source;
        },

        each : function each ( iter ) {
          for ( var i = 0, len = this.source.length; i < len; i++ ) {
            iter( this.source[ i ] );
          }
        },

        empty : function empty () {
          var loc = this.currentLocation || { start : {} };
          return new SourceNode( loc.start.line, loc.start.column, this.srcFile );
        },
        wrap  : function wrap ( chunk ) {
          var loc = arguments.length <= 1 || arguments[ 1 ] === undefined ? this.currentLocation || { start : {} } : arguments[ 1 ];

          if ( chunk instanceof SourceNode ) {
            return chunk;
          }

          chunk = castChunk( chunk, this, loc );

          return new SourceNode( loc.start.line, loc.start.column, this.srcFile, chunk );
        },

        functionCall : function functionCall ( fn, type, params ) {
          params = this.generateList( params );
          return this.wrap( [ fn, type ? '.' + type + '(' : '(', params, ')' ] );
        },

        quotedString : function quotedString ( str ) {
          return '"' + (str + '').replace( /\\/g, '\\\\' ).replace( /"/g, '\\"' ).replace( /\n/g, '\\n' ).replace( /\r/g, '\\r' ).replace( /\u2028/g, '\\u2028' ) // Per Ecma-262 7.3 + 7.8.4
              .replace( /\u2029/g, '\\u2029' ) + '"';
        },

        objectLiteral : function objectLiteral ( obj ) {
          var pairs = [];

          for ( var key in obj ) {
            if ( obj.hasOwnProperty( key ) ) {
              var value = castChunk( obj[ key ], this );
              if ( value !== 'undefined' ) {
                pairs.push( [ this.quotedString( key ), ':', value ] );
              }
            }
          }

          var ret = this.generateList( pairs );
          ret.prepend( '{' );
          ret.add( '}' );
          return ret;
        },

        generateList : function generateList ( entries ) {
          var ret = this.empty();

          for ( var i = 0, len = entries.length; i < len; i++ ) {
            if ( i ) {
              ret.add( ',' );
            }

            ret.add( castChunk( entries[ i ], this ) );
          }

          return ret;
        },

        generateArray : function generateArray ( entries ) {
          var ret = this.generateList( entries );
          ret.prepend( '[' );
          ret.add( ']' );

          return ret;
        }
      };

      exports[ 'default' ] = CodeGen;
      module.exports = exports[ 'default' ];

      /***/
    }
              /******/ ] )
} );

$( function () {

  //Table sorter bootstrap theme options
  $.tablesorter.themes.bootstrap = {
    // these classes are added to the table. To see other table classes available,
    // look here: http://getbootstrap.com/css/#tables
    table        : 'table table-hover table-striped',
    caption      : 'caption',
    // header class names
    header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
    sortNone     : '',
    sortAsc      : '',
    sortDesc     : '',
    active       : '', // applied when column is sorted
    hover        : '', // custom css required - a defined bootstrap style may not override other classes
    // icon class names
    icons        : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
    iconSortNone : 'bootstrap-icon-unsorted', // class name added to icon when column is not sorted
    iconSortAsc  : 'glyphicon glyphicon-chevron-up', // class name added to icon when column has ascending sort
    iconSortDesc : 'glyphicon glyphicon-chevron-down', // class name added to icon when column has descending sort
    filterRow    : '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
    footerRow    : '',
    footerCells  : '',
    even         : '', // even row zebra striping
    odd          : ''  // odd row zebra striping
  };

  //Tablesorter common options
  var tablesorter = {
    theme          : "bootstrap",
    cssAsc         : "tablesorter-headerAsc",
    cssDesc        : "tablesorter-headerDesc",
    cssHeader      : "tablesorter-header",
    cssChildRow    : "tablesorter-childRow",
    widthFixed     : true,
    headerTemplate : '{content} {icon}',
    widgets        : [ "uitheme", "zebra", "filter", "print", "pager" ],
    widgetOptions  : {
      filter_childRows           : false,
      filter_childByColumn       : false,
      filter_columnFilters       : true,
      filter_columnAnyMatch      : false,
      filter_cssFilter           : "form-control",
      filter_reset               : "button.reset",
      filter_filteredRow         : 'filtered',
      filter_hideEmpty           : true,
      filter_hideFilters         : true,
      filter_ignoreCase          : true,
      filter_liveSearch          : true,
      filter_searchDelay         : 500,
      filter_placeholder         : { search : '', select : '' },
      filter_searchFiltered      : true,
      filter_serversideFiltering : true,
      filter_startsWith          : false,
      pager_output               : '{startRow} to {endRow} of {totalRows}',
      pager_updateArrows         : true,
      pager_startPage            : 0,
      pager_size : 3,
      pager_savePages            : true,
      pager_removeRows           : false,
      pager_ajaxUrl              : '/ajax/tablesorter/{page}/{size}/{sortList:sort}/{filterList:filter}',
      pager_customAjaxUrl        : function ( table, url ) { return url; },
      pager_ajaxError            : null,
      pager_ajaxObject           : {
        dataType : 'json',
        type     : 'POST',
        data     : null
      },
      pager_ajaxProcessing       : function ( ajax ) {

        $( '#' + ajax[ 'id' ] ).find( 'tbody' ).empty();
        $( '#' + ajax[ 'id' ] ).find( 'tbody' ).append( ajax[ 'html' ] );
        $( '#' + ajax[ 'id' ] ).find( '.tablesorter-childRow td' ).hide();

        return [ ajax[ 'total' ], [], null ];
      },
      pager_css                  : {
        container : 'tablesorter-pager',    // class added to make included pager.css file work
        errorRow  : 'tablesorter-errorRow', // error information row (don't include period at beginning); styled in theme file
        disabled  : 'disabled'              // class added to arrows @ extremes (i.e. prev/first arrows "disabled" on first page)
      },
      pager_selectors            : {
        container   : '.pager',       // target the pager markup (wrapper)
        first       : '.first',       // go to first page arrow
        prev        : '.prev',        // previous page arrow
        next        : '.next',        // next page arrow
        last        : '.last',        // go to last page arrow
        gotoPage    : '.pagenum',    // go to page selector - select dropdown that sets the current page
        pageDisplay : '.pagedisplay', // location of where the "output" is displayed
        pageSize    : '.pagesize'     // page size selector - select dropdown that sets the "size" option
      }
    }
  };

  //Add tablesorter options from classes and apply tablesorter
  $.each( $( '.tablesorter' ), function ( index, value ) {

    var colspan = $( this ).find( 'th' ).length;
    var export_type = '';

    tablesorter.widgetOptions.pager_ajaxObject.data = null;
    tablesorter.headers = null;

    if ( $( this ).hasClass( 'tablesorter-pvt-prescriptions' ) ) {

      export_type = 'private-prescriptions';

      data = {
        action : 'tbl-pvt-prescriptions',
        user   : $( '#patient' ).val(),
        mode : $( '#mode' ).val(),
        _token : $( this ).find( '.pvt-prescription-token' ).val(),
      };

      tablesorter.widgetOptions.pager_ajaxObject.data = data;
      tablesorter.widgetOptions.pager_selectors.container = '#pager-pvt-prescriptions';
    }
    else if ( $( this ).hasClass( 'tablesorter-poyc-prescriptions' ) ) {

      export_type = 'poyc-prescriptions';

      data = {
        action : 'tbl-poyc-prescriptions',
        user   : $( '#patient' ).val(),
        mode : $( '#mode' ).val(),
        _token : $( this ).find( '.poyc-prescription-token' ).val(),
      };

      tablesorter.widgetOptions.pager_ajaxObject.data = data;
      tablesorter.widgetOptions.pager_selectors.container = '#pager-poyc-prescriptions';
    }
    else if ( $( this ).hasClass( 'tablesorter-patient-products' ) ) {

      export_type = 'patient-products';

      data = {
        action : 'tbl-patient-products',
        user   : $( '#patient' ).val(),
        _token : $( this ).find( '.patient-products-token' ).val(),
      };

      tablesorter.widgetOptions.pager_ajaxObject.data = data;
      tablesorter.widgetOptions.pager_selectors.container = '#pager-patient-products';
    }
    else if ( $( this ).hasClass( 'tablesorter-patient-conditions' ) ) {

      export_type = 'patient-conditions';

      data = {
        action : 'tbl-patient-conditions',
        user   : $( '#patient' ).val(),
        _token : $( this ).find( '.patient-conditions-token' ).val(),
      };

      tablesorter.widgetOptions.pager_ajaxObject.data = data;
      tablesorter.widgetOptions.pager_selectors.container = '#pager-patient-conditions';
    }

    if ( $( this ).hasClass( 'tablesorter-dis' ) ) {

      var headers = {};

      $.each( $( this ).attr( 'class' ).split( ' ' ), function ( index, value ) {

        if ( value.indexOf( 'dis-' ) >= 0 ) {

          var key = value.split( '-' )[ 1 ];

          headers[ key ] = {
            sorter : false
          }
        }
      } );

      tablesorter.headers = headers;
    }

    $( this )
      .bind( "sortStart", function () {

               spinner( $( this ), colspan );
             } )
      .bind( "filterStart", function () {

               spinner( $( this ), colspan );
             } )
      .bind( 'pageMoved', function () {

               spinner( $( this ), colspan );
             } )
      .tablesorter( tablesorter );

    $( this ).next().find( '.reset' ).click( function () {

      $( this ).closest( '.pager' ).prev().trigger( 'filterReset' );
    } );

    $( this ).next().find( '.export' ).click( function () {

      var form = $( this ).parent();
      var table = form.parent().prev();

      form.find( '.action' ).val( export_type );
      form.find( '.sort' ).val( JSON.stringify( table[ 0 ].config.sortList ) );
      form.find( '.filter' ).val( JSON.stringify( table[ 0 ].config.lastSearch ) );

      form[ 0 ].submit();
    } );
  } );

  function spinner ( table, colspan ) {

    var source = $( "#spinner-row-template" ).html();
    var template = Handlebars.compile( source );
    var context = { colspan : colspan, active : "active", color : "spinner-pharma-1" };
    var html = template( context );

    $( table ).find( 'tbody' ).html( html );
  }

} );
$( document ).ready( function () {
  $( ".btn" ).click( function () {
    $( "#myCollapsible" ).collapse();
  } );
} );
$( "input.touchspin" ).TouchSpin( {
  verticalbuttons : true,
  min             : 0,
  max             : 999,
} );

$( '#patient-products #products tbody' ).on( 'click', '.quantity, .bootstrap-touchspin-up, .bootstrap-touchspin-down', function ( e ) {

  e.stopPropagation();
} );
//Make sub-body fill Height
$( '.sub-body' ).css( 'min-height', $( window ).height() - $( 'nav' ).height() - 2 );

//Function call to center content vertically
function center_content () {

  var screen_height = $( window ).height() - $( 'nav' ).height() - 2;
  var content_height = $( '#center-content' ).height();
  console.log( content_height );
  console.log( screen_height );
  if ( content_height < screen_height )
    $( '#push-content' ).css( 'margin-top', ( screen_height - content_height ) / 2 );
}

center_content();

//Error and Warning Message Fade out
$( 'body' ).on( 'click focusout', '.flash-msg', function () {

  $( 'div.alert-success' ).not( '.alert-important' ).delay( 2000 ).slideUp( 300 );
  $( 'div.alert-danger' ).not( '.alert-important' ).delay( 2000 ).slideUp( 300 );
} );

//Tablesorter children rows
$( '.tablesorter-row-toggle' ).on( 'click', '.tablesorter-toggle', function () {

  $( this ).nextUntil( 'tr:not(.tablesorter-childRow)' ).find( 'td' ).toggle();
} );

//Tablesorter prescription products and details rows
$( '.tablesorter-row-toggle-prescription' ).on( 'click', '.tablesorter-toggle', function () {

  var rows = $( this ).nextUntil( 'tr.tablesorter-toggle' );

  $.each( rows, function ( index, value ) {

    if ( $( value ).hasClass( 'product' ) || $( value ).hasClass( 'prescription-description' ) || $( value ).hasClass( 'complete' ) )
      $( value ).find( 'td' ).toggle();
  } );
} );

$( '.tablesorter-row-toggle-prescription' ).on( 'click', '.product', function () {

  $( this ).next().find( 'td' ).toggle();
} );

//Patient product product description row
$( '#patient-products #products' ).on( 'click', '.product-item', function () {

  $( this ).next().find( 'td' ).toggle();
} );
$( '#patient-products' ).on( 'input', '.product-barcode', function () {

  data = {
    barcode : $( this ).val(),
    _token  : $( this ).next().val()
  };

  $.ajax( {
    method   : "POST",
    url : '/ajax/add-product',
    data     : data,
    success  : function ( data ) {

      var last_tr = $( 'table#products #product-items tr:last' );

      if ( $( '.barcode-' + data[ 'id' ] ).length ) {

        $( '.barcode-' + data[ 'id' ] + '.product-description' ).remove();
        $( '.barcode-' + data[ 'id' ] ).replaceWith( data[ 'row' ] );
      }
      else
        last_tr.after( data[ 'row' ] );

      $( '.barcode-null' ).remove();

      $( '.barcode-' + data[ 'id' ] + '.product-item .touchspin' ).TouchSpin( {
        verticalbuttons : true
      } );

      $( '#product-totals .quantity' ).html( data[ 'totals' ][ 'quantity' ] );
      $( '#product-totals .total' ).html( data[ 'totals' ][ 'total' ] );

      $( '.product-barcode' ).val( '' );

      $( '.product-buy-parent button' ).removeClass( 'disabled' );
      $( '.product-buy-parent button' ).prop( 'disabled', false );

      $( '.product-barcode' ).parent().removeClass( 'has-error' );
      $( '.product-barcode' ).parent().addClass( 'has-success' );
      $( '.top-message' ).html( '' );
    },
    error    : function ( data ) {

      data = data[ 'responseJSON' ];

      $( '.top-message' ).html( data[ 'message' ] );
      $( '.product-barcode' ).parent().removeClass( 'has-success' );
      $( '.product-barcode' ).parent().addClass( 'has-error' );
    },
    dataType : 'json'
  } );
} );
$( '.tablesorter-validate-prescriptions' ).on( 'click', '.product-barcode-skip', function ( e ) {

  e.stopPropagation();

  var barcode = $( this ).parentsUntil( 'tbody' ).find( '.product-barcode' );

  if ( $( this ).attr( 'data-click-state' ) == 1 ) {

    $( this ).attr( 'data-click-state', 0 );

    $( barcode ).prop( 'disabled', false );
    $( barcode ).removeClass( 'pharma-skip-disable' );

    $( this ).parentsUntil( 'tbody' ).find( '.product-barcode-status .flash' ).css( 'display', 'none' );
  }
  else {

    $( this ).attr( 'data-click-state', 1 );

    $( barcode ).prop( 'disabled', true );
    $( barcode ).addClass( 'pharma-skip-disable' );
    $( barcode ).parent().removeClass( 'has-success' );
    $( barcode ).parent().removeClass( 'has-error' );
    $( barcode ).val( '' );

    $( this ).parentsUntil( 'tbody' ).find( '.product-barcode-status span' ).css( 'display', 'none' );
    $( this ).parentsUntil( 'tbody' ).find( '.product-barcode-status .flash' ).css( 'display', 'inline-block' );
  }

  var antonym = $( this ).attr( 'antonym' );

  $( this ).attr( 'antonym', $( this ).val() );
  $( this ).val( antonym );
} );
$( '.tablesorter-validate-prescriptions' ).on( 'click', '.product-prescription', function () {

  var table = $( this ).closest( 'table' );
  var product_barcodes = $( this ).closest( 'tr' ).prevUntil( '.tablesorter-toggle' );
  var ordered_product_barcodes = {};
  var barcodes = {};
  var product_ids = {};

  $.each( product_barcodes, function ( index, value ) {

    ordered_product_barcodes[ index ] = {};

    if ( !$( this ).find( '.pharma-skip-disable' )[ 0 ] )
      ordered_product_barcodes[ index ][ 0 ] = value;
    else {

      ordered_product_barcodes[ index ][ 0 ] = value;
      ordered_product_barcodes[ index ][ 1 ] = 0;
    }
  } );

  $.each( ordered_product_barcodes, function ( index, value ) {

    if ( value[ 1 ] != 0 ) {

      var barcode = $( value[ 0 ] ).find( '.product-barcode' ).val();
      barcodes[ index ] = barcode;
    }

    var product_id = $( value[ 0 ] ).find( '.product-barcode' ).attr( 'product-id' );
    product_ids[ index ] = product_id;
  } );

  data = {
    barcode : barcodes,
    product_id : product_ids,
    _token  : $( this ).next().val(),
    id      : $( this ).next().next().val()
  };

  $.ajax( {
    method   : "POST",
    url      : '/ajax/patient/details/updates',
    data     : data,
    success  : function ( data ) {

      table.trigger( "update" ).trigger( "appendCache" ).trigger( "applyWidgets" );
      table.trigger( 'pagerUpdate' );
      $( '.top-message' ).html( data[ 'message' ] );
    },
    error    : function ( data ) {

      data = data[ 'responseJSON' ];

      $( '.top-message' ).html( data[ 'message' ] );

      $.each( data[ 'elements' ][ 'barcode' ], function ( index ) {

        $( ordered_product_barcodes[ index ][ 0 ] ).find( '.product-barcode-parent' ).removeClass( 'has-success' );
        $( ordered_product_barcodes[ index ][ 0 ] ).find( '.product-barcode-parent' ).addClass( 'has-error' );
        $( ordered_product_barcodes[ index ][ 0 ] ).find( '.product-barcode-status span' ).css( 'display', 'none' );
        $( ordered_product_barcodes[ index ][ 0 ] ).find( '.product-barcode-status .remove' ).css( 'display', 'inline-block' );
      } );
    },
    dataType : 'json'
  } );
} );
$( '.tablesorter-validate-prescriptions' ).on( 'input', '.product-barcode', function () {

  data = {
    barcode : $( this ).val(),
    product_id : $( this ).attr( 'product-id' ),
    _token  : $( this ).next().val()
  };

  var product_barcode = $( this );

  $.ajax( {
    method   : "POST",
    url      : '/ajax/validate-product-barcode',
    data     : data,
    success  : function ( data ) {

      $( '.top-message' ).html( data[ 'message' ] );
      $( product_barcode ).parent().removeClass( 'has-error' );
      $( product_barcode ).parent().addClass( 'has-success' );
      $( product_barcode ).closest( 'td' ).next().find( 'span' ).css( 'display', 'none' );
      $( product_barcode ).closest( 'td' ).next().find( '.ok' ).css( 'display', 'inline-block' );
    },
    error    : function ( data ) {

      data = data[ 'responseJSON' ];

      $( '.top-message' ).html( data[ 'message' ] );
      $( product_barcode ).parent().removeClass( 'has-success' );
      $( product_barcode ).parent().addClass( 'has-error' );
      $( product_barcode ).closest( 'td' ).next().find( 'span' ).css( 'display', 'none' );
      $( product_barcode ).closest( 'td' ).next().find( '.remove' ).css( 'display', 'inline-block' );
    },
    dataType : 'json'
  } );
} );

$( '.tablesorter-validate-prescriptions' ).on( 'click', '.product-barcode', function ( e ) {

  e.stopPropagation();
} );
$( '#patient-products #products' ).on( 'click', '.remove', function ( e ) {

  e.stopPropagation();

  data = {
    product : $( this ).closest( 'tr' ).attr( 'data-id' ),
    rows   : $( '.product-item' ).length,
    _token : $( this ).next().val()
  };

  $.ajax( {
    method   : "POST",
    url      : '/ajax/remove-product',
    data     : data,
    success  : function ( data ) {

      $( '.barcode-' + data[ 'id' ] ).remove();

      if ( $( '.product-item' ).length <= 0 ) {

        if ( !$( '.product-buy-parent button' ).hasClass( 'disabled' ) ) {

          $( '.product-buy-parent button' ).addClass( 'disabled' );
          $( '.product-buy-parent button' ).prop( 'disabled', true );
        }

        $( '#products #product-items' ).html( data[ 'empty_row' ] );
      }
      else {

        $( '.product-item .count' ).each( function ( index ) {

          $( this ).html( index + 1 );
        } );
      }

      $( '#product-totals .quantity' ).html( data[ 'totals' ][ 'quantity' ] );
      $( '#product-totals .total' ).html( data[ 'totals' ][ 'total' ] );
    },
    dataType : 'json'
  } );
} );
$( '#patient-products .refresh' ).click( function ( e ) {

  var products = {};

  $( '#products #product-items tr.product-item' ).each( function () {

    products[ $( this ).attr( 'data-id' ) ] = $( this ).find( '.quantity' ).val();
  } );

  data = {
    products : products,
    _token : $( this ).next().val()
  };

  $.ajax( {
    method   : "POST",
    url : '/ajax/refresh-product',
    data     : data,
    success  : function ( data ) {

      $( '.refresh' ).addClass( 'hidden' );
      $( '.top-message' ).html( data[ 'message' ] );

      $.each( data[ 'removed' ], function ( index, value ) {

        $( '.barcode-' + value ).remove();
      } );

      if ( $( '.product-item' ).length <= 0 ) {

        if ( !$( '.product-buy-parent button' ).hasClass( 'disabled' ) )
          $( '.product-buy-parent button' ).addClass( 'disabled' );

        $( '#products #product-items' ).html( data[ 'empty_row' ] );
      }
      else {

        $( '.product-item .count' ).each( function ( index ) {

          $( this ).html( index + 1 );
        } );

        $( '#products #product-items tr.product-item' ).each( function () {

          $( this ).find( '.total' ).html( data[ 'total_prices' ][ $( this ).attr( 'data-id' ) ] );
          $( this ).find( '.quantity' ).attr( 'data-refresh', 0 );
          $( this ).find( '.quantity' ).attr( 'data-value', $( this ).find( '.quantity' ).val() );
        } );
      }

      $( '#product-totals .quantity' ).html( data[ 'totals' ][ 'quantity' ] );
      $( '#product-totals .total' ).html( data[ 'totals' ][ 'total' ] );
    },
    dataType : 'json'
  } );
} );
$( '#patient-products #products #product-items' ).on( 'change input', '.quantity', function () {

  $old_value = $( this ).attr( 'data-value' );

  if ( $old_value != $( this ).val() )
    $( this ).attr( 'data-refresh', '1' );
  else
    $( this ).attr( 'data-refresh', '0' );

  var refresh = false;

  $( '#products #product-items tr .quantity' ).each( function () {

    if ( $( this ).attr( 'data-refresh' ) == '1' ) {

      refresh = true;
      return false;
    }
  } );

  if ( refresh )
    $( '.refresh' ).removeClass( 'hidden' );
  else {

    if ( !$( '.refresh' ).hasClass( 'hidden' ) )
      $( '.refresh' ).addClass( 'hidden' );
  }
} );
$( '#patient-products .cart' ).click( function () {

  data = {
    _token : $( this ).next().val()
  };

  $.ajax( {
    method   : "POST",
    url      : '/ajax/buy-product',
    data     : data,
    success  : function ( data ) {

      $( '.top-message' ).html( data[ 'message' ] );

      $( '#products #product-items' ).html( data[ 'empty_row' ] );
      $( '#product-totals' ).html( data[ 'totals' ] );

      var breadcrumb = $( 'ol.breadcrumb .active' ).text();

      if ( breadcrumb.indexOf( ':' ) != -1 ) {

        breadcrumb = breadcrumb.substr( 0, breadcrumb.indexOf( ':' ) );
        $( 'ol.breadcrumb .active' ).text( breadcrumb );
      }

      var details_url = $( '#menu-patient-details a' ).attr( 'href' );

      details_url = details_url.substr( 0, details_url.indexOf( 'medical-history' ) + 15 );

      $( '#menu-patient-details a' ).attr( 'href', details_url );
    },
    error    : function ( data ) {

      data = data[ 'responseJSON' ];

      $( '.top-message' ).html( data[ 'message' ] );

    },
    complete : function ( xhr, status ) {

      if ( status == 'success' )
        history.replaceState( {}, "", "/patient/products" );
    },
    dataType : 'json'
  } );
} );
$( '.tablesorter' ).on( 'click', '.print', function ( e ) {

  e.stopPropagation();

  var prescription = $( this ).closest( 'tr' );

  var date = prescription.find( '.date' ).html();
  var doctor = prescription.find( '.doctor' ).html();

  var products = [];
  var description;

  var rows = prescription.nextUntil( 'tr.tablesorter-toggle' );

  $.each( rows, function ( index, value ) {

    if ( $( value ).hasClass( 'product' ) ) {

      products.push( {
        name   : $( value ).find( '.name' ).html(),
        dosage : $( value ).find( '.dosage' ).html(),
        period : $( value ).next().find( '.period' ).html(),
        total  : $( value ).next().find( '.total' ).html()
      } );
    }
    else if ( $( value ).hasClass( 'prescription-description' ) )
      description = $( value ).find( '.description' ).html();
  } );

  var source = $( "#print-prescription-template" ).html();
  var template = Handlebars.compile( source );
  var context = { date : date, doctor : doctor, products : products, description : description };
  var html = template( context );

  $( '#section-to-print' ).html( html );

  window.print();

  $( '#section-to-print' ).empty();
} );
//# sourceMappingURL=script.js.map
