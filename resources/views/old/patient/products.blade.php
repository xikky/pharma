@extends('app')

@section('breadcrumbs')
  {!! Breadcrumbs::render('patient_products', $nin ) !!}
@endsection

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="top-message">
          @include('partials.error')
          @include('partials.flash')
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        @include('partials.error')

        <div id="patient-products" class="content">
          <div class="panel panel-pharma">
            <div class="panel-heading">{{ trans('patient.patient_products') }}</div>
            <div class="panel-body">

              <div class="table-responsive">
                <table id="products" class="table table-hover">
                  <thead>
                  <tr>
                    <th class="count">#</th>
                    <th class="product">{{  trans( 'patient.product' ) }}</th>
                    <th class="quantity">{{ trans( 'patient.quantity' ) }}</th>
                    <th class="price">{{ trans( 'patient.price' ) }}</th>
                    <th class="total">{{ trans( 'patient.total_price' ) }}</th>
                    <th class="remove"></th>
                  </tr>
                  </thead>
                  <tbody id="product-items">
                  @if ( empty( $products ) )
                    <tr class="barcode-null">
                      <td>0</td>
                      <td>----</td>
                      <td class="align-center">0</td>
                      <td>{{ Config::get( 'config.currency' )['symbol'] . number_format( '0', 2 ) }}</td>
                      <td>{{ Config::get( 'config.currency' )['symbol'] . number_format( '0', 2 ) }}</td>
                      <td></td>
                    </tr>
                  @else
                    @foreach( $products as $product )
                      <tr class="barcode-{{ $product['id'] }} product-item" data-id="{{ $product['id'] }}">
                        <td class="count">{{ $product['count'] }}</td>
                        <td>{{ $product['name'] }}</td>
                        <td>{!! Form::number( 'nin_number', $product['quantity'], array( 'class' => 'quantity form-number-spinner touchspin', 'data-value' => $product['quantity'], 'data-refresh' => 0 ) ) !!}</td>
                        <td>{{ Config::get( 'config.currency' )['symbol'] . $product['price'] }}</td>
                        <td class="total">{{ Config::get( 'config.currency' )['symbol'] . $product['total'] }}</td>
                        <td>
                          <button type="button" class="btn remove" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                          {!! Form::hidden( '_token', csrf_token(), ['class' => 'product-remove-token'] ) !!}
                        </td>
                      </tr>
                      <tr class="barcode-{{ $product['id'] }} product-description">
                        <td></td>
                        <td colspan="4">{{ $product['description'] }}</td>
                        <td></td>
                      </tr>
                    @endforeach
                  @endif
                  </tbody>
                  <tbody id="product-totals">
                  <tr>
                    <td></td>
                    <td class="align-right">{{ trans( 'patient.total' ) }}</td>
                    <td class="align-center quantity">{{ $totals['quantity'] }}</td>
                    <td></td>
                    <td class="total">{{ Config::get( 'config.currency' )['symbol'] . $totals['total'] }}</td>
                    <td></td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div class="col-md-3">
                <div class="product-refresh-parent">
                  <button type="button" class="btn btn-pharma-1 refresh flash-msg hidden" title="Refresh"><i class="glyphicon glyphicon-refresh"></i> {{ trans( 'patient.refresh') }}</button>
                  {!! Form::hidden( '_token', csrf_token(), ['class' => 'refresh-token'] ) !!}
                </div>
              </div>

              <div class="col-md-5 col-md-offset-4">
                <div class="product-barcode-parent">
                  {!! Form::text('barcode', '', ['class' => 'form-control product-barcode flash-msg']) !!}
                  {!! Form::hidden( '_token', csrf_token(), ['class' => 'product-token'] ) !!}
                </div>
              </div>

              <div class="col-md-5 col-md-offset-7">
                <div class="product-buy-parent">
                  <button type="button" class="btn btn-pharma-1 cart {{ empty( $products ) ? 'disabled' : '' }}" title="Buy" {{ empty( $products ) ? 'disabled' : '' }}><i class="glyphicon glyphicon-shopping-cart"></i> {{ trans( 'patient.buy') }}</button>
                  {!! Form::hidden( '_token', csrf_token(), ['class' => 'buy-token'] ) !!}
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
