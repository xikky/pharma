$( '.tablesorter' ).on( 'click', '.print', function ( e ) {

  e.stopPropagation();

  var prescription = $( this ).closest( 'tr' );

  var date = prescription.find( '.date' ).html();
  var doctor = prescription.find( '.doctor' ).html();

  var products = [];
  var description;

  var rows = prescription.nextUntil( 'tr.tablesorter-toggle' );

  $.each( rows, function ( index, value ) {

    if ( $( value ).hasClass( 'product' ) ) {

      products.push( {
        name   : $( value ).find( '.name' ).html(),
        dosage : $( value ).find( '.dosage' ).html(),
        period : $( value ).next().find( '.period' ).html(),
        total  : $( value ).next().find( '.total' ).html()
      } );
    }
    else if ( $( value ).hasClass( 'prescription-description' ) )
      description = $( value ).find( '.description' ).html();
  } );

  var source = $( "#print-prescription-template" ).html();
  var template = Handlebars.compile( source );
  var context = { date : date, doctor : doctor, products : products, description : description };
  var html = template( context );

  $( '#section-to-print' ).html( html );

  window.print();

  $( '#section-to-print' ).empty();
} );