<?php

return [

  'logo_name'        => 'Pharma',
  'login'            => 'Login',
  'register'         => 'Register',
  'username'         => 'Isem',
  'email'            => 'E-mail Address',
  'password'         => 'Password',
  'confirm_password' => 'Ikkonferma Password',
  'english'          => 'Inglijz',
  'maltese'          => 'Malti',
  'logout'           => 'Logout'
];