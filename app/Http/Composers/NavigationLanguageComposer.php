<?php namespace App\Http\Composers;

use App\Language;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class NavigationLanguageComposer {

  public function compose ( View $view ) {

    $user_language = Session::get( 'preferred_language', Config::get( 'config.language' ) );
    $user_logged = '';

    if ( Auth::check() && !Session::has( 'preferred_language' ) )
      $user_language = Auth::user()->language->toArray();

    if ( Auth::check() )
      $user_logged = Auth::user()->name;

    $view->with( [
                   'languages'     => Language::all()->toArray(),
                   'user_language' => $user_language,
                   'user_logged'   => $user_logged
                 ] );
  }
}