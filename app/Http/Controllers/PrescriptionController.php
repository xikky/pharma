<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserExistsRequest;
use App\Libraries\Helpers\_User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class PrescriptionController extends Controller {

  public function patientSearch ( UserExistsRequest $request ) {

    $nin = $request->nin_number . $request->nin_country;

    ( new _User )->setSession( $nin );

    return Redirect::route( 'prescription', compact( 'nin' ) );
  }

  public function patientForget () {

    Session::forget( 'user' );

    return Redirect::route( 'prescription' );
  }
}
