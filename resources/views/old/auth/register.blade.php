@extends('app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        @include('partials.error')

        <div id="register" class="content">
          <div class="panel panel-pharma">
            <div class="panel-heading">Register</div>
            <div class="panel-body">

              <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                  <label class="col-md-4 control-label">{{ trans('common.username') }}</label>

                  <div class="col-md-6">
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label">{{ trans('common.email') }}</label>

                  <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label">{{ trans('common.password') }}</label>

                  <div class="col-md-6">
                    <input type="password" class="form-control" name="password">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label">{{ trans('common.confirm_password') }}</label>

                  <div class="col-md-6">
                    <input type="password" class="form-control" name="password_confirmation">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label">{{ trans('common.language') }}</label>

                  <div class="col-md-6">
                    <select class="form-control" name="language">
                      @foreach ( $languages as $language )
                        @if( $language['code'] == 'en' )
                          <option value="{{ $language['id'] }}" selected>{{ $language['language'] }}</option>
                        @else
                          <option value="{{ $language['id'] }}">{{ $language['language'] }}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-pharma-1">
                      Register
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
