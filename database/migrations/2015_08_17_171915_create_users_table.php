<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {

    Schema::create( 'users', function ( Blueprint $table ) {

      $table->increments( 'id' )->unsigned();
      $table->string( 'nin' )->unique();
      $table->string( 'name' )->unique();
      $table->string( 'firstname' )->nullable();
      $table->string( 'lastname' )->nullable();
      $table->string( 'email' )->unique();
      $table->string( 'password' );
      $table->string( 'address' )->nullable();
      $table->string( 'post_code' )->nullable();
      $table->integer( 'town_id' )->unsigned()->nullable();
      $table->integer( 'language_id' )->unsigned()->default( 1 );
      $table->rememberToken();
      $table->timestamps();

      $table->foreign( 'town_id' )->references( 'id' )->on( 'towns' );
      $table->foreign( 'language_id' )->references( 'id' )->on( 'languages' );
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {

    Schema::table( 'users', function ( Blueprint $table ) {

      $table->dropForeign( 'users_town_id_foreign' );
      $table->dropForeign( 'users_language_id_foreign' );
    } );

    Schema::drop( 'users' );
  }
}
