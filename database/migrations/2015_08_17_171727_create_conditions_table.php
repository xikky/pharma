<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConditionsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {

    Schema::create( 'conditions', function ( Blueprint $table ) {

      $table->increments( 'id' )->unsigned();
      $table->string( 'name' )->unique();
      $table->text( 'description' )->nullable();
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {

    Schema::drop( 'conditions' );
  }
}
