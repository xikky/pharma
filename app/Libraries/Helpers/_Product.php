<?php

namespace App\Libraries\Helpers;

use App\Barcode;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductBarcodeExistsRequest;
use App\Http\Requests\ProductBarcodeMatchRequest;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class _Product extends Controller {

  protected $productList = null;
  protected $user = null;

  public function __construct () {

    $this->productList = new _ProductList();
    $this->user = new _User();
  }

  public function check ( ProductBarcodeMatchRequest $request ) {

    $data = [
      'messages'  => [ trans( 'patient.barcode_success' ) ],
      'type'      => 'success',
      'important' => false
    ];

    return Response::json( [
                             'message' => (string)View::make( 'partials.flash_response', $data )
                           ], 200 );
  }

  public function add ( ProductBarcodeExistsRequest $request ) {

    $product = product::where( 'barcode', '=', $request[ 'barcode' ] )->first();

    $this->productList->addproduct( $product[ 'id' ] );
    $this->productList->incrementTotals( $product[ 'price' ] );

    $totals = $this->productList->getDisplayTotals();

    $data = [
      'id'          => $product[ 'id' ],
      'count'       => $this->productList->getproductCount( $product[ 'id' ] ),
      'name'        => $product[ 'name' ],
      'description' => $product[ 'description' ],
      'quantity'    => $this->productList->getproductQuantity( $product[ 'id' ] ),
      'price'       => number_format( ( $product[ 'price' ] / 100 ), 2 ),
      'total'       => number_format( ( ( $product[ 'price' ] * $this->productList->getproductQuantity( $product[ 'id' ] ) ) / 100 ), 2 )
    ];

    return Response::json( [
                             'row'    => (string)View::make( 'patient.partials.product_row', $data ),
                             'id' => $product[ 'id' ],
                             'totals' => $totals
                           ], 200 );
  }

  public function remove ( Request $request ) {

    if ( $this->productList->getproductByID( $request[ 'product' ] ) ) {

      $quantity = $this->productList->removeproduct( $request[ 'product' ] );

      $product = Product::find( $request[ 'product' ] );

      $this->productList->decrementTotals( $product[ 'price' ], $quantity );

      $totals = $this->productList->getDisplayTotals();

      $empty_row = '';

      if ( $request[ 'rows' ] == 1 )
        $empty_row = (string)View::make( 'patient.partials.product_row_empty' );

      return Response::json( [
                               'id' => $request[ 'product' ],
                               'empty_row' => $empty_row,
                               'totals'    => $totals
                             ], 200 );
    }
  }

  public function refresh ( Request $request ) {

    $empty_row = '';
    $removed = $this->productList->updateProducts( $request[ 'products' ] );

    if ( empty( $this->productList->getProducts() ) ) {

      $empty_row = (string)View::make( 'patient.partials.product_row_empty' );
    }

    $data = [
      'messages' => [ trans( 'patient.refresh_product_success' ) ],
      'type'      => 'success',
      'important' => false
    ];

    return Response::json( [
                             'message'      => (string)View::make( 'partials.flash_response', $data ),
                             'total_prices' => $this->productList->getTotalPrices(),
                             'empty_row'    => $empty_row,
                             'removed'      => $removed,
                             'totals'       => $this->productList->getDisplayTotals()
                           ], 200 );
  }

  public function buy () {

    if ( $this->productList->checkPending() ) {

      $this->productList->buyProducts();

      Session::forget( 'user' );
      Session::forget( 'patient_products' );
      Session::forget( 'patient_products_total' );

      $data = [
        'messages' => [ trans( 'patient.cart_products_success' ) ],
        'type'      => 'success',
        'important' => true
      ];

      $status = 200;
    }
    else {

      $data = [
        'messages' => [ trans( 'patient.cart_products_fail' ) ],
        'type'      => 'danger',
        'important' => false
      ];

      $status = 400;
    }

    return Response::json( [
                             'message'   => (string)View::make( 'partials.flash_response', $data ),
                             'empty_row' => (string)View::make( 'patient.partials.product_row_empty' ),
                             'totals'    => (string)View::make( 'patient.partials.product_totals' )
                           ], $status );
  }
}