<?php

namespace App\Libraries\CustomValidations;

use App\User;
use Illuminate\Support\Facades\Input;

class UserExists {

  public function validate ( $attribute, $value, $parameters ) {

    $value .= Input::get( $parameters[ 0 ] );

    $user = User::where( 'nin', '=', $value )->first();

    if ( empty( $user ) )
      return false;

    return true;
  }
}
