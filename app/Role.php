<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

  public $timestamps = false;
  protected $table = 'roles';
  protected $fillable = [ 'name' ];

  public function users () {

    return $this->belongsToMany( 'App\User' );
  }

  public function menus () {

    return $this->belongsToMany( 'App\Menu' );
  }
}
