<?php

return [

  'patient'             => 'Patient',
  'patient_products'    => 'Products',
  'patient_search'      => 'Search',
  'patient_history'     => 'Medical History',
  'patient_flush'       => 'Clear',
  'prescription'        => 'Prescription',
  'prescription_create' => 'Create',
];