<?php

namespace App\Libraries\Helpers;

use App\User;
use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class _Export extends Controller {

  public function export () {

    $action = Input::get( 'action' );

    $filter = $this->getFilterArray( json_decode( Input::get( 'filter' ) ) );
    $sort = $this->getSortArray( json_decode( Input::get( 'sort' ) ) );

    switch ( $action ) {
      case 'private-prescriptions' :
        return $this->exportPvtPrescriptions( $filter, $sort );
      case 'poyc-prescriptions' :
        return $this->exportPoycPrescriptions( $filter, $sort );
      case 'patient-products' :
        return $this->exportPatientproducts( $filter, $sort );
      case 'patient-conditions' :
        return $this->exportPatientConditions( $filter, $sort );
    }
  }

  private function getFilterArray ( $array ) {

    $filter = [ ];

    foreach ( $array as $key => $value ) {

      if ( !empty( $value ) )
        $filter[ $key ] = $value;
    }

    return $filter;
  }

  private function getSortArray ( $array ) {

    $sort = [ ];

    foreach ( $array as $key => $value ) {

      $sort[ $value[ 0 ] ] = $value[ 1 ];
    }

    return $sort;
  }

  private function exportPvtPrescriptions ( $filter, $sort ) {

    $user = User::find( Input::get( 'user' ) );

    $prescriptions = $user->patient()->first()->prescriptions()->private()
      ->productInfo()
      ->whereDateDoctorproduct( $filter )
                          ->orderByDispenseDateDoctor( $sort )
                          ->groupByPrescription()
                          ->get();
    $details = [ ];

    foreach ( $prescriptions as $prescription ) {

      $details[] = [
        'prescription' => $prescription->toArray(),
        'products' => $prescription->products()->get()->toArray()
      ];
    }

    Excel::create( "$user->nin's Private Prescriptions " . Carbon::now()->format( 'd-m-Y' ), function ( $excel ) use ( $details ) {

      $excel->setTitle( 'Private Prescriptions' )
            ->setDescription( 'A list of private prescriptions' )
            ->sheet( 'Prescriptions', function ( $sheet ) use ( $details ) {

              $sheet->loadView( 'excel.prescriptions', array( 'prescriptions' => $details ) );
            } );
    } )->download( 'xls' );
  }

  private function exportPoycPrescriptions ( $filter, $sort ) {

    $user = User::find( Input::get( 'user' ) );

    $prescriptions = $user->patient()->first()->prescriptions()->poyc()
      ->productInfo()
      ->whereDateDoctorproduct( $filter )
                          ->orderByDispenseDateDoctor( $sort )
                          ->groupByPrescription()
                          ->get();
    $details = [ ];

    foreach ( $prescriptions as $prescription ) {

      $details[] = [
        'prescription' => $prescription->toArray(),
        'products' => $prescription->products()->get()->toArray()
      ];
    }

    Excel::create( "$user->nin's POYC Prescriptions " . Carbon::now()->format( 'd-m-Y' ), function ( $excel ) use ( $details ) {

      $excel->setTitle( 'POYC Prescriptions' )
            ->setDescription( 'A list of POYC prescriptions' )
            ->sheet( 'Prescriptions', function ( $sheet ) use ( $details ) {

              $sheet->loadView( 'excel.prescriptions', array( 'prescriptions' => $details ) );
            } );
    } )->download( 'xls' );
  }

  private function exportPatientproducts ( $filter, $sort ) {

    $user = User::find( Input::get( 'user' ) );
    $patient = $user->patient()->first();
    $products = $patient->productsInfo( $patient[ 'id' ] )->whereDateproductPharmacist( $filter );

    $products = $products->orderByDateproductPharmacist( $sort )->get()->toArray();

    $product_list = [ ];

    foreach ( $products as $product ) {

      $product_list[] = [
        'submit_at'    => $product[ 'patient_product_date' ],
        'product_name' => $product[ 'product_name' ],
        'pharma_name'  => $product[ 'pharmacist_name' ]
      ];
    }

    Excel::create( "$user->nin's product List " . Carbon::now()->format( 'd-m-Y' ), function ( $excel ) use ( $product_list ) {

      $excel->setTitle( 'product List' )
            ->setDescription( 'A list products bought' )
            ->sheet( 'products', function ( $sheet ) use ( $product_list ) {

              $sheet->loadView( 'excel.productlist', array( 'products' => $product_list ) );
            } );
    } )->download( 'xls' );
  }

  private function exportPatientConditions ( $filter, $sort ) {

    $user = User::find( Input::get( 'user' ) );

    $conditions = $user->patient()->first()->conditions();
    $conditions = $conditions->conditionInfo()
                             ->whereDateNameDoctor( $filter )
                             ->orderByDateNameDoctor( $sort )
                             ->get();

    $condition_list = [ ];

    foreach ( $conditions as $condition ) {

      $condition_list[] = [
        'created_at'  => $condition[ 'condition_patient_created_at' ],
        'name'        => $condition[ 'condition_name' ],
        'fullname'    => $condition[ 'doctor_fullname' ],
        'description' => $condition[ 'condition_description' ]
      ];
    }

    Excel::create( "$user->nin's Condition History " . Carbon::now()->format( 'd-m-Y' ), function ( $excel ) use ( $condition_list ) {

      $excel->setTitle( 'Condition History' )
            ->setDescription( 'The history of conditions for the patient' )
            ->sheet( 'Conditions', function ( $sheet ) use ( $condition_list ) {

              $sheet->loadView( 'excel.conditions', array( 'conditions' => $condition_list ) );
            } );
    } )->download( 'xls' );
  }
}