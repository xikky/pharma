<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class ProductPrescriptionRequest extends Request {

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize () {

    if ( Auth::check() ) {

      $roles = Auth::user()->roles()->lists( 'signature' )->toArray();

      if ( in_array( 'pharmacist', $roles ) ||
           in_array( 'admin', $roles ) ||
           in_array( 'moderator', $roles )
      )
        return true;
    }

    return false;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules () {

    $rules = [ ];

    if ( $this->request->get( 'barcode' ) ) {

      foreach ( $this->request->get( 'barcode' ) as $key => $val ) {

        $rules[ 'barcode.' . $key ] = 'required|product_barcode_match:product_id.' . $key;
      }
    }

    return $rules;
  }

  public function messages () {

    $messages = [ ];

    if ( $this->request->get( 'barcode' ) ) {

      foreach ( $this->request->get( 'barcode' ) as $key => $val ) {

        $messages[ 'barcode.' . $key . '.required' ] = 'patient.barcode_required';
        $messages[ 'barcode.' . $key . '.product_barcode' ] = 'patient.product_barcode_mismatch';
      }
    }

    return $messages;
  }

  public function response ( array $errors ) {

    $messages = [ ];
    $elements = [ ];

    foreach ( $errors as $key => $error ) {

      $keys = explode( '.', $key );

      foreach ( $error as $sub_error ) {

        if ( !array_key_exists( $sub_error, $messages ) )
          $messages[ $sub_error ] = trans_choice( $sub_error, 1 );
        else
          $messages[ $sub_error ] = trans_choice( $sub_error, 2 );

        $elements[ $keys[ 0 ] ][ $keys[ 1 ] ][] = $sub_error;
      }
    }

    if ( $this->ajax() ) {

      $data = [
        'messages'  => $messages,
        'type'      => 'danger',
        'important' => false
      ];

      return Response::json( [
                               'message'  => (string)View::make( 'partials.flash_response', $data ),
                               'elements' => $elements,
                             ], 400 );
    }

    return $this->redirector->to( $this->getRedirectUrl() )
                            ->withInput( $this->except( $this->dontFlash ) )
                            ->withErrors( $errors );
  }
}
