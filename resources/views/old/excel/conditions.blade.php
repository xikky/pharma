<table>
  <tr>
    <th>
      Date
    </th>
    <th>
      Doctor
    </th>
    <th>
      Condition
    </th>
  </tr>
  <tr></tr>
  @foreach( $conditions as $condition )
    <tr>
      <td>{{ \Carbon\Carbon::parse($condition['created_at'])->format('d-m-Y') }}</td>
      <td>{{ $condition['fullname'] }}</td>
      <td>{{ $condition['name'] }}</td>
    </tr>
    <tr>
      <td></td>
      <td colspan="2">
        {{ $condition['description'] }}
      </td>
    </tr>
  @endforeach
</table>