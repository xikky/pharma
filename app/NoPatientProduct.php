<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoPatientProduct extends Model {

  public $timestamps = false;

  protected $fillable = [ 'product_id', 'pharmacist_id', 'quantity', 'buy_at' ];
}
