<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanguagesTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {

    Schema::create( 'languages', function ( Blueprint $table ) {

      $table->increments( 'id' )->unsigned();
      $table->string( 'code' )->unique();
      $table->string( 'language' );
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {

    Schema::drop( 'languages' );
  }
}
