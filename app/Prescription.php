<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model {

  protected $dates = [ 'created_at' ];

  public function patient () {

    return $this->belongsTo( 'App\Patient' );
  }

  public function doctor () {

    return $this->belongsTo( 'App\Doctor' );
  }

  public function pharmacist () {

    return $this->belongsTo( 'App\Pharmacist' );
  }

  public function products () {

    return $this->belongsToMany( 'App\Product' )->withPivot( 'dosage', 'period', 'quantity', 'measurement' );
  }

  public function productsBought () {

    return $this->belongsToMany( 'App\Product' )->withPivot( 'dosage', 'period', 'quantity', 'measurement', 'submit_at' )->wherePivot( 'buy', '=', '1' );
  }

  public function scopePending ( $query ) {

    return $query->where( 'dispensed', '=', 'pending' );
  }

  public function scopeDispensed ( $query ) {

    return $query->where( 'dispensed', '=', 'yes' );
  }

  public function scopeNotDispensed ( $query ) {

    return $query->where( 'dispensed', '=', 'no' );
  }

  public function scopePrivate ( $query ) {

    return $query->where( 'prescription_type', '=', 'private' );
  }

  public function scopePoyc ( $query ) {

    return $query->where( 'prescription_type', '=', 'poyc' );
  }

  public function scopeGroupByPrescription ( $query ) {

    return $query->groupBy( 'prescriptions.id' );
  }

  public function scopeproductInfo ( $query ) {

    $bool_open = trans( 'patient.status_open' );
    $bool_close = trans( 'patient.status_close' );
    $bool_pending = trans( 'patient.status_pending' );

    return $query->select( "prescriptions.id",
                           "prescriptions.dispensed as prescription_dispensed",
                           "prescriptions.created_at as prescription_created_at",
                           "products.id as product_id",
                           "products.name as product_name",
                           "prescriptions.description" )
                 ->selectRaw( "CONCAT(users.firstname, ' ', users.lastname) as prescription_fullname" )
                 ->selectRaw( "CASE WHEN prescriptions.dispensed = 1 THEN '$bool_open'
                                WHEN prescriptions.dispensed = 2 THEN '$bool_close'
                                ELSE '$bool_pending'
                                END as prescription_dispensed_name" )
                 ->join( "doctors", "prescriptions.doctor_id", "=", "doctors.id" )
                 ->join( "users", "doctors.user_id", "=", "users.id" )
                 ->join( "prescription_product", "prescriptions.id", "=", "prescription_product.prescription_id" )
                 ->join( "products", "products.id", "=", "prescription_product.product_id" );
  }

  public function scopeWhereDateDoctorproduct ( $query, $filter_list ) {

    $bool_open = trans( 'patient.status_open' );
    $bool_close = trans( 'patient.status_close' );
    $bool_pending = trans( 'patient.status_pending' );

    if ( !empty( $filter_list ) ) {

      $attributes = [ ];

      foreach ( $filter_list as $key => $filter ) {

        if ( $key == 0 )
          $attributes[ 'created_at' ] = "DATE_FORMAT( prescriptions.created_at, '%d-%m-%Y' ) LIKE '$filter%'";
        else if ( $key == 1 )
          $attributes[ 'fullname' ] = "CONCAT(users.firstname, ' ', users.lastname) LIKE '%$filter%'";
        else if ( $key == 2 )
          $attributes[ 'product' ] = "products.name LIKE '%$filter%'";
        else if ( $key == 4 )
          $attributes[ 'dispensed' ] = "CASE WHEN prescriptions.dispensed = 1 THEN '$bool_open'
                                WHEN prescriptions.dispensed = 2 THEN '$bool_close'
                                ELSE '$bool_pending'
                                END LIKE '$filter%'";
      }

      $query->where( function ( $query ) use ( $attributes ) {

        foreach ( $attributes as $key => $value ) {

          $query->whereRaw( $value );
        }
      } );
    }

    return $query;
  }

  public function scopeOrderByDispenseDateDoctor ( $query, $sort_list ) {

    if ( !empty( $sort_list ) ) {

      foreach ( $sort_list as $key => $sort ) {

        if ( $sort == '0' )
          $order_by[ intval( $key ) ] = 'desc';
        else
          $order_by[ intval( $key ) ] = 'asc';
      }

      foreach ( $sort_list as $key => $sort ) {

        if ( $key == 0 )
          $query->orderBy( 'prescription_created_at', $order_by[ 0 ] );
        else if ( $key == 1 )
          $query->orderBy( 'prescription_fullname', $order_by[ 1 ] );
        else if ( $key == 4 )
          $query->orderBy( 'prescription_dispensed', $order_by[ 4 ] );
      }
    }
    else {

      $query->orderBy( 'prescription_dispensed', 'asc' )
            ->orderBy( 'prescription_created_at', 'asc' )
            ->orderBy( 'prescription_fullname', 'asc' );
    }

    return $query;
  }

  public function types () {

    return [
      'private',
      'poyc'
    ];
  }

  /*
  public function getCreatedAtAttribute ( $date ) {

    return Carbon::parse($date)->format('d-m-Y');
  }
  */
}
