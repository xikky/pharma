@extends('app')

@section('breadcrumbs')
  {!! Breadcrumbs::render('patient_search') !!}
@endsection

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        @include('partials.error')

        <div id="patient-search" class="content">
          <div class="panel panel-pharma">
            <div class="panel-heading">{{ trans('patient.patient_search') }}</div>
            <div class="panel-body">

              {!! Form::open(['action' => 'PharmacistController@patientPostSearch', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'GET' ]) !!}

              <div class="form-group">
                <label class="col-md-3 control-label">{{ trans('patient.patient_id') }}</label>

                <div class="col-md-5">
                  {!! Form::number( 'nin_number', old('nin_number'), array( 'class' => 'form-control form-number-spinner' ) ) !!}
                </div>
                <div class="col-md-2">
                  <select name="nin_country" class="form-control">
                    <option value="A">A</option>
                    <option value="G">G</option>
                    <option value="M" selected>M</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <button type="submit" class="btn btn-pharma-1">{{ trans('common.search') }}</button>
                </div>
              </div>

              {!! Form::close() !!}

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
