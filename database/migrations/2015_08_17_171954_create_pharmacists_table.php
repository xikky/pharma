<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePharmacistsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {

    Schema::create( 'pharmacists', function ( Blueprint $table ) {

      $table->increments( 'id' )->unsigned();
      $table->integer( 'user_id' )->unsigned();
      $table->string( 'name' )->nullable();
      $table->timestamps();

      $table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {

    Schema::table( 'pharmacists', function ( Blueprint $table ) {

      $table->dropForeign( 'pharmacists_user_id_foreign' );
    } );

    Schema::drop( 'pharmacists' );
  }
}
