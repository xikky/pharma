@extends('app')

@section('content')

  <div id="index">
    <div class="first-content">
      <div class="container">
        <div class="sub-content">
          <div class="row">
            <div class="col-sm-12 icon">
              <img src="{{ asset( 'assets/images/logo_black.png') }}" alt="{{ trans( 'common.logo_name' ) }}">
            </div>
            <div class="col-sm-12 welcome">
              <span>{{ trans( 'home.welcome' ) }}</span>
            </div>
          </div>
          <div class="row">
            <div class="welcome-btn">
              <div class="col-sm-6 login">
                <a href="{{ action('Auth\AuthController@getLogin') }}"
                   class="btn btn-lg btn-pharma-2">{{ trans( 'common.login' ) }}</a>
              </div>
              <div class="col-sm-6 register">
                <a href="{{ action('Auth\AuthController@getRegister') }}"
                   class="btn btn-lg btn-pharma-2">{{ trans( 'common.register' ) }}</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="second-content">
      <div class="container">
        <div class="sub-content">
          <div class="row">

          </div>
        </div>
      </div>
    </div>
  </div>

@endsection