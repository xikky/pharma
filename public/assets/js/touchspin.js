$( "input.touchspin" ).TouchSpin( {
  verticalbuttons : true,
  min             : 0,
  max             : 999,
} );

$( '#patient-products #products tbody' ).on( 'click', '.quantity, .bootstrap-touchspin-up, .bootstrap-touchspin-down', function ( e ) {

  e.stopPropagation();
} );