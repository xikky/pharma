<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConditionPatientTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {

    Schema::create( 'condition_patient', function ( Blueprint $table ) {

      $table->increments( 'id' )->unsigned();
      $table->integer( 'patient_id' )->unsigned();
      $table->integer( 'doctor_id' )->unsigned();
      $table->integer( 'condition_id' )->unsigned();
      $table->text( 'description' )->nullable();
      $table->timestamps();

      $table->foreign( 'patient_id' )->references( 'id' )->on( 'patients' );
      $table->foreign( 'doctor_id' )->references( 'id' )->on( 'doctors' );
      $table->foreign( 'condition_id' )->references( 'id' )->on( 'conditions' );
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {

    Schema::table( 'condition_patient', function ( Blueprint $table ) {

      $table->dropForeign( 'condition_patient_patient_id_foreign' );
      $table->dropForeign( 'condition_patient_doctor_id_foreign' );
      $table->dropForeign( 'condition_patient_condition_id_foreign' );
    } );

    Schema::drop( 'condition_patient' );
  }
}
