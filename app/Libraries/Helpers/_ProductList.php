<?php

namespace App\Libraries\Helpers;

use App\NoPatientProduct;
use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class _ProductList {

  public function getTotalPrices () {

    $patient_products = $this->getProducts();

    $total = [ ];

    foreach ( $patient_products as $key => $patient_product ) {

      $product = Product::find( $key );
      $total[ $key ] = Config::get( 'config.currency' )[ 'symbol' ] . number_format( ( $patient_product[ 'quantity' ] * $product[ 'price' ] ) / 100, 2 );
    }

    return $total;
  }

  public function getProducts () {

    return Session::get( 'patient_products', [ ] );
  }

  public function getProductByID ( $product_id ) {

    $patient_products = $this->getProducts();

    return $patient_products[ $product_id ];
  }

  public function getProductCount ( $product_id ) {

    $patient_products = $this->getProducts();

    return $patient_products[ $product_id ][ 'count' ];
  }

  public function getProductQuantity ( $product_id ) {

    $patient_products = $this->getProducts();

    return $patient_products[ $product_id ][ 'quantity' ];
  }

  public function updateProducts ( $products ) {

    $patient_products = $this->getProducts();
    $removed = [ ];

    foreach ( $products as $product => $quantity ) {

      if ( $quantity <= 0 ) {

        $this->removeProduct( $product );
        $this->decrementTotals( Product::find( $product )->price, $patient_products[ $product ][ 'quantity' ] );
        $removed[] = $product;
      }
      else {

        $new_quantity = $quantity - $patient_products[ $product ][ 'quantity' ];
        $this->addProduct( $product, $new_quantity );
        $this->incrementTotals( Product::find( $product )->price, $new_quantity );
      }
    }

    if ( !empty( $removed ) )
      $this->resetProductCount();

    return $removed;
  }

  public function removeProduct ( $product_id ) {

    $patient_products = $this->getProducts();

    $quantity = $patient_products[ $product_id ][ 'quantity' ];

    unset( $patient_products[ $product_id ] );

    $this->setProducts( $patient_products );

    $this->resetProductCount();

    return $quantity;
  }

  public function setProducts ( $products ) {

    Session::put( 'patient_products', $products );
  }

  public function resetProductCount () {

    $patient_products = $this->getProducts();

    $counter = 1;

    foreach ( $patient_products as $key => $patient_product )
      $patient_product[ $key ][ 'count' ] = $counter++;

    $this->setProducts( $patient_products );
  }

  public function decrementTotals ( $price, $quantity ) {

    $totals = $this->getTotals();

    $totals[ 'quantity' ] -= $quantity;
    $totals[ 'total' ] = number_format( $totals[ 'total' ] - ( ( $price * $quantity ) / 100 ), 2 );

    $this->setTotals( $totals );
  }

  public function getTotals () {

    return Session::get( 'patient_products_total', [ 'quantity' => 0, 'total' => 0 ] );
  }

  public function setTotals ( $totals ) {

    Session::put( 'patient_products_total', $totals );
  }

  public function addProduct ( $product_id, $quantity = 1 ) {

    $patient_products = $this->getProducts();

    if ( array_key_exists( $product_id, $patient_products ) )
      $patient_products[ $product_id ][ 'quantity' ] += $quantity;
    else
      $patient_products[ $product_id ] = [ 'count' => ( count( $patient_products ) + 1 ), 'quantity' => $quantity ];

    $this->setProducts( $patient_products );
  }

  public function incrementTotals ( $price, $quantity = 1 ) {

    $totals = $this->getTotals();

    $totals[ 'quantity' ] += $quantity;
    $totals[ 'total' ] = number_format( $totals[ 'total' ] + ( ( $price * $quantity ) / 100 ), 2 );

    $this->setTotals( $totals );
  }

  public function getDisplayTotals () {

    $totals = $this->getTotals();

    $totals[ 'total' ] = Config::get( 'config.currency' )[ 'symbol' ] . $totals[ 'total' ];

    return $totals;
  }

  public function checkPending () {

    $user = new _User();

    if ( !empty( $user->getSession() ) ) {

      $pending_prescriptions = User::where( 'nin', '=', $user->getSession() )->first()
                                   ->patient()->first()
                                   ->prescriptions()->pending();

      $pending_products = $pending_prescriptions->productInfo()->get()->toArray();

      $prescription_products = [ ];

      foreach ( $pending_products as $pending_product ) {

        if ( array_key_exists( $pending_product[ 'product_id' ], $prescription_products ) )
          $prescription_products[ $pending_product[ 'product_id' ] ]++;
        else
          $prescription_products[ $pending_product[ 'product_id' ] ] = 1;
      }

      $cart_products = $this->getProducts();

      foreach ( $prescription_products as $drug => $quantity ) {

        if ( !array_key_exists( $drug, $cart_products ) )
          return false;

        if ( $quantity > $cart_products[ $drug ][ 'quantity' ] )
          return false;
      }
    }

    return true;
  }

  public function buyProducts () {

    $user = new _User();

    if ( !empty( $user->getSession() ) ) {

      User::where( 'nin', '=', $user->getSession() )->first()->patient()->first()
          ->prescriptions()->pending()->get()->each( function ( $prescription ) {

          $prescription->dispensed = 2;
          $prescription->save();
        } );

      foreach ( $this->getProducts() as $key => $product ) {

        User::where( 'nin', '=', $user->getSession() )->first()->patient()->first()
            ->products()->attach(
            [ $key =>
                [ 'pharmacist_id' => Auth::user()->pharmacist()->first()->id,
                  'quantity'      => $product[ 'quantity' ],
                  'buy_at'        => Carbon::now()
                ]
            ] );
      }
    }
    else {

      foreach ( $this->getProducts() as $key => $product ) {

        NoPatientProduct::create( [
                                    'product_id'    => $key,
                                    'pharmacist_id' => Auth::user()->pharmacist()->first()->id,
                                    'quantity'      => $product[ 'quantity' ],
                                    'buy_at'        => Carbon::now()
                                  ] );
      }
    }
  }
}