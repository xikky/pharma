@extends('app')

@section('content')

  <div id="login" class="sub-body">
    <div id="push-content"></div>
    <div id="center-content">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">

            @include('partials.error')

            <div class="content">

              <div class="page-title">Login</div>

              <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                  <label class="col-xs-4 control-label">{{ trans('common.username') }}</label>

                  <div class="col-xs-6">
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-xs-4 control-label">{{ trans('common.password') }}</label>

                  <div class="col-xs-6">
                    <input type="password" class="form-control" name="password">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-xs-6 col-xs-offset-4">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="remember"> Remember Me
                      </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-xs-6 col-xs-offset-4">
                    <button type="submit" class="btn btn-pharma-1">Login</button>

                    <div class="btn btn-pharma-link">
                      <a href="{{ url('/password/email') }}">Forgot Your Password?</a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
