<?php namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class NavigationMenuComposer {

  public function compose ( View $view ) {

    $menu = [ ];

    if ( Auth::check() ) {

      $menu_list = Auth::user()->menu();

      foreach ( $menu_list as $key => $menu_item ) {

        if ( !empty( $menu_item[ 'parameters' ] ) ) {

          $parameter_list = [ ];
          $parameters = explode( '|', $menu_item[ 'parameters' ] );

          foreach ( $parameters as $parameter ) {

            $parameter_list[ $parameter ] = Session::get( $parameter, null );
          }

          $menu_list[ $key ][ 'parameters' ] = $parameter_list;
        }
        else
          $menu_list[ $key ][ 'parameters' ] = [ ];
      }

      foreach ( $menu_list as $menu_item ) {

        if ( $menu_item[ 'parent' ] == $menu_item[ 'id' ] )
          $menu[ $menu_item[ 'id' ] ] = $menu_item;
      }

      foreach ( $menu_list as $menu_item ) {

        if ( $menu_item[ 'parent' ] != $menu_item[ 'id' ] )
          $menu[ $menu_item[ 'parent' ] ][ 'submenu' ][] = $menu_item;
      }

      $menu = array_values( array_sort( $menu, function ( $value ) {

        return $value[ 'order' ];
      } ) );

      foreach ( $menu as $menu_item ) {

        if ( array_key_exists( 'submenu', $menu_item ) ) {
          $menu_item[ 'submenu' ] = array_values( array_sort( $menu_item[ 'submenu' ], function ( $value ) {

            return $value[ 'order' ];
          } ) );
        }
      }
    }

    $active = Session::get( 'active_menu', '' );
    $active_sub = Session::get( 'active_submenu', '' );

    $view->with( [
                   'menu'   => $menu,
                   'active'     => $active,
                   'active_sub' => $active_sub
                 ] );
  }
}