<table id="tbl-patient-conditions" data-sortlist="[[0,0]]" class="tablesorter tablesorter-row-toggle tablesorter-patient-conditions">
  {!! Form::hidden( '_token', csrf_token(), ['class' => 'patient-conditions-token'] ) !!}
  <thead>
  <tr>
    <th class="tbl-col-date">{{ trans( 'patient.date' ) }}</th>
    <th class="tbl-col-doctor">{{ trans( 'patient.doctor' ) }}</th>
    <th class="tbl-col-condition">{{ trans( 'patient.condition_name' ) }}</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
</table>

<div id="pager-patient-conditions" class="pager">
  @include('partials.pager')
</div>