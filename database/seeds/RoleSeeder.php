<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $roles = [
      'Admin', 'Moderator', 'Pharmacist', 'Doctor', 'Patient'
    ];

    foreach ( $roles as $role ) {

      Role::create( [
                      'signature' => strtolower( $role ),
                      'name' => $role
                    ] );
    }
  }
}
