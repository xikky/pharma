var elixir = require( 'laravel-elixir' );

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir( function ( mix ) {

  mix
    .sass( [
             'bootstrap.scss',
             'index.scss',
             'home.scss',
             'common.scss',
             'tablesorter.scss',
             'print.scss',
             'patient.scss',
             'prescription.scss',
           ]
    )
    .styles( [
               'assets/fonts/raleway.css',
               'assets/lib/bootstrap/css/bootstrap.min.css',
               'assets/lib/tablesorter/css/theme.bootstrap.min.css',
               'assets/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css',
               'css/app.css',
             ], 'public/css/style.css', 'public'
    )
    .scripts( [
                'assets/lib/jQuery/jquery-2.1.4.min.js',
                'assets/lib/bootstrap/js/bootstrap.min.js',
                'assets/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js',
                'assets/lib/tablesorter/js/jquery.tablesorter.min.js',
                'assets/lib/tablesorter/js/jquery.tablesorter.widgets.min.js',
                'assets/lib/tablesorter/js/widgets/widget-pager.min.js',
                'assets/lib/handlebars/handlebars-v4.0.2.js',
                'assets/js/tablesorter.js',
                'assets/js/accordion.js',
                'assets/js/touchspin.js',
                'assets/js/dom.js',
                'assets/js/product-add.js',
                'assets/js/product-skip.js',
                'assets/js/product-prescription.js',
                'assets/js/product-barcode-validate.js',
                'assets/js/product-remove.js',
                'assets/js/product-refresh.js',
                'assets/js/product-change-quantity.js',
                'assets/js/product-buy.js',
                'assets/js/print-prescription.js',
              ], 'public/js/script.js', 'public'
    )
    .version( [ '/css/style.css', '/js/script.js' ] );

} );
