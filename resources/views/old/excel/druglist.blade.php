<table>
  <tr>
    <th>
      Date
    </th>
    <th>
      product
    </th>
    <th>
      Pharmacy
    </th>
  </tr>
  <tr></tr>
  @foreach( $products as $product )
    <tr>
      <td>{{ \Carbon\Carbon::parse($product['submit_at'])->format('d-m-Y') }}</td>
      <td>{{ $product['product_name'] }}</td>
      <td>{{ $product['pharma_name'] }}</td>
    </tr>
  @endforeach
</table>