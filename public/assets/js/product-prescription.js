$( '.tablesorter-validate-prescriptions' ).on( 'click', '.product-prescription', function () {

  var table = $( this ).closest( 'table' );
  var product_barcodes = $( this ).closest( 'tr' ).prevUntil( '.tablesorter-toggle' );
  var ordered_product_barcodes = {};
  var barcodes = {};
  var product_ids = {};

  $.each( product_barcodes, function ( index, value ) {

    ordered_product_barcodes[ index ] = {};

    if ( !$( this ).find( '.pharma-skip-disable' )[ 0 ] )
      ordered_product_barcodes[ index ][ 0 ] = value;
    else {

      ordered_product_barcodes[ index ][ 0 ] = value;
      ordered_product_barcodes[ index ][ 1 ] = 0;
    }
  } );

  $.each( ordered_product_barcodes, function ( index, value ) {

    if ( value[ 1 ] != 0 ) {

      var barcode = $( value[ 0 ] ).find( '.product-barcode' ).val();
      barcodes[ index ] = barcode;
    }

    var product_id = $( value[ 0 ] ).find( '.product-barcode' ).attr( 'product-id' );
    product_ids[ index ] = product_id;
  } );

  data = {
    barcode : barcodes,
    product_id : product_ids,
    _token  : $( this ).next().val(),
    id      : $( this ).next().next().val()
  };

  $.ajax( {
    method   : "POST",
    url      : '/ajax/patient/details/updates',
    data     : data,
    success  : function ( data ) {

      table.trigger( "update" ).trigger( "appendCache" ).trigger( "applyWidgets" );
      table.trigger( 'pagerUpdate' );
      $( '.top-message' ).html( data[ 'message' ] );
    },
    error    : function ( data ) {

      data = data[ 'responseJSON' ];

      $( '.top-message' ).html( data[ 'message' ] );

      $.each( data[ 'elements' ][ 'barcode' ], function ( index ) {

        $( ordered_product_barcodes[ index ][ 0 ] ).find( '.product-barcode-parent' ).removeClass( 'has-success' );
        $( ordered_product_barcodes[ index ][ 0 ] ).find( '.product-barcode-parent' ).addClass( 'has-error' );
        $( ordered_product_barcodes[ index ][ 0 ] ).find( '.product-barcode-status span' ).css( 'display', 'none' );
        $( ordered_product_barcodes[ index ][ 0 ] ).find( '.product-barcode-status .remove' ).css( 'display', 'inline-block' );
      } );
    },
    dataType : 'json'
  } );
} );