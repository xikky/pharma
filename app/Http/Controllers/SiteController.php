<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class SiteController extends Controller {

  public function language ( $code, $language ) {

    Session::put( 'preferred_language', [ 'code' => $code, 'language' => $language ] );

    return Redirect::back();
  }
}
