<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNoPatientProductsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {

    Schema::create( 'no_patient_products', function ( Blueprint $table ) {

      $table->increments( 'id' )->unsigned();
      $table->integer( 'product_id' )->unsigned()->index();
      $table->integer( 'pharmacist_id' )->unsigned()->index();
      $table->integer( 'quantity' );
      $table->timestamp( 'buy_at' );

      $table->foreign( 'product_id' )->references( 'id' )->on( 'products' );
      $table->foreign( 'pharmacist_id' )->references( 'id' )->on( 'pharmacists' );
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {

    Schema::drop( 'no_patient_products', function ( Blueprint $table ) {

      $table->dropForeign( 'patient_product_product_id_foreign' );
      $table->dropForeign( 'patient_product_pharmacist_id_foreign' );
    } );
  }
}
