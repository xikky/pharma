$( '#patient-products #products' ).on( 'click', '.remove', function ( e ) {

  e.stopPropagation();

  data = {
    product : $( this ).closest( 'tr' ).attr( 'data-id' ),
    rows   : $( '.product-item' ).length,
    _token : $( this ).next().val()
  };

  $.ajax( {
    method   : "POST",
    url      : '/ajax/remove-product',
    data     : data,
    success  : function ( data ) {

      $( '.barcode-' + data[ 'id' ] ).remove();

      if ( $( '.product-item' ).length <= 0 ) {

        if ( !$( '.product-buy-parent button' ).hasClass( 'disabled' ) ) {

          $( '.product-buy-parent button' ).addClass( 'disabled' );
          $( '.product-buy-parent button' ).prop( 'disabled', true );
        }

        $( '#products #product-items' ).html( data[ 'empty_row' ] );
      }
      else {

        $( '.product-item .count' ).each( function ( index ) {

          $( this ).html( index + 1 );
        } );
      }

      $( '#product-totals .quantity' ).html( data[ 'totals' ][ 'quantity' ] );
      $( '#product-totals .total' ).html( data[ 'totals' ][ 'total' ] );
    },
    dataType : 'json'
  } );
} );