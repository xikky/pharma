<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get( '/', 'PageController@index' );

Route::controllers( [
                      'auth'     => 'Auth\AuthController',
                      'password' => 'Auth\PasswordController'
                    ] );

Route::get( 'language/{code}/{language}', 'SiteController@language' );

Route::group( [ 'middleware' => 'auth' ], function () {

  Route::get( 'home', 'PageController@home' );
  Route::get( 'account/{name}', 'UserController@show' );

  Route::group( [ 'middleware' => 'pharmacist' ], function () {

    Route::get( 'patient', [ 'as' => 'patient', 'uses' => 'MenuController@patient' ] );
    Route::get( 'patient/products/{nin?}', [ 'as' => 'patient_products', 'uses' => 'MenuController@patientProducts' ] );
    Route::get( 'patient/search', [ 'as' => 'patient_search', 'uses' => 'MenuController@patientSearch' ] );
    Route::get( 'patient/medical-history/{nin?}', [ 'as' => 'patient_history', 'uses' => 'MenuController@patientHistory' ] );
    Route::get( 'patient/clear', [ 'as' => 'patient_flush', 'uses' => 'MenuController@patientFlush' ] );

    Route::get( 'patient/post-search', 'PharmacistController@patientPostSearch' );

    Route::post( 'ajax/patient/details/updates', 'PharmacistController@updatePrescription' );
    Route::post( 'ajax/buy-product', '\App\Libraries\Helpers\_Product@buy' );
    Route::post( 'ajax/add-product', '\App\Libraries\Helpers\_Product@add' );
    Route::post( 'ajax/remove-product', '\App\Libraries\Helpers\_Product@remove' );
    Route::post( 'ajax/refresh-product', '\App\Libraries\Helpers\_Product@refresh' );
    Route::post( 'ajax/validate-product-barcode', '\App\Libraries\Helpers\_Product@check' );
    Route::post( 'ajax/tablesorter/{page}/{size}/{sort_list}/{filter_list}', '\App\Libraries\Helpers\_TableSorter@getPage' );

    Route::post( 'export', '\App\Libraries\Helpers\_Export@export' );
  } );

  Route::group( [ 'middleware' => 'doctor' ], function () {

    Route::get( 'post-prescription/{nin?}', 'MenuController@postPrescription' );
    Route::get( 'prescription/{nin?}', [ 'as' => 'prescription', 'uses' => 'MenuController@prescription' ] );

    Route::post( 'ajax/prescription-patient-search', 'PrescriptionController@patientSearch' );
    Route::post( 'ajax/prescription-patient-forget', 'PrescriptionController@patientForget' );
  } );
} );