@extends('app')

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        @include('partials.error')

        <div id="login" class="content">

          <div class="panel panel-pharma">
            <div class="panel-heading">Login</div>
            <div class="panel-body">

              <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                  <label class="col-md-4 control-label">{{ trans('common.username') }}</label>

                  <div class="col-md-6">
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label">{{ trans('common.password') }}</label>

                  <div class="col-md-6">
                    <input type="password" class="form-control" name="password">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="remember"> Remember Me
                      </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-pharma-1">Login</button>

                    <a class="btn btn-pharma-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
