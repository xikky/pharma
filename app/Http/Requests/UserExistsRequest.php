<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;

class UserExistsRequest extends Request {

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize () {

    if ( Auth::check() ) {

      $roles = Auth::user()->roles()->lists( 'signature' )->toArray();

      if ( in_array( 'pharmacist', $roles ) ||
           in_array( 'doctor', $roles ) ||
           in_array( 'admin', $roles ) ||
           in_array( 'moderator', $roles )
      )
        return true;
    }

    return false;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules () {

    return [
      'nin_number' => 'required|numeric|user-exists:nin_country|user-type:patient,nin_country',
      'nin_country' => 'required',
    ];
  }

  public function attributes () {

    return [
      'nin_number' => trans( 'pharmacist.patient_id' )
    ];
  }
}
