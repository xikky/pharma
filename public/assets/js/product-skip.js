$( '.tablesorter-validate-prescriptions' ).on( 'click', '.product-barcode-skip', function ( e ) {

  e.stopPropagation();

  var barcode = $( this ).parentsUntil( 'tbody' ).find( '.product-barcode' );

  if ( $( this ).attr( 'data-click-state' ) == 1 ) {

    $( this ).attr( 'data-click-state', 0 );

    $( barcode ).prop( 'disabled', false );
    $( barcode ).removeClass( 'pharma-skip-disable' );

    $( this ).parentsUntil( 'tbody' ).find( '.product-barcode-status .flash' ).css( 'display', 'none' );
  }
  else {

    $( this ).attr( 'data-click-state', 1 );

    $( barcode ).prop( 'disabled', true );
    $( barcode ).addClass( 'pharma-skip-disable' );
    $( barcode ).parent().removeClass( 'has-success' );
    $( barcode ).parent().removeClass( 'has-error' );
    $( barcode ).val( '' );

    $( this ).parentsUntil( 'tbody' ).find( '.product-barcode-status span' ).css( 'display', 'none' );
    $( this ).parentsUntil( 'tbody' ).find( '.product-barcode-status .flash' ).css( 'display', 'inline-block' );
  }

  var antonym = $( this ).attr( 'antonym' );

  $( this ).attr( 'antonym', $( this ).val() );
  $( this ).val( antonym );
} );