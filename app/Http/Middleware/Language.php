<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Language {

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Closure $next
   *
   * @return mixed
   */
  public function handle ( $request, Closure $next ) {

    $lang = Session::get( 'preferred_language', Config::get( 'config.language' ) );

    if ( Auth::check() && !Session::has( 'preferred_language' ) )
      $lang = Auth::user()->language->toArray();

    App::setLocale( $lang[ 'code' ] );

    return $next( $request );
  }
}
