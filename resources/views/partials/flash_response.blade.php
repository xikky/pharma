@foreach($messages as $message)
  <div class="alert-pharma {{ $important ? 'alert-important' : '' }}">
    <div class="alert alert-{{ $type }} alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close" aria-hidden="true">
        <span aria-hidden="true">&times;</span>
      </button>

      {{ $message }}
    </div>
  </div>
@endforeach