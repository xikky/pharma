@extends('app')

@section('content')

  <div id="register" class="sub-body">
    <div id="push-content"></div>
    <div id="center-content">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">

            @include('partials.error')

            <div class="content">

              <div class="page-title">Register</div>

              <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                  <label class="col-xs-4 control-label">{{ trans('common.username') }}</label>

                  <div class="col-xs-6">
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-xs-4 control-label">{{ trans('common.email') }}</label>

                  <div class="col-xs-6">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-xs-4 control-label">{{ trans('common.password') }}</label>

                  <div class="col-xs-6">
                    <input type="password" class="form-control" name="password">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-xs-4 control-label">{{ trans('common.confirm_password') }}</label>

                  <div class="col-xs-6">
                    <input type="password" class="form-control" name="password_confirmation">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-xs-4 control-label">{{ trans('common.language') }}</label>

                  <div class="col-xs-6">
                    <select class="form-control" name="language">
                      @foreach ( $languages as $language )
                        @if( $language['code'] == 'en' )
                          <option value="{{ $language['id'] }}" selected>{{ $language['language'] }}</option>
                        @else
                          <option value="{{ $language['id'] }}">{{ $language['language'] }}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-xs-6 col-xs-offset-4">
                    <button type="submit" class="btn btn-pharma-1">
                      Register
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
