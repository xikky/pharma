@extends('app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        @include('partials.error')

        <div id="pass-reset" class="content">
          <div class="panel panel-pharma">
            <div class="panel-heading">Reset Password</div>
            <div class="panel-body">
              @if (session('status'))
                <div class="alert alert-success">
                  {{ session('status') }}
                </div>
              @endif

              <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                  <label class="col-md-4 control-label">{{ trans( 'common.email' ) }}</label>

                  <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-pharma-1">
                      Send Password Reset Link
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
