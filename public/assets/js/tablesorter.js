$( function () {

  //Table sorter bootstrap theme options
  $.tablesorter.themes.bootstrap = {
    // these classes are added to the table. To see other table classes available,
    // look here: http://getbootstrap.com/css/#tables
    table        : 'table table-hover table-striped',
    caption      : 'caption',
    // header class names
    header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
    sortNone     : '',
    sortAsc      : '',
    sortDesc     : '',
    active       : '', // applied when column is sorted
    hover        : '', // custom css required - a defined bootstrap style may not override other classes
    // icon class names
    icons        : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
    iconSortNone : 'bootstrap-icon-unsorted', // class name added to icon when column is not sorted
    iconSortAsc  : 'glyphicon glyphicon-chevron-up', // class name added to icon when column has ascending sort
    iconSortDesc : 'glyphicon glyphicon-chevron-down', // class name added to icon when column has descending sort
    filterRow    : '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
    footerRow    : '',
    footerCells  : '',
    even         : '', // even row zebra striping
    odd          : ''  // odd row zebra striping
  };

  //Tablesorter common options
  var tablesorter = {
    theme          : "bootstrap",
    cssAsc         : "tablesorter-headerAsc",
    cssDesc        : "tablesorter-headerDesc",
    cssHeader      : "tablesorter-header",
    cssChildRow    : "tablesorter-childRow",
    widthFixed     : true,
    headerTemplate : '{content} {icon}',
    widgets        : [ "uitheme", "zebra", "filter", "print", "pager" ],
    widgetOptions  : {
      filter_childRows           : false,
      filter_childByColumn       : false,
      filter_columnFilters       : true,
      filter_columnAnyMatch      : false,
      filter_cssFilter           : "form-control",
      filter_reset               : "button.reset",
      filter_filteredRow         : 'filtered',
      filter_hideEmpty           : true,
      filter_hideFilters         : true,
      filter_ignoreCase          : true,
      filter_liveSearch          : true,
      filter_searchDelay         : 500,
      filter_placeholder         : { search : '', select : '' },
      filter_searchFiltered      : true,
      filter_serversideFiltering : true,
      filter_startsWith          : false,
      pager_output               : '{startRow} to {endRow} of {totalRows}',
      pager_updateArrows         : true,
      pager_startPage            : 0,
      pager_size : 3,
      pager_savePages            : true,
      pager_removeRows           : false,
      pager_ajaxUrl              : '/ajax/tablesorter/{page}/{size}/{sortList:sort}/{filterList:filter}',
      pager_customAjaxUrl        : function ( table, url ) { return url; },
      pager_ajaxError            : null,
      pager_ajaxObject           : {
        dataType : 'json',
        type     : 'POST',
        data     : null
      },
      pager_ajaxProcessing       : function ( ajax ) {

        $( '#' + ajax[ 'id' ] ).find( 'tbody' ).empty();
        $( '#' + ajax[ 'id' ] ).find( 'tbody' ).append( ajax[ 'html' ] );
        $( '#' + ajax[ 'id' ] ).find( '.tablesorter-childRow td' ).hide();

        return [ ajax[ 'total' ], [], null ];
      },
      pager_css                  : {
        container : 'tablesorter-pager',    // class added to make included pager.css file work
        errorRow  : 'tablesorter-errorRow', // error information row (don't include period at beginning); styled in theme file
        disabled  : 'disabled'              // class added to arrows @ extremes (i.e. prev/first arrows "disabled" on first page)
      },
      pager_selectors            : {
        container   : '.pager',       // target the pager markup (wrapper)
        first       : '.first',       // go to first page arrow
        prev        : '.prev',        // previous page arrow
        next        : '.next',        // next page arrow
        last        : '.last',        // go to last page arrow
        gotoPage    : '.pagenum',    // go to page selector - select dropdown that sets the current page
        pageDisplay : '.pagedisplay', // location of where the "output" is displayed
        pageSize    : '.pagesize'     // page size selector - select dropdown that sets the "size" option
      }
    }
  };

  //Add tablesorter options from classes and apply tablesorter
  $.each( $( '.tablesorter' ), function ( index, value ) {

    var colspan = $( this ).find( 'th' ).length;
    var export_type = '';

    tablesorter.widgetOptions.pager_ajaxObject.data = null;
    tablesorter.headers = null;

    if ( $( this ).hasClass( 'tablesorter-pvt-prescriptions' ) ) {

      export_type = 'private-prescriptions';

      data = {
        action : 'tbl-pvt-prescriptions',
        user   : $( '#patient' ).val(),
        mode : $( '#mode' ).val(),
        _token : $( this ).find( '.pvt-prescription-token' ).val(),
      };

      tablesorter.widgetOptions.pager_ajaxObject.data = data;
      tablesorter.widgetOptions.pager_selectors.container = '#pager-pvt-prescriptions';
    }
    else if ( $( this ).hasClass( 'tablesorter-poyc-prescriptions' ) ) {

      export_type = 'poyc-prescriptions';

      data = {
        action : 'tbl-poyc-prescriptions',
        user   : $( '#patient' ).val(),
        mode : $( '#mode' ).val(),
        _token : $( this ).find( '.poyc-prescription-token' ).val(),
      };

      tablesorter.widgetOptions.pager_ajaxObject.data = data;
      tablesorter.widgetOptions.pager_selectors.container = '#pager-poyc-prescriptions';
    }
    else if ( $( this ).hasClass( 'tablesorter-patient-products' ) ) {

      export_type = 'patient-products';

      data = {
        action : 'tbl-patient-products',
        user   : $( '#patient' ).val(),
        _token : $( this ).find( '.patient-products-token' ).val(),
      };

      tablesorter.widgetOptions.pager_ajaxObject.data = data;
      tablesorter.widgetOptions.pager_selectors.container = '#pager-patient-products';
    }
    else if ( $( this ).hasClass( 'tablesorter-patient-conditions' ) ) {

      export_type = 'patient-conditions';

      data = {
        action : 'tbl-patient-conditions',
        user   : $( '#patient' ).val(),
        _token : $( this ).find( '.patient-conditions-token' ).val(),
      };

      tablesorter.widgetOptions.pager_ajaxObject.data = data;
      tablesorter.widgetOptions.pager_selectors.container = '#pager-patient-conditions';
    }

    if ( $( this ).hasClass( 'tablesorter-dis' ) ) {

      var headers = {};

      $.each( $( this ).attr( 'class' ).split( ' ' ), function ( index, value ) {

        if ( value.indexOf( 'dis-' ) >= 0 ) {

          var key = value.split( '-' )[ 1 ];

          headers[ key ] = {
            sorter : false
          }
        }
      } );

      tablesorter.headers = headers;
    }

    $( this )
      .bind( "sortStart", function () {

               spinner( $( this ), colspan );
             } )
      .bind( "filterStart", function () {

               spinner( $( this ), colspan );
             } )
      .bind( 'pageMoved', function () {

               spinner( $( this ), colspan );
             } )
      .tablesorter( tablesorter );

    $( this ).next().find( '.reset' ).click( function () {

      $( this ).closest( '.pager' ).prev().trigger( 'filterReset' );
    } );

    $( this ).next().find( '.export' ).click( function () {

      var form = $( this ).parent();
      var table = form.parent().prev();

      form.find( '.action' ).val( export_type );
      form.find( '.sort' ).val( JSON.stringify( table[ 0 ].config.sortList ) );
      form.find( '.filter' ).val( JSON.stringify( table[ 0 ].config.lastSearch ) );

      form[ 0 ].submit();
    } );
  } );

  function spinner ( table, colspan ) {

    var source = $( "#spinner-row-template" ).html();
    var template = Handlebars.compile( source );
    var context = { colspan : colspan, active : "active", color : "spinner-pharma-1" };
    var html = template( context );

    $( table ).find( 'tbody' ).html( html );
  }

} );