//Make sub-body fill Height
$( '.sub-body' ).css( 'min-height', $( window ).height() - $( 'nav' ).height() - 2 );

//Function call to center content vertically
function center_content () {

  var screen_height = $( window ).height() - $( 'nav' ).height() - 2;
  var content_height = $( '#center-content' ).height();
  console.log( content_height );
  console.log( screen_height );
  if ( content_height < screen_height )
    $( '#push-content' ).css( 'margin-top', ( screen_height - content_height ) / 2 );
}

center_content();

//Error and Warning Message Fade out
$( 'body' ).on( 'click focusout', '.flash-msg', function () {

  $( 'div.alert-success' ).not( '.alert-important' ).delay( 2000 ).slideUp( 300 );
  $( 'div.alert-danger' ).not( '.alert-important' ).delay( 2000 ).slideUp( 300 );
} );

//Tablesorter children rows
$( '.tablesorter-row-toggle' ).on( 'click', '.tablesorter-toggle', function () {

  $( this ).nextUntil( 'tr:not(.tablesorter-childRow)' ).find( 'td' ).toggle();
} );

//Tablesorter prescription products and details rows
$( '.tablesorter-row-toggle-prescription' ).on( 'click', '.tablesorter-toggle', function () {

  var rows = $( this ).nextUntil( 'tr.tablesorter-toggle' );

  $.each( rows, function ( index, value ) {

    if ( $( value ).hasClass( 'product' ) || $( value ).hasClass( 'prescription-description' ) || $( value ).hasClass( 'complete' ) )
      $( value ).find( 'td' ).toggle();
  } );
} );

$( '.tablesorter-row-toggle-prescription' ).on( 'click', '.product', function () {

  $( this ).next().find( 'td' ).toggle();
} );

//Patient product product description row
$( '#patient-products #products' ).on( 'click', '.product-item', function () {

  $( this ).next().find( 'td' ).toggle();
} );