<?php

return [

  'logo_name'        => 'Pharma',
  'login'            => 'Login',
  'register'         => 'Register',
  'username'         => 'Username',
  'email'            => 'E-mail Address',
  'password'         => 'Password',
  'confirm_password' => 'Confirm Password',
  'english'          => 'English',
  'maltese'          => 'Maltese',
  'language'         => 'Language',
  'signed_in_as'     => 'Signed in as',
  'logout'      => 'Logout',
  'search'      => 'Search',
  'forget'      => 'Forget',
  'woops'       => 'Woops!',
  'input_probs' => 'There were some problems with your input.',
  'no_patient'  => 'No patient available.'
];