@foreach( $rows as $row )
  <tr>
    <td>{{ \Carbon\Carbon::parse($row['submit_at'])->format('d-m-Y') }}</td>
    <td>{{ $row['product_name'] }}</td>
    <td>{{ $row['pharma_name'] }}</td>
  </tr>
@endforeach