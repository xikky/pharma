$( '#patient-products #products #product-items' ).on( 'change input', '.quantity', function () {

  $old_value = $( this ).attr( 'data-value' );

  if ( $old_value != $( this ).val() )
    $( this ).attr( 'data-refresh', '1' );
  else
    $( this ).attr( 'data-refresh', '0' );

  var refresh = false;

  $( '#products #product-items tr .quantity' ).each( function () {

    if ( $( this ).attr( 'data-refresh' ) == '1' ) {

      refresh = true;
      return false;
    }
  } );

  if ( refresh )
    $( '.refresh' ).removeClass( 'hidden' );
  else {

    if ( !$( '.refresh' ).hasClass( 'hidden' ) )
      $( '.refresh' ).addClass( 'hidden' );
  }
} );