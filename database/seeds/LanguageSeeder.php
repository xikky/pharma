<?php

use App\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $languages = [
      [ 'code' => 'en', 'language' => 'English' ],
      [ 'code' => 'mt', 'language' => 'Maltese' ]
    ];

    foreach ( $languages as $language ) {

      Language::create( [
                          'code'     => $language[ 'code' ],
                          'language' => $language[ 'language' ]
                        ] );
    }
  }
}
