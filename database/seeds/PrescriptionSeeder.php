<?php

use App\Prescription;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PrescriptionSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $prescriptions = [
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => ''
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => ''
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => ''
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => ''
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => ''
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '2' => [ 'dosage' => '2-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '3' => [ 'dosage' => '1-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-2-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '3' => [ 'dosage' => '1-3', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => ''
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '1',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => '1',
        'products' => [
          '2' => [ 'dosage' => '2-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 13 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 13 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'private',
        'dosage'            => '1-1-1',
        'dispensed'         => '2',
        'description'       => ''
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => '1',
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 14 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 14 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '2',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => '1',
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 15 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 15 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '2',
        'description'       => ''
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => '1',
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 16 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 16 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '2',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => '1',
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 17 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 17 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '2',
        'description'       => ''
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => '1',
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 18 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 18 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '2',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => '1',
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 19 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 19 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '2',
        'description'       => ''
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => '1',
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 20 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 20 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '2',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => '1',
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 21 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 21 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '2',
        'description'       => ''
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => '1',
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 22 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 22 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '2',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '1',
        'pharmacist_id'     => '1',
        'products' => [
          '1' => [ 'dosage' => '1-0-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 23 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 23 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dosage'            => '1-1-1',
        'dispensed'         => '2',
        'description'       => ''
      ],
      [ 'patient_id'        => '2',
        'pharmacist_id'     => null,
        'products' => [
          '1' => [ 'dosage' => '1-2-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ],
          '2' => [ 'dosage' => '1-2', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets' ]
        ],
        'doctor_id'         => '1',
        'prescription_type' => 'private',
        'dispensed'         => '1',
        'description'       => 'A description for the prescription'
      ],
      [ 'patient_id'        => '2',
        'pharmacist_id'     => '1',
        'products' => [
          '2' => [ 'dosage' => '1-1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 2 ), 'buy' => 1 ],
          '3' => [ 'dosage' => '1-1', 'duration' => '5', 'quantity' => '10', 'measurement' => 'tablets', 'submit_at' => Carbon::now()->subDays( 2 ), 'buy' => 1 ]
        ],
        'doctor_id'         => '2',
        'prescription_type' => 'poyc',
        'dispensed'         => '2',
        'description'       => ''
      ],
    ];

    $days = 10;

    foreach ( $prescriptions as $prescription ) {

      $new = Prescription::create( [
                                     'patient_id'        => $prescription[ 'patient_id' ],
                                     'pharmacist_id'     => $prescription[ 'pharmacist_id' ],
                                     'doctor_id'         => $prescription[ 'doctor_id' ],
                                     'prescription_type' => $prescription[ 'prescription_type' ],
                                     'dispensed'         => $prescription[ 'dispensed' ],
                                     'description'       => $prescription[ 'description' ],
                                     'created_at'        => Carbon::now()->subDays( $days )
                                   ] );

      $new->products()->sync( $prescription[ 'products' ] );

      $days++;
    }
  }
}