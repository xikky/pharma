<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UserExistsRequest;
use App\Libraries\Helpers\_ProductList;
use App\Prescription;
use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class PharmacistController extends Controller {

  public function patientPostSearch ( UserExistsRequest $request ) {

    $nin = $request->nin_number . $request->nin_country;

    ( new _User )->setSession( $nin );

    return Redirect::route( Session::get( 'redirect_route', 'patient_products' ), [ 'nin' => $nin ] );
  }

  public function updatePrescription ( ProductPrescriptionRequest $request ) {

    $prescription = Prescription::find( $request->id );

    $prescription->dispensed = 3;
    $prescription->pharmacist_id = Auth::user()->pharmacist()->first()->id;

    $prescription->save();

    $products = [ ];
    $productList = new _ProductList();

    foreach ( $request->product_id as $key => $product ) {

      $buy = 0;

      if ( isset( $request->barcode[ $key ] ) ) {

        $buy = 1;

        $productList->addproduct( $product );
        $productList->incrementTotals( Product::find( $product )->price );
      }

      $products[ $product ] = [ 'submit_at' => Carbon::now(), 'buy' => $buy ];
    }

    $prescription->products()->sync( $products );

    $data = [
      'messages'  => [ trans( 'patient.prescription_success' ) ],
      'type'      => 'success',
      'important' => false
    ];

    return Response::json( [
                             'message' => (string)View::make( 'partials.flash_response', $data )
                           ], 200 );
  }
}
