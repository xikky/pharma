<?php

namespace App\Libraries\Helpers;

use App\User;
use Illuminate\Support\Facades\Session;

class _User {

  public $user = null;

  public function __construct ( User $user = null ) {

    $this->user = $user;
  }

  public function setSession ( $nin ) {

    Session::put( 'user', $nin );
  }

  public function search ( $nin, $store = true ) {

    $user = User::where( 'nin', '=', $nin )->first();

    if ( $store )
      $this->set( $user );

    return $user;
  }

  public function set ( $user ) {

    Session::put( 'user', $user->nin );
    $this->user = $user;
  }

  public function getSession () {

    return Session::get( 'user', '' );
  }
}