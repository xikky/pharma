$( '.tablesorter-validate-prescriptions' ).on( 'input', '.product-barcode', function () {

  data = {
    barcode : $( this ).val(),
    product_id : $( this ).attr( 'product-id' ),
    _token  : $( this ).next().val()
  };

  var product_barcode = $( this );

  $.ajax( {
    method   : "POST",
    url      : '/ajax/validate-product-barcode',
    data     : data,
    success  : function ( data ) {

      $( '.top-message' ).html( data[ 'message' ] );
      $( product_barcode ).parent().removeClass( 'has-error' );
      $( product_barcode ).parent().addClass( 'has-success' );
      $( product_barcode ).closest( 'td' ).next().find( 'span' ).css( 'display', 'none' );
      $( product_barcode ).closest( 'td' ).next().find( '.ok' ).css( 'display', 'inline-block' );
    },
    error    : function ( data ) {

      data = data[ 'responseJSON' ];

      $( '.top-message' ).html( data[ 'message' ] );
      $( product_barcode ).parent().removeClass( 'has-success' );
      $( product_barcode ).parent().addClass( 'has-error' );
      $( product_barcode ).closest( 'td' ).next().find( 'span' ).css( 'display', 'none' );
      $( product_barcode ).closest( 'td' ).next().find( '.remove' ).css( 'display', 'inline-block' );
    },
    dataType : 'json'
  } );
} );

$( '.tablesorter-validate-prescriptions' ).on( 'click', '.product-barcode', function ( e ) {

  e.stopPropagation();
} );