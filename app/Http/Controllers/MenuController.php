<?php

namespace App\Http\Controllers;

use App\Barcode;
use App\Http\Requests;
use App\Http\Requests\UserNINRequest;
use App\Libraries\Helpers\_User;
use App\Prescription;
use App\Product;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class MenuController extends Controller {

  public function patient () {

    Session::flash( 'active_menu', 'patient' );

    return Redirect::route( 'patient_products' );
  }

  public function patientProducts ( $nin = null ) {

    Session::flash( 'active_menu', 'patient' );
    Session::flash( 'active_submenu', 'patient_products' );
    Session::put( 'redirect_route', Route::currentRouteName() );

    $patient_products = Session::get( 'patient_products', [ ] );

    $products = [ ];
    $totals = [ 'quantity' => 0, 'total' => 0 ];

    foreach ( $patient_products as $key => $patient_product ) {

      $product = Product::find( $key );

      $products[] = [
        'id'          => $key,
        'count'       => $patient_product[ 'count' ],
        'name'        => $product[ 'name' ],
        'description' => $product[ 'description' ],
        'quantity'    => $patient_product[ 'quantity' ],
        'price'       => number_format( ( $product[ 'price' ] / 100 ), 2 ),
        'total'       => number_format( ( ( $product[ 'price' ] * $patient_product[ 'quantity' ] ) / 100 ), 2 )
      ];

      $totals[ 'quantity' ] += $patient_product[ 'quantity' ];
      $totals[ 'total' ] = number_format( ( $totals[ 'total' ] + ( ( $product[ 'price' ] * $patient_product[ 'quantity' ] ) / 100 ) ), 2 );
    }

    return view( 'patient.products', compact( 'nin', 'products', 'totals' ) );
  }

  public function patientSearch () {

    Session::flash( 'active_menu', 'patient' );
    Session::flash( 'active_submenu', 'patient_search' );

    return view( 'patient.search' );
  }

  public function patientHistory ( $nin = null ) {

    Session::flash( 'active_menu', 'patient' );
    Session::flash( 'active_submenu', 'patient_history' );
    Session::put( 'redirect_route', Route::currentRouteName() );

    if ( empty( $nin ) )
      return Redirect::route( 'patient_search' );

    $user = ( new _User )->search( $nin, false );

    return view( 'patient.history', compact( 'nin', 'user' ) );
  }

  public function patientFlush () {

    Session::flash( 'active_menu', 'patient' );
    Session::flash( 'active_submenu', 'patient_flush' );
    Session::forget( 'user' );
    Session::forget( 'patient_products' );
    Session::forget( 'patient_products_total' );
    Session::put( 'redirect_route', 'patient_products' );

    return Redirect::route( 'patient_search' );
  }

  public function postPrescription ( $nin = null ) {

    if ( empty( $nin ) )
      $nin = ( new _User )->getSession();

    return Redirect::route( 'prescription', [ 'nin' => $nin ] );
  }

  public function prescription ( $nin = null ) {

    Session::flash( 'active_menu', 'prescription' );

    $user = ( new _User )->search( $nin, false );
    $types = ( new Prescription() )->types();

    return view( 'prescription.prescription', compact( 'user', 'types' ) );
  }
}
