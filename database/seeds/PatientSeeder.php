<?php

use App\Patient;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $patients = [
      [ 'user'     => '5', 'conditions' =>
        [
          '1' => [ 'doctor_id'  => '1', 'description' => 'Description for patient condition 1',
                   'created_at' => Carbon::now()->subDays( 1 ), 'updated_at' => Carbon::now()->subDays( 1 )
          ],
          '2' => [ 'doctor_id'  => '1', 'description' => 'Description for patient condition 2',
                   'created_at' => Carbon::now()->subDays( 2 ), 'updated_at' => Carbon::now()->subDays( 2 )
          ],
          '3' => [ 'doctor_id'  => '1', 'description' => 'Description for patient condition 3',
                   'created_at' => Carbon::now()->subDays( 3 ), 'updated_at' => Carbon::now()->subDays( 3 )
          ],
          '4' => [ 'doctor_id'  => '1', 'description' => 'Description for patient condition 4',
                   'created_at' => Carbon::now()->subDays( 7 ), 'updated_at' => Carbon::now()->subDays( 7 )
          ]
        ], 'products'                   =>
          [
            '1' => [ 'pharmacist_id' => '1', 'quantity' => 1, 'buy_at' => Carbon::now()->subDays( 1 ) ],
            '2' => [ 'pharmacist_id' => '1', 'quantity' => 1, 'buy_at' => Carbon::now()->subDays( 2 ) ],
            '3' => [ 'pharmacist_id' => '2', 'quantity' => 1, 'buy_at' => Carbon::now()->subDays( 3 ) ]
          ]
      ],
      [ 'user'     => '6', 'conditions' =>
        [
          '1' => [ 'doctor_id'  => '1', 'description' => 'Description for patient condition 1',
                   'created_at' => Carbon::now()->subDays( 1 ), 'updated_at' => Carbon::now()->subDays( 1 )
          ],
          '3' => [ 'doctor_id'  => '2', 'description' => 'Description for patient condition 3',
                   'created_at' => Carbon::now()->subDays( 2 ), 'updated_at' => Carbon::now()->subDays( 2 )
          ]
        ], 'products'                   =>
          [
            '1' => [ 'pharmacist_id' => '1', 'quantity' => 1, 'buy_at' => Carbon::now()->subDays( 1 ) ],
            '2' => [ 'pharmacist_id' => '2', 'quantity' => 1, 'buy_at' => Carbon::now()->subDays( 2 ) ]
          ]
      ],
      [ 'user'     => '9', 'conditions' =>
        [
          '1' => [ 'doctor_id'  => '1', 'description' => 'Description for patient condition 1',
                   'created_at' => Carbon::now()->subDays( 1 ), 'updated_at' => Carbon::now()->subDays( 1 )
          ],
          '3' => [ 'doctor_id'  => '2', 'description' => 'Description for patient condition 3',
                   'created_at' => Carbon::now()->subDays( 2 ), 'updated_at' => Carbon::now()->subDays( 2 )
          ]
        ], 'products'                   =>
          [
            '1' => [ 'pharmacist_id' => '1', 'quantity' => 1, 'buy_at' => Carbon::now()->subDays( 1 ) ],
            '3' => [ 'pharmacist_id' => '1', 'quantity' => 1, 'buy_at' => Carbon::now()->subDays( 2 ) ]
          ]
      ]
    ];

    foreach ( $patients as $patient ) {

      $new = Patient::create( [
                                'user_id' => $patient[ 'user' ]
                              ] );

      $new->conditions()->sync( $patient[ 'conditions' ] );
      $new->products()->sync( $patient[ 'products' ] );
    }
  }
}
