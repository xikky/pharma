<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model {

  public function user () {

    return $this->belongsTo( 'App\User' );
  }

  public function prescriptions () {

    return $this->hasMany( 'App\Prescription' );
  }

  public function products () {

    return $this->belongsToMany( 'App\Product' );
  }

  public function conditions () {

    return $this->belongsToMany( 'App\Condition' );
  }

  public function scopeProductsInfo ( $query, $patient_id ) {

    return $query->select( 'patient_product.buy_at as patient_product_date',
                           'products.name as product_name',
                           'pharmacists.name as pharmacist_name' )
                 ->join( 'patient_product', 'patients.id', '=', 'patient_product.patient_id' )
                 ->join( 'products', 'products.id', '=', 'patient_product.product_id' )
                 ->join( 'pharmacists', 'pharmacists.id', '=', 'patient_product.pharmacist_id' )
                 ->where( 'patients.id', '=', $patient_id );
  }

  public function scopeWhereDateProductPharmacist ( $query, $filter_list ) {

    if ( !empty( $filter_list ) ) {

      $attributes = [ ];

      foreach ( $filter_list as $key => $filter ) {

        if ( $key == 0 )
          $attributes[ 'buy_at' ] = "DATE_FORMAT( patient_product.buy_at, '%d-%m-%Y' ) LIKE '$filter%'";
        else if ( $key == 1 )
          $attributes[ 'product' ] = "products.name LIKE '%$filter%'";
        else if ( $key == 2 )
          $attributes[ 'pharmacist' ] = "pharmacists.name LIKE '%$filter%'";
      }

      $query->where( function ( $query ) use ( $attributes ) {

        foreach ( $attributes as $key => $value ) {

          $query->whereRaw( $value );
        }
      } );
    }

    return $query;
  }

  public function scopeOrderByDateproductPharmacist ( $query, $sort_list ) {

    if ( !empty( $sort_list ) ) {

      foreach ( $sort_list as $key => $sort ) {

        if ( $sort == '0' )
          $order_by[ intval( $key ) ] = 'desc';
        else
          $order_by[ intval( $key ) ] = 'asc';
      }

      foreach ( $sort_list as $key => $sort ) {

        if ( $key == 0 )
          $query->orderBy( 'patient_product_date', $order_by[ 0 ] );
        else if ( $key == 1 )
          $query->orderBy( 'product_name', $order_by[ 1 ] );
        else if ( $key == 2 )
          $query->orderBy( 'pharmacist_name', $order_by[ 2 ] );
      }
    }
    else
      $query->orderBy( 'patient_product_date', 'asc' )
            ->orderBy( 'product_name', 'asc' )
            ->orderBy( 'pharmacist_name', 'asc' );

    return $query;
  }
}
