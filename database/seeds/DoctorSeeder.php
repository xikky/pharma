<?php

use App\Doctor;
use Illuminate\Database\Seeder;

class DoctorSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run () {

    $doctors = [
      '4', '6', '8',
    ];

    foreach ( $doctors as $doctor ) {

      Doctor::create( [
                        'user_id' => $doctor
                      ] );
    }
  }
}
