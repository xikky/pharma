<table id="tbl-patient-products" data-sortlist="[[0,0]]" class="tablesorter tablesorter-patient-products">
  {!! Form::hidden( '_token', csrf_token(), ['class' => 'patient-products-token'] ) !!}
  <thead>
  <tr>
    <th class="tbl-col-date">{{ trans( 'patient.date' ) }}</th>
    <th class="tbl-col-product">{{ trans( 'patient.product' ) }}</th>
    <th class="tbl-col-pharma-min">{{ trans( 'patient.pharmacist' ) }}</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
</table>

<div id="pager-patient-products" class="pager">
  @include('partials.pager')
</div>