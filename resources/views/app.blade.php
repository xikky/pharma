<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Pharma</title>
  <link rel="stylesheet" href="{{ elixir('css/style.css') }}">
  @yield('header')
  @yield('templates')
</head>

<body>

@include('partials.nav')

@yield('breadcrumbs')

@yield('content')

<script src="{{ elixir('js/script.js') }}"></script>
@yield('footer')
</body>

</html>