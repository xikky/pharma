<table id="tbl-poyc-prescriptions" data-sortlist="[[0,0]]" class="tablesorter tablesorter-poyc-prescriptions tablesorter-validate-prescriptions tablesorter-row-toggle-prescription tablesorter-dis dis-2 dis-3 dis-5">
  {!! Form::hidden( '_token', csrf_token(), ['class' => 'poyc-prescription-token'] ) !!}
  <thead>
  <tr>
    <th class="tbl-col-date">{{ trans( 'patient.date' ) }}</th>
    <th class="tbl-col-doctor">{{ trans( 'patient.doctor' ) }}</th>
    <th class="tbl-col-product">{{ trans( 'patient.product' ) }}</th>
    <th class="tbl-col-dosage filter-false">{{ trans( 'patient.dosage_regiment' ) }}</th>
    <th class="tbl-col-status">{{ trans( 'patient.status' ) }}</th>
    <th class="tbl-col-icon filter-false">{{ trans( 'patient.barcode_status' ) }}</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
</table>

<div id="pager-poyc-prescriptions" class="pager">
  @include('partials.pager')
</div>