@extends('app')

@section('content')

  <div id="index" class="sub-body">
    <div id="push-content"></div>
    <div id="center-content">
      <div class="container">
        <div class="sub-content">
          <div class="row">
            <div class="col-xs-12">
              <div class="center-horizontal logo">
                <img src="{{ asset( 'assets/images/logo_black.png') }}" alt="{{ trans( 'common.logo_name' ) }}">
              </div>
            </div>
            <div class="col-xs-12">
              <div class="center-horizontal welcome">
                <span>{{ trans( 'home.welcome' ) }}</span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="welcome-btn">
              <div class="col-xs-offset-2 col-xs-3 login">
                <a href="{{ action('Auth\AuthController@getLogin') }}"
                   class="btn btn-lg btn-pharma-1">{{ trans( 'common.login' ) }}</a>
              </div>
              <div class="col-xs-offset-2 col-xs-3 register">
                <a href="{{ action('Auth\AuthController@getRegister') }}"
                   class="btn btn-lg btn-pharma-1">{{ trans( 'common.register' ) }}</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection