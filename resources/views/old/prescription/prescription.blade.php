@extends('app')

@section('templates')
  @include('templates.spinner-row')
  @include('templates.print-prescription')
@endsection

@section('breadcrumbs')
  {!! Breadcrumbs::render('prescription') !!}
@endsection

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="top-message">
          @include('partials.error')
          @include('partials.flash')
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div id="prescription" class="content">

          @if ( !empty($user) )
            {!! Form::hidden( 'patient', $user->id, ['id' => 'patient'] ) !!}
          @endif

          {!! Form::hidden( 'mode', 'prescription', ['id' => 'mode'] ) !!}

          <div id="accordion">
            <div class="panel-group">

              <div id="prescription-patient-search" class="panel panel-pharma">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="link-button">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      {{ trans( 'prescription.patient_search') }}
                    </h4>
                  </div>
                </a>
                <div id="collapseOne" class="panel-collapse collapse {{ empty( $user ) ? 'in' : '' }}">
                  <div class="panel-body">

                    @if ( empty( $user ) )
                      {!! Form::open(['action' => 'PrescriptionController@patientSearch', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST' ]) !!}
                      <div class="form-group">
                        <label class="col-md-3 control-label">{{ trans('patient.patient_id') }}</label>

                        <div class="col-md-5">
                          {!! Form::number( 'nin_number', old('nin_number'), array( 'class' => 'form-control form-number-spinner' ) ) !!}
                        </div>
                        <div class="col-md-2">
                          <select name="nin_country" class="form-control">
                            <option value="A">A</option>
                            <option value="G">G</option>
                            <option value="M" selected>M</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-3 col-md-offset-3">
                          <button type="submit" class="btn btn-pharma-1 search" value="search">{{ trans('common.search') }}</button>
                          {!! Form::hidden( '_token', csrf_token(), ['class' => 'prescription-patient-search-token'] ) !!}
                        </div>
                      </div>
                      {!! Form::close() !!}
                    @else
                      {!! Form::open(['action' => 'PrescriptionController@patientForget', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST' ]) !!}
                      <div class="form-group">
                        <label class="col-md-3 control-label">{{ trans('prescription.current_patient') }}</label>
                        <div class="col-md-9">
                          <p class="form-control-static"><strong>ID</strong> {{ $user->nin }}</p>
                          <p class="form-control-static"><strong>Username</strong> {{ $user->name }}</p>
                          <p class="form-control-static"><strong>Full Name</strong> {{ $user->firstname . ' ' . $user->lastname }}</p>
                        </div>

                        <div class="col-md-3 col-md-offset-3">
                          <button type="submit" class="btn btn-pharma-1 forget" value="forget">{{ trans('common.forget') }}</button>
                          {!! Form::hidden( '_token', csrf_token(), ['class' => 'prescription-patient-forget-token'] ) !!}
                        </div>
                      </div>
                      {!! Form::close() !!}
                    @endif

                  </div>
                </div>
              </div>

              <div id="prescription-create" class="panel panel-pharma">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="link-button">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      {{ trans( 'prescription.prescription') }}
                    </h4>
                  </div>
                </a>
                <div id="collapseTwo" class="panel-collapse collapse  {{ empty( $user ) ? '' : 'in' }}">
                  <div class="panel-body">

                    @if ( !empty( $user ) )
                      @include('prescription.partials.create')
                    @else
                      @include('partials.no_patient')
                    @endif

                  </div>
                </div>
              </div>

              <div class="panel panel-pharma">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="link-button">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      {{ trans( 'prescription.private_prescriptions') }}
                    </h4>
                  </div>
                </a>
                <div id="collapseThree" class="panel-collapse collapse">
                  <div class="panel-body">

                    @if ( !empty( $user ) )
                      @include('partials.tables.pvt_prescriptions')
                    @else
                      @include('partials.no_patient')
                    @endif

                  </div>
                </div>
              </div>

              <div class="panel panel-pharma">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="link-button">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      {{ trans( 'prescription.poyc_prescriptions') }}
                    </h4>
                  </div>
                </a>
                <div id="collapseFour" class="panel-collapse collapse">
                  <div class="panel-body">

                    @if ( !empty( $user ) )
                      @include('partials.tables.poyc_prescriptions')
                    @else
                      @include('partials.no_patient')
                    @endif

                  </div>
                </div>
              </div>

              <div class="panel panel-pharma">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="link-button">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      {{ trans( 'prescription.patient_products') }}
                    </h4>
                  </div>
                </a>
                <div id="collapseFive" class="panel-collapse collapse">
                  <div class="panel-body">

                    @if ( !empty( $user ) )
                      @include('partials.tables.patient_products')
                    @else
                      @include('partials.no_patient')
                    @endif

                  </div>
                </div>
              </div>

              <div class="panel panel-pharma">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" class="link-button">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      {{ trans( 'prescription.patient_conditions') }}
                    </h4>
                  </div>
                </a>
                <div id="collapseSix" class="panel-collapse collapse">
                  <div class="panel-body">

                    @if ( !empty( $user ) )
                      @include('partials.tables.patient_conditions')
                    @else
                      @include('partials.no_patient')
                    @endif

                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="section-to-print">
    </div>
  </div>
@endsection