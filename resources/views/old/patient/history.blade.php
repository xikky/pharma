@extends('app')

@section('templates')
  @include('templates.spinner-row')
  @include('templates.print-prescription')
@endsection

@section('breadcrumbs')
  {!! Breadcrumbs::render('patient_history', $nin) !!}
@endsection

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="top-message">
          @include('partials.error')
          @include('partials.flash')
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div id="patient-history" class="content">

          @if ( !empty($user) )
            {!! Form::hidden( 'patient', $user->id, ['id' => 'patient'] ) !!}
          @endif

          {!! Form::hidden( 'mode', 'patient-history', ['id' => 'mode'] ) !!}

          <div id="accordion">
            <div class="panel-group">
              <div class="panel panel-pharma">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="link-button">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      {{ trans( 'patient.private_prescriptions') }}
                    </h4>
                  </div>
                </a>
                <div id="collapseOne" class="panel-collapse collapse in">
                  <div class="panel-body">

                    @include('partials.tables.pvt_prescriptions')

                  </div>
                </div>
              </div>

              <div class="panel panel-pharma">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="link-button">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      {{ trans( 'patient.poyc_prescriptions') }}
                    </h4>
                  </div>
                </a>
                <div id="collapseTwo" class="panel-collapse collapse">
                  <div class="panel-body">

                    @include('partials.tables.poyc_prescriptions')

                  </div>
                </div>
              </div>
              <div class="panel panel-pharma">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="link-button">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      {{ trans( 'patient.patient_products') }}
                    </h4>
                  </div>
                </a>
                <div id="collapseThree" class="panel-collapse collapse">
                  <div class="panel-body">

                    @include('partials.tables.patient_products')

                  </div>
                </div>
              </div>
              <div class="panel panel-pharma">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="link-button">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      {{ trans( 'patient.patient_conditions') }}
                    </h4>
                  </div>
                </a>
                <div id="collapseFour" class="panel-collapse collapse">
                  <div class="panel-body">

                    @include('partials.tables.patient_conditions')

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="section-to-print">
    </div>
  </div>
@endsection