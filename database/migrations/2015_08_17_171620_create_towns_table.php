<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTownsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up () {

    Schema::create( 'towns', function ( Blueprint $table ) {

      $table->increments( 'id' )->unsigned();
      $table->integer( 'country_id' )->unsigned();
      $table->string( 'name' );

      $table->foreign( 'country_id' )->references( 'id' )->on( 'countries' );
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down () {

    Schema::table( 'towns', function ( Blueprint $table ) {

      $table->dropForeign( 'towns_country_id_foreign' );
    } );

    Schema::drop( 'towns' );
  }
}
