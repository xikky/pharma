<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerViewServiceProvider extends ServiceProvider {

  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot () {

    view()->composer( 'partials.nav', 'App\Http\Composers\NavigationMenuComposer' );
    view()->composer( 'partials.nav', 'App\Http\Composers\NavigationLanguageComposer' );
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register () {
    //
  }
}
