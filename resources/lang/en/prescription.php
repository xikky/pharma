<?php

return [

  'patient_search'        => 'Search Patient',
  'prescription'          => 'Prescription',
  'private_prescriptions' => 'Private Prescriptions',
  'poyc_prescriptions'    => 'POYC Prescriptions',
  'patient_products'      => 'Patient products Bought',
  'patient_conditions'    => 'Patient Condition History',
  'current_patient'       => 'Current patient:'
];