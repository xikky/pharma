<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

  use Authenticatable, CanResetPassword;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'users';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [ 'name', 'email', 'password', 'language_id' ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [ 'password', 'remember_token' ];

  //protected $appends = [ 'fullname' ];

  public function menu () {

    $roles = $this->roles()->get();

    $menu_list = [ ];

    foreach ( $roles as $role ) {

      foreach ( $role->menus()->get()->toArray() as $role_menu )
        $menu_list[ $role_menu[ 'id' ] ] = $role_menu;
    }

    return $menu_list;
  }

  public function roles () {

    return $this->belongsToMany( 'App\Role' );
  }

  public function language () {

    return $this->belongsTo( 'App\Language' );
  }

  public function patient () {

    return $this->hasOne( 'App\Patient' );
  }

  public function pharmacist () {

    return $this->hasOne( 'App\Pharmacist' );
  }

  public function doctor () {

    return $this->hasOne( 'App\Doctor' );
  }

  public function getFullnameAttribute () {

    return $this->firstname . " " . $this->lastname;
  }
}
