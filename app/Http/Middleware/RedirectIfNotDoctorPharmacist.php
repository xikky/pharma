<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Guard;

class RedirectIfNotDoctorPharmacist {

  protected $auth;

  /**
   * Create a new filter instance.
   *
   * @param  Guard $auth
   *
   * @return void
   */
  public function __construct ( Guard $auth ) {

    $this->auth = $auth;
  }

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Closure $next
   *
   * @return mixed
   */
  public function handle ( $request, Closure $next ) {

    if ( $this->auth->check() ) {

      $roles = $this->auth->user()->roles()->lists( 'signature' )->toArray();

      if ( in_array( 'doctor', $roles ) || in_array( 'pharmacist', $roles ) )
        return $next( $request );
    }

    return redirect( '/' );
  }
}
